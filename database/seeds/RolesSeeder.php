<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use App\Models\PermissionRole;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        Permission::truncate();
        PermissionRole::truncate();
        Role::truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

		$owner = new Role();
		$owner->name         = 'superadmin';
		$owner->display_name = 'Super Admin'; // optional
		$owner->description  = 'Super Admin'; // optional
		$owner->save(); 

        $doctor = new Role();
        $doctor->name         = 'doctor';
        $doctor->display_name = 'Doctor'; // optional
        $doctor->description  = 'Doctor'; // optional
        $doctor->save();

        $patient = new Role();
        $patient->name         = 'patient';
        $patient->display_name = 'Patient'; // optional
        $patient->description  = 'Patient'; // optional
        $patient->save(); 

         

        // Settings
        $viewDoctors = new Permission();
        $viewDoctors->name         = 'view-doctors';
        $viewDoctors->display_name = 'View Doctor'; // optional
        $viewDoctors->description  = 'View Doctor'; // optional
        $viewDoctors->save();

        
        /*********************************** Defining Permissions For Roles ***********************************/
                                    /*******************************************/

        // Owner Permissions
        $owner->attachPermissions(array($viewDoctors));

        /****************************************** Defining Admin User **************************************/
                                    /*******************************************/
      
        $user = new User();
        $user->first_name = 'Jason';
        $user->last_name = 'Bourne';
        $user->email = 'jasonbourne501@gmail.com';
        $user->password = bcrypt('Qwerty89!');
        $user->email_verified = 1;
        $user->save();

	    $user->attachRole($owner);

    }
}
