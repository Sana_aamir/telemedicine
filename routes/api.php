<?php

date_default_timezone_set('Europe/London');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/time', function() {
		$timestamp = date("Y-m-d H:i:s");
	    echo $timestamp;die();
	});

Route::post(
	    'stripe/webhook',
	    '\App\Http\Controllers\WebhookController@handleWebhook'
	);

// API Routes Authenticated
Route::middleware('jwt.auth:api')->group(function () {
	
	Route::put('user/reset-password-admin', 'UserController@resetPasswordByAdmin');
	Route::post('/user', 'UserController@save');
	Route::get('/users', 'UserController@getAll');
	Route::put('/user', 'UserController@update');
	Route::put('/user/update-status', 'UserController@updateStatus');
	Route::delete('/user/{id}', 'UserController@delete');
	Route::put('user/profile', 'UserController@updateProfile');


	
	Route::get('media/download', 'MediaController@download');

	

	//specialties module
	Route::post('/specialty', 'SpecialtyController@save');
	/*Route::get('/specialties', 'SpecialtyController@getAll');*/
	Route::get('/specialty/{id}', 'SpecialtyController@get');
	Route::patch('/specialty/{id}', 'SpecialtyController@update');
	Route::delete('/specialty/{id}', 'SpecialtyController@delete');

	//questionnaire module
	Route::post('/questionnaire', 'QuestionnaireController@save');
	Route::get('/questionnaires', 'QuestionnaireController@getAll');
	Route::get('/questionnaire/{id}', 'QuestionnaireController@get');
	Route::patch('/questionnaire/{id}', 'QuestionnaireController@update');
	Route::delete('/questionnaire/{id}', 'QuestionnaireController@delete');
	Route::get('/clinical/hospital-records', 'QuestionnaireController@getClinicalRecords');
	Route::get('/clinical/hospital-record/{id}', 'QuestionnaireController@getClinicalRecord');
	Route::delete('/clinical/hospital-record/{id}', 'QuestionnaireController@deleteClinicalRecord');
	Route::post('/clinical/send-email', 'QuestionnaireController@sendClinicalRecord');

	//doctors module
	Route::post('/doctor', 'DoctorController@save');
	/*Route::get('/doctors', 'DoctorController@getAll');*/
	/*Route::get('/doctor/{id}', 'DoctorController@get');*/
	Route::patch('/doctor/{id}', 'DoctorController@update');
	Route::delete('/doctor/{id}', 'DoctorController@delete');
	Route::put('/doctor/update-blocked-login', 'DoctorController@updateBlockLogin');
	Route::patch('/doctor/field-update/{id}', 'DoctorController@doctorFieldUpdate');
	Route::post('/doctor/send-documents', 'DoctorController@sendDocuments');

	// doctor schedule routes
	Route::patch('/doctor-schedule/{id}', 'DoctorScheduleController@update');
	Route::get('/doctor-schedule/{id}', 'DoctorScheduleController@getAll');
	/*Route::get('/doctor-schedule/chart/{id}', 'DoctorScheduleController@getAvailChart');*/

	//patient routes
	
	Route::get('/patients', 'PatientController@getAll');
	Route::get('/patient/{id}', 'PatientController@get');
	Route::patch('/patient/{id}', 'PatientController@update');
	Route::put('/patient/update-approval', 'PatientController@updateApproval');
	Route::put('/patient/update-blocked-login', 'PatientController@updateBlockLogin');
	Route::patch('/patient-common-update/{id}', 'PatientController@updateCommon');
	Route::delete('/patient/{id}', 'PatientController@delete');
	Route::post('/patient/send-documents', 'PatientController@sendDocuments');

	//Appointment routes
	Route::get('/appointments', 'AppointmentController@getAll');
	/*Route::get('/appointment/{id}', 'AppointmentController@get');*/
	Route::post('/appointment/create', 'AppointmentController@save');
	Route::delete('/appointment/{id}', 'AppointmentController@delete');
	Route::patch('/appointment/{id}', 'AppointmentController@update');
	Route::post('/appointment/availability_cal', 'AppointmentController@getAppointmentAvailabilityCal');
	Route::post('/appointment/time_slots/{date}', 'AppointmentController@getAvailTimeSlots');
	Route::put('/appointment/update-status', 'AppointmentController@updateStatus');
	Route::put('/appointment/update-appointment-status', 'AppointmentController@updateAppointmentStatus');
	Route::put('/appointment/fee', 'AppointmentController@appointmentFee');
	Route::get('/appointments/fee', 'AppointmentController@getAppointmentFee');
	Route::get('/booking-fee', 'AppointmentController@getAppointmentFeeForBooking');
	Route::get('/latest-appointment', 'AppointmentController@getLatestAppointment');
	Route::put('/appointment/reminders', 'AppointmentController@appointmentReminders');
	Route::get('/appointments/reminders', 'AppointmentController@getAppointmentReminders');
	Route::post('/appointment/assessment-completed', 'AppointmentController@assessmentCompleted');
	/*Route::post('/appointment/start-assessment/{id}', 'AppointmentController@startAssessment');*/
	Route::post('/appointment/send-invitation-link/{id}', 'AppointmentController@sendInvitationLink');
	Route::put('/appointment-durations', 'AppointmentController@appointmentDurations');
	Route::get('/appointments/durations', 'AppointmentController@getAppointmentDurations');
	Route::get('/check-appointment-exist', 'AppointmentController@checkAppointmentExist');
	

	//Mental health hospital routes
	Route::post('/mental_health_hospital', 'MentalHealthHospitalController@save');
	Route::get('/mental_health_hospitals', 'MentalHealthHospitalController@getAll');
	Route::get('/mental_health_hospital/{id}', 'MentalHealthHospitalController@get');
	Route::patch('/mental_health_hospital/{id}', 'MentalHealthHospitalController@update');
	Route::delete('/mental_health_hospital/{id}', 'MentalHealthHospitalController@delete');
	Route::post('/hospital-import-csv', 'MentalHealthHospitalController@importExcel')->name('importCsv');

	//laboratory routes
	Route::post('/laboratory', 'LaboratoryController@save');
	Route::get('/laboratories', 'LaboratoryController@getAll');
	Route::get('/laboratories-distance', 'LaboratoryController@laboratoriesDistance');
	Route::get('/laboratory/{id}', 'LaboratoryController@get');
	Route::patch('/laboratory/{id}', 'LaboratoryController@update');
	Route::delete('/laboratory/{id}', 'LaboratoryController@delete');
	Route::post('/laboratory-import-csv', 'LaboratoryController@importExcel');

	//audit routes
	Route::post('/audits', 'AuditsController@save');
	Route::get('/auditss', 'AuditsController@getAll');
	Route::get('/audits/{id}', 'AuditsController@get');
	Route::patch('/audits/{id}', 'AuditsController@update');
	Route::delete('/audits/{id}', 'AuditsController@delete');
	Route::put('/audit/update-status', 'AuditsController@updateStatus');

	//policy routes
	Route::post('/policy', 'PolicyController@save');
	Route::get('/policys', 'PolicyController@getAll');
	Route::get('/policy/{id}', 'PolicyController@get');
	Route::patch('/policy/{id}', 'PolicyController@update');
	Route::delete('/policy/{id}', 'PolicyController@delete');
	Route::put('/policy/update-status', 'PolicyController@updateStatus');

	//staff routes
	Route::post('/staff', 'StaffController@save');
	Route::get('/staffs', 'StaffController@getAll');
	Route::get('/staff/{id}', 'StaffController@get');
	Route::patch('/staff/{id}', 'StaffController@update');
	Route::delete('/staff/{id}', 'StaffController@delete');
	Route::put('/staff/update-status', 'StaffController@updateStatus');

	//serious incidents routes
	Route::post('/serious-incident', 'SeriousIncidentController@save');
	Route::get('/serious-incidents', 'SeriousIncidentController@getAll');
	Route::get('/serious-incident/{id}', 'SeriousIncidentController@get');
	Route::patch('/serious-incident/{id}', 'SeriousIncidentController@update');
	Route::delete('/serious-incident/{id}', 'SeriousIncidentController@delete');
	Route::put('/serious-incident/update-status', 'SeriousIncidentController@updateStatus');

	//support routes
	Route::post('/support', 'SupportController@save');
	Route::get('/supports', 'SupportController@getAll');
	Route::get('/support', 'SupportController@get');
	//Route::patch('/support/{id}', 'SupportController@update');
	Route::delete('/support/{id}', 'SupportController@delete');
	Route::patch('/support/update-status/{id}', 'SupportController@updateStatus');
	Route::patch('/support/send-reply', 'SupportController@sendReply');
	Route::get('/support/messages', 'SupportController@messages');
	Route::get('/support/message/{id}', 'SupportController@getSupportMessage');
	Route::delete('/support/message/{id}', 'SupportController@deleteMessage');
	Route::put('/support/message/toggle-flag/{id}', 'SupportController@toggleFlag');
	Route::get('/support-new-messages', 'SupportController@countNewSupportMessage');
	Route::get('/complaint-new-messages', 'SupportController@countNewComplaintMessage');

	// Notes ROutes
	Route::patch('/note', 'NoteController@save');
	Route::get('/note', 'NoteController@get');
	Route::get('/notes', 'NoteController@getAll');
	Route::delete('/note/{id}', 'NoteController@delete');

	// Email Routes
	Route::post('/email', 'EmailController@send');

	//Pages routes
	Route::post('/page', 'PageController@save');
	Route::get('/pages', 'PageController@getAll');
	Route::get('/page/{id}', 'PageController@get');
	/*Route::get('/page-content/{slug}', 'PageController@getContent');*/
	Route::patch('/page/{id}', 'PageController@update');
	Route::delete('/page/{id}', 'PageController@delete');

	//Doctor Holidays routes
	Route::post('/doctor-holiday', 'DoctorHolidayController@save');
	Route::get('/doctor-holidays', 'DoctorHolidayController@getAll');
	Route::get('/doctor-holiday/{id}', 'DoctorHolidayController@get');
	Route::patch('/doctor-holiday/{id}', 'DoctorHolidayController@update');
	Route::delete('/doctor-holiday/{id}', 'DoctorHolidayController@delete');

	//Invoice routes
	Route::post('/invoice', 'InvoiceController@save');
	Route::get('/invoices', 'InvoiceController@getAll');
	Route::get('/invoice/{id}', 'InvoiceController@get');
	Route::patch('/invoice/{id}', 'InvoiceController@update');
	Route::delete('/invoice/{id}', 'InvoiceController@delete');

	//Stripe Payment 
	Route::post('/payment/appointment', 'PaymentController@appointmentPayment');

	//Subscription Routes
	/*Route::post('/subscription/blood-test', 'PaymentController@bloodTestSubsciption');*/
	Route::get('/subscription/invoices', 'PaymentController@invoices');
	Route::get('/subscription/invoice/{id}', 'PaymentController@getInvoiceFromStripe');
	Route::get('/subscription/download-invoice/{patient_id}/{invoice_id}', 'PaymentController@downloadInvoice');
	Route::delete('/subscription/invoice-delete/{id}', 'PaymentController@invoiceDelete');
	Route::get('/subscriptions/rbt', 'PaymentController@allRBT');
	Route::get('/subscription/rbt', 'PaymentController@getRBT');
	Route::put('/subscription/rbt', 'PaymentController@updateRBT');

	// Membership subscrption
	Route::post('/subscription/membership', 'PaymentController@membershipSubscription');
	Route::get('/subscription/memberships', 'PaymentController@getMembershipSubscriptions');
	Route::get('/subscription/membership', 'PaymentController@getMembershipSubscription');
	Route::delete('/subscription/membership/{id}', 'PaymentController@deleteSubscription');
	Route::post('/subscription/membership-cancel-request', 'PaymentController@membershipCancelRequest');
	Route::get('/subscription/membership-cancel-requests', 'PaymentController@getMembershipCancelRequests');
	Route::get('/subscription/membership-cancel-request/{id}', 'PaymentController@getMembershipCancelRequest');
	Route::post('/subscription/membership-cancel', 'PaymentController@membershipCancel');
	Route::delete('/subscription/membership-cancel-request/{id}', 'PaymentController@deleteSubscriptionCancelRequest');
	Route::post('/refund-payment', 'PaymentController@refundPayment');
	/*Route::get('/charge-object', 'PaymentController@getChargeObject');*/

	// lab forms
	Route::post('/labform', 'LabFormController@save');
	Route::get('/labforms', 'LabFormController@getAll');
	Route::get('/labform/{id}', 'LabFormController@get');
	Route::post('/labform/send-email', 'LabFormController@sendLabForm');

	//Log Route
	Route::get('/activity-log', 'ActivityLogController@getAll');

	//testimonial module
	Route::post('/testimonial', 'TestimonialController@save');
	
	Route::get('/testimonial/{id}', 'TestimonialController@get');
	Route::patch('/testimonial/{id}', 'TestimonialController@update');
	Route::patch('/testimonial-update-status', 'TestimonialController@updateStatus');
	Route::delete('/testimonial/{id}', 'TestimonialController@delete');

	//business settings module
	Route::get('/business-setting', 'BusinessSettingController@get');
	Route::patch('/business-setting/{id}', 'BusinessSettingController@update');

	Route::post('/authenticate', 'UserController@authenticateUser');

});

Route::post('/logout', 'UserController@logout');
Route::group([
    'middleware' => 'api',
    //'prefix' => 'auth'
], function ($router) {
    Route::post('/login', 'UserController@login');
    
    Route::post('/request-password', 'UserController@requestPassword');
	Route::put('/reset-password', 'UserController@resetPasswordByToken')->middleware(App\Http\Middleware\CheckPasswordReset::class);
	Route::get('/verify/{code}', 'UserController@verifyEmail');
	Route::put('user/reset-password', 'UserController@resetPassword');
	Route::post('media/upload', 'MediaController@upload');
	Route::put('media/upload', 'MediaController@upload');
	Route::post('/patient', 'PatientController@save');
	Route::get('/specialties', 'SpecialtyController@getAll');
	Route::get('/doctors', 'DoctorController@getAll');
	Route::get('/appointment/check-avail', 'AppointmentController@checkDoctorAvailibility');
	Route::post('contact-email', 'UserController@contactEmail');
	Route::get('/testimonials', 'TestimonialController@getAll');
	Route::get('/appointment/{id}', 'AppointmentController@get');
	Route::post('/appointment/start-assessment/{id}', 'AppointmentController@startAssessment');
	Route::get('/appointment', 'AppointmentController@getAppointmentFromEncryptedId');
	
	Route::put('check-unique-user', 'UserController@checkForUniqueFields');
	
	Route::get('/doctor/{id}', 'DoctorController@get');
	Route::get('/doctor-schedule/chart/{id}', 'DoctorScheduleController@getAvailChart');
	Route::get('/doctor-next/{id}', 'DoctorController@getNextDoctor');
	Route::get('/page-content/{slug}', 'PageController@getContent');
	

	Route::get('refresh', 'UserController@refresh');
	// Print Routes
	Route::get('payslip', 'UserController@generatePayslip');
	Route::get('receipt', 'UserController@generateReceipt');
	Route::get('message', 'UserController@generateMessage');
	Route::get('print-labform', 'UserController@generateLabform');
	Route::get('print-hospital-rec', 'UserController@generateHospitalRec');
	
   
});






