<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Admin Routes
Route::get('admin/{path}', function () {
    return view('app');
})->where('path', '([A-z\d-\/_.=+]+)?');


// Web app routes
Route::get('{path}', function () {
    return view('site');
})->where('path', '([A-z\d-\/_.=+]+)?');

// forgive me for this patch
function p($d)
{
	echo "<pre>";
	print_r($d);
	die();
}