let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/admin/app.js', 'public/js')
	.js('resources/assets/js/frontend/app.js', 'public/js/frontend/all.js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	/*.styles([
	    'public/assets/plugins/pace/pace-theme-flash.css',
	    'public/assets/plugins/bootstrap/css/bootstrap.min.css',
	    'public/assets/plugins/font-awesome/css/font-awesome.css',
	    'public/assets/plugins/jquery-scrollbar/jquery.scrollbar.css',
	    'public/assets/css/amaran.min.css',
	    'public/assets/css/summernote/summernote.min.css',
	    'public/assets/pages/css/pages-icons.css',
	    'public/assets/pages/css/themes/corporate.css',
	], 'public/css/plain-all.css')*/
	.styles([
	    'public/htmlv1/stylesheets/bootstrap.min.css',
	    'public/htmlv1/stylesheets/icomoon.css',
	    'public/htmlv1/stylesheets/bootstrap-datetimepicker.css',
	    'public/htmlv1/stylesheets/datepicker.css',
	    'public/htmlv1/stylesheets/summernote/summernote.min.css',
	    'public/htmlv1/stylesheets/style.css',
	    'public/htmlv1/stylesheets/custom.css'
	], 'public/front/stylesheets/plain-all.css');
	
/*mix.webpackConfig({
	entry: {
   		app: ['babel-polyfill', 'whatwg-fetch', './public/js/app.js'],
 	}
});*/
