<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="register-page">
    <?php include_once('include/header.php') ?>
   <div class="form-right-column section">
      <div class="middle-align">
         <div class="inner">
            <div class="row">
               <div class="col-xs-12 col-md-12">
                  <h2 class="section-heading text-center">Create Your Account</h2>
                  <p class="text-center">Happy Clinic is much better when you have a account!</p>
               </div>
               <div class="col-md-12 col-xs-12">
                  <section id="wizard">
                     <div id="rootwizard">
                        <div class="navbar ">
                           <div class="navbar-inner">
                              <ul class="form-wizard-steps">
                                 <li><a href="#tab1" data-toggle="tab">1
                                    <span>Step 1</span>
                                    </a>
                                 </li>
                                 <li><a href="#tab2" data-toggle="tab">2
                                    <span>Step 2</span>
                                    </a>
                                 </li>
                                 <li><a href="#tab3" data-toggle="tab">3
                                    <span>Step 3</span>
                                    </a>
                                 </li>
                                 <li><a href="#tab4" data-toggle="tab">4
                                    <span>Step 4</span>
                                    </a>
                                 </li>
                                 <li><a href="#tab5" data-toggle="tab">5
                                    <span>Step 5</span>
                                    </a>
                                 </li>
                              </ul>
                              <div id="bar" class="progress">
                                 <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-content">
                           <div class="tab-pane PD-tab-content" id="tab1">
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Personal Details of Patient</h2>
                                 <div class="clearfix"></div>
                                 <div class="row">
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Title</label>
                                         <select class="form-control">
                                             <option selected="">Mr</option>
                                             <option >Miss</option>
                                             <option >Mrs</option>
                                             <option >Ms</option>
                                             <option >Dr</option>
                                             <option >Prof</option>
                                         </select>
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">First Name</label>
                                         <input type="text" name="fname" class="form-control" placeholder="Enter first name">
                                          <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Last Name</label>
                                         <input type="text" name="lname" class="form-control" placeholder="Enter last name">
                                          <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Email</label>
                                         <input type="text" name="email" class="form-control" placeholder="Enter email">
                                          <div class="clearfix"></div>
                                     </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Date Of Birth</label>
                                         <div class="row dob-row">
                                              <div class="col-xs-4 col-md-4 day-col">
                                                 <select class="form-control">
                                                      <option selected="">Day</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                      <option value="9">9</option>
                                                      <option value="10">10</option>
                                                      <option value="11">11</option>
                                                      <option value="12">12</option>
                                                      <option value="13">13</option>
                                                      <option value="14">14</option>
                                                      <option value="15">15</option>
                                                      <option value="16">16</option>
                                                      <option value="17">17</option>
                                                      <option value="18">18</option>
                                                      <option value="19">19</option>
                                                      <option value="20">20</option>
                                                      <option value="21">21</option>
                                                      <option value="22">22</option>
                                                      <option value="23">23</option>
                                                      <option value="24">24</option>
                                                      <option value="25">25</option>
                                                      <option value="26">26</option>
                                                      <option value="27">27</option>
                                                      <option value="28">28</option>
                                                      <option value="29">29</option>
                                                      <option value="30">30</option>
                                                      <option value="31">31</option>
                                                 </select>                                                
                                              </div>
                                              <div class="col-xs-14 col-md-4 month-col">
                                                 <select class="form-control">
                                                     <option selected="">Month</option>
                                                      <option selected value='1'>Janaury</option>
                                                      <option value='2'>February</option>
                                                      <option value='3'>March</option>
                                                      <option value='4'>April</option>
                                                      <option value='5'>May</option>
                                                      <option value='6'>June</option>
                                                      <option value='7'>July</option>
                                                      <option value='8'>August</option>
                                                      <option value='9'>September</option>
                                                      <option value='10'>October</option>
                                                      <option value='11'>November</option>
                                                      <option value='12'>December</option>
                                                 </select>                                                
                                              </div>
                                              <div class="col-xs-4 col-md-4 col-year">
                                                 <select class="form-control">
                                                     <option selected="">Year</option>
                                                      <?php

                                                      foreach(range(1950, (int)date("Y")) as $year) {
                                                          echo "\t<option value='".$year."'>".$year."</option>\n\r";
                                                      }

                                                      ?>
                                                 </select>                                                                                           
                                              </div>
                                         </div>
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Gender</label>
                                         <label class="radio-inline">
                                         <input type="radio" name="optradio" checked>Male
                                         </label>
                                         <label class="radio-inline">
                                         <input type="radio" name="optradio">Female
                                         </label>
                                         <label class="radio-inline">
                                         <input type="radio" name="optradio">Other
                                         </label>
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Mobile Number</label>
                                         <input type="text" name="phone" class="form-control" placeholder="Enter phone number">
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>
                                 </div>
                              </div>
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Address</h2>
                                 <div class="row">
                                 <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                       <label class="required">Address First Line</label>
                                       <input type="text" name="address" class="form-control" placeholder="Enter first line of address">
                                       <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                       <label class="required">Town/City</label>
                                        <input class="form-control" type="text" name="town_city" placeholder="Enter town/city">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                       <label class="required">PostCode</label>
                                       <input type="text" name="post_code" class="form-control" placeholder="Enter postcode">
                                       <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group">
                                       <label class="required">Country</label>
                                       <select class="form-control">
                                           <option>Select Country</option>
                                            <option value="" selected="selected">Select Country</option> 
                                            <option value="United States">United States</option> 
                                            <option value="United Kingdom">United Kingdom</option> 
                                            <option value="Afghanistan">Afghanistan</option> 
                                            <option value="Albania">Albania</option> 
                                            <option value="Algeria">Algeria</option> 
                                            <option value="American Samoa">American Samoa</option> 
                                            <option value="Andorra">Andorra</option> 
                                            <option value="Angola">Angola</option> 
                                            <option value="Anguilla">Anguilla</option> 
                                            <option value="Antarctica">Antarctica</option> 
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                            <option value="Argentina">Argentina</option> 
                                            <option value="Armenia">Armenia</option> 
                                            <option value="Aruba">Aruba</option> 
                                            <option value="Australia">Australia</option> 
                                            <option value="Austria">Austria</option> 
                                            <option value="Azerbaijan">Azerbaijan</option> 
                                            <option value="Bahamas">Bahamas</option> 
                                            <option value="Bahrain">Bahrain</option> 
                                            <option value="Bangladesh">Bangladesh</option> 
                                            <option value="Barbados">Barbados</option> 
                                            <option value="Belarus">Belarus</option> 
                                            <option value="Belgium">Belgium</option> 
                                            <option value="Belize">Belize</option> 
                                            <option value="Benin">Benin</option> 
                                            <option value="Bermuda">Bermuda</option> 
                                            <option value="Bhutan">Bhutan</option> 
                                            <option value="Bolivia">Bolivia</option> 
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
                                            <option value="Botswana">Botswana</option> 
                                            <option value="Bouvet Island">Bouvet Island</option> 
                                            <option value="Brazil">Brazil</option> 
                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
                                            <option value="Brunei Darussalam">Brunei Darussalam</option> 
                                            <option value="Bulgaria">Bulgaria</option> 
                                            <option value="Burkina Faso">Burkina Faso</option> 
                                            <option value="Burundi">Burundi</option> 
                                            <option value="Cambodia">Cambodia</option> 
                                            <option value="Cameroon">Cameroon</option> 
                                            <option value="Canada">Canada</option> 
                                            <option value="Cape Verde">Cape Verde</option> 
                                            <option value="Cayman Islands">Cayman Islands</option> 
                                            <option value="Central African Republic">Central African Republic</option> 
                                            <option value="Chad">Chad</option> 
                                            <option value="Chile">Chile</option> 
                                            <option value="China">China</option> 
                                            <option value="Christmas Island">Christmas Island</option> 
                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
                                            <option value="Colombia">Colombia</option> 
                                            <option value="Comoros">Comoros</option> 
                                            <option value="Congo">Congo</option> 
                                            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
                                            <option value="Cook Islands">Cook Islands</option> 
                                            <option value="Costa Rica">Costa Rica</option> 
                                            <option value="Cote D'ivoire">Cote D'ivoire</option> 
                                            <option value="Croatia">Croatia</option> 
                                            <option value="Cuba">Cuba</option> 
                                            <option value="Cyprus">Cyprus</option> 
                                            <option value="Czech Republic">Czech Republic</option> 
                                            <option value="Denmark">Denmark</option> 
                                            <option value="Djibouti">Djibouti</option> 
                                            <option value="Dominica">Dominica</option> 
                                            <option value="Dominican Republic">Dominican Republic</option> 
                                            <option value="Ecuador">Ecuador</option> 
                                            <option value="Egypt">Egypt</option> 
                                            <option value="El Salvador">El Salvador</option> 
                                            <option value="Equatorial Guinea">Equatorial Guinea</option> 
                                            <option value="Eritrea">Eritrea</option> 
                                            <option value="Estonia">Estonia</option> 
                                            <option value="Ethiopia">Ethiopia</option> 
                                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
                                            <option value="Faroe Islands">Faroe Islands</option> 
                                            <option value="Fiji">Fiji</option> 
                                            <option value="Finland">Finland</option> 
                                            <option value="France">France</option> 
                                            <option value="French Guiana">French Guiana</option> 
                                            <option value="French Polynesia">French Polynesia</option> 
                                            <option value="French Southern Territories">French Southern Territories</option> 
                                            <option value="Gabon">Gabon</option> 
                                            <option value="Gambia">Gambia</option> 
                                            <option value="Georgia">Georgia</option> 
                                            <option value="Germany">Germany</option> 
                                            <option value="Ghana">Ghana</option> 
                                            <option value="Gibraltar">Gibraltar</option> 
                                            <option value="Greece">Greece</option> 
                                            <option value="Greenland">Greenland</option> 
                                            <option value="Grenada">Grenada</option> 
                                            <option value="Guadeloupe">Guadeloupe</option> 
                                            <option value="Guam">Guam</option> 
                                            <option value="Guatemala">Guatemala</option> 
                                            <option value="Guinea">Guinea</option> 
                                            <option value="Guinea-bissau">Guinea-bissau</option> 
                                            <option value="Guyana">Guyana</option> 
                                            <option value="Haiti">Haiti</option> 
                                            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
                                            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
                                            <option value="Honduras">Honduras</option> 
                                            <option value="Hong Kong">Hong Kong</option> 
                                            <option value="Hungary">Hungary</option> 
                                            <option value="Iceland">Iceland</option> 
                                            <option value="India">India</option> 
                                            <option value="Indonesia">Indonesia</option> 
                                            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
                                            <option value="Iraq">Iraq</option> 
                                            <option value="Ireland">Ireland</option> 
                                            <option value="Israel">Israel</option> 
                                            <option value="Italy">Italy</option> 
                                            <option value="Jamaica">Jamaica</option> 
                                            <option value="Japan">Japan</option> 
                                            <option value="Jordan">Jordan</option> 
                                            <option value="Kazakhstan">Kazakhstan</option> 
                                            <option value="Kenya">Kenya</option> 
                                            <option value="Kiribati">Kiribati</option> 
                                            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
                                            <option value="Korea, Republic of">Korea, Republic of</option> 
                                            <option value="Kuwait">Kuwait</option> 
                                            <option value="Kyrgyzstan">Kyrgyzstan</option> 
                                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
                                            <option value="Latvia">Latvia</option> 
                                            <option value="Lebanon">Lebanon</option> 
                                            <option value="Lesotho">Lesotho</option> 
                                            <option value="Liberia">Liberia</option> 
                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
                                            <option value="Liechtenstein">Liechtenstein</option> 
                                            <option value="Lithuania">Lithuania</option> 
                                            <option value="Luxembourg">Luxembourg</option> 
                                            <option value="Macao">Macao</option> 
                                            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
                                            <option value="Madagascar">Madagascar</option> 
                                            <option value="Malawi">Malawi</option> 
                                            <option value="Malaysia">Malaysia</option> 
                                            <option value="Maldives">Maldives</option> 
                                            <option value="Mali">Mali</option> 
                                            <option value="Malta">Malta</option> 
                                            <option value="Marshall Islands">Marshall Islands</option> 
                                            <option value="Martinique">Martinique</option> 
                                            <option value="Mauritania">Mauritania</option> 
                                            <option value="Mauritius">Mauritius</option> 
                                            <option value="Mayotte">Mayotte</option> 
                                            <option value="Mexico">Mexico</option> 
                                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
                                            <option value="Moldova, Republic of">Moldova, Republic of</option> 
                                            <option value="Monaco">Monaco</option> 
                                            <option value="Mongolia">Mongolia</option> 
                                            <option value="Montserrat">Montserrat</option> 
                                            <option value="Morocco">Morocco</option> 
                                            <option value="Mozambique">Mozambique</option> 
                                            <option value="Myanmar">Myanmar</option> 
                                            <option value="Namibia">Namibia</option> 
                                            <option value="Nauru">Nauru</option> 
                                            <option value="Nepal">Nepal</option> 
                                            <option value="Netherlands">Netherlands</option> 
                                            <option value="Netherlands Antilles">Netherlands Antilles</option> 
                                            <option value="New Caledonia">New Caledonia</option> 
                                            <option value="New Zealand">New Zealand</option> 
                                            <option value="Nicaragua">Nicaragua</option> 
                                            <option value="Niger">Niger</option> 
                                            <option value="Nigeria">Nigeria</option> 
                                            <option value="Niue">Niue</option> 
                                            <option value="Norfolk Island">Norfolk Island</option> 
                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option> 
                                            <option value="Norway">Norway</option> 
                                            <option value="Oman">Oman</option> 
                                            <option value="Pakistan">Pakistan</option> 
                                            <option value="Palau">Palau</option> 
                                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
                                            <option value="Panama">Panama</option> 
                                            <option value="Papua New Guinea">Papua New Guinea</option> 
                                            <option value="Paraguay">Paraguay</option> 
                                            <option value="Peru">Peru</option> 
                                            <option value="Philippines">Philippines</option> 
                                            <option value="Pitcairn">Pitcairn</option> 
                                            <option value="Poland">Poland</option> 
                                            <option value="Portugal">Portugal</option> 
                                            <option value="Puerto Rico">Puerto Rico</option> 
                                            <option value="Qatar">Qatar</option> 
                                            <option value="Reunion">Reunion</option> 
                                            <option value="Romania">Romania</option> 
                                            <option value="Russian Federation">Russian Federation</option> 
                                            <option value="Rwanda">Rwanda</option> 
                                            <option value="Saint Helena">Saint Helena</option> 
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                                            <option value="Saint Lucia">Saint Lucia</option> 
                                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
                                            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
                                            <option value="Samoa">Samoa</option> 
                                            <option value="San Marino">San Marino</option> 
                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
                                            <option value="Saudi Arabia">Saudi Arabia</option> 
                                            <option value="Senegal">Senegal</option> 
                                            <option value="Serbia and Montenegro">Serbia and Montenegro</option> 
                                            <option value="Seychelles">Seychelles</option> 
                                            <option value="Sierra Leone">Sierra Leone</option> 
                                            <option value="Singapore">Singapore</option> 
                                            <option value="Slovakia">Slovakia</option> 
                                            <option value="Slovenia">Slovenia</option> 
                                            <option value="Solomon Islands">Solomon Islands</option> 
                                            <option value="Somalia">Somalia</option> 
                                            <option value="South Africa">South Africa</option> 
                                            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
                                            <option value="Spain">Spain</option> 
                                            <option value="Sri Lanka">Sri Lanka</option> 
                                            <option value="Sudan">Sudan</option> 
                                            <option value="Suriname">Suriname</option> 
                                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
                                            <option value="Swaziland">Swaziland</option> 
                                            <option value="Sweden">Sweden</option> 
                                            <option value="Switzerland">Switzerland</option> 
                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option> 
                                            <option value="Taiwan, Province of China">Taiwan, Province of China</option> 
                                            <option value="Tajikistan">Tajikistan</option> 
                                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
                                            <option value="Thailand">Thailand</option> 
                                            <option value="Timor-leste">Timor-leste</option> 
                                            <option value="Togo">Togo</option> 
                                            <option value="Tokelau">Tokelau</option> 
                                            <option value="Tonga">Tonga</option> 
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option> 
                                            <option value="Tunisia">Tunisia</option> 
                                            <option value="Turkey">Turkey</option> 
                                            <option value="Turkmenistan">Turkmenistan</option> 
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
                                            <option value="Tuvalu">Tuvalu</option> 
                                            <option value="Uganda">Uganda</option> 
                                            <option value="Ukraine">Ukraine</option> 
                                            <option value="United Arab Emirates">United Arab Emirates</option> 
                                            <option selected="" value="United Kingdom">United Kingdom</option> 
                                            <option value="United States">United States</option> 
                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
                                            <option value="Uruguay">Uruguay</option> 
                                            <option value="Uzbekistan">Uzbekistan</option> 
                                            <option value="Vanuatu">Vanuatu</option> 
                                            <option value="Venezuela">Venezuela</option> 
                                            <option value="Viet Nam">Viet Nam</option> 
                                            <option value="Virgin Islands, British">Virgin Islands, British</option> 
                                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
                                            <option value="Wallis and Futuna">Wallis and Futuna</option> 
                                            <option value="Western Sahara">Western Sahara</option> 
                                            <option value="Yemen">Yemen</option> 
                                            <option value="Zambia">Zambia</option> 
                                            <option value="Zimbabwe">Zimbabwe</option>
                                       </select>
                                       <div class="clearfix"></div>
                                    </div>
                                 </div>
                                 </div>
                              </div>
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Your Password</h2>
                                 <div class="row">
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Password</label>
                                         <input type="password" name="password" class="form-control" placeholder="Enter password">
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>
                                   <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                         <label class="required">Confirm Password</label>
                                         <input type="password" name="password" class="form-control" placeholder="Re-Enter password">
                                         <div class="clearfix"></div>
                                      </div>
                                   </div>                                   
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="tab2">
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Photo</h2>
                                 <p class="upload-type-heading">Upload Your Photo <span>Take selfie of head and face or upload picture of face from device (to use as profile picture)</span></p>
                                 <div class="upload-photo-wrapper">
                                    <ul class="upload-types-btn">
                                       <li class="active">
                                          <i class="icon-cloud-upload"></i>
                                          <p>Profile Photo</p>
                                       </li>
                                       <li class="">
                                          <i class="icon-camera"></i>
                                          <p>Take </p>
                                       </li>
                                    </ul>
                                    <div class="upload-photo-block text-center">
                                       <div class="display-profile-picture">
                                          <i class="icon-image"></i>
                                          <div class="uploaded-photo">
                                             <img src="images/dummy/profile-photo.jpg">
                                          </div>
                                       </div>
                                       <button class="btn btn-primary upload-photo-btn btn-block">
                                          <span>Upload </span>
                                          <div class="loader"></div>
                                          <input type="file" name="upload_photo">
                                       </button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane tab-upload-photo-id" id="tab3">
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Documents</h2>
                                 <p class="upload-type-heading with-border"> Verify Your Identification
 <span>Upload/Take/scan your Photo ID and proof of address <br>(Upload up to 2 documents)</span></p>
                                 <div class="upload-photo-wrapper">
                                    <ol class="steps-blts">
                                        <li>UK Driving licence or</li>
                                        <li>Passport and a proof of address( utility bill, bank statement etc)</li>
                                    </ol>
                                    <ul class="upload-types-btn">
                                       <li class="active">
                                          <i class="icon-cloud-upload"></i>
                                          <p>Upload</p>
                                       </li>
                                       <li class="">
                                          <i class="icon-camera"></i>
                                          <p>Take</p>
                                       </li>
                                       <li class="">
                                          <i class="icon-document-scan"></i>
                                          <p>Scan </p>
                                       </li>
                                    </ul>
                                    <div class="upload-photo-block style-2 text-center">
                                        <input type="file" name="upload_photo">
                                        <div class="display-profile-picture">
                                          <i class="icon-upload"></i>
                                          <div class="uploaded-photo">     
                                                <button class="btn btn-primary upload-photo-btn btn-block">
                                                    <span>Upload Documents</span>
                                                    <div class="loader"></div>
                                               </button>                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="section-separater"></div>
                                    <div class="uploaded-reports">
                                        <ul>
                                            <li>
                                                <i class="icon-file-text-o attached-file-icon"></i>
                                                <p class="attached-file-name"><a href="javascript:;">driving-license.pdf</a></p>
                                                <span class="icon-remove remove-attached-file"></span>
                                            </li>
                                            <li>
                                                <i class="icon-file-text-o attached-file-icon"></i>
                                                <p class="attached-file-name"><a href="javascript:;">passport.pdf</a></p>
                                                <span class="icon-remove remove-attached-file"></span>
                                            </li>
                                        </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="tab4">
                              <div class="tab-pane-inner">
                                 <h2 class="heading">Reports</h2>
                                 <p class="upload-type-heading">Upload Your Reports <span>Upload/Take/scan picture of recent psychiatric reports (Maximum 2 Reports)</span></p>
                                 <div class="upload-photo-wrapper">
                                    <ul class="upload-types-btn">
                                       <li class="active">
                                          <i class="icon-cloud-upload"></i>
                                          <p>Upload</p>
                                       </li>
                                       <li class="">
                                          <i class="icon-camera"></i>
                                          <p>Take</p>
                                       </li>
                                       <li class="">
                                          <i class="icon-document-scan"></i>
                                          <p>Scan</p>
                                       </li>
                                    </ul>
                                    <div class="upload-photo-block style-2 text-center">
                                        <input type="file" name="upload_photo">
                                        <div class="display-profile-picture">
                                          <i class="icon-upload"></i>
                                          <div class="uploaded-photo">     
                                                <button class="btn btn-primary upload-photo-btn btn-block">
                                                    <span>Upload Reports</span>
                                                    <div class="loader"></div>
                                               </button>                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="section-separater"></div>
                                    <div class="uploaded-reports">
                                        <ul>
                                            <li>
                                                <i class="icon-file-text-o attached-file-icon"></i>
                                                <p class="attached-file-name"><a href="javascript:;">report-1.pdf</a></p>
                                                <span class="icon-remove remove-attached-file"></span>
                                            </li>
                                            <li>
                                                <i class="icon-file-text-o attached-file-icon"></i>
                                                <p class="attached-file-name"><a href="javascript:;">report-2.jpeg</a></p>
                                                <span class="icon-remove remove-attached-file"></span>
                                            </li>
                                        </ul>
                                    </div>
                                 </div>
                              </div>
                            </div>
                           <div class="tab-pane i-agree-tab-pane" id="tab5">
                              <div class="tab-pane-inner">
                                 <h2 class="heading">I Agree</h2>
                                 <p class="upload-type-heading">I Agree <span> I Agree With Terms & Conditions and Privacy Policy</span></p>
                                 <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="steps-divider"></div>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox">
                                              <label><input type="checkbox" value="">I Agree to Consent and <a href="javascript:;">privacy policy</a></label>
                                            </div>
                                            <div class="checkbox">
                                              <label><input type="checkbox" value="">I Agree to <a href="javascript:;">terms and conditions</label>
                                            </div>
                                        </div>
                                    </div>    
                                 </div>
                              </div>
                           </div>
                           <ul class="pager wizard">
                              <li class="previous float-left"><a class="btn btn-warning" href="#">Previous</a></li>
                              <li class="next float-right"><a class="btn btn-primary" href="#">Next</a></li>
                           </ul>
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>

   </div>
                <?php include_once('include/footer.php') ?>
</body>
</html>



<!--                                     <div class="col-xs-12 col-md-12">
                                        <p>By creating your account you confirm you have read and understood the  <a href="javascript;">Privacy & Cookie</a> Policy and agreed to the <a href="javascript;">Terms and conditions.</a></p>
                                    </div> -->  