<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner inner-banner about-banner">
        <div class="middle-align">
          <div class="inner">
            <div class="max-wrapper">
                <h1 class="inner-banner-heading">Terms & Conditions
                    <h1>
                    </div>              
                </div>
            </div>
        </div>
        <!-- steps section -->
        <div class="section static-pages">
            <div class="max-wrapper"> 

                <div class="col-md-12">
                  <h3>WhatLorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                  <hr>
                  <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. Fusce urna arcu, pharetra non luctus sed, dapibus in arcu. Quisque in mollis nulla. In vitae odio vulputate ligula varius pulvinar ut sit amet lorem.</p>

                  <h3>Curabitur dignissim lectus id tortor rhoncus, eget egestas est tristique.</h3>
                  <hr>
                  <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>
                  <h3>Curabitur dignissim lectus id tortor rhoncus, eget egestas est tristique.</h3>
                  <hr>
                  <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>
                  <ul>
                    <li>sit amet lorem eu, consequat </li>
                    <li>Cras diam mi, pharetra </li>
                    <li>Phasellus eget sodales nunc, </li>
                    <li>vel placerat ante porta ne</li>
                    <li>dolor magna venenatis </li>
                </ul>
                <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>  <h3>Curabitur dignissim lectus id tortor rhoncus, eget egestas est tristique.</h3>
                <hr>
                <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>                      <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p> 

                <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>
                <ol>
                    <li>sit amet lorem eu, consequat </li>
                    <li>Cras diam mi, pharetra </li>
                    <li>Phasellus eget sodales nunc, </li>
                    <li>vel placerat ante porta ne</li>
                    <li>dolor magna venenatis </li>
                    <li>sit amet lorem eu, consequat </li>
                    <li>Cras diam mi, pharetra </li>
                    <li>Phasellus eget sodales nunc, </li>
                    <li>vel placerat ante porta ne</li>
                    <li>dolor magna venenatis </li>
                </ol>             
                <h3>Curabitur dignissim lectus id tortor rhoncus, eget egestas est tristique.</h3>
                <hr>
                <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>                      <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p> 

                <p class="mb-20">Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia. </p>

            </div>
            <div class="clearfix"></div>   
        </div>
    </div>

    <?php include_once('include/footer.php') ?>
</body>
</html>