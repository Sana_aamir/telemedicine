<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <?php include_once('include/banner.php') ?>

    <!-- steps section -->
    <div class="section steps-section">
        <div class="max-wrapper">
            <ul>
                <li>
                    <i class="steps-count">1</i>
                    <span><i><img src="images/icons/book-appointment.png"></i> Book An Appoinment</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p>
                </li>
                <li>
                    <i class="steps-count">2</i>
                    <span><i><img src="images/icons/doctor-online.png"></i> See a Psychiatrist Online</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p>
                </li>
                <li>
                    <i class="steps-count">3</i>
                    <span><i><img src="images/icons/recovery.png"></i> Start Recovery</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p>
                </li>
            </ul>        
        </div>
    </div>
    <div class="max-wrapper">
        <div class="section-separater"></div>
    </div>
    <!-- welcome section -->
    <div class="section welcome-section">
        <div class="max-wrapper">
          <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-8 welcome-section-left">
                  <h2 class="section-heading">WE GIVE YOU <span>THE BEST</span></h2>
                  <p class="section-heading-detail">WE GIVE YOU THE BEST
                    Just two good old boys never meaning no harm beats all you have ever saw been in trouble with the law since the day they was born so the most of day</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 feature-colmns">
                          <h2 class="sub-heading">
                              <i class="services-icon"><img src="images/icons/secure.svg"></i>              
                              Confidential and Discreet Service
                          </h2>
                          <p>What might be right for you may not be right for some so lets make the most of this day since we are together evryday</p>
                      </div>
                      <div class="col-xs-12 col-md-6 feature-colmns">
                          <h2 class="sub-heading">
                              <i class="services-icon"><img src="images/icons/encrypted.svg"></i>                
                              Secure, Encrypted video Consultations 
                          </h2>
                          <p>Make all our dreams come true for me and you and we know flipper lives in a world full of wonder flying sea shore out there.</p>
                      </div>
                      <div class="col-xs-12 col-md-6 feature-colmns">
                          <h2 class="sub-heading">
                              <i class="services-icon"><img src="images/icons/blood-service.svg"></i>               
                              Repeat Blood Test service
                          </h2>
                          <p>On your mark get set and go now got a dream and we just know now we are gonna make our dream come true to our destiny</p>
                      </div>
                      <div class="col-xs-12 col-md-6 feature-colmns">
                          <h2 class="sub-heading">
                              <i class="services-icon"><img src="images/icons/cqc.svg"></i>                
                              CQC Regulated 
                          </h2>
                          <p>They were four men living all together yet they were all alone well we are moving on up to the east side to a apartment</p>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12 col-md-4 welcome-section-right">
                  <div class="appointment-panel">
                      <h2>Check Doctors’ Availability</h2>
                      <p>Just make an appointment to get help from our experts</p>
                      <div class="appointment-form">
                          <form>
                              <div class="row">
                                  <div class="col-xs-12 col-md-12">
                                      <div class="form-group">
                                          <select class="form-control">
                                              <option>Select Specialty</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <select class="form-control">
                                              <option>Select Doctor</option>
                                          </select>
                                      </div>
                                      <div class="form-group datepicker-field">
                                        <input class="form-control datepicker" type="text" name="enter_date" id="enter_date" placeholder="Enter date">
                                        <i class="calender-icon icon-calendar"></i>
                                    </div>
                                    <a href="book-appoinment.php" class="btn btn-primary btn-block book-appointment-btn">
                                        <span>Check Availability</span>
                                        <div class="loader"></div>                                      
                                    </a>     
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="form-footer">
                        <button class="btn btn-warning btn-block choose-doc-pre-seen">
                            <span>Choose Doctor previously Seen</span>
                            <div class="loader"></div>
                        </button>                                                                  
                    </div>
                </div>           
            </div>
        </div>
    </div>        
</div>


<div class="section grey membership-section">
    <div class="max-wrapper">
        <div class="row custom-box-row">
            <div class="col-xs-12 col-md-3  custom-col">
                <div class="column benefits-req-column">
                    <div class="inner">
                       <h2 class="section-heading ">Benefits</h2>
                       <div class="price-list">
                            <ul class="list-unstyled">
                                <li>Lorem ipsum dolor sit amet</li>
                                <li>Aliquam molestie vestibulum</li>
                                <li>Ut fringilla aliquam leo. Duis dictum</li>
                                <li>justo vel augue pellentesque suscipit</li>
                                <li>Nunc porta libero est, </li>
                                <li>Nam egestas sem eget finibus</li>
                            </ul>
                        </div>  
                    </div>                    
                </div>
            </div>
            <div class="col-xs-12 col-md-3  custom-col">
                <div class="column membership-column">
                        <div class="inner">
                            <h2 class="section-heading ">Membership</h2>
                            <div class="price-value">
                                <h2>
                                    <span class="price-dollar">$</span>25<span>/ Month</span>
                                </h2>
                            </div>
                            <div class="price-list">
                              <ul class="list-unstyled">
                                <li>Appointment length: <strong>unlimited</strong></li>
                                <li>Additional 10 minute extensions: <strong>free</strong></li>
                                <li>Prescription admin fee: <strong>free</strong></li>
                                <li>Fit notes: <strong>free</strong></li>
                                <li>Referrals: <strong>free</strong></li>
                                <li>Personalised content</li>
                            </ul>
                        </div>  
                        <a href="javascript:;" class="btn btn-white">
                            Get Membership
                        </a>                                      
                    </div>                
                </div>
            </div>

            <div class="col-xs-12 col-md-3  custom-col">
                <div class="column membership-column pay-us-column">
                        <div class="inner">
                            <h2 class="section-heading ">Pay as you go</h2>
                            <div class="price-value">
                                <h2>
                                    <span class="price-dollar">$</span>30<span>/ Month</span>
                                </h2>
                            </div>
                            <div class="price-list">
                              <ul class="list-unstyled">
                                <li>Appointment length:  <strong>10 minutes</strong></li>
                                <li>Additional 10 minute extensions: <strong>£15</strong></li>
                                <li>Prescription admin fee: <strong>£8</strong></li>
                                <li>Fit notes: <strong>£15</strong></li>
                                <li>Referrals: <strong>£15</strong></li>
                            </ul>
                        </div>  
                        <a href="book-appoinment.php" class="btn btn-white">
                            Book Appointment
                        </a>                                      
                    </div>                
                </div>
            </div>

            <div class="col-xs-12 col-md-3 custom-col">
                <div class="column benefits-req-column">
                    <div class="inner">
                       <h2 class="section-heading ">Requirements</h2>
                        <div class="price-list">
                            <ul class="list-unstyled">
                                <li>Lorem ipsum dolor sit amet</li>
                                <li>Aliquam molestie vestibulum</li>
                                <li>Ut fringilla aliquam leo. Duis dictum</li>
                                <li>justo vel augue pellentesque suscipit</li>
                                <li>Nunc porta libero est, </li>
                                <li>Nam egestas sem eget finibus</li>
                            </ul>
                        </div>  
                    </div>                    
                </div>
            </div>                        
</div>
</div>
</div>

<div class="section no-pad grey membership-section" style="display: none;">
    <div class="middle-align">
        <ul class="parallel-colmn">
            <li></li>
            <li>
                <div class="middle-align">
                    <div class="inner">
                        <h2 class="section-heading ">Membership</h2>
                        <div class="price-value">
                            <h2>
                                <span class="price-dollar">$</span>25<span>/ Month</span>
                            </h2>
                        </div>
                        <div class="price-list">
                          <ul class="list-unstyled">
                            <li>Appointment length: <strong>unlimited</strong></li>
                            <li>Additional 10 minute extensions: <strong>free</strong></li>
                            <li>Prescription admin fee: <strong>free</strong></li>
                            <li>Fit notes: <strong>free</strong></li>
                            <li>Referrals: <strong>free</strong></li>
                            <li>Personalised content</li>
                        </ul>
                    </div>
                    <a href="javascript:;" class="btn btn-white">
                        Get Membership
                    </a>
                </div>
            </div>
        </li>            
    </ul>            
</div>
</div>

<!--Doctors section -->
<div class="section  doctors-section">
    <div class="max-wrapper">
        <h2 class="section-heading text-center">Meet Our <span>Doctors</span></h2>
        <p class="section-heading-detail center-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem
            voluptatem obcaecati!</p>
            <div class="row doctors-slider" style="overflow:hidden; ">
                <div class="col-lg-12 col-md-12">
                    <div class="team-member style-2 text-center">
                      <div class="team-images">
                        <img class="img-fluid" src="images/dummy/1.jpg" alt="">
                    </div>
                    <div class="team-description"> 
                        <h5 class="mb-2">Dr. John Maxwell</h5> 
                        <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 md-mt-5">
                <div class="team-member style-2 text-center">
                  <div class="team-images">
                    <img class="img-fluid" src="images/dummy/2.jpg" alt="">
                </div>
                <div class="team-description"> 
                    <h5 class="mb-2">Dr. Matthew Doe</h5> 
                    <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 md-mt-5">
            <div class="team-member style-2 text-center">
              <div class="team-images">
                <img class="img-fluid" src="images/dummy/3.jpg" alt="">
            </div>
            <div class="team-description"> 
                <h5 class="mb-2">Dr. Romi Keely</h5> 
                <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
            </div>
        </div>
    </div>
        <div class="col-lg-12 col-md-12 md-mt-5">
            <div class="team-member style-2 text-center">
              <div class="team-images">
                <img class="img-fluid" src="images/dummy/3.jpg" alt="">
            </div>
            <div class="team-description"> 
                <h5 class="mb-2">Dr. Romi Keely</h5> 
                <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
            </div>
        </div>
    </div>
        <div class="col-lg-12 col-md-12 md-mt-5">
            <div class="team-member style-2 text-center">
              <div class="team-images">
                <img class="img-fluid" src="images/dummy/3.jpg" alt="">
            </div>
            <div class="team-description"> 
                <h5 class="mb-2">Dr. Romi Keely</h5> 
                <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
            </div>
        </div>
    </div>

</div>
</div>
</div>




<div class="section grey comparision-chart ">
    <div class="max-wrapper">
    <h2 class="section-heading text-center">What We <span>Provide</span></h2>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered what-we-offer-table">
                <thead>
                      <tr>
                        <th scope="col" rowspan="2" class="table-service-col row-middle-align" width="100">Services</th>
                        <th scope="col" colspan="2" class="table-fees-col text-center" >Fees</th>
                        <th scope="col" rowspan="2" class="table-description-col row-middle-align" width="350">Description</th>
                        <th scope="col" rowspan="2" class="table-prescription-col row-middle-align" width="200">Prescription Issue</th>
                    </tr>
                    <tr>
                        <th scope="row" width="120" class="text-center">Member</th>
                        <th width="120" class="text-center">None Member</th>
                    </tr>                    
                </thead>
                <tbody>
                    <tr>
                        <td><strong>Initial <br> Assessment</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
                        <td>Lorem Ipsum</td>
                    </tr>
                    <tr>
                        <td><strong>Follow up <br>Assessment- 1</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
                        <td>Lorem Ipsum</td>
                    </tr>
                    <tr>
                        <td><strong>Follow up <br>Assessment- 2</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis venenatis </td>
                        <td>Lorem Ipsum</td>
                    </tr>
                    <tr>
                        <td><strong>Telephone  <br>Adivce 3</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer </td>
                        <td>Lorem Ipsum</td>
                    </tr>
                    <tr>
                        <td><strong>Repeat blood  <br>test service</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer.</td>
                        <td>Lorem Ipsum</td>
                    </tr>
                    <tr>
                        <td><strong>Fit Notes</strong></td>
                        <td class="text-center"><strong>$10</strong></td>
                        <td class="text-center"><strong>$20</strong></td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer.</td>
                        <td>Lorem Ipsum</td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>


<div class="section  our-promo-section">
    <div class="max-wrapper">
        <h2 class="section-heading text-center">WATCH OUR <span>VIDEO</span></h2>
        <p class="section-heading-detail center-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem
            voluptatem obcaecati!</p>
            <div class="video-wrapper">
                <div class="video-cover">
                    <img src="images/video/cover.jpg">
                    <div class="video-overlay"></div>
                    <div class="play-btn"><img src="images/icons/play-button.svg"></div>
                </div>
            </div>
        </div>
    </div>


<div class="section grey testimonial-section">
    <div class="max-wrapper">
            <h2 class="section-heading text-center">Testimonials</h2>
            <ul class="testimonial-slides owl-carousel owl-theme">
                <li class="item">
                    <h2>Corvin Adams
                    <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>   
                </li>
                <li class="item">
                    <h2>Corvin Adams
                    <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>   
                </li>
                <li class="item">
                    <h2>Corvin Adams
                    <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>   
                </li>
            </ul>
            <div class="slider-blts">
                <a href="javascript:;" class="active"></a> 
                <a href="javascript:;" ></a> 
                <a href="javascript:;" ></a> 
            </div>
    </div>
</div>

<?php include_once('include/footer.php') ?>
</body>
</html>