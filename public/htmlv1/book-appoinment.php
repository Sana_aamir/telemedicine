<!DOCTYPE HTML>
<html>
    <?php include_once('include/head.php') ?>
    <body class="register-page">
        <?php include_once('include/header.php') ?>
        <div class="form-right-column section">
            <div class="middle-align">
                <div class="inner">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <h2 class="section-heading text-center">Book Online Appointment</h2>
                            <!--                   <p class="text-center">Happy Clinic is much better when you have a account!</p> -->
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <section id="wizard">
                                <div id="rootwizard">
                                    <div class="navbar ">
                                        <div class="navbar-inner">
                                            <ul class="form-wizard-steps">
                                                <li><a href="#tab1" data-toggle="tab">1
                                                    <span>Step 1</span>
                                                    </a>
                                                </li>
                                                <li><a href="#tab2" data-toggle="tab">2
                                                    <span>Step 2</span>
                                                    </a>
                                                </li>
                                                <li><a href="#tab3" data-toggle="tab">3
                                                    <span>Step 3</span>
                                                    </a>
                                                </li>
                                                <li><a href="#tab4" data-toggle="tab">4
                                                    <span>Step 4</span>
                                                    </a>
                                                </li>
                                                <li><a href="#tab5" data-toggle="tab">5
                                                    <span>Step 5</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div id="bar" class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane PD-tab-content" id="tab1">
                                            <div class="tab-pane-inner">
                                                <p class="upload-type-heading">Lorem Ipsum </p>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="required">Select Speciality</label>
                                                        <select class="form-control">
                                                            <option value="" selected=""> Select Speciality</option>
                                                            <option>Speciality 1</option>
                                                            <option>Speciality 2</option>
                                                            <option>Speciality 3</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="required">Select Doctor</label>
                                                        <select class="form-control">
                                                            <option value="" selected=""> Select Doctor</option>
                                                            <option>Doctor 1</option>
                                                            <option>Doctor 2</option>
                                                            <option>Doctor 3</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="required">Select Appointment Type</label>
                                                        <select class="form-control">
                                                            <option value="" selected=""> Select Type</option>
                                                            <option>Appointment type 1</option>
                                                            <option>Appointment type 2</option>
                                                            <option>Appointment type 3</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                     <label class="required">Select Apointment Date/Time</label>
                                                     <div class="row">
                                                         <div class="col-xs-12 col-md-6">
                                                          <div class="form-group datepicker-field style-2">
                                                            <input class="form-control datepicker initial-field" type="text" name="enter_date" id="enter_date" placeholder="Enter Date">
                                                            <i class="calender-icon icon-calendar"></i>
                                                        </div> 
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                      <div class="form-group datepicker-field style-2">
                                                        <input class="form-control timepicker initial-field" type="text" name="enter_date" id="enter_date" placeholder="Enter Time" id="example1">
                                                        <i class="calender-icon icon-clock-o"></i>
                                                        <div class="clearfix"></div>
                                                    </div> 
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div> -->
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <div class="tab-pane-inner">
                                                <p class="upload-type-heading"> Find Hospital</p>
                                                <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">Search by Hospital Name</label>
                                                                    <input type="text" name="search_hospital" class="form-control" placeholder="Enter hospital name">                                                   
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">Search by Town/City or Postcode</label>
                                                                    <input type="search_hospital" name="searchhospital" class="form-control" placeholder="Search by town/city or postcode">                                                   
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-2">
                                                                <button class="btn btn-primary btn-md"><span>Search</span></button>
                                                            </div>
                                                        </div>

                                                        <!-- hospital list after search -->
                                                        <div class="hospital-list-wrapper">
                                                            <h4>Search results</h4>
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="team-member style-3">
<!--                                                                         <div class="hospital-logo"><img src="images/dummy/express-healthcare-excellence-award.jpg"></div> -->
                                                                        <div class="team-description">
                                                                            <h4>Express Healthcare Excellence</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>                                  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="team-member style-3">
<!--                                                                         <div class="hospital-logo"><img src="images/dummy/ihf-excellence-award.jpg"></div> -->
                                                                        <div class="team-description">
                                                                            <h4>Internation Hospital Federation</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="team-description notify-question">
                                                            <div class="form-group">
                                                            <p><strong>Would you like us to notify the hospital regarding your condition?</strong>
                                                                <div class="radio custom-radio btn-inline-block">
                                                                    <label>
                                                                        <input type="radio" value="" name="radio1" checked>
                                                                        <span class="radio-circle"></span>
                                                                        Yes</label>
                                                                </div>
                                                                <div class="radio custom-radio btn-inline-block">
                                                                    <label>
                                                                        <input type="radio" value="" name="radio1" checked>
                                                                        <span class="radio-circle"></span>
                                                                        No</label>
                                                                </div>
                                                            </div>
                                                            </p>                                
                                                        </div>
                                                        <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane tab-upload-photo-id" id="tab3">
                                            <div class="tab-pane-inner">
                                                <p class="upload-type-heading"> Find Your Nearest Laboratory</p>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="required">Enter Your Postcode</label>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-10">
                                                                <input type="search_hospital" name="searchhospital" class="form-control" placeholder="Enter your postcode">                                                   
                                                            </div>
                                                            <div class="col-xs-12 col-md-2">
                                                                <button class="btn btn-primary btn-md"><span>Search</span></button>
                                                            </div>
                                                        </div>
                                                        <!-- hospital list after search -->
                                                        <div class="hospital-list-wrapper">
                                                            <h4>Search results</h4>
                                                            <ul class="owl-carousel-laboratory">
                                                                <li class="item">
                                                                    <div class="team-member style-3">
                                                                        <div class="team-description text-left">
                                                                            <h4>SYNLAB UK</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>  
                                                                        </div>
                                                                    </div>                                                                    
                                                                </li>
                                                                <li class="item">
                                                                    <div class="team-member style-3 text-left">
                                                                        <div class="team-description">
                                                                            <h4>SYNLAB UK</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>  
                                                                        </div>
                                                                    </div>                                                                    
                                                                </li>
                                                                <li class="item">
                                                                    <div class="team-member style-3">
                                                                        <div class="team-description">
                                                                            <h4>SYNLAB UK</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>  
                                                                        </div>
                                                                    </div>                                                                    
                                                                </li>
                                                                <li class="item">
                                                                    <div class="team-member style-3">
                                                                        <div class="team-description">
                                                                            <h4>SYNLAB UK</h4>
                                                                            <p><strong>Address: </strong>2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN</p>
                                                                            <p><strong>Town/City: </strong>London</p>
                                                                            <p><strong>Postcode: </strong> 12564</p>
                                                                            <button class="btn btn-primary btn-block">
                                                                            <span>Select</span>
                                                                            <input type="radio" value="" name="select_radio">
                                                                            <i class="icon-check"></i>                                  
                                                                            </button>  
                                                                        </div>
                                                                    </div>                                                                    
                                                                </li>


                                                            </ul>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <div class="tab-pane-inner">
                                                <!--    <h2 class="heading">Appointment Details</h2> -->
                                                <p class="upload-type-heading">Medical Questionnaire</p>
                                                <ol class="steps-blts question-blts">
                                                    <li>
                                                        <p>Please tell us about your problems or what you need help with us?</p>
                                                        <div class="form-group">
                                                            <textarea class="form-control" rows="4" placeholder="Start Typing"></textarea>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio1" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio1">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio2" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio2" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio3" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio3" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio4" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio4" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio5" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio5" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio6" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio6" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio7" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio7" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio8" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio8" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio9" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio9" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo.</p>
                                                        <div class="form-group">
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio10" checked="">
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">Yes</p>
                                                                </label>
                                                            </div>
                                                            <div class="radio custom-radio">
                                                                <label><input type="radio" value="" name="qRadio10" >
                                                                    <span class="radio-circle"></span>
                                                                    <p class="label-text">No</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ol>
                                            </div>
                                        </div>
                                        <div class="tab-pane i-agree-tab-pane" id="tab5">
                                            <div class="tab-pane-inner">
                                                <h2 class="heading">I Agree</h2>
                                                <p class="upload-type-heading">I Agree <span> I Agree With Terms & Conditions and Privacy Policy</span></p>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="steps-divider"></div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label><input type="checkbox" value="">I Agree to Consent and <a href="javascript:;">privacy policy</a></label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" value="">I Agree to 
                                                                    <a href="javascript:;">
                                                                        terms and conditions
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="pager wizard">
                                        <li class="previous float-left"><a class="btn btn-warning" href="#">Previous</a></li>
                                        <li class="next float-right"><a class="btn btn-primary" href="#">Next</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>

                </div>
            </div>

        </div>
                        <?php include_once('include/footer.php') ?>
        <!--previously seen doctor-->
        <div class="modal modal-md fade in show" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document" style="width: 100%; max-width: 400px;">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Consulted Doctor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                    <label>if you already consulted a doctor earlier?</label>
                    <button class="btn btn-primary btn-block">
                        <span>
                            Choose Doctor Previously Seen
                        </span>
                    </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            $('.modal').modal('show');
        </script>

    </body>
</html>