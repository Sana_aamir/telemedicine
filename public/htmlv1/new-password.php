<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="register-page login">
 <?php include_once('include/header.php') ?>
<div class="form-right-column section">
  <div class="middle-align">
     <div class="inner">
        <div class="row">
           <div class="col-xs-12 col-md-12">
              <h2 class="section-heading text-center">New Password</h2>
              <p class="text-center">Create your new password</p>
          </div>
          <div class="col-md-12 col-xs-12">
              <section id="wizard">
                 <div id="rootwizard">

                    <div class="tab-content">
                       <div class="tab-pane" id="tab1" style="display: block !important; ">
                          <div class="tab-pane-inner">
                            <div class="login-screen">
                                <div class="row">                       
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 password-field">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" name="new_password" class="form-control" placeholder="Enter new password">
                                        </div >
                                    </div>
                                    <div class="col-xs-12 col-md-12 password-field">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Re-enter new password">
                                        </div>
                                    </div>
                                  <div class="col-xs-12 col-md-12">
                                    <a href="javascript:;" class="btn btn-primary">
                                        <span>Update</span>
                                        <div class="loader"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        </div>
                    <div class="text-center">
                        <p>Don't have an account? <a href="javascript:;">Register</a></p> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
</div>
</div>
</div>      

 <?php include_once('include/footer.php') ?>
</body>
</html>