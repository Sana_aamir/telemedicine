<div class="header">
	<div class="fixed-header">
			<div class="top-bar">
				<div class="max-wrapper full-wrapper clearfix">
				<ul class="float-left left">
					<li><a href="javascript:;"><i class="icon-phone"></i><span>123-456-789</span></a></li>
					<li><a href="javascript:;"><i class="icon-envelope"></i><span>contact@yourdomain.com</span></a></li>
				</ul>
				</div>
			</div>
			<div class="logo">
				<a href="index.php">
					<img src="images/logo-wide.png">
				</a>
			</div>
			<div class="float-right menu-main-wrapper">
				<span class="menu-icon">
					<a href="javascript:;">
						<i class="icon-bars"></i>
					</a>
				</span>	
				<div class="main-navigation">
					<div class="main-navigation-inner">
						<h2 class="menu-hd">Menu</h2>
						<i class="menu-icon-remove icon-remove"></i>
						<ul class="">
							<li class="">
								<a class="btn btn-primary membership-btn gredeint-btn" href="memberships.php">Membership</a>
							</li>
							<li>
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about-us.php">About Us</a>
							</li>
							<li>
								<a href="how-its-work.php">How It Works</a>
							</li>
							<li>
								<a href="javascript:;">Pricing</a>
							</li>
							<li>
								<a href="javascript:;">What We Treat</a>
							</li>
							<li>
								<a href="faq.php">Faq</a>
							</li>
							<li>
								<a class="btn  gredeint-btn-blue membership-btn " href="register.php">Register</a>
							</li>
							<li>
								<a class="btn  gredeint-btn-blue membership-btn" href="login.php">Login</a>
							</li>
						</ul>						
					</div>											
				</div>
			</div>
	</div>
</div>