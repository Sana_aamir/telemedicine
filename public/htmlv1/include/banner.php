<div class="landing-banner">
	<ul>
		<li>
			<img src="images/banner/landing/4.jpg" class="hide-mobile">
			<img src="images/mobile/4.jpg" class="hide-desktop">
			<div class="banner-content">
				<div class="middle-align">
					<div class="inner">
						<div class="max-wrapper">
							<div class="th-slidercontent">
								<h1> Are you stressed, anxious <span>or depressed?</span></h1>
								<div class="th-description">
									<p>Do you have problems with your sleep? Do you feel tired? Do you find it difficult to concentrate on your studies or work? Do you find it difficult to socialise? Do you get flashbacks of past traumatic events? Are there any strange experiences or any other mental health problem causing difficulties? <strong>If yes<strong>,<br><strong>  we can help you.</strong></p>
								</div>
								<a href="book-appoinment.php" class="btn btn-md btn-primary book-appointment-btn">
									<span>Book Online Oppointment</span>
								</a>
								<a href="javascript:;" class="btn btn-md btn-dark-grey book-appointment-btn">
									<span>Download Web App</span>
								</a>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>
