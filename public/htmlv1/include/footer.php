<div class="section download-web-app">
    <div class="max-wrapper text-center">
        <div class="row">
            <div class="col-xs-12 col-md-6">
            <span class="app-icon iphone"><img src="images/apple.svg"></span>
            <h2 class="section-heading text-center">How to Download <span>Web App</span></h2>
            </div>
            <div class="col-xs-12 col-md-6">
                <span class="app-icon android"><img src="images/android.svg"></span>
                <h2 class="section-heading text-center">How to Download <span>Web App</span></h2>
            </div>
        </div>
    </div>
</div>
<div class="section footer-section">
	<div class="max-wrapper">
		<ul class="footer-menu">
			<li><a href="index.php">Home</a></li>
			<li><a href="about-us.php">About Us</a></li>
			<li><a href="how-its-work.php">How It Works</a></li>
			<li><a href="javascript:;">Pricing</a></li>
			<li><a href="javascript:;">What We Treat</a></li>
			<li><a href="register.php">Register</a></li>
			<li><a href="login.php">Login</a></li>
		</ul>
		<ul class="footer-menu">
			<li><a href="faq.php">Faq</a></li>
			<li><a href="terms.php">Terms & Conditions</a></li>
			<li><a href="privacy.php">Privacy & Cookies'</a></li>
			<li><a href="javascript:;">Sitemap</a></li>
			<li><a href="cqc.php">CQC</a></li>		
			<li><a href="for-doctors.php">For Doctors</a></li>	
			<li><a href="contact-us.php">Contact Us</a></li>
		</ul>
		<ul class="social-icons">
			<li><i class="icon-facebook"></i></li>
			<li><i class="icon-twitter"></i></li>
			<li><i class="icon-linkedin"></i></li>
		</ul>
	</div>	
		<span class="copyright-text">© 2018 loremipsum.com | All Rights Reserved</span>
</div>