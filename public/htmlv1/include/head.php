<head>
	<meta charset="utf-8">
	<title>Health Center</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link href="stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="stylesheets/icomoon.css" rel="stylesheet" type="text/css" />
	<link href="stylesheets/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
	<link href="stylesheets/owl-carousel.css" rel="stylesheet" type="text/css" />
	<link href="stylesheets/style.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/moment.js" type="text/javascript"></script>
	<script src="js/bootstrap-datetimepicker.js" type="text/javascript"></script>
	<script src="js/toogle.js" type="text/javascript"></script>
	<script src="js/amcharts.js" type="text/javascript"></script>
	<script src="js/serial.js" type="text/javascript"></script>
	<script src="js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
	<script src="js/prettify.js" type="text/javascript"></script>
	<script src="js/owl-carousel.js" type="text/javascript"></script>
	<script src="js/plug.js" type="text/javascript"></script>
</head>