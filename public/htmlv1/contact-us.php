<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <!-- steps section -->
    <div class="section grey">
        <div class="max-wrapper">
<!--         <h2 class="section-heading text-center">Contact Us</h2> -->
        	<div class="contact-form-wrapper">
        		<div class="contact-form-left">
        			<h2 class="section-heading text-center">Lets <span>Help You!</span></h2>
        			<ul>
        				<li>
        					<i><img src="images/clock.png"></i>
        					<strong>Business Hours</strong>
        					9:00 AM To 6:00 PM
        				</li>
        				<li>
        					<i><img src="images/location.png"></i>
        					<strong>Postal Address</strong>
        					The Business Centre 61 Wellfield Road Roath Cardiff CF24 3DG
        				</li>
        				<li>
        					<i><img src="images/phone.png"></i>
        					<strong>Call</strong>
        					0835 788296
        				</li>
        			</ul>
        		</div>
        		<div class="contact-form-right">
		            <h2 class="section-heading text-center">Get In Touch  <span>With Us!</span></h2>
		            <h3 class="our-pricing-sub-heading">For general questions to see <a href="faq.php">Faq</a></h3>
		            <div class="contact-form">
		            	<form action="" method="">
		            		<div class="row">
		            			<div class="col-xs-12 col-md-12">
		            				<div class="form-group">
		            					<label >Full Name</label>
		            					<input type="text" name="fullname" class="form-control" placeholder="Enter full name">
		            				</div>
		            			</div>
		            			<div class="col-xs-12 col-md-12">
		            				<div class="form-group">
		            					<label >Email</label>
		            					<input type="email" name="email" class="form-control" placeholder="Enter email">
		            				</div>
		            			</div>
		            			<div class="col-xs-12 col-md-12">
		            				<div class="form-group">
		            					<label >Phone</label>
		            					<input type="text" name="phone" class="form-control" placeholder="Enter phone">
		            				</div>
		            			</div>
		            			<div class="col-xs-12 col-md-12">
		            				<div class="form-group">
		            					<label >Message</label>
		            					<textarea class="form-control" rows="4" placeholder="Type message"></textarea>
		            				</div>
		            			</div>
		            			<div class="col-xs-12 col-md-12">
		            				<button class="btn btn-primary btn-block">
		            					<span>Send</span>
		            				</button>
		            			</div>
		            		</div>
		            	</form>
		            </div>        			
        		</div>
            <div class="clearfix"></div> 
        	</div>
        	
        		<div class="contact-form-wrapper urgent-help-section with-background">
        			<h2 class="urgent-help-hd">Urgent Help Needed?</h2>
        			<p>Mild to moderate side effects from medication- discuss with our clinician using our telephone advice service or by booking a follow up appointment. If you find it difficult to get an appointment with us soon or if your side effects get worse, please see your own GP.</p>
        			<p><strong>If there are intolerable or severe side effects, seek help immediately by attending A&E</strong></p>
        			<p><strong>NHS 111 - You can call 111 when you need medical help but it’s not a 999 emergency. </strong></p>
        			<p><strong>You or the person assisting you should call 999 in a critical or life-threatening situation such as if someone has</strong></p>
        			<ul>
        				<li>Breathing Difficulty</li>
							<li>Severe Abdominal(tummy) pain</li>
							<li>Severe Dizziness</li>
							<li>Severe Bleeding which can't be stopped</li>
							<li>Severe Chest Pain</li>
							<li>Loss of Consciousness</li>
							<li>Sudden onset of confusion</li>
							<li>Fits</li>
							<li>Suspected Heart Attack or Stroke</li>
        			</ul>
        		</div>
        </div>
    </div>
    <div class="clearfix"></div>
<?php include_once('include/footer.php') ?>
</body>
</html>