<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="register-page login">
    <?php include_once('include/header.php') ?>
<div class="form-right-column section">
  <div class="middle-align">
     <div class="inner">
        <div class="row">
           <div class="col-xs-12 col-md-12">
              <h2 class="section-heading text-center">Log In</h2>
              <p class="text-center login-sub-heading">Log in to access your account</p>
          </div>
          <div class="col-md-12 col-xs-12">
              <section id="wizard">
                 <div id="rootwizard">

                    <div class="tab-content">
                       <div class="tab-pane" id="tab1" style="display: block !important; ">
                          <div class="tab-pane-inner">
                            <div class="login-screen">
                                <div class="row">                       
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <label class="required after-text">Email Address</label>
                                            <input type="email" name="email" class="form-control" placeholder="Enter your email">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-12 password-field">
                                        <div class="form-group">
                                            <label class="required after-text">Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Enter password">
                                        </div>
                                        <a href="javascript:;" class="forgot-password authStepsClick">Forgot Password</a>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                            <div class="checkbox">
                                              <label><input type="checkbox" value="">Remember Me</label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-xs-12 col-md-12">
                                <a href="index.php" class="btn btn-primary">
                                        <span>Log In</span>
                                        <div class="loader"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="forgot-screen" style="display: none;">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                <p>Please enter the email address you used when registering with us. We will send you an email with directions to reset your password.</p>
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="email" name="email" class="form-control" placeholder="Enter your email">
                                    </div>
                                    <a href="javascript:;" class="back-to-login authStepsClick">Back to Login</a>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <a href="javascript:;" class="btn btn-primary">
                                        <span>Send</span>
                                        <div class="loader"></div>
                                    </a>
                                </div>
                            </div>                    
                        </div>  

                        </div>
                    <div class="text-center">
                        <p>Don't have an account? <a href="javascript:;">Register</a></p> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
</div>
</div>
</div>      

                <?php include_once('include/footer.php') ?>
</body>
</html>