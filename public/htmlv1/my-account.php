<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <!-- steps section -->
    <div class="section grey no-pad tab-content-main">
    	<div class="left-sidebar">
    		<ul>

    			<li class="active"><a href="javascript:;" data-content=".sharing-tab-content">Information Sharing <i class="icon-angle-right"></i> </a></li>
    			<li><a  href="javascript:;" data-content=".membership-tab-content">Membership <i class="icon-angle-right"></i></a></li>
    			<li><a href="javascript:;" data-content=".reciept-tab-content">Receipt <i class="icon-angle-right"></i> </a></li><!-- 
    			<li><a href="javascript:;" data-content=".appointments-tab-content">My Records <i class="icon-angle-right"></i> </a></li> -->
    			<li><a href="javascript:;" data-content=".appointments-tab-content">My Appointments <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".message-tab-content">Send Message <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".document-tab-content">Send Document <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".info-tab-content">Edit Personal Details <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".password-tab-content">Change Password <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".hospital-tab-content">Change Hospital  <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".laboratory-tab-content">Change Laboratory <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".blood-service-tab-content">Repeat blood test service <i class="icon-angle-right"></i> </a></li>
    			<li><a href="javascript:;" data-content=".testimonial-tab-content">Write a Testimonial <i class="icon-angle-right"></i> </a></li>
    		</ul>
    	</div>
    	<div class="my-account-right">
    		<div class="content">
    			   <div class="account-tab-content sharing-tab-content active">
    				<h2>Information Sharing</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label>Would you like to notify the hospital regarding your condition?</label>
							<div class="clearfix"></div>
							<div class="radio custom-radio btn-inline-block">
						        <label>
						            <input type="radio" value="" name="radio1" checked="">
						            <span class="radio-circle"></span>
						            Yes</label>
						    </div>
						    <div class="radio custom-radio btn-inline-block">
						        <label>
						            <input type="radio" value="" name="radio1" checked="">
						            <span class="radio-circle"></span>
						            No</label>
						    </div>
						</div>    					
    						<div class="clearfix"></div>
    					</div>
    				</div>
    			</div>
    			<!--membership content-->
    			<div class="account-tab-content membership-tab-content ">
    				<h2>Membership</h2>
    				<p class="dark-text"><strong>Duration:</strong>12 months subscription and then it will recur on a month to month basis </p>
    				<p class="dark-text"><strong>Start Date:</strong> 12/12/2018</p>
    				<p class="dark-text"><strong>Status:</strong> Active</p>
    				<div class="on-off-btn">
						<label class="checkbox-inline">
	  						<input type="checkbox" data-on="Subscribe" data-off="Unsubscribe" data-toggle="toggle" checked="">
						</label>    					
    				</div>
    			</div>
    			<!--Receipts content-->
    			<div class="account-tab-content reciept-tab-content">
    				<h2>Receipts</h2>
    				<div class="table responsive-table">
    					<table class="table">
    						<thead>
    							<tr>
    								<th>ID</th>
    								<th>Description</th>
    								<th>Date</th>
    								<th>Total</th>
    								<th>Status</th>
    							</tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td>1</td>
    								<td>Membership (at $25.00/month)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Paid</td>
    							</tr>
    							<tr>
    								<td>2</td>
    								<td>Repeat blood test service)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Pending</td>
    							</tr>
    							<tr>
    								<td>1</td>
    								<td>Membership (at $25.00/month)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Paid</td>
    							</tr>
    							<tr>
    								<td>2</td>
    								<td>Repeat blood test service)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Pending</td>
    							</tr>
    							<tr>
    								<td>1</td>
    								<td>Membership (at $25.00/month)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Paid</td>
    							</tr>
    							<tr>
    								<td>2</td>
    								<td>Repeat blood test service)</td>
    								<td>29-12-2018 14:48 pm</td>
    								<td>$25</td>
    								<td>Pending</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    			<!--Appointments content-->
   				<div class="account-tab-content appointments-tab-content">
    				<h2>My Appointments</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-3">
    						<label>Filter By Type</label>
    						<select class="form-control">
    							<option>All</option>
    						</select>
    					</div>
    					<div class="col-xs-12 col-md-3">
    						<label>Filter By Doctor</label>
    						<select class="form-control">
    							<option>All</option>
    						</select>
    					</div>
    					<div class="col-xs-12 col-md-4">
    						<label>Filter By Date Range</label>
    						<select class="form-control">
    							<option>All</option>
    						</select>
    					</div>
    					<div class="col-xs-12 col-md-2">
    						<button class="btn btn-primary filter-search">
    							<span>Search</span>
    							<div class="loader"></div>
    						</button>
    					</div>
    				</div>
    				<div class="table responsive-table">
    					<table class="table">
    						<thead>
    							<tr>
    								<th>Date Time</th>
    								<th>Type</th>
    								<th>Doctor</th>
    								<th>Specialty</th>
    								<th>Status</th>
    								<th>Action</th>
    							</tr>
    						</thead>
    						<tbody>
    							<tr>
    								<td>29-12-2018 14:48 pm</td>
    								<td>Standard</td>
    								<td>Desuza</td>
    								<td>Lorem IPsum</td>
    								<td>Pending</td>
    								<td class="action-table-col">
    									<i class="icon-pencil"></i>
    									<i class="icon-remove"></i>
    								</td>
    							</tr>
    							<tr>
    								<td>29-12-2018 14:48 pm</td>
    								<td>Standard</td>
    								<td>Desuza</td>
    								<td>Lorem IPsum</td>
    								<td>Pending</td>
    								<td class="action-table-col">
    									<i class="icon-pencil"></i>
    									<i class="icon-remove"></i>
    								</td>
    							</tr>
    							<tr>
    								<td>29-12-2018 14:48 pm</td>
    								<td>Standard</td>
    								<td>Desuza</td>
    								<td>Lorem IPsum</td>
    								<td>Pending</td>
    								<td class="action-table-col">
    									<i class="icon-pencil"></i>
    									<i class="icon-remove"></i>
    								</td>
    							</tr>
    							<tr>
    								<td>29-12-2018 14:48 pm</td>
    								<td>Standard</td>
    								<td>Desuza</td>
    								<td>Lorem IPsum</td>
    								<td>Pending</td>
    								<td class="action-table-col">
    									<i class="icon-pencil"></i>
    									<i class="icon-remove"></i>
    								</td>
    							</tr>
    							<tr>
    								<td>29-12-2018 14:48 pm</td>
    								<td>Standard</td>
    								<td>Desuza</td>
    								<td>Lorem IPsum</td>
    								<td>Pending</td>
    								<td class="action-table-col">
    									<i class="icon-pencil"></i>
    									<i class="icon-remove"></i>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    			<!--send message content-->
    			<div class="account-tab-content message-tab-content">
    				<h2>Send Message</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    						<div class="form-group">
    							<label>Select Category</label>
    							<select class="form-control">
    								<option>Category 1</option>
    								<option>Category 2</option>
    								<option>Category 3</option>
    								<option>Category 4</option>
    								<option>Category 5</option>
    							</select>
    						</div>
    						<div class="form-group">
    							<label>Subject</label>
    							<input type="text" name="subject" placeholder="Enter subject" class="form-control">
    						</div>
    						<div class="form-group">
    							<label>Message</label>
    							<textarea class="form-control" rows="4" placeholder="Write something"></textarea>
    						</div>
							<ul class="upload-types-btn">
							   <li class="active">
							      <i class="icon-cloud-upload"></i>
							      <p>Upload</p>
							   </li>
							   <li class="">
							      <i class="icon-camera"></i>
							      <p>Take</p>
							   </li>
							   <li class="">
							      <i class="icon-document-scan"></i>
							      <p>Scan </p>
							   </li>
							</ul>
    						<div class="form-group">
    							<label>Upload File</label>
									<div class="upload-photo-block  full style-2 text-center">
                                        <input type="file" name="upload_photo">
                                        <div class="display-profile-picture">
                                          <i class="icon-upload"></i>
                                          <div class="uploaded-photo">     
                                                <button class="btn btn-primary upload-photo-btn btn-block">
                                                    <span>Upload Documents</span>
                                                    <div class="loader"></div>
                                               </button>                                             
                                          </div>
                                       </div>
                                    </div>    							
    						</div>
    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Send</span>
    						</button>
    					</div>
    				</div>
    			</div>
    			<!--send document content-->
    			<div class="account-tab-content document-tab-content">
    				<h2>Send Document</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    						<div class="form-group">
    							<label>Select Category</label>
    							<select class="form-control">
    								<option>Category 1</option>
    								<option>Category 2</option>
    								<option>Category 3</option>
    								<option>Category 4</option>
    								<option>Category 5</option>
    							</select>
    						</div>
    						<div class="form-group">
    							<label>Subject</label>
    							<input type="text" name="subject" placeholder="Enter subject" class="form-control">
    						</div>
    						<div class="form-group">
    							<label>Message</label>
    							<textarea class="form-control" rows="4" placeholder="Write something"></textarea>
    						</div>
							<ul class="upload-types-btn">
							   <li class="active">
							      <i class="icon-cloud-upload"></i>
							      <p>Upload</p>
							   </li>
							   <li class="">
							      <i class="icon-camera"></i>
							      <p>Take</p>
							   </li>
							   <li class="">
							      <i class="icon-document-scan"></i>
							      <p>Scan </p>
							   </li>
							</ul>
    						<div class="form-group">
    							<label>Upload File</label>
									<div class="upload-photo-block  full style-2 text-center">
                                        <input type="file" name="upload_photo">
                                        <div class="display-profile-picture">
                                          <i class="icon-upload"></i>
                                          <div class="uploaded-photo">     
                                                <button class="btn btn-primary upload-photo-btn btn-block">
                                                    <span>Upload Documents</span>
                                                    <div class="loader"></div>
                                               </button>                                             
                                          </div>
                                       </div>
                                    </div>    							
    						</div>
    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Send</span>
    						</button>
    					</div>
    				</div>
    			</div>
    			<!--send document content-->
    			<div class="account-tab-content info-tab-content">
    				<h2>Edit Personel Info</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    					<div class="row">
	    					<div class="col-xs-12 col-md-4">
	    						<div class="form-group">
	    							<label>Mobile</label>
	    							<input type="text" name="subject" placeholder="Enter Mobile" class="form-control">
	    						</div>
	    					</div>    						
    					</div>
    						<div class="form-group">
    							<label>Address Firs Line</label>
    							<input type="text" name="subject" placeholder="Enter address" class="form-control">
    						</div>
    						<div class="row">
    							<div class="col-xs-12 col-md-4">
    								<div class="form-group">
    									<label>Town/City</label>
    									<input type="text" name="town_city" placeholder="Enter town/city" class="form-control">
    								</div>
    							</div>
    							<div class="col-xs-12 col-md-4">
    								<div class="form-group">
    									<label>Postcode</label>
    									<input type="text" name="town_city" placeholder="Enter Postcode" class="form-control">
    								</div>
    							</div>
    							<div class="col-xs-12 col-md-4">
    								<div class="form-group">
    									<label>Country</label>
                                       <select class="form-control">
                                           <option>Select Country</option>
                                            <option value="" selected="selected">Select Country</option> 
                                            <option value="United States">United States</option> 
                                            <option value="United Kingdom">United Kingdom</option> 
                                            <option value="Afghanistan">Afghanistan</option> 
                                            <option value="Albania">Albania</option> 
                                            <option value="Algeria">Algeria</option> 
                                            <option value="American Samoa">American Samoa</option> 
                                            <option value="Andorra">Andorra</option> 
                                            <option value="Angola">Angola</option> 
                                            <option value="Anguilla">Anguilla</option> 
                                            <option value="Antarctica">Antarctica</option> 
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                            <option value="Argentina">Argentina</option> 
                                            <option value="Armenia">Armenia</option> 
                                            <option value="Aruba">Aruba</option> 
                                            <option value="Australia">Australia</option> 
                                            <option value="Austria">Austria</option> 
                                            <option value="Azerbaijan">Azerbaijan</option> 
                                            <option value="Bahamas">Bahamas</option> 
                                            <option value="Bahrain">Bahrain</option> 
                                            <option value="Bangladesh">Bangladesh</option> 
                                            <option value="Barbados">Barbados</option> 
                                            <option value="Belarus">Belarus</option> 
                                            <option value="Belgium">Belgium</option> 
                                            <option value="Belize">Belize</option> 
                                            <option value="Benin">Benin</option> 
                                            <option value="Bermuda">Bermuda</option> 
                                            <option value="Bhutan">Bhutan</option> 
                                            <option value="Bolivia">Bolivia</option> 
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
                                            <option value="Botswana">Botswana</option> 
                                            <option value="Bouvet Island">Bouvet Island</option> 
                                            <option value="Brazil">Brazil</option> 
                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
                                            <option value="Brunei Darussalam">Brunei Darussalam</option> 
                                            <option value="Bulgaria">Bulgaria</option> 
                                            <option value="Burkina Faso">Burkina Faso</option> 
                                            <option value="Burundi">Burundi</option> 
                                            <option value="Cambodia">Cambodia</option> 
                                            <option value="Cameroon">Cameroon</option> 
                                            <option value="Canada">Canada</option> 
                                            <option value="Cape Verde">Cape Verde</option> 
                                            <option value="Cayman Islands">Cayman Islands</option> 
                                            <option value="Central African Republic">Central African Republic</option> 
                                            <option value="Chad">Chad</option> 
                                            <option value="Chile">Chile</option> 
                                            <option value="China">China</option> 
                                            <option value="Christmas Island">Christmas Island</option> 
                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
                                            <option value="Colombia">Colombia</option> 
                                            <option value="Comoros">Comoros</option> 
                                            <option value="Congo">Congo</option> 
                                            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
                                            <option value="Cook Islands">Cook Islands</option> 
                                            <option value="Costa Rica">Costa Rica</option> 
                                            <option value="Cote D'ivoire">Cote D'ivoire</option> 
                                            <option value="Croatia">Croatia</option> 
                                            <option value="Cuba">Cuba</option> 
                                            <option value="Cyprus">Cyprus</option> 
                                            <option value="Czech Republic">Czech Republic</option> 
                                            <option value="Denmark">Denmark</option> 
                                            <option value="Djibouti">Djibouti</option> 
                                            <option value="Dominica">Dominica</option> 
                                            <option value="Dominican Republic">Dominican Republic</option> 
                                            <option value="Ecuador">Ecuador</option> 
                                            <option value="Egypt">Egypt</option> 
                                            <option value="El Salvador">El Salvador</option> 
                                            <option value="Equatorial Guinea">Equatorial Guinea</option> 
                                            <option value="Eritrea">Eritrea</option> 
                                            <option value="Estonia">Estonia</option> 
                                            <option value="Ethiopia">Ethiopia</option> 
                                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
                                            <option value="Faroe Islands">Faroe Islands</option> 
                                            <option value="Fiji">Fiji</option> 
                                            <option value="Finland">Finland</option> 
                                            <option value="France">France</option> 
                                            <option value="French Guiana">French Guiana</option> 
                                            <option value="French Polynesia">French Polynesia</option> 
                                            <option value="French Southern Territories">French Southern Territories</option> 
                                            <option value="Gabon">Gabon</option> 
                                            <option value="Gambia">Gambia</option> 
                                            <option value="Georgia">Georgia</option> 
                                            <option value="Germany">Germany</option> 
                                            <option value="Ghana">Ghana</option> 
                                            <option value="Gibraltar">Gibraltar</option> 
                                            <option value="Greece">Greece</option> 
                                            <option value="Greenland">Greenland</option> 
                                            <option value="Grenada">Grenada</option> 
                                            <option value="Guadeloupe">Guadeloupe</option> 
                                            <option value="Guam">Guam</option> 
                                            <option value="Guatemala">Guatemala</option> 
                                            <option value="Guinea">Guinea</option> 
                                            <option value="Guinea-bissau">Guinea-bissau</option> 
                                            <option value="Guyana">Guyana</option> 
                                            <option value="Haiti">Haiti</option> 
                                            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
                                            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
                                            <option value="Honduras">Honduras</option> 
                                            <option value="Hong Kong">Hong Kong</option> 
                                            <option value="Hungary">Hungary</option> 
                                            <option value="Iceland">Iceland</option> 
                                            <option value="India">India</option> 
                                            <option value="Indonesia">Indonesia</option> 
                                            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
                                            <option value="Iraq">Iraq</option> 
                                            <option value="Ireland">Ireland</option> 
                                            <option value="Israel">Israel</option> 
                                            <option value="Italy">Italy</option> 
                                            <option value="Jamaica">Jamaica</option> 
                                            <option value="Japan">Japan</option> 
                                            <option value="Jordan">Jordan</option> 
                                            <option value="Kazakhstan">Kazakhstan</option> 
                                            <option value="Kenya">Kenya</option> 
                                            <option value="Kiribati">Kiribati</option> 
                                            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
                                            <option value="Korea, Republic of">Korea, Republic of</option> 
                                            <option value="Kuwait">Kuwait</option> 
                                            <option value="Kyrgyzstan">Kyrgyzstan</option> 
                                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
                                            <option value="Latvia">Latvia</option> 
                                            <option value="Lebanon">Lebanon</option> 
                                            <option value="Lesotho">Lesotho</option> 
                                            <option value="Liberia">Liberia</option> 
                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
                                            <option value="Liechtenstein">Liechtenstein</option> 
                                            <option value="Lithuania">Lithuania</option> 
                                            <option value="Luxembourg">Luxembourg</option> 
                                            <option value="Macao">Macao</option> 
                                            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
                                            <option value="Madagascar">Madagascar</option> 
                                            <option value="Malawi">Malawi</option> 
                                            <option value="Malaysia">Malaysia</option> 
                                            <option value="Maldives">Maldives</option> 
                                            <option value="Mali">Mali</option> 
                                            <option value="Malta">Malta</option> 
                                            <option value="Marshall Islands">Marshall Islands</option> 
                                            <option value="Martinique">Martinique</option> 
                                            <option value="Mauritania">Mauritania</option> 
                                            <option value="Mauritius">Mauritius</option> 
                                            <option value="Mayotte">Mayotte</option> 
                                            <option value="Mexico">Mexico</option> 
                                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
                                            <option value="Moldova, Republic of">Moldova, Republic of</option> 
                                            <option value="Monaco">Monaco</option> 
                                            <option value="Mongolia">Mongolia</option> 
                                            <option value="Montserrat">Montserrat</option> 
                                            <option value="Morocco">Morocco</option> 
                                            <option value="Mozambique">Mozambique</option> 
                                            <option value="Myanmar">Myanmar</option> 
                                            <option value="Namibia">Namibia</option> 
                                            <option value="Nauru">Nauru</option> 
                                            <option value="Nepal">Nepal</option> 
                                            <option value="Netherlands">Netherlands</option> 
                                            <option value="Netherlands Antilles">Netherlands Antilles</option> 
                                            <option value="New Caledonia">New Caledonia</option> 
                                            <option value="New Zealand">New Zealand</option> 
                                            <option value="Nicaragua">Nicaragua</option> 
                                            <option value="Niger">Niger</option> 
                                            <option value="Nigeria">Nigeria</option> 
                                            <option value="Niue">Niue</option> 
                                            <option value="Norfolk Island">Norfolk Island</option> 
                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option> 
                                            <option value="Norway">Norway</option> 
                                            <option value="Oman">Oman</option> 
                                            <option value="Pakistan">Pakistan</option> 
                                            <option value="Palau">Palau</option> 
                                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
                                            <option value="Panama">Panama</option> 
                                            <option value="Papua New Guinea">Papua New Guinea</option> 
                                            <option value="Paraguay">Paraguay</option> 
                                            <option value="Peru">Peru</option> 
                                            <option value="Philippines">Philippines</option> 
                                            <option value="Pitcairn">Pitcairn</option> 
                                            <option value="Poland">Poland</option> 
                                            <option value="Portugal">Portugal</option> 
                                            <option value="Puerto Rico">Puerto Rico</option> 
                                            <option value="Qatar">Qatar</option> 
                                            <option value="Reunion">Reunion</option> 
                                            <option value="Romania">Romania</option> 
                                            <option value="Russian Federation">Russian Federation</option> 
                                            <option value="Rwanda">Rwanda</option> 
                                            <option value="Saint Helena">Saint Helena</option> 
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                                            <option value="Saint Lucia">Saint Lucia</option> 
                                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
                                            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
                                            <option value="Samoa">Samoa</option> 
                                            <option value="San Marino">San Marino</option> 
                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
                                            <option value="Saudi Arabia">Saudi Arabia</option> 
                                            <option value="Senegal">Senegal</option> 
                                            <option value="Serbia and Montenegro">Serbia and Montenegro</option> 
                                            <option value="Seychelles">Seychelles</option> 
                                            <option value="Sierra Leone">Sierra Leone</option> 
                                            <option value="Singapore">Singapore</option> 
                                            <option value="Slovakia">Slovakia</option> 
                                            <option value="Slovenia">Slovenia</option> 
                                            <option value="Solomon Islands">Solomon Islands</option> 
                                            <option value="Somalia">Somalia</option> 
                                            <option value="South Africa">South Africa</option> 
                                            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
                                            <option value="Spain">Spain</option> 
                                            <option value="Sri Lanka">Sri Lanka</option> 
                                            <option value="Sudan">Sudan</option> 
                                            <option value="Suriname">Suriname</option> 
                                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
                                            <option value="Swaziland">Swaziland</option> 
                                            <option value="Sweden">Sweden</option> 
                                            <option value="Switzerland">Switzerland</option> 
                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option> 
                                            <option value="Taiwan, Province of China">Taiwan, Province of China</option> 
                                            <option value="Tajikistan">Tajikistan</option> 
                                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
                                            <option value="Thailand">Thailand</option> 
                                            <option value="Timor-leste">Timor-leste</option> 
                                            <option value="Togo">Togo</option> 
                                            <option value="Tokelau">Tokelau</option> 
                                            <option value="Tonga">Tonga</option> 
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option> 
                                            <option value="Tunisia">Tunisia</option> 
                                            <option value="Turkey">Turkey</option> 
                                            <option value="Turkmenistan">Turkmenistan</option> 
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
                                            <option value="Tuvalu">Tuvalu</option> 
                                            <option value="Uganda">Uganda</option> 
                                            <option value="Ukraine">Ukraine</option> 
                                            <option value="United Arab Emirates">United Arab Emirates</option> 
                                            <option selected="" value="United Kingdom">United Kingdom</option> 
                                            <option value="United States">United States</option> 
                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
                                            <option value="Uruguay">Uruguay</option> 
                                            <option value="Uzbekistan">Uzbekistan</option> 
                                            <option value="Vanuatu">Vanuatu</option> 
                                            <option value="Venezuela">Venezuela</option> 
                                            <option value="Viet Nam">Viet Nam</option> 
                                            <option value="Virgin Islands, British">Virgin Islands, British</option> 
                                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
                                            <option value="Wallis and Futuna">Wallis and Futuna</option> 
                                            <option value="Western Sahara">Western Sahara</option> 
                                            <option value="Yemen">Yemen</option> 
                                            <option value="Zambia">Zambia</option> 
                                            <option value="Zimbabwe">Zimbabwe</option>
                                       </select>
    								</div>
    							</div>
    						</div>
    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Update</span>
    						</button>
    					</div>
    				</div>
    			</div>
    			<!--Change password-->
    			<div class="account-tab-content password-tab-content">
    				<h2>Change Password</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    						<div class="form-group">
    							<label>Old Password</label>
								<input type="text" name="subject" placeholder="Enter old password" class="form-control">
    						</div>
    						<div class="form-group">
    							<label>New Password</label>
								<input type="text" name="subject" placeholder="Enter new password" class="form-control">
    						</div>
    						<div class="form-group">
    							<label>Confirm New Password</label>
								<input type="text" name="subject" placeholder="Re-Enter new password" class="form-control">
    						</div>
    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Update</span>
    						</button>
    					</div>
    				</div>
    			</div>

    			<!--Change Hospital-->
    			<div class="account-tab-content hospital-tab-content">
    				<h2>Change Hospital</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    						<div class="row">
							    <div class="col-xs-12 col-md-6">
							        <div class="form-group">
							            <label class="">Search by Hospital Name</label>
							            <input type="text" name="search_hospital" class="form-control" placeholder="Enter hospital name">
							        </div>
							    </div>
							    <div class="col-xs-12 col-md-6">
							        <div class="form-group">
							            <label class="">Search by Town/City or Postcode</label>
							            <input type="search_hospital" name="searchhospital" class="form-control" placeholder="Search by town/city or postcode">                                                   
							        </div>
							    </div>    
							    <div class="col-xs-12 col-md-2">
							        <button class="btn btn-primary btn-md"><span>Search</span></button>
							    </div>	
							    <div class="col-xs-12 col-md-12">
							    	<hr class="md-space"> 
							    </div>						      							
							</div>
    					</div>
    				</div>

    				<div class="row">
    					<div class="col-xs-12 col-md-6">
					        <div class="form-group">
					            <label class="">Your existing hospital address is</label>
					            <input type="text" name="search_hospital" class="form-control" value="2nd Floor Maple House 149 Tottenham Court Road London W1T 7BN" placeholder="Enter hospital address">                                      
					        </div>

    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Update</span>
    						</button>
    					</div>
    				</div>
    			</div>
    			<!--Change laboratory -->
    			<div class="account-tab-content laboratory-tab-content">
    				<h2>Change laboratory </h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
							<div class="row">
							    <div class="col-xs-12 col-md-10">
							        <input type="search_hospital" name="searchhospital" class="form-control" placeholder="Enter your postcode">                                                   
							    </div>
							    <div class="col-xs-12 col-md-2">
							        <button class="btn btn-primary btn-md"><span>Search</span></button>
							    </div>
							</div>   
							<hr class="md-space"> 						
    					</div>
    				</div>

    				<div class="row">
    					<div class="col-xs-12 col-md-6">
					        <div class="form-group">
					            <label class="">Your existing lab address is</label>
					            <input type="text" name="search_hospital" class="form-control" value="The York Hospital, Wigginton Road, York, North Yorkshire, YO31 8HE" placeholder="Enter hospital address">                                            
					        </div>

    						<div class="clearfix"></div>
    						<button class="btn btn-primary btn-md">
    							<span>Update</span>
    						</button>
    					</div>
    				</div>
    			</div>
    			<!--Write testimonial -->
    			<div class="account-tab-content testimonial-tab-content">
    				<h2>Testimonial</h2>
    				<div class="row">
    					<div class="col-xs-12 col-md-6">
    						<div class="form-group">
    							<label class="">Write Testimonial</label>
    							<textarea class="form-control" placeholder="Write Testimonial" rows="4"></textarea>                             
    						</div>

    						<div class="form-group">
    							<div class="checkbox">
    								<label>
    									<input type="checkbox" value="">I Agree to 
    									<a href="javascript:;">
    										terms and conditions
    									</a></label><a href="javascript:;">
    								</a></div><a href="javascript:;">
    							</a></div>
    						<div class="clearfix"></div>
			    				<div class="row">
			    					<div class="col-xs-12 col-md-6">
										  <!-- Rating Stars Box -->
										  <div class='rating-stars text-center'>
										    <ul id='stars'>
										      <li class='star' title='Poor' data-value='1'>
										        <i class='icon-star'></i>
										      </li>
										      <li class='star' title='Fair' data-value='2'>
										        <i class='icon-star'></i>
										      </li>
										      <li class='star' title='Good' data-value='3'>
										        <i class='icon-star'></i>
										      </li>
										      <li class='star' title='Excellent' data-value='4'>
										        <i class='icon-star'></i>
										      </li>
										      <li class='star' title='WOW!!!' data-value='5'>
										        <i class='icon-star'></i>
										      </li>
										    </ul>
										  </div>    						
			    					</div>
			    				</div>
    						<button class="btn btn-primary btn-md">
    							<span>Send</span>
    						</button>
    					</div>
    				</div>

    			</div>
    			<!--Write testimonial -->
    			<div class="account-tab-content blood-service-tab-content">
    				<h2>Repeat Blood Test Service</h2>
						<div class="row">
	    					<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<label class="with-sub-text">Activated</label>
								<div class="clearfix"></div>
								<div class="radio custom-radio btn-inline-block">
							        <label>
							            <input type="radio" value="" name="radio1" checked="">
							            <span class="radio-circle"></span>
							            Yes</label>
							    </div>
							    <div class="radio custom-radio btn-inline-block">
							        <label>
							            <input type="radio" value="" name="radio1" checked="">
							            <span class="radio-circle"></span>
							            No</label>
							    </div>
							</div>    					
	    					<p class="info-text"><i class="icon-info-circle"></i>To make changes to your repeat blood test service please discuss with your doctor during the next appointment</p>
	    					<div class="clearfix"></div>
	    					</div>
	    				</div>
    			</div>


    		</div>

			<?php include_once('include/footer.php') ?>
    	</div>
    	<div class="clearfix"></div>
    </div>

</body>
</html>