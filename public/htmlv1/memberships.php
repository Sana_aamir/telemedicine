<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner for-doctor-banner membership-banner">
        <ul>
            <li>
                <img src="images/banner/membership-banner.jpg">
                <div class="banner-content">
                    <div class="middle-align">
                        <div class="inner">
                            <div class="price-list">
                                  <ul class="list-unstyled">
                                    <li>Huge savings on appointment costs </li>
                                    <li>Free Repeat Blood Test service</li>
                                    <li>Free Fit Notes and much more</li>
                                </ul>
                                <a href="javascript:;" class="btn btn-primary btn-md"> Get Membership </a>
                            </div>                        
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    
    <div class="section grey no-pad" style="display: none;">
        <div class="alternat-columns">
            <div class="cols col-left">
                <div class="middle-align">
                        <div class="inner">
                            <div class="content">
<!--                                 <h2 class="section-heading ">Membership</h2>
                                <div class="price-value">
                                </div> -->
                                <div class="price-list">
                                  <ul class="list-unstyled">
                                    <li>Huge savings on appointment costs </li>
                                    <li>Free Repeat Blood Test service</li>
                                    <li>Free Fit Notes and much more</li>
                                </ul>
                                <a href="javascript:;" class="btn btn-primary btn-md"> Get Membership </a>
                            </div>                            
                            </div>                                      
                        </div>
                </div>    
            </div>
            <div class="cols col-right">
                <div class="middle-align">
                    <div class="inner">
                        <div class="content">
                            <div class="inner">
<!--                                 <h2>The most convenient way to see a doctor</h2>
                                <p>Pate velit essle duis irue dolor repa enderitn dolor fugiat pariaturs Excepeur sintusa ocae cupdat proident suntin culpa officia deserunt laborums perspiciat aute. Lorem ipsum dolor sit amet, consectetur elit sed dore eiusmod tempor incididunt.</p> -->
                                <div class="price-list">
                                  <ul class="list-unstyled">
                                    <li>Many benefits for online consultation</li>
                                    <li>No need to travel</li>
                                    <li>No need to spend on travelling and parking</li>
                                    <li>No need to take time off work.</li>
                                </ul>
                            </div>  
                            </div>
                        </div>                            
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="section" >
        <div class="row">
            <div class="col-xs-12 col-md-12">
            <div class="max-wrapper">
            <h2 class="section-heading text-center">Service We <span>Provide</span></h2>
                <div class="table-responsive">
                    <table class="table table-bordered what-we-offer-table">
                    <thead>
                          <tr>
                            <th scope="col" rowspan="2" class="table-service-col row-middle-align" width="100">Services</th>
                            <th scope="col" colspan="2" class="table-fees-col text-center">Fees</th>
                            <th scope="col" rowspan="2" class="table-description-col row-middle-align" width="350">Description</th>
                            <th scope="col" rowspan="2" class="table-prescription-col row-middle-align" width="200">Prescription Issue</th>
                        </tr>
                        <tr>
                            <th scope="row" width="120" class="text-center">Member</th>
                            <th width="120" class="text-center">None Member</th>
                        </tr>                    
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>Initial <br> Assessment</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
                            <td>Lorem Ipsum</td>
                        </tr>
                        <tr>
                            <td><strong>Follow up <br>Assessment- 1</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </td>
                            <td>Lorem Ipsum</td>
                        </tr>
                        <tr>
                            <td><strong>Follow up <br>Assessment- 2</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis venenatis </td>
                            <td>Lorem Ipsum</td>
                        </tr>
                        <tr>
                            <td><strong>Telephone  <br>Adivce 3</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer </td>
                            <td>Lorem Ipsum</td>
                        </tr>
                        <tr>
                            <td><strong>Repeat blood  <br>test service</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer.</td>
                            <td>Lorem Ipsum</td>
                        </tr>
                        <tr>
                            <td><strong>Fit Notes</strong></td>
                            <td class="text-center"><strong>$10</strong></td>
                            <td class="text-center"><strong>$20</strong></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer.</td>
                            <td>Lorem Ipsum</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-center btn-group-membership">
                <a href="javascript:;" class="btn btn-primary btn-md">Subscribe Now</a>
                <span>OR</span>
                <a href="javascript:;" class="btn btn-warning btn-md">Book Standard Appointment</a>
            </div>
                </div>                
            </div>
        </div>
    </div>
<!--     <div class="section">
        <div class="max-wrapper">
            <h2>Terms and Conditions</h2>
            <p>Cras diam mi, pharetra sit amet lorem eu, consequat accumsan lacus. Vestibulum vitae erat sed dui ultrices placerat eget vel neque. Etiam porta ligula odio, at vehicula ipsum auctor sed. Cras erat quam, sodales sed dolor non, fringilla sagittis sapien. Donec dapibus sapien quam, vel placerat ante porta nec. Praesent ac mattis risus, at lacinia velit. Nunc eu quam pellentesque, malesuada lectus id, mollis justo. Phasellus eget sodales nunc, vehicula iaculis felis. Curabitur condimentum, erat tempor porta tempus, dolor magna venenatis felis, at bibendum metus eros vitae mauris. Morbi dapibus gravida consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam lobortis est quis risus imperdiet lacinia.</p>
            <div class="checkbox">
              <label><input type="checkbox" value="">I Agree to <a href="javascript:;">terms and conditions</a></label><a href="javascript:;">
            </a></div>
            
        </div>  
    </div> -->
<?php include_once('include/footer.php') ?>
</body>
</html>