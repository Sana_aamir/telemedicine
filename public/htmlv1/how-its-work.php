<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner inner-banner about-banner">
        <div class="middle-align">
          <div class="inner">
            <div class="max-wrapper">
                <h1 class="inner-banner-heading">How It Works
                    <h1>
                    </div>              
                </div>
            </div>
        </div>
        <!-- steps section -->
        <div class="section">
            <div class="max-wrapper">
               <div class="appointment-panel howItsWorkLeft">
                   <h2>What do you need to know?</h2>
                   <a name="chapter0"></a>
                   <a href="#chapter1"><p><i class="icon-check"></i> How it works</p></a>
                   <a href="#chapter2"><p><i class="icon-check"></i> How can a doctor treat me online?</p></a>
                   <a href="#chapter3"><p><i class="icon-check"></i> How do I have my appointment?</p></a>
                   <a href="#chapter4"><p><i class="icon-check"></i> How do I get a prescription?</p></a>
                   <a href="#chapter5"><p><i class="icon-check"></i> Who are your doctors?</p></a>
                   <a href="#chapter6"><p><i class="icon-check"></i> How confidential and secure is the service?</p></a>
                   <a href="#chapter7"><p><i class="icon-check"></i> How do I get referral letters and fit notes?</p></a>
                   <a href="#chapter8"><p><i class="icon-check"></i> What do you treat?</p></a>
                   <a href="javascript:;" class="btn btn-primary btn-block btn-md">Book Appointment Online</a>
               </div>  
               <div class="howItsWorkRight">
                   <div id="MVTContentBlock">
                      <h2>It's simple. See a Pshychiatrist online in minutes using your smartphone, tablet or laptop</h2>
                      <div class="section steps-section how-its-work-steps">
                        <ul>
                            <li>
                                <span><i class=""><img src="images/icons/book-appointment-w.png"></i> Book Appointment</span>
                                <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p> -->
                            </li>
                            <li>
                                <span><i><img src="images/icons/doctor-online-w.png"></i> See PSYCHIATRIST online</span>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p> -->
                            </li>
                            <li>
                                <span><i><img src="images/icons/recovery-w.png"></i> Get treatment</span>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem voluptatem obcaecati!</p> -->
                            </li>
                        </ul>   
                    </div>
                    <div class="row how-its-work-content">
                    <div class="col-md-12">
                        <div id="section-one" class="mb-50">
                          <h3>What do you mean by item and end product?</h3>
                          <hr>
                          <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis, accusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium a!</p>
                          <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Ut tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit at lacus blandit, commodo iaculis justo viverra. Etiam vitae est arcu. Mauris vel congue dolor. Aliquam eget mi mi. Fusce quam tortor, commodo ac dui quis, bibendum viverra erat. Maecenas mattis lectus enim, quis tincidunt dui molestie euismod. Curabitur et diam tristique, accumsan nunc eu, hendrerit tellus.</p>
                      </div>
                      <div id="section-two" class="mb-50">
                          <h3>What are some examples of permitted end products?</h3>
                          <hr>
                          <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis, accusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium a!</p>
                          <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Ut tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit at lacus blandit, commodo iaculis justo viverra. Etiam vitae est arcu. Mauris vel congue dolor. Aliquam eget mi mi. Fusce quam tortor, commodo ac dui quis, bibendum viverra erat. Maecenas mattis lectus enim, quis tincidunt dui molestie euismod. Curabitur et diam tristique, accumsan nunc eu, hendrerit tellus.</p>
                      </div>
                      <div id="section-three" class="mb-50">
                          <h3>Am I allowed to modify the item that I purchased?</h3>
                          <hr>
                          <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis, accusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium a!</p>
                          <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Ut tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit at lacus blandit, commodo iaculis justo viverra. Etiam vitae est arcu. Mauris vel congue dolor. Aliquam eget mi mi. Fusce quam tortor, commodo ac dui quis, bibendum viverra erat. Maecenas mattis lectus enim, quis tincidunt dui molestie euismod. Curabitur et diam tristique, accumsan nunc eu, hendrerit tellus.</p>
                      </div>
                      <div id="section-four" class="mb-50">
                          <h3>What does non-exclusive mean?</h3>
                          <hr>
                          <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis, accusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium a!</p>
                          <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Ut tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit at lacus blandit, commodo iaculis justo viverra. Etiam vitae est arcu. Mauris vel congue dolor. Aliquam eget mi mi. Fusce quam tortor, commodo ac dui quis, bibendum viverra erat. Maecenas mattis lectus enim, quis tincidunt dui molestie euismod. Curabitur et diam tristique, accumsan nunc eu, hendrerit tellus.</p>
                      </div>
                      <div id="section-five" class="mb-50">
                          <h3>What is a single application?</h3>
                          <hr>
                          <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis, accusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium a!</p>
                          <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Ut tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit at lacus blandit, commodo iaculis justo viverra. Etiam vitae est arcu. Mauris vel congue dolor. Aliquam eget mi mi. Fusce quam tortor, commodo ac dui quis, bibendum viverra erat. Maecenas mattis lectus enim, quis tincidunt dui molestie euismod. Curabitur et diam tristique, accumsan nunc eu, hendrerit tellus.</p>
                      </div>
                  </div>
                 </div>                  
              </div>

          </div>
          <div class="clearfix"></div>   
      </div>
  </div>
<!-- 
  <div class="section grey">
      <div class="max-wrapper">
        <div class="features-wrapper">
            <div class="features-col-lt">
                <img src="images/dummy/doctor-mobile.png" alt="iphone">
            </div>
            <div class="features-col-rt">
              <ul>
                <li>
                    <h4>Great Design</h4>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas omnis officiis veniam non laborum reprehenderit quae ratione blanditiis delectus facilis facere. </p>
                </li>
                <li>
                    <h4>Responsive Layout</h4>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas omnis officiis veniam non laborum reprehenderit quae ratione blanditiis delectus facilis facere. </p>
                </li>
                <li>
                    <h4>SEO Ready</h4>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit quas omnis officiis veniam non laborum reprehenderit quae ratione blanditiis delectus facilis facere. </p>
                </li>
            </ul>                
            </div>
        </div>  
</div>
</div> -->

<?php include_once('include/footer.php') ?>
</body>
</html>