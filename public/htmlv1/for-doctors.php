<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page for-doctors-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner for-doctor-banner">
        <ul>
            <li>
                <img src="images/banner/doctors.jpg">
                <div class="g-overlay"></div>
                <div class="banner-content">
                    <div class="middle-align">
                        <div class="inner">
                            <div class="max-wrapper">
                                <div class="th-slidercontent">
                                    <h1> Join <strong class="brand-name">Happy Clinic</strong> and  <span>you'll enjoy!</span></h1>
                                    <div class="th-description">
                                        <p>Would you like the opportunity to earn in a flexible, safe and convenient way? Are you looking for a way to extend your private consulting <strong>  experience & increase your digital healthcare knowledge?</strong></p>
                                    </div>
                                    <a href="book-appoinment.php" class="btn btn-md btn-primary book-appointment-btn">
                                        <span>Apply Now</span>
                                    </a>
                                </div>
                            </div>                          
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <!-- steps section -->
    <div class="section section-1 text-center">
        <div class="max-wrapper">
            <h2 class="section-heading text-center ">Be the future of <span>healthcare</span></h2>
            <p>Happy Clinic is an online GP service that allows you to connect face-to-face with patients via video consultation. You'll help people get the medical advice they need from a location of your choice, at a time that suits you. If you’re a registered UK GP who's on the NHS Performers List, we'd love you to join our network.</p>
        </div>
    </div>
    <div class="section grey doctor-facilities-section">
        <div class="max-wrapper">
            <div class="card testimonial-card">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="img"><img src="images/for-doctors.jpg"></div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="content">
                            <h2>Go Beyond Limits </h2>
                            <p>Duis aute irure dolor reprehenderit in voluptate velit essl cillum dolore eud fugiat nulla pariatur. Excepteur sint ocaec atus cupdatat proident suntin culpa qui officia deserunt mol anim.</p>
                            <div class="price-list">
                              <ul class="list-unstyled">
                                <li>Acepteur sintas haecate sed ipsums cupidates bondus</li>
                                <li>Proident sunlt culpa qui tempore officia sed ipsum tempor</li>
                                <li>Cupidatat non proident sunt in culpa qui officia deserunt</li>
                                <li>Mollit anim id est laborum ged ut perspiciatis unde</li>
                                <li>Proident sunlt culpa qui tempore officia</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="section  doctors-section">
    <div class="max-wrapper">
    <h2 class="section-heading text-center">Some of Our <span>Doctors</span></h2>
        <p class="section-heading-detail center-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem
            voluptatem obcaecati!</p>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="team-member style-2 text-center">
                      <div class="team-images">
                        <img class="img-fluid" src="images/dummy/1.jpg" alt="">
                    </div>
                    <div class="team-description"> 
                        <h5 class="mb-2">Dr. John Maxwell</h5> 
                        <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 md-mt-5">
                <div class="team-member style-2 text-center">
                  <div class="team-images">
                    <img class="img-fluid" src="images/dummy/2.jpg" alt="">
                </div>
                <div class="team-description"> 
                    <h5 class="mb-2">Dr. Matthew Doe</h5> 
                    <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 md-mt-5">
            <div class="team-member style-2 text-center">
              <div class="team-images">
                <img class="img-fluid" src="images/dummy/3.jpg" alt="">
            </div>
            <div class="team-description"> 
                <h5 class="mb-2">Dr. Romi Keely</h5> 
                <p>Cras ultricies ligula sed magna dictum porta, iste natus error sit voluptat</p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="section grey text-center join-clinic-section">
    <div class="max-wrapper">
        <h2 class="section-heading text-center">Join <span> Happy Clinic</span></h2>
        <p class="section-heading-detail center-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem
            voluptatem obcaecati.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem
            voluptatem obcaecati!</p>
            <a href="javascript:;" class="btn btn-primary btn-md">
                Apply Now
            </a>        
    </div>
</div>
<?php include_once('include/footer.php') ?>
</body>
</html>