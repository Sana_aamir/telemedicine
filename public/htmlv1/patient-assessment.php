<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page patient-assesment-page">
    <div class="header">
        <div class="fixed-header">
            <a href="javascript:;" class="go-back-link">Go Back</a>
            <h2>Patient Assessment</h2>
        </div>
    </div>
    <div class="assesment-left">
        <div class="scrollbar" id="style-4">
            <div class="force-overflow">
            <div class="assesment-profile-photo">
                <img src="images/dummy/profile-photo.jpg">
                <h2 class="patient-name">Alena Johnson</h2>
            </div>
            <div class="assesment-profile-info">
                <div class="row">
                    <div class="col-xs-12 col-md-5"><p><span>DOB:</span> 18 Aug 1986</p></div>
                    <div class="col-xs-12 col-md-3"><p><span>Age:</span> 32</p></div>
                    <div class="col-xs-12 col-md-4"><p><span>Gender:</span> Male</p></div>
                </div>
                <p><span>Address:</span> 853 Queensway walsall WS13 5NR</p>
                <p><span>Mobile:</span> 4452685693</p>
                <p><span>Email:</span> alenajohnson@xyz.com</p>
                <div class="row">
                    <div class="col-xs-12 col-md-4"><p><span>Member:</span> Yes</p></div>
                    <div class="col-xs-12 col-md-8"><p><span>Start Date:</span> 12/12/2018</p></div>
                </div>
                <p><span>Hospital Address:</span> 9 Victoria Road paisley PA61 5IE </p>
                <p><span>Lab Address:</span> 9 Victoria Road paisley PA61 5IE</p>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="video-calling-box">
                <img src="images/dummy/video-call.jpg">
                <div class="doctor-camera-view">
                    <img src="images/dummy/profile-photo.jpg">
                </div>
            </div>
            <ul class="schedule-table">
                <li class="text-center"><strong>Today's Date:</strong> 1/06/2019</li>
                <li class="text-center"><strong>Appointment Date:</strong> 1/06/2019</li>
                <li class="text-center"><strong>Currernt Time</strong> 6:04 AM</li>
                <li class="text-center"><strong>Appointment Time:</strong> 6:15 AM</li>
                <li class="text-center"><strong>Initial Assesments</strong></li>
            </ul>
            <hr>
            <div class="tab-content-main clearfix">
            <div class="left-sidebar">
                <ul class="patient-tabs-side">
                    <li class="item active">
                        <a href="javascript:;" data-content=".hospital-records-tab-content">Hospital Records </a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".lab-records-tab-content">Lab Records</a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".message-tab-content">Messages</a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".notes-tab-content">Notes</a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".attachment-tab-content">Attachments</a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".assessment-tab-content">Assessments</a>
                    </li>
                    <li class="item">
                        <a href="javascript:;" data-content=".psychatric-tab-content">Psychatric Records</a>
                    </li>
                </ul>
            </div>
                <div class="account-tab-content hospital-records-tab-content active">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                 <div class="account-tab-content lab-records-tab-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-tab-content message-tab-content">
                                       <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-tab-content notes-tab-content">
                                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-tab-content attachment-tab-content">
                                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-tab-content assessment-tab-content">
                                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-tab-content psychatric-tab-content">
                                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Document Name</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                                <tr>
                                    <td>12/12/2018</td>
                                    <td>abc.pdf</td>
                                    <td><a href="javascript;;">View</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="assesment-center">
        <div class="scrollbar" id="style-4">
            <div class="force-overflow">
                <div class="tab-content-main">
                    <div class="left-sidebar">
                        <ul class="patient-tabs-rt dark-tab">
                            <li class="item">
                                <a href="javascript:;" data-content=".send-msg-tab-content">Photo ID</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".send-msg-tab-content">Send Message</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".add-notes-tab-content">Add Notes</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".email-tab-content">Email</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".appointments-tab-content">Appointments</a>
                            </li>
                        </ul>
                    </div>            
<!--                      <div class="account-tab-content send-msg-tab-content active">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div>
                     <div class="account-tab-content add-notes-tab-content ">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div>
                     <div class="account-tab-content email-tab-content ">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div>
                     <div class="account-tab-content appointments-tab-content ">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div> -->
                    </div>
                    <div class="clearfix"></div>
                <div class="tab-content-main">
                    <div class="left-sidebar">
                        <ul class="patient-tabs-rt-2">
                            <li class="item active">
                                <a href="javascript:;" data-content=".clinical-tab-content">Clinical</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".lab-form-tab-content">Lab Form</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 1</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 2</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 3</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 4</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 5</a>
                            </li>
                            <li class="item">
                                <a href="javascript:;" data-content=".medical-tab-content">Medical 6</a>
                            </li>
                        </ul>
                    </div>                      
                     <div class="account-tab-content clinical-tab-content active">
<!--                         <p class="text-center no-records-found-text">No Records Found</p> -->
                    <ol class="steps-blts question-blts">
                        <li>
                            <p><strong>Please tell us about your problems or what you need help with us?</strong></p>
                            <p>faucibus nisi a justo sagittis, et viverra nisi interdum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras sit amet tortor quis urna sollicitudin blandit id finibus nunc. </p>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id sem leo. </p>
                                    <div class="form-group patient-question-row">
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1" checked="">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">Yes</p>
                                            </label>
                                        </div>
                                        <div class="radio custom-radio">
                                            <label><input type="radio" value="" name="qRadio1">
                                                <span class="radio-circle"></span>
                                                <p class="label-text">No</p>
                                            </label>
                                        </div>
                                        <input type="text" name="" class="form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis pharetra justo at porttitor. Aliquam scelerisque nisi a ultrices laoreet. Integer scelerisque vel neque eu tempus.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p><label>Doctor Final Comments</label></p>
                            <div class="form-group">
                                <textarea class="form-control" rows="4" placeholder="Start Typing"></textarea>
                            </div>                            
                        </li>
                    </ol>                    
                    <div class="form-group">
                        <label class="">Consent for sharing information with hospital?</label>
                        <div class="clearfix"></div>
                            <div class="radio custom-radio btn-inline-block ">
                                <label>
                                    <input type="radio" value="" name="radio1" checked="">
                                    <span class="radio-circle"></span>
                                    Yes
                                </label>
                            </div>
                            <div class="radio custom-radio btn-inline-block ">
                                <label>
                                    <input type="radio" value="" name="radio1">
                                    <span class="radio-circle"></span>No
                                </label>
                            </div>
                    </div>
                    </div>
                     <div class="account-tab-content lab-form-tab-content ">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div>
                     <div class="account-tab-content medical-tab-content ">
                        <p class="text-center no-records-found-text">No Records Found</p>
                    </div>
                </div>   
                    <div class="clearfix"></div>
                      
            </div>
        </div>
    </div><!-- 
    <div class="assesment-right">

    </div> -->
    <div class="clearfix"></div>
</body>
</html>