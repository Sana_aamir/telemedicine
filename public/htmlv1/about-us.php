<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner inner-banner about-banner">
        <div class="middle-align">
          <div class="inner">
            <div class="max-wrapper">
                <h1 class="inner-banner-heading">About Us
                </h1>
            </div>              
          </div>
        </div>
    </div>
    <!-- steps section -->
    <div class="section">
        <div class="max-wrapper">
            <div class="about-left">
                <div class="video-holder">
                    <div class="inner">
                        <img src="images/dummy/doctor.png">
                        <span class="overlay"></span>
                        <span class="c-play-icon">
                            <i class="icon-play"></i>
                        </span>
                    </div>
                </div>  
            </div>
            <div class="about-right">
                <h2 class="inner-section-heading">The online doctor consultation service. </h2>
                <p>Our mission is to take the strain away from physical waiting rooms and provide patients with access to UK GP’s at a time and a place that suits their busy lifestyle. Proin auctor euismod erat, id lobortis tortor ultrices in. Mauris malesuada elit nec magna ultrices, a imperdiet sapien lobortis. rutrum id bibendum sed, dapibus nec nisl. Sed ut libero tempor, convallis sapien sed, commodo elit. Vestibulum eleifend tincidunt est sed ultrices. In ligula dui, molestie at cursus quis, suscipit sed ligula. Ut mollis eget tortor sit amet mollis.</p>
                <ul class="page-blts-points">
                    <li><i class="icon-check-circle"></i> Great Technology</li>
                    <li><i class="icon-check-circle"></i> Best Branding</li>
                    <li><i class="icon-check-circle"></i> Delivery On Time</li>
                    <li><i class="icon-check-circle"></i> We are Expert</li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin auctor euismod erat, id lobortis tortor ultrices in. Mauris malesuada elit nec magna ultrices, a imperdiet sapien lobortis. Sed libero metus, aliquam nec lacus vel, rutrum gravida libero. Nunc neque arcu, rutrum id bibendum sed, dapibus nec nisl. Sed ut libero tempor, convallis sapien sed, commodo elit. Vestibulum eleifend tincidunt est sed ultrices. In ligula dui, molestie at cursus quis, suscipit sed ligula. Ut mollis eget tortor sit amet mollis. Vestibulum sit amet elementum lectus, eu pharetra quam. In iaculis justo ligula, eu pulvinar enim porta ut. In dignissim consectetur enim id scelerisque. Pellentesque ultrices fermentum pulvinar. Ut non dui ac purus laoreet lobortis. Sed iaculis congue tempus. In hac habitasse platea dictumst.</p>
                <p>Proin posuere nibh ac rutrum vestibulum. Nulla ligula sem, blandit a scelerisque quis, mollis ac nisi. Etiam id ante et tortor aliquam commodo. Praesent eu nulla ut urna iaculis aliquam in sit amet nisi. Proin mattis tellus risus, sed convallis mi egestas sit amet. Vivamus vitae turpis nec sem auctor bibendum. Vivamus non tellus fermentum, pretium arcu vel, sodales leo.</p>
                

            </div>
            <div class="clearfix"></div>  

        </div>
    </div>
 <!--     <div class="section  about-center-quote  text-center">
         <div class="max-wrapper">
            We're here to help people live happier, longer lives
         </div>
     </div> 
     
    <div class="section switch-inner-cols">
        <div class="max-wrapper">
            <div class="about-left">
                <div class="mobile-holder">
                    <div class="inner">
                        <img src="images/dummy/doctor.png">
                    </div>
                </div>  
            </div>
            <div class="about-right">
                <h2 class="inner-section-heading">WHY CHOOSE US</h2>
                <p>Our mission is to take the strain away from physical waiting rooms and provide patients with access to UK GP’s at a time and a place that suits their busy lifestyle. Proin auctor euismod erat, id lobortis tortor ultrices in. Mauris malesuada elit nec magna ultrices, a imperdiet sapien lobortis. rutrum id bibendum sed, dapibus nec nisl. Sed ut libero tempor, convallis sapien sed, commodo elit. Vestibulum eleifend tincidunt est sed ultrices. In ligula dui, molestie at cursus quis, suscipit sed ligula. Ut mollis eget tortor sit amet mollis.</p>
                <ul class="page-blts-points">
                    <li><i class="icon-check-circle"></i> Great Technology</li>
                    <li><i class="icon-check-circle"></i> Best Branding</li>
                    <li><i class="icon-check-circle"></i> Delivery On Time</li>
                    <li><i class="icon-check-circle"></i> We are Expert</li>
                    <li><i class="icon-check-circle"></i> Delivery On Time</li>
                    <li><i class="icon-check-circle"></i> We are Expert</li>
                </ul>
                <p>Proin posuere nibh ac rutrum vestibulum. Nulla ligula sem, blandit a scelerisque quis, mollis ac nisi. Etiam id ante et tortor aliquam commodo. Praesent eu nulla ut urna iaculis aliquam in sit amet nisi. Proin mattis tellus risus, sed convallis mi egestas sit amet. Vivamus vitae turpis nec sem auctor bibendum. Vivamus non tellus fermentum, pretium arcu vel, sodales leo.</p>
                

            </div>
            <div class="clearfix"></div>  

        </div>
    </div> -->

<?php include_once('include/footer.php') ?>
</body>
</html>