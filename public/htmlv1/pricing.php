<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <!-- steps section -->
    <div class="section grey">
        <div class="max-wrapper">
            <h2 class="our-pricing-heading">OUR <span>PRICING</span></h2>
            <h3 class="our-pricing-sub-heading">Choose a suitable plan</h3>


            <div class="clearfix"></div>  
        </div>
    </div>


<?php include_once('include/footer.php') ?>
</body>
</html>