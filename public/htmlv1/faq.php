<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page faq-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner inner-banner faq-banner">
        <div class="middle-align">
          <div class="inner">
            <div class="max-wrapper">
                <h1 class="inner-banner-heading">Faq
                <h1>
            </div>              
          </div>
        </div>
    </div>
    <!-- steps section -->
    <div class="section">
        <div class="max-wrapper">
         <h2 class="section-heading text-center">Need Some  <span>Help?</span></h2>
            <p class="section-heading-detail center-heading">Just click on one of the sections below to find the answer... If you still can't find an answer to your question, please give our Customer Service guys a call on 123646689 or drop us an email at <a href="javascript:;">info@happyclinic.co.uk</a></p>
            <hr>
            <br>
            <div class="faq-sec-heading">
                <h2 class="inner-section-heading">Register</h2>            
            </div>
            <dl class="accordion">
                <dt>
                    <a href=""> Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu. Nunc ante sed phasellus, libero erat a massa varius. Vel libero dolor ut pretium wisi nam, mollis leo eget duis vulputate feugiat pede, aliquam eu porta in pellentesque, neque sit elit cubilia porta ullamcorper. In vivamus cursus nullam, diam nascetur, mus sed sodales cursus, est sem libero.</dd>

                <dt>
                    <a href=""> Vitae nonummy wisi natoque. Eget et parturient pede elit, etiam suscipit sed et ve?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>met cursus suspendisse montes sed ornare, iaculis nec felis sodales nulla qui, et est elit aptent congue urna. Metus ultrices, nunc pede ut orci luctus sem suspendisse, at orci non fusce, </dd>

                <dt>
                    <a href=""> Nunc maecenas duis quis tortor platea massa, arcu nullam vel velit?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>laoreet mi quis aliquet non. Vivamus wisi integer ut leo, bibendum mauris sit nulla. Non nam sem a nunc, pede adipiscing cras nunc id, nulla in auctor pellentesque leo id lectus. Sit nam, ac porttitor etiam mauris.</dd>

                <dt>
                    <a href=""> Sit quis at, iaculis gravida augue proin. Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae vestibulum. Turpis scelerisque tempor ipsum facilisi ut suspendisse. Elit consectetuer nascetur mauris, integer non animi quis, duis diam volutpat. Felis amet aenean necessitatibus lectus augue ac,</dd>

                <dt>
                    <a href=""> Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu. Nunc ante sed phasellus, libero erat a massa varius. Vel libero dolor ut pretium wisi nam, mollis leo eget duis vulputate feugiat pede, aliquam eu porta in pellentesque, neque sit elit cubilia porta ullamcorper. In vivamus cursus nullam, diam nascetur, mus sed sodales cursus, est sem libero.</dd>
            </dl>     
            <div class="faq-sec-heading">
                <h2 class="inner-section-heading">How Its Work</h2>             
            </div>
            <dl class="accordion">
                <dt>
                    <a href=""> Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu. Nunc ante sed phasellus, libero erat a massa varius. Vel libero dolor ut pretium wisi nam, mollis leo eget duis vulputate feugiat pede, aliquam eu porta in pellentesque, neque sit elit cubilia porta ullamcorper. In vivamus cursus nullam, diam nascetur, mus sed sodales cursus, est sem libero.</dd>

                <dt>
                    <a href=""> Nunc maecenas duis quis tortor platea massa, arcu nullam vel velit?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>laoreet mi quis aliquet non. Vivamus wisi integer ut leo, bibendum mauris sit nulla. Non nam sem a nunc, pede adipiscing cras nunc id, nulla in auctor pellentesque leo id lectus. Sit nam, ac porttitor etiam mauris.</dd>

                <dt>
                    <a href=""> Sit quis at, iaculis gravida augue proin. Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae vestibulum. Turpis scelerisque tempor ipsum facilisi ut suspendisse. Elit consectetuer nascetur mauris, integer non animi quis, duis diam volutpat. Felis amet aenean necessitatibus lectus augue ac,</dd>
            </dl>  
    
            <div class="faq-sec-heading">
                <h2 class="inner-section-heading">Fees</h2>             
            </div>

            <dl class="accordion">
                <dt>
                    <a href=""> Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Lorem ipsum dolor sit amet, dignissim sapien egestas volutpat in mauris arcu. Nunc ante sed phasellus, libero erat a massa varius. Vel libero dolor ut pretium wisi nam, mollis leo eget duis vulputate feugiat pede, aliquam eu porta in pellentesque, neque sit elit cubilia porta ullamcorper. In vivamus cursus nullam, diam nascetur, mus sed sodales cursus, est sem libero.</dd>

                <dt>
                    <a href=""> Nunc maecenas duis quis tortor platea massa, arcu nullam vel velit?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>laoreet mi quis aliquet non. Vivamus wisi integer ut leo, bibendum mauris sit nulla. Non nam sem a nunc, pede adipiscing cras nunc id, nulla in auctor pellentesque leo id lectus. Sit nam, ac porttitor etiam mauris.</dd>

                <dt>
                    <a href=""> Sit quis at, iaculis gravida augue proin. Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae?
                    <i class="icon-plus plus-minus"></i>    
                    <i class="icon-minus plus-minus"></i>    
                    </a>
                </dt>
                <dd>Cras et euismod, id vestibulum vitae, dolor integer et metus, velit vitae vestibulum. Turpis scelerisque tempor ipsum facilisi ut suspendisse. Elit consectetuer nascetur mauris, integer non animi quis, duis diam volutpat. Felis amet aenean necessitatibus lectus augue ac,</dd>
            </dl>  

        </div>
    </div>

<?php include_once('include/footer.php') ?>
</body>
</html>