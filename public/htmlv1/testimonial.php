<!DOCTYPE HTML>
<html>
<?php include_once('include/head.php') ?>
<body class="landing-page">
    <?php include_once('include/header.php') ?>
    <div class="landing-banner inner-banner about-banner">
        <div class="middle-align">
          <div class="inner">
            <div class="max-wrapper">
                <h1 class="inner-banner-heading">Testimonials
                <h1>
            </div>              
          </div>
        </div>
    </div>
    <!-- steps section -->
    <div class="section">
        <div class="max-wrapper">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                    <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                    <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                   <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                    <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                    <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="card testimonial-card">
                   <h2>Corvin Adams <span>Mechanical Engineer</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel esse quibusdam nihil a necessitatibus natus, voluptas fugiat officiis ipsa earum unde sit consequatur. Maxime harum ab pariatur obcaecati ut eos voluptatum.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 text-center">
                <a href="javascript:;" class="btn btn-primary loadmore-btn">
                    <span>Loadmore</span>
                </a>
            </div>
        </div>
            <div class="clearfix"></div>  
        </div>
    </div>
<?php include_once('include/footer.php') ?>
</body>
</html>