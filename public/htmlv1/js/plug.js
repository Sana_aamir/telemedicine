$(document).ready(function () {
	//mobile menu
	if ($(window).width()<= 1100) {
		$('.menu-icon').click(function(){
			$('.main-navigation').fadeIn();
		});
		$('.menu-icon-remove').click(function(){
			$('.main-navigation').fadeOut();
		});
	}

	//login forgot
	$('.authStepsClick').click(function(){
		if ($(this).hasClass('forgot-password')) {
			$('.login-screen').hide();
			$('.forgot-screen').show();
			$('.section-heading').text('Forgot Password');
			$('.login-sub-heading').text('Please provide your email address');
		}else{
			$('.forgot-screen').hide();
			$('.login-screen').show();
			$('.section-heading').text('Log In');
			$('.login-sub-heading').text('Log in to access your account');
		}
	});

	//initialize datepicker
	$('.datepicker').datetimepicker({
		format: 'DD/MM/YYYY'
    });
	$('.timepicker').datetimepicker({
		format: 'LT',
    });
	
	  var allPanels = $('.accordion > dd').hide();
	  var allPanels = $('.accordion > dd').first().show();
	  $('.accordion > dt > a').first().addClass('active');
	  $('.accordion > dt > a').click(function() {
	    $('.accordion > dd').slideUp();
	    $('.accordion > dt a').removeClass('active');
	    $(this).addClass('active');
	    $(this).parent().next().slideDown();
	    return false;
	  });

	$('.owl-carousel').owlCarousel({
	 	loop:true,
	    margin:10,
	    nav:true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	//labotry slider
	$('.owl-carousel-laboratory').owlCarousel({
	 	loop:false,
	    margin:20,
	    nav:false,
		autoplay:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:2
	        }
	    }
	});	
	
	//patient assessments
	$('.patient-tabs-side').owlCarousel({
	 	loop:false,
	    margin:0,
	    nav:false,
		autoplay:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:3
	        }
	    }
	});	

	$('.patient-tabs-rt-2').owlCarousel({
	 	loop:false,
	    margin:0,
	    nav:false,
		autoplay:false,
	    responsive:{
	        0:{
	            items:3
	        },
	        600:{
	            items:4
	        },
	        1000:{
	            items:6
	        }
	    }
	});	

	$('.doctors-slider').owlCarousel({
	 	loop:false,
	    margin:0,
	    nav:false,
		autoplay:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    }
	});	
//toogle
$('#toggle-two').bootstrapToggle({
      on: 'Enabled',
      off: 'Disabled'
    });

//wizar form
$('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
	var $total = navigation.find('li').length;
	var $current = index+1;
	var $percent = ($current/$total) * 100;
	$('#rootwizard .progress-bar').css({width:$percent+'%'});
}});
window.prettyPrint && prettyPrint();

//tabs 

$('.left-sidebar ul li a').click(function(){
	var currentTab = $(this).attr('data-content');
	$(this).parents('.tab-content-main').find('.left-sidebar ul li').removeClass('active');
	$(this).parent().addClass('active');
	$(this).parents('.tab-content-main').find('.account-tab-content').removeClass('active');
	$(currentTab).addClass('active');
});

  
//  start rating
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg);
    
  });

	function responseMessage(msg) {
	  $('.success-box').fadeIn(200);  
	  $('.success-box div.text-message').html("<span>" + msg + "</span>");
	}

});
