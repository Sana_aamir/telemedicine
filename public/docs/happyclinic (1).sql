-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module` enum('users','user_profile','appointment','lab_form','rbt','membership','email','support','site_visit','doctor','patient') NOT NULL,
  `log_id` int(11) NOT NULL,
  `log_type` enum('created','updated','updated_status','deleted','login','logout','change_password','cancelled','sent','address_change','lab_address_change','hospital_address_change','sharing_info_change','mobile_number_change','registered') NOT NULL,
  `user_type` enum('doctor','patient','superadmin') DEFAULT NULL,
  `text` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `appointments`;
CREATE TABLE `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encrypted_id` varchar(255) DEFAULT NULL,
  `appointment_type_id` int(11) NOT NULL,
  `specialty_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `sms_reminder` enum('pending','sent') NOT NULL DEFAULT 'pending',
  `email_reminder` enum('pending','sent') NOT NULL DEFAULT 'pending',
  `status` enum('booked','cancelled','completed') NOT NULL DEFAULT 'booked',
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_charge_id` varchar(255) DEFAULT NULL,
  `is_member` tinyint(4) NOT NULL DEFAULT '0',
  `fee` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `appointment_durations`;
CREATE TABLE `appointment_durations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty_id` int(11) NOT NULL,
  `appointment_type` varchar(500) NOT NULL,
  `duration` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `appointment_fee`;
CREATE TABLE `appointment_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty_id` int(11) NOT NULL DEFAULT '0',
  `appointment_type` varchar(255) NOT NULL,
  `patient_type` enum('member','non_member') NOT NULL,
  `fee` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `appointment_reminder_schedule`;
CREATE TABLE `appointment_reminder_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `appointment_sessions`;
CREATE TABLE `appointment_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archive_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `token` text COLLATE utf8_unicode_ci,
  `mod_token` text COLLATE utf8_unicode_ci,
  `pub_token` text COLLATE utf8_unicode_ci,
  `token_updated` datetime DEFAULT NULL,
  `status` enum('in_process','completed') COLLATE utf8_unicode_ci NOT NULL,
  `session_closed_time` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `appointment_types`;
CREATE TABLE `appointment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `audits`;
CREATE TABLE `audits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `document` varchar(255) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `business_settings`;
CREATE TABLE `business_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address_first_line` text NOT NULL,
  `city` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `postcode` varchar(200) NOT NULL,
  `admin_fee_percentage` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `clinical`;
CREATE TABLE `clinical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `doctor_final_comment` text,
  `is_emailed` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `clinical_doctor_answers`;
CREATE TABLE `clinical_doctor_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clinical_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` enum('yes','no') NOT NULL DEFAULT 'no',
  `comment` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `doctors`;
CREATE TABLE `doctors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `specialty_id` int(11) NOT NULL,
  `title` enum('Mr','Ms','Mrs','Miss','Dr','Prof') NOT NULL DEFAULT 'Dr',
  `dob` date DEFAULT NULL,
  `telephone_number` varchar(255) NOT NULL,
  `gmc_number` varchar(255) NOT NULL,
  `home_address` tinytext NOT NULL,
  `work_address` tinytext NOT NULL,
  `signature_image` varchar(255) DEFAULT NULL,
  `qualification` text,
  `designation` varchar(255) DEFAULT NULL,
  `description` text,
  `employment_start_date` date DEFAULT NULL,
  `employment_end_date` date DEFAULT NULL,
  `appraisal_date` date DEFAULT NULL,
  `revalidation_date` date DEFAULT NULL,
  `national_insurance_number` varchar(255) DEFAULT NULL,
  `tax_code` varchar(255) DEFAULT NULL,
  `bank_name` tinytext,
  `sort_code` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `doctor_documents`;
CREATE TABLE `doctor_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `doctor_holidays`;
CREATE TABLE `doctor_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `doctor_schedule`;
CREATE TABLE `doctor_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `appointment_type_id` int(11) DEFAULT NULL,
  `opening_time` int(11) NOT NULL,
  `opening_time_name` varchar(50) NOT NULL,
  `closing_time` int(11) NOT NULL,
  `closing_time_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `flagged_messages`;
CREATE TABLE `flagged_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `support_message_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` enum('patient','doctor') NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `description` text,
  `amount` float NOT NULL,
  `issue_date` date NOT NULL,
  `pay_period_start` datetime NOT NULL,
  `pay_period_end` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `laboratories`;
CREATE TABLE `laboratories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address_first_line` text,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(50) NOT NULL,
  `telephone_number` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fax_number` varchar(100) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lab_forms`;
CREATE TABLE `lab_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_number` varchar(255) DEFAULT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `laboratory_address_id` int(11) DEFAULT NULL,
  `appointment_id` int(11) NOT NULL,
  `sent_date` datetime NOT NULL,
  `is_emailed` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lab_form_details`;
CREATE TABLE `lab_form_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_form_id` int(11) NOT NULL,
  `test` text NOT NULL,
  `sample` enum('blood','serum') NOT NULL DEFAULT 'blood',
  `additional_info` text,
  `status` enum('single','repeat','stopped') NOT NULL DEFAULT 'single',
  `started_at_date` date DEFAULT NULL,
  `stopped_at_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_cancel_requests`;
CREATE TABLE `membership_cancel_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membership_subscription_id` int(11) NOT NULL,
  `requested_date` date NOT NULL,
  `cancel_at` date NOT NULL,
  `status` enum('due','cancelled') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership_subscriptions`;
CREATE TABLE `membership_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `stripe_sub_id` varchar(200) NOT NULL,
  `subscription_date` date NOT NULL,
  `cooling_off_period_end` datetime DEFAULT NULL,
  `stripe_charge_id` varchar(255) DEFAULT NULL,
  `status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `mental_health_hospitals`;
CREATE TABLE `mental_health_hospitals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address_first_line` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `postcode` varchar(50) NOT NULL,
  `telephone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `module` enum('doctor','patient','assessment') NOT NULL,
  `subject` text NOT NULL,
  `note` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `slug` varchar(200) NOT NULL,
  `draft_content` mediumblob NOT NULL,
  `content` mediumblob,
  `view` enum('show','hide') NOT NULL,
  `status` enum('published','unpublished') NOT NULL DEFAULT 'unpublished',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `patients`;
CREATE TABLE `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` enum('Mr','Ms','Mrs','Miss','Dr','Prof') CHARACTER SET utf8 NOT NULL,
  `dob` date NOT NULL,
  `photo_id` varchar(255) DEFAULT NULL,
  `utility_bill` varchar(255) DEFAULT NULL,
  `mental_hospital_address_id` int(11) NOT NULL DEFAULT '0',
  `laboratory_address_id` int(11) NOT NULL DEFAULT '0',
  `address_first_line` tinytext,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `is_member` tinyint(4) NOT NULL DEFAULT '0',
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `sharing_consent` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_id` varchar(200) DEFAULT NULL,
  `card_brand` varchar(200) DEFAULT NULL,
  `card_last_four` varchar(200) DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `patient_answers`;
CREATE TABLE `patient_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` enum('yes','no') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `patient_attachments`;
CREATE TABLE `patient_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `patient_identity_docs`;
CREATE TABLE `patient_identity_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `patient_text_fields`;
CREATE TABLE `patient_text_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `specialty_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `policies`;
CREATE TABLE `policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `document` varchar(255) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `psychiatric_reports`;
CREATE TABLE `psychiatric_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` varchar(100) NOT NULL,
  `filetype` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `questionnaire`;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specialty_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `reminder_emails`;
CREATE TABLE `reminder_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('schedule_reminder','appointment_reminder') NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `repeat_blood_test_service`;
CREATE TABLE `repeat_blood_test_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `upcoming_test_date` date NOT NULL,
  `upcoming_form_send_date` date NOT NULL,
  `status` enum('activated','deactivated') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `serious_incidents`;
CREATE TABLE `serious_incidents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `document` varchar(255) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `specialties`;
CREATE TABLE `specialties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `document` varchar(255) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `support`;
CREATE TABLE `support` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` enum('general','medication','prescription','complaint','tests','changes_messages') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'general',
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `status` enum('open','closed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `is_updated` bit(1) NOT NULL DEFAULT b'1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `support` (`id`, `slug`, `category`, `subject`, `sender_id`, `receiver_id`, `status`, `is_updated`, `created_at`, `updated_at`) VALUES
(37,	'zyeuyymspecz+q=',	'changes_messages',	'Password Changed',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-01 14:04:44',	'2019-04-01 14:04:44'),
(38,	'hyr3fe09zzc=',	'changes_messages',	'Password Changed',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-01 14:13:23',	'2019-04-01 14:13:23'),
(39,	'p9h18t92228=',	'general',	'Ggygi',	35,	1,	'open',	CONV('0', 2, 10) + 0,	'2019-04-02 10:58:46',	'2019-04-02 10:59:09'),
(40,	'w8ryz8t+ylm=',	'general',	'Test',	35,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-02 11:02:15',	'2019-04-02 11:02:15'),
(41,	'+uvzfbvkddq=',	'general',	'take medicines',	35,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-28 12:47:37',	'2019-04-28 12:47:37'),
(42,	'YTBnSjlsS3ZjSExYbWZBRWlsaERpQT09',	'changes_messages',	'Hospital Address Changed',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-30 21:43:05',	'2019-04-30 21:43:05'),
(43,	'WjVkMGpaQ0NmZDRBVmx1MGtteDZDdz09',	'changes_messages',	'Laboratory Address Changed',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-04-30 23:45:47',	'2019-04-30 23:45:47'),
(44,	'bitYaGVBRGlmNWlMUVVDZmxUdTl6dz09',	'general',	'fdsf',	34,	35,	'open',	CONV('0', 2, 10) + 0,	'2019-05-01 17:38:49',	'2019-05-01 17:38:49'),
(45,	'dWFENTNtNDR3ck9DMjExNXBtQVdoZz09',	'general',	'hi',	1,	43,	'open',	CONV('0', 2, 10) + 0,	'2019-05-06 10:49:53',	'2019-05-06 10:49:53'),
(46,	'Y09aY2xnV0lGZG5MV2xhMVBnM3Btdz09',	'changes_messages',	'Sharing Information Consent Changed',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-05-16 20:13:43',	'2019-05-16 20:13:43'),
(47,	'eDN2YTNRdkZLRitURUdoVmJDMkFPdz09',	'tests',	'blood tests',	1,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-05-17 00:40:30',	'2019-05-17 00:40:30'),
(48,	'UW1hWms2bG0wTEZnSzBpTHBwTWlpZz09',	'general',	'hello',	34,	51,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:48',	'2019-05-19 04:58:48'),
(49,	'cGJmQi8xWG0rODEwWXArYnZ6TTFydz09',	'general',	'hello',	34,	49,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:49',	'2019-05-19 04:58:49'),
(50,	'U0g5STZGdjRqSm9TU2JiNjZrTUZNQT09',	'general',	'hello',	34,	47,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:49',	'2019-05-19 04:58:49'),
(51,	'NHQrVERSNjluR3g0S2w5dTBiejB1dz09',	'general',	'hello',	34,	45,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:49',	'2019-05-19 04:58:49'),
(52,	'YVhwcTJpdVJZNEY5ZnN0ZUlFRWtIQT09',	'general',	'hello',	34,	44,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(53,	'ejQvZURDWmxsb0NUV2ZPNVZQU0pvUT09',	'general',	'hello',	34,	42,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(54,	'TUdzSTR0bEhKR2FXcG9JUUdJMHlkZz09',	'general',	'hello',	34,	41,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(55,	'ZTFjK213RERhc0pBYWl4QzYxMUZPZz09',	'general',	'hello',	34,	40,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(56,	'Um5KSXU5M3ZuR3dkR3hGOVU4RlVvdz09',	'general',	'hello',	34,	39,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(57,	'V2huQzVFTmU0dXE0OWdXbkhVT3ozQT09',	'general',	'hello',	34,	38,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(58,	'NVQwekhTWDMvVjBvUk8xa0RzdUFPdz09',	'general',	'hello',	34,	37,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(59,	'NnZsdUhxVW5lcUZpVXU4TkE0TzN4Zz09',	'general',	'hello',	34,	36,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(60,	'aFVxaU5UQXVIZlhxNHpqUDVnVUVDUT09',	'general',	'hello',	34,	34,	'open',	CONV('0', 2, 10) + 0,	'2019-05-19 04:58:50',	'2019-05-19 04:58:50'),
(61,	'OEhlV2NKK3QyQlo1Yjh1eGVDR3pSdz09',	'general',	'blood tests',	1,	52,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:49',	'2019-05-19 22:42:49'),
(62,	'RGYyRWhtZEtsOGQ0YUhVbk5oWmRaZz09',	'general',	'blood tests',	1,	51,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(63,	'aHZ3WmRrVDNGNEVIUE1ic2VhdU84dz09',	'general',	'blood tests',	1,	49,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(64,	'N2xiOU1jWnBFNmplWmhvUkhMSWozQT09',	'general',	'blood tests',	1,	47,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(65,	'aUdjZ2lPYU9MNjlzd1hNRWF4Vm1Bdz09',	'general',	'blood tests',	1,	45,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(66,	'dmRmcE5ua1Z0eitReWhwblBiYWxzZz09',	'general',	'blood tests',	1,	44,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(67,	'eERoeis4QVp4T2xFL3N2Nk1lRE1YZz09',	'general',	'blood tests',	1,	42,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(68,	'bzJnSysrOEtZNUtLVTdkajhJT2ViQT09',	'general',	'blood tests',	1,	41,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(69,	'NlZyRTduY1prNjNjeGVKcysxU2ZRUT09',	'general',	'blood tests',	1,	40,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(70,	'WVRzTnJEMGtwMk5rSjV1L2o0L24wQT09',	'general',	'blood tests',	1,	39,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(71,	'UWlLU1FRclU2eWxFdzZ6QkNEd2tyUT09',	'general',	'blood tests',	1,	38,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(72,	'K1JaMXlnTnY1TElPUys5N1Y1T3hrZz09',	'general',	'blood tests',	1,	37,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(73,	'K0ZYZDlWSmhoc1NqZXhJaU5FWTRTZz09',	'general',	'blood tests',	1,	36,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(74,	'NVlXN1pXakwwVkRFbVc3MFFvTGRNUT09',	'general',	'blood tests',	1,	34,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:42:50',	'2019-05-19 22:42:50'),
(75,	'WklNeGxmYklqYmhlYkZ5allhdzdnQT09',	'general',	'CV',	1,	43,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:45:01',	'2019-05-19 22:45:01'),
(76,	'U3N3OHd5T2xOYXBENnlDeGo4VG9Ldz09',	'general',	'CV',	1,	35,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:45:01',	'2019-05-19 22:45:01'),
(77,	'c2RCWlJmMm11ZGVGQi93Ry9hM25jUT09',	'general',	'CV',	1,	33,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:45:01',	'2019-05-19 22:45:01'),
(78,	'VHhLaVpxZWIrOGFCTDRyTDcrV1Evdz09',	'general',	'CV',	1,	24,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:45:01',	'2019-05-19 22:45:01'),
(79,	'Q2F1TXNwVVpJdEtpNDQ5L2JiUU5kZz09',	'general',	'hi',	1,	43,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:52:22',	'2019-05-19 22:52:22'),
(80,	'OVYwSm4ydnBlZlBDVVNKdWQrQmNuQT09',	'general',	'hi',	1,	51,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 22:58:17',	'2019-05-19 22:58:17'),
(81,	'dUVqYzU2WkFvQlU5Vm1UQUJxVHdDZz09',	'medication',	'hi',	34,	43,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 23:48:34',	'2019-05-19 23:48:34'),
(82,	'bTlzUWZxeEprbXltVGUrQlRPczNMZz09',	'medication',	'hi',	34,	35,	'open',	CONV('1', 2, 10) + 0,	'2019-05-19 23:54:56',	'2019-05-19 23:54:56'),
(83,	'OXB3VDI0UVVKTUp5N09CV2lJeFNzZz09',	'changes_messages',	'Address Changed',	1,	34,	'open',	CONV('1', 2, 10) + 0,	'2019-05-20 00:29:09',	'2019-05-20 00:29:09');

DROP TABLE IF EXISTS `support_messages`;
CREATE TABLE `support_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `support_id` int(10) unsigned NOT NULL,
  `sender_id` bigint(20) unsigned NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `support_message_docs`;
CREATE TABLE `support_message_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `support_message_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('approve','disapprove') NOT NULL DEFAULT 'disapprove',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT '0',
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  `is_blocked_login` tinyint(4) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2019-05-20 11:40:56
