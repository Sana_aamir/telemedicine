alert();
export default {
    alert();
    data: function () {
        return {
            projects: []
        }
    },
    mounted() {
        var app = this;
        axios.get($apiUrl'/projects')
            .then(function (resp) {
                app.projects = resp.data;
            })
            .catch(function (resp) {
                console.log(resp);
                alert("Could not load projects");
            });
    },
    methods: {
        deleteEntry(id, index) {
            if (confirm("Do you really want to delete it?")) {
                var app = this;
                axios.delete('/api/projects/' + id)
                    .then(function (resp) {
                        app.projects.splice(index, 1);
                    })
                    .catch(function (resp) {
                        alert("Could not delete company");
                    });
            }
        }
    }
}
