$( document ).ready(function() {


	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('.cal_tabl tr td input').on('input', function() {
  		this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
	});

	$('#is_super_admin').change(function () {
	    if ($(this).is(":checked")) {
	    	$('.no_super_admin').addClass('hidden');
	    }
	    else
	    {
	    	$('.no_super_admin').removeClass('hidden');
	    }
	    // not checked
	});


    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });




	$("#profile_upload_btn").click(function() {
		$('#fileupload').trigger('click'); 
	})


	$('#fileupload').fileupload({
        dataType: 'json',
		url: config.apiURL + 'media/upload',        
		start: function (e) {
			$('#upload_spinner').removeClass('hidden');
		},
        done: function (e, data) {
			$('#upload_spinner').addClass('hidden');        	
        	$('#file_path').val(data.result.name);
        	changeFile(0);

        	$('#profile_avatar').attr('src',  URL.createObjectURL(data.files[0]) );


            // $.each(data.result.files, function (index, file) {
            //     $('<p/>').text(file.name).appendTo(document.body);
            // });
        },
		fail: function (e, data) {
			$('#upload_spinner').addClass('hidden');
		  	ParseErrors(data.jqXHR.responseText, '.alert');

		}        
    });

	// initialize date picker
	$('#datepicker-component, .start_date, .end_date').datepicker({
			autoclose:true,
			format: "dd/mm/yyyy"
	}).on('hide', function(e) {
		removeDateError(this);
    });

	if($('#user_listing').length)
		getUsers(0);


	var validator = $("#reset_form").validate({

		rules: {
            'confirm_password': {
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            'password_mirror': {
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
	  submitHandler: function(form) {
	  	resetPasswordByToken();
	  }
	 });

	var validator = $("#reset_password_form").validate({

		rules: {
            'confirm_password': {
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            'password_mirror': {
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
	  submitHandler: function(form) {
	  	resetPassword();
	  }
	 });

	var validator = $("#register_form").validate({
	  submitHandler: function(form) {
	  	var userType = $('input[name=user_type]:checked').val();
	  	if(userType == '2' || userType == '3')
		  	addUpdateUser();
		else
			$.msgShow('#register_msg', 'Please select the account type', 'error');
	  }
	 });

	var validator = $("#user_form").validate({
	  submitHandler: function(form) {
	  	addUpdateUser();
	  }
	 });

	var validator = $("#profile_form").validate({
	  submitHandler: function(form) {
	  	updateProfile();
	  }
	 });

	var validator = $("#forgot_form").validate({
	  submitHandler: function(form) {
	  	requestPassword();
	  }
	 });

	var validator = $("#login-form").validate({
	  submitHandler: function(form) {
	  	login();
	  }
	 });

});


function scrollTop()
{
	var body = $("html, body");
	body.stop().animate({scrollTop:0}, 500, 'swing', function() { 

	});	
}

function confirmDelete(id, callback)
{
	$('#modalStickUpSmall').modal('show');
	$('#deleteButton').attr('onclick', callback + '('+ id +')');
}

function deleteUser(id)
{
	$.ajax({
	  type: 'DELETE',
	  url: config.apiURL + 'user/' + id,
	  dataType:"JSON",
	  data: $( "#user_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			$('#modalStickUpSmall').modal('hide');
	  		getUsers(0);
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, msgId);

	  }
	});		
}

function updateProfile()
{
	var msgId = '#user_msg';

	$.ajax({
	  type: 'PUT',
	  url: config.apiURL + 'user/profile',
	  dataType:"JSON",
	  data: $( "#profile_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			showAmaran('success', data.message);
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, msgId);

	  }
	});		
}

function getUsers(page)
{
	var hotelId = $('#hotel_filter').val();

	if(hotelId != '0')
	{
		if($('.paginations').data("twbs-pagination")){
			$('.paginations').twbsPagination('destroy');
		}
	}


	$.ajax({
	  type: 'GET',
	  url: config.apiURL + 'users',
	  dataType:"JSON",
	  data: {page:page, hotel_id: hotelId},
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
	  		var html = '';
	  		if(data.data.data.length > 0)
	  		{
				$.each(data.data.data, function( index, record ) {
					html += '<tr>\
	                        <td class="v-align-middle ">\
	                          <p>'+record.id+'</p>\
	                        </td>\
	                        <td class="v-align-middle">\
	                          <p>'+record.first_name+ ' ' + record.last_name +'</p>\
	                        </td>\
	                        <td class="v-align-middle">\
	                          <p>'+record.email+'</p>\
	                        </td>\
	                        <td class="v-align-middle">\
	                          <p>'+moment(record.created_at).format('Do MMM YYYY')+'</p>\
	                        </td>\
	                        <td class="v-align-middle">\
	                          <p><a href="'+config.webURL+'user/edit/'+record.id+'">Edit</a> | <a onclick="confirmDelete('+record.id+', \'deleteUser\');" href="javascript:void(0);">Delete</a></p>\
	                        </td>\
	                      </tr>';
				});


				$('#user_listing').html(html);

				// Show pagination
				$('.paginations').twbsPagination({
				  totalPages: Math.ceil(data.data.total / data.data.per_page),
				  visiblePages: data.data.per_page,
				  cssStyle: '',
				  pageClass:"pagination-items",
				  anchorClass:"paginate-anchor",
				  prevText: '<span aria-hidden="true">&laquo;</span>',
			      nextText: '<span aria-hidden="true">&raquo;</span>',
				  onPageClick: function (event, page) {
				  	getUsers(page);
				  	scrollTop();
				  }
				});

	  		}
	  		else
	  		{
	  			html = emptyRec($('#basicTable th').length);
				$('#user_listing').html(html);	  			
	  		}

	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, msgId);

	  }
	});	
}

function emptyRec(span)
{
	return '<tr><td style="text-align:center;" colspan="'+span+'" style="text">No Records found</td></tr>';
}

function getMethod()
{
	var method = 'POST';
	var id = $.trim($('#module_id').val())
	if(id != '')
		method = 'PUT';
	return method;
}

function addUpdateUser()
{
	var msgId = '#register_msg';
	
	$.ajax({
	  type: getMethod(),
	  url: config.apiURL + 'user',
	  dataType:"JSON",
	  data: $( "#register_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			window.location = config.webURL + 'login';
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, msgId);

	  }
	});
}

function requestPassword()
{
	var msgId = '#forgot_msg';
	
	$.ajax({
	  type: 'POST',
	  url: config.apiURL + 'request-password',
	  dataType:"JSON",
	  data: $( "#forgot_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			$.msgShow('#forgot_msg', data.message, 'success');
			$('#forgot_email').val('');			
			// window.location = config.webURL + 'dashboard';
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, msgId);

	  }
	});
}

function resetPassword()
{
	$.ajax({
	  type: 'PUT',
	  url: config.apiURL + 'user/reset-password',
	  dataType:"JSON",
	  data: $( "#reset_password_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			$.msgShow('#reset_msg', data.message, 'success');
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, '#reset_msg');

	  }
	});	
}

function resetPasswordByToken()
{
	$.ajax({
	  type: 'PUT',
	  url: config.apiURL + 'reset-password',
	  dataType:"JSON",
	  data: $( "#reset_form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			$.msgShow('#reset_msg', data.message, 'success');

			window.location = config.webURL + 'login';

	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, '#reset_msg');

	  }
	});	
}

function login()
{
	$.ajax({
	  type: 'POST',
	  url: config.apiURL + 'login',
	  dataType:"JSON",
	  data: $( "#login-form" ).serialize(),
	  beforeSend:function(){

	  },
	  success:function(data){
	  	if(data.status == 'success')
	  	{
			$.msgShow('#login_msg', data.message, 'success');
			window.location = config.webURL + 'dashboard';
	  	}
	  },
	  error:function(xhr, ajaxOptions, thrownError){

	  	ParseErrors(xhr.responseText, '#login_msg');

	  }
	});	
}


function ParseErrors(jsonResp, msgId)
{
	var messages = '';
  	var errorJson = jQuery.parseJSON(jsonResp);
	if (typeof errorJson.errors != "undefined") 
	{
	  	var ObjectCount = Object.keys(errorJson.errors).length;

	  	if(ObjectCount > 0)
	  	{
			$.each(errorJson.errors, function( field, errorObj ) {
				$('#' + field).parent().addClass('has-error');
				messages += errorObj;
			});

			if(messages != '')
			{
				$.msgShow(msgId, messages, 'error');
			}
	  	}
	}
	else
	{
		if(errorJson.status == 'error')
		{
			$.msgShow(msgId, errorJson.message, 'error');
		}
	}		
}

function removeDateError(obj)
{
	if($(obj).val() != '')
	{
		$(obj).removeClass('error');
		$(obj).parent().removeClass('has-error');			
		$(obj).next('label.error').remove();
	}
}

$.msgShow = function(id, msg, type)
{
  if($(id).css('display') == 'block')
    return false;

  $(id).removeClass('alert-danger');
  $(id).removeClass('alert-success')  
  if(type == 'success')
  {
      $(id).addClass('alert-success');
      $(id).html('<button class="close" data-dismiss="alert"></button>' + msg).fadeIn().delay(2500).fadeOut();
  }
  else
  { 
      $(id).addClass('alert-danger');
      $(id).html('<button class="close" data-dismiss="alert"></button>' + msg).fadeIn().delay(2500).fadeOut();
  }
}

function showAmaran(type, msg)
{
	$.amaran({
	        'message'   :"Saved",
	        'position'  :'top right',
	        'inEffect'  :'slideTop'
	});
}


$( "#forgotbtn" ).click(function() {
    $('#login_wrapper').hide();
    $('#forgot_wrapper').fadeIn();
});

$( "#loginbtn" ).click(function() {
    $('#forgot_wrapper').hide();
    $('#login_wrapper').fadeIn();
});


function changeFile(type)
{
	if(type == 1)
	{
		$('#file_path').val('');
	}
	else
	{
		$('#workbook_upload_btn').addClass('hidden');
		$('#upload_confirm').removeClass('hidden');

	}

}
