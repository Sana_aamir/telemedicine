var server = window.location.hostname;

if (server == 'localhost') {
    var canvasUrl = location.protocol + "//" + server + "/telemedicine/public/";
} 
else if (server == 'codekernal.com') {
    var canvasUrl = location.protocol + "//" + server + "/telemedicine/public/";
}
else {
    var canvasUrl = location.protocol + "//"+server+"/";
}


var config = {};

// Front app url
config.webURL = canvasUrl;

// API url
config.apiURL = canvasUrl + "api/";

// javascript validation on/off
config.client_validation = false;

function c(value)
{
	console.log(value);
}