if($('#sample_listing').length)
  getSamples(0);

var validator = $("#sample_form").validate({
  submitHandler: function(form) {
    addUpdateSample();
  }
 });

function getSamples(page)
{
  $.ajax({
    type: 'GET',
    url: config.apiURL + 'slugs',
    dataType:"JSON",
    data: {page:page},
    beforeSend:function(){

    },
    success:function(data){
      if(data.status == 'success')
      {
        var html = '';
        if(data.data.data.length > 0)
        {
        $.each(data.data.data, function( index, record ) {
          html += '<tr>\
                          <td class="v-align-middle ">\
                            <p>'+record.id+'</p>\
                          </td>\
                          <td class="v-align-middle">\
                            <p>'+record.name +'</p>\
                          </td>\
                          <td class="v-align-middle">\
                            <p>'+moment(record.created_at).format('Do MMM YYYY')+'</p>\
                          </td>\
                          <td class="v-align-middle">\
                            <p><a href="'+config.webURL+'sample/edit/'+record.id+'">Edit</a> | <a onclick="confirmDelete('+record.id+', \'deleteSample\');" href="javascript:void(0);">Delete</a></p>\
                          </td>\
                        </tr>';
        });

        $('#sample_listing').html(html);

        // Show pagination
        $('.paginations').twbsPagination({
          totalPages: Math.ceil(data.data.total / data.data.per_page),
          visiblePages: data.data.per_page,
          cssStyle: '',
          pageClass:"pagination-items",
          anchorClass:"paginate-anchor",
          prevText: '<span aria-hidden="true">&laquo;</span>',
            nextText: '<span aria-hidden="true">&raquo;</span>',
          onPageClick: function (event, page) {
            getSamples(page);
            scrollTop();
          }
        });

        }
        else
        {
          html = emptyRec($('#basicTable th').length);
        $('#sample_listing').html(html);          
        }

      }
    },
    error:function(xhr, ajaxOptions, thrownError){

      ParseErrors(xhr.responseText, msgId);

    }
  }); 
}




function addUpdateSample()
{
  var msgId = '#sample_msg';
  
  $.ajax({
    type: getMethod(),
    url: config.apiURL + 'slug',
    dataType:"JSON",
    data: $( "#sample_form" ).serialize(),
    beforeSend:function(){

    },
    success:function(data){
      if(data.status == 'success')
      {
      window.location = config.webURL + 'samples';
      }
    },
    error:function(xhr, ajaxOptions, thrownError){

      ParseErrors(xhr.responseText, msgId);

    }
  });
}


function deleteSample(id)
{
  var msgId = '#sample_msg';

  $.ajax({
    type: 'DELETE',
    url: config.apiURL + 'slug/' + id,
    dataType:"JSON",
    data: $( "#sample_form" ).serialize(),
    beforeSend:function(){

    },
    success:function(data){
      if(data.status == 'success')
      {
      $('#modalStickUpSmall').modal('hide');
        getSamples(0);
      }
    },
    error:function(xhr, ajaxOptions, thrownError){

      ParseErrors(xhr.responseText, msgId);

    }
  });   
}