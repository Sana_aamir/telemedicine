<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\BusinessSettingService;

class BusinessSettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\BusinessSettingServiceInterface', function ($app) {
          return new BusinessSettingService();
        });
    }
}
