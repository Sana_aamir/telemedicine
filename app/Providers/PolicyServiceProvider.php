<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\PolicyService;

class PolicyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\PolicyServiceInterface', function ($app) {
          return new PolicyService();
        });
    }
}
