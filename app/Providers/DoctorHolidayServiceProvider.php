<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\DoctorHolidayService;

class DoctorHolidayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\DoctorHolidayServiceInterface', function ($app) {
          return new DoctorHolidayService();
        });
    }
}
