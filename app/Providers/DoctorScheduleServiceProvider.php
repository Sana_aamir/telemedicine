<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\DoctorScheduleService;

class DoctorScheduleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\DoctorScheduleServiceInterface', function ($app) {
          return new DoctorScheduleService();
        });
    }
}
