<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\ActivityLogService;

class ActivityLogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\ActivityLogServiceInterface', function ($app) {
          return new ActivityLogService(/*new ActivityLog, new User, new Role, new Patient, new Doctor, new Appointment, new LabForm, new RepeatBloodTest, new Membership, new Email, new Support*/);
        });
    }
}
