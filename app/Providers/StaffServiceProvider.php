<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\StaffService;

class StaffServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\StaffServiceInterface', function ($app) {
          return new StaffService();
        });
    }
}
