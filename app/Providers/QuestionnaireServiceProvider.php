<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\QuestionnaireService;

class QuestionnaireServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\QuestionnaireServiceInterface', function ($app) {
          return new QuestionnaireService();
        });
    }
}
