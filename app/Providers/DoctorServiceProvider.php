<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\DoctorService;

class DoctorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\DoctorServiceInterface', function ($app) {
          return new DoctorService();
        });
    }
}
