<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\EmailService;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('App\Library\Contracts\EmailServiceInterface', function ($app) {
          return new EmailService();
        });
    }
}
