<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AuditsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                return [];
            }
            case 'POST':
            {
                if($route == 'api/audits')
                {
                    return [
                            'name' => 'required|alpha_spaces|unique:audits,name',
                            'date' => 'required|date_format:"Y-m-d"',
                         ];
                }
                else
                {
                    return [];
                }
            }
            case 'PATCH':
            {
                if($route == 'api/audits')
                {
                    return [
                            'name' => 'required|alpha_spaces|unique:audits,name,'.\Request::input('id'),
                            'date' => 'required|date_format:"Y-m-d"',
                         ];
                }
                else
                {
                    return [];
                }
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
