<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class LaboratoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                return [];
            }
            case 'POST':
            {
                if($route == 'api/laboratory')
                {
                    return [
                        'name' => 'required|alpha_spaces',
                        'address_first_line' => 'required',
                        'city' => 'required|alpha_spaces',
                        'country' => 'required|alpha_spaces',
                        'postcode' => 'required|alpha_num',
                        'fax_number' => 'required|alpha_num',
                        'email' => 'required|email|unique:laboratories,email',
                        'telephone_number' => 'required|unique:laboratories,telephone_number',
                        
                    ];
                }
                else if ($route == 'api/laboratory-import-csv') 
                {
                    return [
                        'import_file' => 'required|max:50000|mimes:xlsx,doc,docx,xls',
                    ];
                }
                else
                {
                    return [];
                }
            }
            case 'PATCH':
            {
                if($route == 'api/laboratory/{id}')
                {
                    return [
                        'name' => 'required|alpha_spaces',
                        'address_first_line' => 'required',
                        'city' => 'required|alpha_spaces',
                        'country' => 'required|alpha_spaces',
                        'postcode' => 'required|alpha_num',
                        'fax_number' => 'required|alpha_num',
                        'email' => 'required|email|unique:laboratories,email,'.\Request::input('id'),
                        'telephone_number' => 'required|unique:laboratories,telephone_number,'.\Request::input('id'),
                        
                    ];
                }
                else
                {
                    return [];
                }
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
