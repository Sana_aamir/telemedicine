<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                return [];
            }
            case 'POST':
            {
                if($route == 'api/doctor')
                {
                    return [
                        'user.first_name' => 'required|alpha_spaces',
                        'user.last_name' => 'required|alpha_spaces',
                        'user.gender' => 'required',
                        'user.profile_pic' => 'required',
                        'user.role' => 'required',
                        'user.email' => 'required|email|unique:users,email',
                        'user.mobile_number' => 'required|unique:users,mobile_number',
                        'specialty_id' => 'required',
                        'telephone_number' => 'required',
                        'home_address' => 'required',
                        'work_address' => 'required',
                        'gmc_number' => 'required|numeric',
                        'employment_start_date' => 'required|date_format:"Y-m-d"',
                        'employment_end_date' => 'required|date_format:"Y-m-d"',
                        'appraisal_date' => 'required|date_format:"Y-m-d"',
                        'revalidation_date' => 'required|date_format:"Y-m-d"',
                        'designation' => 'required|alpha_spaces',
                        'qualification' => 'required',
                        'national_insurance_number' => 'required|alpha_num',
                        'tax_code' => 'required|alpha_num',
                        'bank_name' => 'required|alpha_spaces',
                        'sort_code' => 'required',
                        'account_number' => 'required|numeric',
                        
                    ];
                }
                else
                {
                    return [];
                }
            }
            case 'PATCH':
            {
                if($route == 'api/doctor/{id}')
                {
                    return [
                        'user.first_name' => 'required',
                        'user.last_name' => 'required',
                        'user.gender' => 'required',
                        'user.profile_pic' => 'required',
                        'user.role' => 'required',
                        'user.email' => 'required|email|unique:users,email,'.\Request::input('user_id'),
                        'user.mobile_number' => 'required|unique:users,mobile_number,'.\Request::input('user_id'),
                        'specialty_id' => 'required',
                        'telephone_number' => 'required',
                        'home_address' => 'required',
                        'work_address' => 'required',
                        'gmc_number' => 'required|numeric',
                        'employment_start_date' => 'required|date_format:"Y-m-d"',
                        'employment_end_date' => 'required|date_format:"Y-m-d"',
                        'appraisal_date' => 'required|date_format:"Y-m-d"',
                        'revalidation_date' => 'required|date_format:"Y-m-d"',
                        'designation' => 'required|alpha_spaces',
                        'qualification' => 'required',
                        'national_insurance_number' => 'required|alpha_num',
                        'tax_code' => 'required|alpha_num',
                        'bank_name' => 'required|alpha_spaces',
                        'sort_code' => 'required',
                        'account_number' => 'required|numeric',
                    ];
                }
                else
                {
                    return [];
                }
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
