<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                return [];
            }
            case 'POST':
            {
                if($route == 'api/patient')
                {
                    return [
                        'dob' => 'required',
                        'title' => 'required',
                        //'photo_id' => 'required',
                        'address_first_line' => 'required',
                        'country' => 'required',
                        'city' => 'required',
                        'postcode' => 'required',
                        'user.first_name' => 'required|alpha_spaces',
                        'user.last_name' => 'required|alpha_spaces',
                        'user.password' => 'required',
                        'user.gender' => 'required',
                        'user.profile_pic' => 'required',
                        'user.role' => 'required',
                        'user.email' => 'required|email|unique:users,email',
                        'user.mobile_number' => 'required|unique:users,mobile_number',
                        
                    ];
                }
                else
                {
                    return [];
                }
            }
            case 'PATCH':
            {
                if($route == 'api/patient/{id}')
                {
                    return [
                        'dob' => 'required',
                        'title' => 'required',
                        //'photo_id' => 'required',
                        'user.first_name' => 'required|alpha_spaces',
                        'user.last_name' => 'required|alpha_spaces',
                        'user.gender' => 'required',
                        'user.profile_pic' => 'required',
                        'user.role' => 'required',
                        'user.email' => 'required|email|unique:users,email,'.\Request::input('user_id'),
                        'user.mobile_number' => 'required|unique:users,mobile_number,'.\Request::input('user_id'),
                    ];
                }
                else
                {
                    return [];
                }
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
