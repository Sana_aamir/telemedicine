<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                if($route == 'api/user/profile')
                {
                    return [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|unique:users,email,'.\Auth::user()->id,
                    ];
                }
                else if($route == 'api/user/reset-password')
                {
                    return [
                        'password' => 'required',
                        'old_password' => 'required',
                    ];
                }
                else if($route == 'api/check-unique-user')
                {
                    return [
                        'email' => 'required|email|unique:users,email',
                        'mobile_number' => 'required|unique:users,mobile_number',
                    ];
                }
                else
                {
                    return [];
                }

            }
            case 'POST':
            {
                if($route == 'api/login')
                {
                    return [
                        'email' => 'required|email',
                        'password' => 'required',
                    ];
                }
                else if($route == 'api/user')
                {
                    return [
                        'email' => 'required|email|unique:users',
                        'password' => 'required',
                    ];
                }
                else if($route == 'api/request-password')
                {
                    return [
                        'forgot_email' => 'required|email',
                    ];
                }
            }
            case 'PATCH':
            {
                return [];
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
