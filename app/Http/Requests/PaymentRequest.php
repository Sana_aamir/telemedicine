<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();
        $route = \Route::current()->uri;

        switch($method)
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'PUT':
            {
                return [];
            }
            case 'POST':
            {
                
                if($route == 'api/subscription/blood-test')
                {
                    return [
                        'patient_id' => 'required',
                        'token' => 'required',
                        ];
                } 
                else if($route == 'api/payment/appointment')
                {
                    return [
                        'patient_id' => 'required',
                        'token' => 'required',
                        'amount' => 'required',
                        'appointment_type_id' => 'required',
                        ];
                } 
                else if($route == 'api/subscription/membership')
                {
                    return [
                        'patient_id' => 'required',
                        'token' => 'required',
                        ];
                } 
                else
                {
                    return [];
                }
            }
            case 'PATCH':
            {
                return [];
            }
            default:break;
        }
    }

    public function response(array $errors) {
        return response()->json(['status' => 'error', 'message' => $errors, 'code' => 400], 400);
    }

}
