<?php

namespace App\Http\Middleware;

use Closure;
use App\Library\Services\UserService;
use Teapot\StatusCode;

class CheckPasswordReset
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->route('token');
        if(empty($token))
            $token = $request->input('token');

        $userService = new UserService();

        $canResetPass = $userService->canResetPass($token);
        if($canResetPass)
        {
            return $next($request);            
        }
        else
        {
            if($request->ajax())
            {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Some error occurred.'
                ], StatusCode::OK);                
            }
            else
            {
               return redirect('error/'."Password Link is invalid or expired");                
            }

        }
    }
}
