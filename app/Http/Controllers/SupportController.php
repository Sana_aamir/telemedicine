<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\SupportServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\SupportRequest;
use App\Library\Helper\Utility;

class SupportController extends Controller
{
	public $serviceInstance;
	function __construct(SupportServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function save(SupportRequest $request)
    {
    	$captchaResponse = $request->input('recaptcha');
    	if(!empty($captchaResponse)) {
        	$captchaVerification = Utility::captchaVerification($captchaResponse);
        	if ($captchaVerification) {
				$resp = $this->serviceInstance->save($request);
				if($resp)
				{
					return response()->json([
					    'status' => 'success',
					    'code' => StatusCode::OK,
					    'message' => 'Support has been created successfully'
					], StatusCode::OK);
				}
				else
				{
					return response()->json([
					    'status' => 'error',
					    'message' => 'Error occured'
					], StatusCode::BAD_REQUEST);
				} 
			} else {
        		return response()->json(['status' => 'error', 'message' => 'You look like robot', 'code' => StatusCode::BAD_REQUEST], StatusCode::BAD_REQUEST);
        	}
		} else {
        	return response()->json(['status' => 'error', 'message' => 'Please check on captcha', 'code' => StatusCode::BAD_REQUEST], StatusCode::BAD_REQUEST);
        }		   	
    }

    public function updateStatus(SupportRequest $request, $id)
    {
		$resp = $this->serviceInstance->updateStatus($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Status has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function sendReply(SupportRequest $request)
    {
		$resp = $this->serviceInstance->sendReply($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Message has been sent successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(SupportRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Support has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Support has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(SupportRequest $request)
    {
		$resp = $this->serviceInstance->getAllMergedData($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Support has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get(SupportRequest $request)
    {
        $resp = $this->serviceInstance->get($request->column, $request->value, true);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Support has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function messages(SupportRequest $request)
    {
		$resp = $this->serviceInstance->getAllMessages($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Messages has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getSupportMessage($id)
    {
		$resp = $this->serviceInstance->getSupportMessage($id);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Messages has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function deleteMessage($id)
    {
		$resp = $this->serviceInstance->deleteMessage($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Message has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function toggleFlag($id)
    {
		$resp = $this->serviceInstance->toggleFlag($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Flag has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function countNewSupportMessage(SupportRequest $request)
    {
		$resp = $this->serviceInstance->countNewSupportMessage();
		return response()->json([
			'data' => $resp,
		    'status' => 'success',
		    'code' => StatusCode::OK,
		    'message' => 'Count fetched successfully'
		], StatusCode::OK);
		  	
    }

    public function countNewComplaintMessage(SupportRequest $request)
    {
		$resp = $this->serviceInstance->countNewComplaintMessage();
		return response()->json([
			'data' => $resp,
		    'status' => 'success',
		    'code' => StatusCode::OK,
		    'message' => 'Count fetched successfully'
		], StatusCode::OK);
		  	
    }

}
