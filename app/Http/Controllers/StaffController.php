<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\StaffServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\StaffRequest;

class StaffController extends Controller
{
	public $serviceInstance;
	function __construct(StaffServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function save(StaffRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Staff has been created successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(StaffRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Staff has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function updateStatus(StaffRequest $request)
    {
		$resp = $this->serviceInstance->updateStatus($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Staff member status has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Staff has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(StaffRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Staff has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get($id)
    {
        $resp = $this->serviceInstance->get('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Staff has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }
}
