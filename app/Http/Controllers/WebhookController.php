<?php

namespace App\Http\Controllers;

use App\Mail\InvoiceSuccessEmail;
use App\Mail\Receipt; 
use App\Library\Services\PatientService;
use App\Library\Services\PaymentService;
use App\Models\MembershipSubscription;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use Symfony\Component\HttpFoundation\Response;

class WebhookController extends CashierController
{
    /**
     * Handle invoice payment succeeded.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleInvoicePaymentSucceeded($payload)
    {
        // Handle The Event
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);
        if ($user) {
            // send email to patient
            
            $paymentService = new PaymentService;
            $inputData = $paymentService->getStripeInvoice($payload['data']['object']['id'], $user['id']);


            MembershipSubscription::where('stripe_sub_id','=', $payload['data']['object']['subscription'])->update(['stripe_charge_id' => $payload['data']['object']['charge'] ]); 
            try {
                $emailData = array (
                    'subject' => 'Successful Payment Receipt ',
                    'data' => $inputData,
                );
                \Mail::to($inputData['patient']->user->email)->send(new Receipt($emailData));
            } catch(\Exception $e) {
            }
        }

        return new Response('Webhook Handled', 200);
    }
}