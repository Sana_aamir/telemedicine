<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\QuestionnaireServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\QuestionnaireRequest;

class QuestionnaireController extends Controller
{
	public $serviceInstance;
	function __construct(QuestionnaireServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function showQuestionnaires(QuestionnaireRequest $request)
    {
        return view('backend.questionnaire.index', array());
    }

    public function save(QuestionnaireRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Questionnaire has been created successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(QuestionnaireRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Questionnaire has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Questionnaire has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(QuestionnaireRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Questionnaire has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get($id)
    {
        $resp = $this->serviceInstance->get('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Questionnaire has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getClinicalRecords(QuestionnaireRequest $request)
    {
		$resp = $this->serviceInstance->getClinicalRecords($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Hospital records has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getClinicalRecord($id)
    {
        $resp = $this->serviceInstance->getClinicalRecord('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Questionnaire has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function deleteClinicalRecord($id)
    {
		$resp = $this->serviceInstance->deleteClinicalRecord($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Hospital record has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function sendClinicalRecord(QuestionnaireRequest $request)
    {
		$resp = $this->serviceInstance->sendHostpitalRecord($request->id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Hospital record has been sent successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }
}
