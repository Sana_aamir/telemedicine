<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\DoctorScheduleServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\DoctorScheduleRequest;

class DoctorScheduleController extends Controller
{
	public $serviceInstance;
	function __construct(DoctorScheduleServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}


    public function update(DoctorScheduleRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Doctor Schedule has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(DoctorScheduleRequest $request, $id)
    {
		$resp = $this->serviceInstance->getAll($request, $id);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'DoctorSchedule has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getAvailChart(DoctorScheduleRequest $request, $id)
    {
		$resp = $this->serviceInstance->getAvailChart($request, $id);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'DoctorSchedule has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }
}
