<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class SiteController extends Controller
{
    public function showLogin()
    {
        return view('backend.user.login');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }

    public function showResetPassword($token)
    {
        $data = ['token'=>$token];
        return view('backend.user.reset-password', $data);
    }

    public function error($message)
    {
        $data = ['message'=>$message];
        return view('backend.error.error', $data);
    }

}
