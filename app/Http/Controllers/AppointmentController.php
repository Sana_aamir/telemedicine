<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\AppointmentServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\AppointmentRequest;

class AppointmentController extends Controller
{
	public $serviceInstance;
	function __construct(AppointmentServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function save(AppointmentRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Appointment has been booked successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(AppointmentRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Appointment has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Appointment has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(AppointmentRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Appointment has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get(AppointmentRequest $request, $id)
    {
        $resp = $this->serviceInstance->get('id',$id, true, $request);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getAppointmentFromEncryptedId(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->get($request->column, $request->value, true);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getLatestAppointment(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->getLatestAppointment($request);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Latest appointment has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::NOT_FOUND);
        }
    }

    public function getAppointmentAvailabilityCal(AppointmentRequest $request) 
    {
    	$resp = $this->serviceInstance->getAppointmentAvailabilityCal($request);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment calendar has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getAvailTimeSlots(AppointmentRequest $request, $date) 
    {
    	$resp = $this->serviceInstance->getAvailTimeSlots($request, $date);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Time slots has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function updateStatus(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->updateStatus($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment Status has been updated successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function updateAppointmentStatus(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->updateAppointmentStatus($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment Status has been updated successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function appointmentFee(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->appointmentFee($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment fee has been updated successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }       
    }

    public function getAppointmentFee(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->getAppointmentFee($request);

        if(!empty($resp))
        {
            return response()->json([
                'data' => $resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment fee has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }               
    }

    public function getAppointmentFeeForBooking(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->getAppointmentFeeForBooking($request);

        if(!empty($resp))
        {
            return response()->json([
                'data' => $resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment fee has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }               
    }

    public function appointmentReminders(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->appointmentReminders($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment reminders has been updated successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }       
    }

    public function getAppointmentReminders(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->getAppointmentReminders($request);

        if(!empty($resp))
        {
            return response()->json([
                'data' => $resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment reminders has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }               
    }

    public function checkDoctorAvailibility(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->checkDoctorAvailibility($request);

        if(!empty($resp))
        {
            return response()->json([
                'data' => $resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Availibility has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }               
    }

    public function assessmentCompleted(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->assessmentCompleted($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Assessment has been completed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }       
    }

    public function startAssessment(AppointmentRequest $request, $id)
    {
        $appointment = $this->serviceInstance->get('id', $id);
        if (!empty($appointment)) {
            $session = $this->serviceInstance->checkIfSessionExist($id);
            if ($session == 'not_exist') {
                $resp =  $this->serviceInstance->createOpenTalkSession($appointment);
                if ($resp) {
                     return response()->json([
                        'data' => $resp,
                        'status' => 'success',
                        'code' => StatusCode::OK,
                        'message' => 'Session has been created successfully'
                    ], StatusCode::OK);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error occured'
                    ], StatusCode::BAD_REQUEST);
                }
               
            } else if ($session == 'completed') {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error occured'
                ], StatusCode::BAD_REQUEST);
            } else {
                return response()->json([
                    'data' => $session,
                    'status' => 'success',
                    'code' => StatusCode::OK,
                    'message' => 'Session has been created successfully'
                ], StatusCode::OK);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
        
    }

    public function sendInvitationLink(AppointmentRequest $request, $id)
    {
        $resp = $this->serviceInstance->sendInvitationLink($id);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Invitation link has been sent successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }       
    }

    public function appointmentDurations(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->appointmentDurations($request);

        if($resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment durations has been updated successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }       
    }

    public function getAppointmentDurations(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->getAppointmentDurations($request);

        if(!empty($resp))
        {
            return response()->json([
                'data' => $resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment durations has been fetched successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }               
    }

    public function checkAppointmentExist(AppointmentRequest $request)
    {
        $resp = $this->serviceInstance->checkAppointmentExist($request);

        if(!$resp)
        {
            return response()->json([
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Appointment not exist'
            ], StatusCode::OK);
        }
        else
        {
           return response()->json([
                'status' => 'warning',
                'code' => StatusCode::OK,
                'message' => 'You can not remove schedule because you have booked appointment(s) for that day.'
            ], StatusCode::OK);
        }       
    }
}
