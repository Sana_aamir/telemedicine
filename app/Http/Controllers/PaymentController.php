<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\PaymentServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\PaymentRequest;
use App\Models\Patient;

class PaymentController extends Controller
{
	public $serviceInstance;
	function __construct(PaymentServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function appointmentPayment(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->stripePayment($request);

		if($resp)
		{
			return response()->json([
				'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment has been created successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => $resp
			], StatusCode::BAD_REQUEST);
		}    	
    }

    /*public function update(PaymentRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function updateStatus(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->updateStatus($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment status has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get($id)
    {
        $resp = $this->serviceInstance->get('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Payment has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }*/

    /*public function bloodTestSubsciption(PaymentRequest $request)
    {
        $resp = $this->serviceInstance->bloodTestSubsciption($request);

        if($resp == true)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'You have subscribed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => $resp
            ], StatusCode::BAD_REQUEST);
        }
    }*/

    public function membershipSubscription(PaymentRequest $request)
    {
        $resp = $this->serviceInstance->membershipSubscription($request);

        if($resp == true)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'You have subscribed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => $resp
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function downloadInvoice(PaymentRequest $request, $patient_id, $invoice_id) {

        $patient = Patient::find($patient_id);

         return $patient->downloadInvoice($invoice_id, [
            'vendor'  => 'Telemedicine',
            'product' => 'Appointment Booking',
        ]);


    }

    public function invoices(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->invoices($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Invoices has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    /*public function getInvoiceFromStripe(PaymentRequest $request, $id)
    {
		$resp = $this->serviceInstance->getInvoiceFromStripe($id);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Invoice has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }*/
    public function invoiceDelete($id)
    {
		$resp = $this->serviceInstance->invoiceDelete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Invoice has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function allRBT(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->allRBT($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'RBT subscriptions has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getRBT(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getRBT($request->column, $request->value);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'RBT subscriptions has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function updateRBT(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->updateRBT($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Repeat blood test date has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function getMembershipSubscriptions(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getMembershipSubscriptions($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Membership Subscriptions has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getMembershipSubscription(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getMembershipSubscription($request->column, $request->value, true);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Membership Subscription has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function deleteSubscription($id)
    {
		$resp = $this->serviceInstance->deleteSubscription($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Subscription has been cancelled successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function deleteSubscriptionCancelRequest($id)
    {
		$resp = $this->serviceInstance->deleteSubscriptionCancelRequest($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Subscription Cancel Request has been removed successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    

	public function refundPayment(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->refundPayment($request->patient_id, $request->charge_id, $request->amount);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Payment has been refund successfully.'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function membershipCancelRequest(PaymentRequest $request)
    {
        $resp = $this->serviceInstance->membershipCancelRequest($request);

        if($resp == true)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Your request has been sent successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => $resp
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function membershipCancel(PaymentRequest $request)
    {
        $resp = $this->serviceInstance->membershipCancel($request);

        if($resp == true)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Your membership has been cancelled successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'An error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getMembershipCancelRequests(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getMembershipCancelRequests($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Membership Cancel Requests has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function getMembershipCancelRequest(PaymentRequest $request, $id)
    {
		$resp = $this->serviceInstance->getMembershipCancelRequest('id',$id);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Membership Cancel Requests has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    /*public function getChargeObject(PaymentRequest $request)
    {
		$resp = $this->serviceInstance->getChargeObject($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Charge has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }*/
}
