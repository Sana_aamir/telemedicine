<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\DoctorHolidayServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\DoctorHolidayRequest;

class DoctorHolidayController extends Controller
{
	public $serviceInstance;
	function __construct(DoctorHolidayServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}


    public function save(DoctorHolidayRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp == 'success')
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Holiday has been added successfully'
			], StatusCode::OK);
		}
		else if($resp == 'appoitment_exist')
		{
			return response()->json([
			    'status' => 'warning',
			    'code' => StatusCode::OK,
			    'message' => 'You can not add holiday schedule for these days because you have booked appointment(s).'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(DoctorHolidayRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp == 'success')
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Holiday has been updated successfully'
			], StatusCode::OK);
		}
		else if($resp == 'appoitment_exist')
		{
			return response()->json([
			    'status' => 'warning',
			    'code' => StatusCode::OK,
			    'message' => 'You can not update holiday schedule for these days because you have booked appointment(s).'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'DoctorHoliday has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(DoctorHolidayRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'DoctorHoliday has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get($id)
    {
        $resp = $this->serviceInstance->get('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'DoctorHoliday has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }
}
