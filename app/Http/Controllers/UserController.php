<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Helper\Utility;
use App\Library\Contracts\UserServiceInterface;
use App\Library\Services\InvoiceService;
use App\Library\Services\PaymentService;
use App\Library\Services\SupportService;
use App\Library\Services\LabFormService;
use App\Library\Services\QuestionnaireService;
use Teapot\StatusCode;
use App\Http\Requests\UserRequest;
use App\Library\Helper\ActivityLogManager;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
	public $serviceInstance;

	function __construct(UserServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
		/*$this->middleware('auth:api', ['except' => ['login']]);*/
	}

	public function login(Request $request)
    {
    	$errorMessage = '';
        // grab credentials from the request
        $credentials = request(['email', 'password']);
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = auth()->attempt($credentials)) {
                return response()->json([
				    'status' => 'error',
				    'message' => 'Unable to login with these credentials.'
				], StatusCode::BAD_REQUEST);
            } else {
	            $user = $this->serviceInstance->get('email',  $request->input('email'), true);
	            $is_approved = $this->serviceInstance->getIsApproved($user->id);
	            if ($user->role == 'Superadmin' && $request->input('from') == 'site') {
	                $errorMessage = 'Unable to login with these credentials.';
	            } else if (($user->role == 'Patient' ||  $user->role == 'Doctor') && $request->input('from') == 'admin') {
	                $errorMessage = 'Unable to login with these credentials.';
	            } else if ($user->email_verified == 0) {
	               $errorMessage = 'Your account is not verified. Please check your email to verify your account.';
	            } else if ($is_approved == 0 && $user->role == 'Patient') {
	               $errorMessage = 'Your account is not approved yet.';
	            } else if ($user->is_archived == 1) {
					$errorMessage = 'Unable to login with these credentials.';
	            } else if ($user->is_blocked_login == 1) {
					$errorMessage = 'This account has been blocked by admin. Please contact admin for futhure assistance.';
	            } else {
	                // to maintain log
	                try {
	                    $newParams = array(
	                                'user_id'   => $user->id,
	                                'user_type'   => $user->role,
	                                'module'    => 'user_profile',
	                                'log_id'    => $user->id,
	                                'text'    => Utility::get_ip_address(),
	                                'log_type'  => 'login'
	                            );
	                    
	                    ActivityLogManager::create($newParams);
	                } catch(\Exception $e){
	                }
	                // all good so return the token
        			//return $this->respondWithToken($token, $user);
        			return response()->json([

						'data'=>Utility::appSettings(),
					    'status' => 'success',
					    'code' => StatusCode::OK,
					    'access_token' => $token,
					    'message' => 'You logged in successfully. Redirecting...'
					], StatusCode::OK);
	            }
	        }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json([
		    'status' => 'error',
		    'message' => $errorMessage
		], StatusCode::BAD_REQUEST);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    public function authenticateUser(Request $request) {
    	 // grab credentials from the request
        $credentials = request(['email', 'password']);
        if (auth()->attempt($credentials)) {
        	return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Success'
			], StatusCode::OK);
        } else {
        	return response()->json([
			    'status' => 'error',
			    'message' => 'Invalid credentials.'
			], StatusCode::BAD_REQUEST);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
    	// to maintain log
        try {
            $newParams = array(
                        'user_id'   => Auth::user()->id,
                        'user_type'   => ucfirst(Auth::user()->roles->first()->name),
                        'module'    => 'user_profile',
                        'log_id'    => Auth::user()->id,
                        'log_type'  => 'logout'
                    );
            
            ActivityLogManager::create($newParams);
        } catch(\Exception $e){
        }
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            //'user' => $user
        ]);
    }

	public function generatePayslip(UserRequest $request)
	{
		$id = Utility::encryptDecrypt('decrypt', $request->id);
		$invoiceService = new InvoiceService;
		$inputData = $invoiceService->get('id', $id, true);
		if (!empty($inputData)) {
			$inputData['doctor'] = json_decode($inputData['doctor'], true);
			$view = \View::make('backend.print.payslip', ['data' => $inputData]);
			$html = $view->render();
			
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
									'format' => 'A4',
									'margin_left' => 0,
									'margin_right' => 0,
									'margin_top' => 0,
									'margin_bottom' => 0,
									'margin_header' => 0,
									'margin_footer' => 0,
									'tempDir' => storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR]);

			$mpdf->writeHTML($html);
			$mpdf->output();
		} else {
			return redirect('/admin/404');
		}
		
	}

	public function generateReceipt(UserRequest $request) {
		$paymentService = new PaymentService;
		$inputData = $paymentService->getStripeInvoice($request->id, $request->patient_id);
		
		if (!empty($inputData)) {
			$inputData['patient'] = json_decode($inputData['patient'], true);
			$view = \View::make('backend.print.receipt', ['data' => $inputData]);
			$html = $view->render();
			
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
									'format' => 'A4',
									'margin_left' => 0,
									'margin_right' => 0,
									'margin_top' => 0,
									'margin_bottom' => 0,
									'margin_header' => 0,
									'margin_footer' => 0,
									'tempDir' => storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR]);

			$mpdf->writeHTML($html);
			$mpdf->output();
		} else {
			return redirect('/admin/404');
		}
	}

	public function generateMessage(UserRequest $request) {
		$id = Utility::encryptDecrypt('decrypt', $request->id);
		$supportService = new SupportService;
		$inputData = $supportService->getSupportMessage($id, true);
		if (!empty($inputData)) {
			//p($inputData);
			$view = \View::make('backend.print.message', ['data' => $inputData]);
			$html = $view->render();
			
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
									'format' => 'A4',
									'margin_left' => 0,
									'margin_right' => 0,
									'margin_top' => 0,
									'margin_bottom' => 0,
									'margin_header' => 0,
									'margin_footer' => 0,
									'tempDir' => storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR]);

			$mpdf->writeHTML($html);
			$mpdf->output();
		} else {
			return redirect('/admin/404');
		}
	}

	public function generateLabform(UserRequest $request) {

		$id = Utility::encryptDecrypt('decrypt', $request->id);
		$labFormService = new LabFormService;
		$inputData = $labFormService->get('id', $id, $request->repeat, $request->single);
		if (!empty($inputData)) {
			$view = \View::make('backend.print.labform', ['data' => $inputData]);
			$html = $view->render();
			
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
									'format' => 'A4',
									'margin_left' => 0,
									'margin_right' => 0,
									'margin_top' => 0,
									'margin_bottom' => 0,
									'margin_header' => 0,
									'margin_footer' => 0,
									'debug' => true,
									'tempDir' => storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR]);

			$mpdf->writeHTML($html);
			$mpdf->output();
		} else {
			return redirect('/admin/404');
		}
	}

	public function generateHospitalRec(UserRequest $request) {

		$id = Utility::encryptDecrypt('decrypt', $request->id);
		$questionnaireService = new QuestionnaireService;
		$inputData = $questionnaireService->getClinicalRecord('id', $id);
		if (!empty($inputData)) {
			$view = \View::make('backend.print.hospital_record', ['data' => $inputData]);
			$html = $view->render();
			
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
									'format' => 'A4',
									'margin_left' => 0,
									'margin_right' => 0,
									'margin_top' => 0,
									'margin_bottom' => 0,
									'margin_header' => 0,
									'margin_footer' => 0,
									'debug' => true,
									'tempDir' => storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR]);

			$mpdf->writeHTML($html);
			$mpdf->output();
		} else {
			return redirect('/admin/404');
		}
	}


    public function getAll(UserRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'super_admin' => Auth::user()->hasRole('superadmin'),
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Users have been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'User has been deleted success'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function updateProfile(UserRequest $request)
    {
		$resp = $this->serviceInstance->updateProfile($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Profile has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function update(UserRequest $request)
    {
		$resp = $this->serviceInstance->update($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'User has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function updateStatus(UserRequest $request)
    {
		$resp = $this->serviceInstance->updateStatus($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'User Status has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }


    public function save(UserRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'User has been created successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    
   


    public function requestPassword(UserRequest $request)
    {    	
        $request->merge(['template' => 'request-password']);

		$auth = $this->serviceInstance->requestPassword($request);

		if($auth)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'We\'ve sent an email with instructions to reset password'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'success',
			    'message' => 'You will get an email with instructions if you have account.'
			], StatusCode::OK);
		}
    }

    
    public function showUsers(UserRequest $request)
    {
    	if(Auth::user()->hasRole('superadmin'))
			$users = $this->serviceInstance->getAll($request);

 
        return view('tool.user.users', $users);
    }

    public function resetPassword(UserRequest $request)
    {
    	$checkOldPassword = $this->serviceInstance->checkOldPassword(Auth::user()->email, $request->input('old_password'));
    	if ($checkOldPassword) {
    		$resp = $this->serviceInstance->resetPassword(Auth::user()->email, $request->input('password'));

			if($resp)
			{
				return response()->json([
				    'status' => 'success',
				    'code' => StatusCode::OK,
				    'message' => 'Password has been updated successfully'
				], StatusCode::OK);
			}
			else
			{
				return response()->json([
				    'status' => 'error',
				    'message' => 'Some error occurred.'
				], StatusCode::BAD_REQUEST);
			}
    	}
    	else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Old password is incorrect. Please enter correct password.'
			], StatusCode::NOT_FOUND);
		}
    	
		
    }

    public function resetPasswordByToken(UserRequest $request)
    {
		$resp = $this->serviceInstance->resetPasswordByToken($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Password has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Some error occurred.'
			], StatusCode::OK);
		}
    }

    public function resetPasswordByAdmin(UserRequest $request)
    {
        $resp = $this->serviceInstance->resetPasswordByAdmin($request);
        if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Reset password link has been sent successfully.'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Some error occurred.'
			], StatusCode::OK);
		}

    }

    public function getCities($county)
    {
		$resp = Utility::getCities($county);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'data' => $resp,
			    'code' => StatusCode::OK,
			    'message' => 'Password has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Some error occurred.'
			], StatusCode::OK);
		}
    }

    /*public function logout(UserRequest $request)
    {
    	$resp = $this->serviceInstance->logout($request);
		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'data' => $resp,
			    'code' => StatusCode::OK,
			    'message' => 'Logout successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Some error occurred.'
			], StatusCode::OK);
		}
    }*/

    public function verifyEmail(UserRequest $request, $code)
    {
        $resp = $this->serviceInstance->verifyEmail($code);

        if($resp)
		{
			\Session::flash('flash', 'Your account has been verified successfully');
			return redirect('/login');
		}
		else
		{
			return redirect('/404');
		}
    }

    public function contactEmail(UserRequest $request)
    {
		$resp = $this->serviceInstance->contactEmail($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Your message has been sent successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}
    }

    public function checkForUniqueFields(UserRequest $request) {
    	return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'success'
			], StatusCode::OK);
    }



}
