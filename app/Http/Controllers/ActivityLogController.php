<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\ActivityLogServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\ActivityLogRequest;

class ActivityLogController extends Controller
{
	public $serviceInstance;
	function __construct(ActivityLogServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function getAll(ActivityLogRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Log has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}  
    }

    
}
