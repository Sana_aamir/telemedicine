<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\PageServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\PageRequest;

class PageController extends Controller
{
	public $serviceInstance;
	function __construct(PageServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function showPages(PageRequest $request)
    {
        return view('backend.page.index', array());
    }

    public function save(PageRequest $request)
    {
		$resp = $this->serviceInstance->save($request);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Page has been created successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function update(PageRequest $request, $id)
    {
		$resp = $this->serviceInstance->update($request, $id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Page has been updated successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Page has been deleted successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(PageRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Page has been fetched successfully'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function get($id)
    {
        $resp = $this->serviceInstance->get('id',$id);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Page has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }

    public function getContent($slug)
    {
        $resp = $this->serviceInstance->get('slug',$slug);

        if($resp)
        {
            return response()->json([
                'data'=>$resp,
                'status' => 'success',
                'code' => StatusCode::OK,
                'message' => 'Page has been fecthed successfully'
            ], StatusCode::OK);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Error occured'
            ], StatusCode::BAD_REQUEST);
        }
    }
}
