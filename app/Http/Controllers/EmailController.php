<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\EmailServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests\EmailRequest;
use App\Library\Helper\Utility;

class EmailController extends Controller
{
	public $serviceInstance;
	function __construct(EmailServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
	}

    public function send(EmailRequest $request)
    {
    	$captchaResponse = $request->input('recaptcha');
    	if(!empty($captchaResponse)) {
        	$captchaVerification = Utility::captchaVerification($captchaResponse);
        	if ($captchaVerification) {
				$resp = $this->serviceInstance->send($request);

				if($resp)
				{
					return response()->json([
					    'status' => 'success',
					    'code' => StatusCode::OK,
					    'message' => 'Email has been sent successfully'
					], StatusCode::OK);
				}
				else
				{
					return response()->json([
					    'status' => 'error',
					    'message' => 'Error occured'
					], StatusCode::BAD_REQUEST);
				} 
			} else {
        		return response()->json(['status' => 'error', 'message' => 'You look like robot', 'code' => StatusCode::BAD_REQUEST], StatusCode::BAD_REQUEST);
        	}
		} else {
        	return response()->json(['status' => 'error', 'message' => 'Please check on captcha', 'code' => StatusCode::BAD_REQUEST], StatusCode::BAD_REQUEST);
        }		   	
    }

    
}
