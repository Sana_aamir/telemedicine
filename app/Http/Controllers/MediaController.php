<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\Contracts\MediaServiceInterface;
use Teapot\StatusCode;
use App\Http\Requests;
use App\Http\Requests\MediaRequest;

class MediaController extends Controller
{
	public $serviceInstance;
	public $tmpPath;

	function __construct(MediaServiceInterface $customServiceInstance)
	{
		$this->serviceInstance = $customServiceInstance;
		$this->tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
	}

	/*public function upload(Request $request){

        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');
        $input = [];
        $input['file'] = $request->file('file');
        $input['from'] = $request->input('from');
  
        $file =$request->file('file');

        //$fileName = $file->getClientOriginalName();
        $originalFileName = $file->getClientOriginalName();
        $fileName = md5(time()*rand()).'.'.strtolower($file->getClientOriginalExtension());
        
        $tmpfolder = $this->tmpPath;
        $tmpfolder = $tmpfolder . basename( $fileName) ;
        if(move_uploaded_file($_FILES['file']['tmp_name'], $tmpfolder)){
            
            //$this->serviceInstance->moveFile($tmpfolder, storage_path('app/public/'.$input['from'].'/').$fileName);

            $status = 200;
            $output = array('status'=>$status,
            				'file'=>$fileName, 
            				'original_file_name' => $originalFileName,
            				//'url'=>url('storage/'.$input['from'].'/'.$fileName),
                            'messages'=>"Uploaded");
        }else {
            $status = 400;
            $output = array('error' => array(
                                'messages'  => ['Error occured'],
                                'type'      => 'BadRequest',
                                'status'      => $status
                ));
        }

        
        return response()->json($output, $status);
    }*/

    public function upload(MediaRequest $request)
    {
		$resp = $this->serviceInstance->upload($request);

		if ($resp['status'] == 'error') 
		{
			return response()->json([
			    'status' => 'error',
			    'message' => $resp['messages']
			], StatusCode::BAD_REQUEST);
			
		}
		else
		{
			\Session::flash('flash_message','Media added successfully');

			return response()->json([
			    'status' => 'success',
			    'name' => $resp['name'],
			    'created_at' => date('Y-m-d H:i:s'),
			    'code' => StatusCode::OK,
			    'message' => 'Media created'
			], StatusCode::OK);
		}    	
    }

    public function update(MediaRequest $request)
    {
		$resp = $this->serviceInstance->update($request);

		if($resp)
		{
			\Session::flash('flash_message','Media updated successfully');

			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Media updatd'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function delete($id)
    {
		$resp = $this->serviceInstance->delete($id);

		if($resp)
		{
			return response()->json([
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Media deleted'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	
    }

    public function getAll(MediaRequest $request)
    {
		$resp = $this->serviceInstance->getAll($request);

		if(!empty($resp))
		{
			return response()->json([
			    'data' => $resp,
			    'status' => 'success',
			    'code' => StatusCode::OK,
			    'message' => 'Media fetched'
			], StatusCode::OK);
		}
		else
		{
			return response()->json([
			    'status' => 'error',
			    'message' => 'Error occured'
			], StatusCode::BAD_REQUEST);
		}    	    	
    }

    public function download(MediaRequest $request)
    {
		if(!empty($request->input('file'))) {
			$filename = $request->input('file');
			$directory = $request->input('directory');
			$path =  $directory.'/'.$filename;
	       	//$result = Utility::decryptS3Files($path, 'acmvppublic');

	        header('Content-Type: application/octet-stream');
	        header("Content-Disposition: attachment; filename=".$filename.";");
	        readfile($path);
	        exit();
		} 	    	
    }

}
