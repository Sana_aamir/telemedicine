<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Library\Services\AppointmentService;
use Illuminate\Http\Request;

class AutoCompleteAssessment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:auto_complete_assessment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto complete assessment from 5 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AppointmentService::completeSession();
    }
}
