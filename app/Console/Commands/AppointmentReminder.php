<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Library\Services\AppointmentService;
use Illuminate\Http\Request;

class AppointmentReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:appointment_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Appointment reminder email before 24 hours from appointment start time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AppointmentService::appointmentReminder();
    }
}
