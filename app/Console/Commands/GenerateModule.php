<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will generate boilerplate of module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $space = '------------------------------------------------';
        $moduleName = $this->ask('What is Module Name');
        $slug = $this->ask('What is Route Slug');
        $modelName = $this->ask('What is Model name');
        $tableName = $this->ask('What is Table Name');
        $viewDir = $this->ask('What is View directory name');



        if(!empty($moduleName))
        {
            $this->info('Module Generation started');
            $this->info($space);
            /*$this->info('Module Front Routes');
            $this->info($space);
            $this->line("Route::get('".strtolower($slug)."s', '".$moduleName."Controller@show".$moduleName."s');");
            $this->info($space);*/
            $this->info('Module Api Routes');
            $this->info($space);
            $this->line("Route::post('".strtolower($slug)."', '".$moduleName."Controller@save');");
            $this->line("Route::patch('".strtolower($slug)."/{id}', '".$moduleName."Controller@update');");
            $this->line("Route::get('".strtolower($slug)."/{id}', '".$moduleName."Controller@get');");
            $this->line("Route::get('".strtolower($slug)."s', '".$moduleName."Controller@getAll');");
            $this->line("Route::delete('".strtolower($slug)."/{id}', '".$moduleName."Controller@delete');");
            $this->info($space);
            $this->info('Module Component Registeration');
            $this->line("import ".$moduleName."Index from './components/".$slug."/".$moduleName."Index.vue';");
            $this->line("import ".$moduleName."Create from './components/".$slug."/".$moduleName."Create.vue';");
            $this->line("import ".$moduleName."Edit from './components/".$slug."/".$moduleName."Edit.vue';");
            $this->info($space);
            $this->info('Module Vue Routes');
            $this->line("{path: '/".$slug."', component: ".$moduleName."Index, name: '".$moduleName."Index'},");
            $this->line("{path: '/".$slug."/create', component: ".$moduleName."Create, name: 'Create".$moduleName."'},");
            $this->line("{path: '/".$slug."/edit/:id', component: ".$moduleName."Edit, name: 'Edit".$moduleName."'},");
            $this->info($space);
            $this->info('Add application service provider in config/app.php');
            $this->info("App\Providers\\".$moduleName.'ServiceProvider::class,');
            $this->info($space);
            $this->info('Generating Module Request');

            try
            {
                // Request Genertation
                $contents = \File::get('app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR.'SampleRequest.php');

                $contents = str_replace('SampleRequest', $moduleName.'Request', $contents);

                $bytes_written = \File::put('app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR.$moduleName.'Request.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Module Request has been generated: '.'app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR.$moduleName.'Request.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Request not found');
            }

            $this->info('Generating Module Controller');
            // module controller generation

            try
            {
                // Request Genertation
                $contents = \File::get('app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR.'SampleController.php');

                $contents = str_replace('Sample', $moduleName, $contents);
                $contents = str_replace('sample', strtolower($moduleName), $contents);
                $contents = str_replace('view_dir', strtolower($viewDir), $contents);


                $bytes_written = \File::put('app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR.$moduleName.'Controller.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Controller has been generated: '.'app'.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR.$moduleName.'Controller.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Controller not found');
            }

            $this->info('Generating Service Provider');
            // module controller generation

            try
            {
                // Request Genertation
                $contents = \File::get('app'.DIRECTORY_SEPARATOR.'Providers'.DIRECTORY_SEPARATOR.'SampleServiceProvider.php');

                $contents = str_replace('Sample', $moduleName, $contents);

                $bytes_written = \File::put('app'.DIRECTORY_SEPARATOR.'Providers'.DIRECTORY_SEPARATOR.$moduleName.'ServiceProvider.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Service Provider has been generated: '.'app'.DIRECTORY_SEPARATOR.'Providers'.DIRECTORY_SEPARATOR.$moduleName.'ServiceProvider.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Service Provider not found');
            }

            $this->info('Generating Interface');
            // module controller generation

            try
            {
                // Request Genertation
                $dir = 'app'.DIRECTORY_SEPARATOR.'Library'.DIRECTORY_SEPARATOR.'Contracts'.DIRECTORY_SEPARATOR;
                $contents = \File::get($dir.'SampleServiceInterface.php');

                $contents = str_replace('Sample', $moduleName, $contents);

                $bytes_written = \File::put($dir.$moduleName.'ServiceInterface.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Interface has been generated: '.$dir.$moduleName.'ServiceInterface.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Interface not found');
            }

            $this->info('Generating Service');
            // module controller generation

            try
            {
                // Request Genertation
                $dir = 'app'.DIRECTORY_SEPARATOR.'Library'.DIRECTORY_SEPARATOR.'Services'.DIRECTORY_SEPARATOR;
                $contents = \File::get($dir.'SampleService.php');

                $contents = str_replace('Sample', $moduleName, $contents);
                $contents = str_replace('ModelName', $modelName, $contents);

                $bytes_written = \File::put($dir.$moduleName.'Service.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Service has been generated: '.$dir.$moduleName.'Service.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Service not found');
            }

            $this->info('Generating model');
            // model generation

            try
            {
                // Request Genertation
                $dir = 'app'.DIRECTORY_SEPARATOR.'Models'.DIRECTORY_SEPARATOR;
                $contents = \File::get($dir.'Sample.php');

                $contents = str_replace('Sample', $modelName, $contents);
                $contents = str_replace('Table', $tableName, $contents);

                $bytes_written = \File::put($dir.$modelName.'.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Model has been generated: '.$dir.$modelName.'.php');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample Model not found');
            }

            /*$this->info('Creating view dir');

            // Creating view folder
            $viewDirPath = 'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.$viewDir.DIRECTORY_SEPARATOR;

            try
            {
                $result = \File::makeDirectory($viewDirPath, 0775);
                $this->info('View directory created');

            }
            catch (\Exception $exception)
            {
                $this->error('error occurred while creating view directory');
            }

            $this->info('generating views');

            // creating views
            $sampleViewDir = 'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'sample'.DIRECTORY_SEPARATOR;*/

            try
            {
                /*$contents = \File::get($sampleViewDir.'index.blade.php');

                $contents = str_replace('Sample', $moduleName, $contents);
                $contents = str_replace('sample', strtolower($moduleName), $contents);
                $contents = str_replace('slug', $slug, $contents);

                $bytes_written = \File::put($viewDirPath.'index.blade.php', $contents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('View blade has been generated: '.$viewDirPath.'index.blade.php');*/

                // creating vue components
                $componentDirPath = 'resources'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$viewDir.DIRECTORY_SEPARATOR;

                try
                {
                    $result = \File::makeDirectory($componentDirPath, 0775);
                    $this->info('Component directory created');

                }
                catch (\Exception $exception)
                {
                    $this->error('error occurred while creating component directory');
                }

                $sampleComponentDir = 'resources'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'samples'.DIRECTORY_SEPARATOR;

                $IndexContents = \File::get($sampleComponentDir.'SampleIndex.vue');
                $IndexContents = str_replace('Sample', $moduleName, $IndexContents);
                $IndexContents = str_replace('sample', strtolower($moduleName), $IndexContents);
 
                $bytes_written = \File::put($componentDirPath.$moduleName.'Index.vue', $IndexContents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Index component has been generated: '.$componentDirPath.$moduleName.'Index.vue');

                $CreateContents = \File::get($sampleComponentDir.'SampleCreate.vue');
                $CreateContents = str_replace('Sample', $moduleName, $CreateContents);
                $CreateContents = str_replace('sample', strtolower($moduleName), $CreateContents);
 
                $bytes_written = \File::put($componentDirPath.$moduleName.'Create.vue', $CreateContents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Create component has been generated: '.$componentDirPath.$moduleName.'Create.vue');

                $EditContents = \File::get($sampleComponentDir.'SampleEdit.vue');
                $EditContents = str_replace('Sample', $moduleName, $EditContents);
                $EditContents = str_replace('sample', strtolower($moduleName), $EditContents);
 
                $bytes_written = \File::put($componentDirPath.$moduleName.'Edit.vue', $EditContents);

                if ($bytes_written === false)
                {
                    $this->error('Error writing to file');
                }

                $this->info('Edit component has been generated: '.$componentDirPath.$moduleName.'Edit.vue');

            }
            catch (\Exception $exception)
            {
                $this->error('Sample JS not found');
            }
        }
        else
        {

        }
    }
}
