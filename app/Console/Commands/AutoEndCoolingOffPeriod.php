<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Library\Services\PaymentService;
use Illuminate\Http\Request;

class AutoEndCoolingOffPeriod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:end_cooling_off_period';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'auto cancel cooling off period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PaymentService::autoEndCoolingOffPeriod();
    }
}
