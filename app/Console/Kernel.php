<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AppointmentScheduleReminder::class,
        Commands\AppointmentReminder::class,
        Commands\GenerateInvoice::class,
        Commands\AutoCancelSubscription::class,
        Commands\AutoSendRepeatBloodTestForm::class,
        Commands\AutoEndCoolingOffPeriod::class,
        Commands\AutoCompleteAssessment::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:appointment_schedule_reminder')->daily();
        $schedule->command('command:appointment_reminder')->everyTenMinutes();
        $schedule->command('command:generate_invoice')->monthlyOn(3, '15:00');
        $schedule->command('command:cancel_subscriptions')->daily();
        $schedule->command('command:auto_send_rbt_form')->daily();
        $schedule->command('command:end_cooling_off_period')->everyTenMinutes();
        $schedule->command('command:auto_complete_assessment')->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
