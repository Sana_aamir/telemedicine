<?php
  
namespace App\Library\Helper;
use Illuminate\Http\Request;
use App\Models\Postcode;
use App\Models\Patient;
use App\Models\Doctor;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Cashier;
class Utility
{
    public static function selectedNav($modules)
    {

       $class = '';
       $currentPath = \Route::getFacadeRoot()->current()->uri();
       foreach ($modules as $key => $module) {
        if($module == $currentPath)
        {
           return 'active';
        }
       else
           $class = '';
       }
       return $class;
    } 

    public static function classActivePath($path)
    {
    	$path = explode(',', $path);
    	if(in_array(\Route::getFacadeRoot()->current()->uri(), $path))
    		return ' active';
    	else
    		return '';
    }

    public static function paginator($data, $paginate, $limit) {

        $data['pagination'] = [];
        $data['pagination']['to'] = $paginate->firstItem();
        $data['pagination']['from'] = $paginate->lastItem();
        $data['pagination']['total'] = $paginate->total();
        $data['pagination']['current_page'] = $paginate->currentPage();
        $data['pagination']['first_page'] = 1;
        $data['pagination']['last_page'] = $paginate->lastPage();
        $data['pagination']['per_page'] = $limit;

        // return data
        return $data;
    }

    public static function classActiveSegment($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? ' active' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return ' active';
        }
        return '';
    }

	public static function getYearsDiff($startDate, $endDate)
	{
		if(empty($endDate))
			$endDate = date('Y-m-d');
		$interval = date_diff(date_create($endDate), date_create($startDate));

		echo $interval->format("%Y yr %M mo");
	}
	
	public static function appSettings() {
        $is_member = 0;
        $patient_id = 0;
        $doctor_id = 0;
        if (Auth::user()) {
            if (Auth::user()->roles->first()->name == 'patient') {
                $patient = Patient::where('user_id', '=', Auth::user()->id)->first();
                if (!empty($patient)) {
                    if ($patient->subscribed('main')) {
                        $is_member = 1;
                    } else {
                        Patient::where('user_id', '=', Auth::user()->id)->update(['is_member' => '0']);
                    }
                    $patient_id = $patient->id;
                }
               
            } else if (Auth::user()->roles->first()->name == 'doctor') {
                $doctor = Doctor::where('user_id', '=', Auth::user()->id)->first();
                if (!empty($doctor)) {
                    $doctor_id = $doctor->id;
                }
            }
        }
		$settings = [
				'baseUrl' => url('/'),
            	'apiUrl' => url('/').'/api/',
            	'user' => Auth::user(),
                'role' => (Auth::user())?Auth::user()->roles->first()->name:'',
            	'user_picture'=>(Auth::user())?Auth::user()->getProfilePicture(Auth::user()):'',
                'is_member' => $is_member,
                'patient_id' => $patient_id,
                'doctor_id' => $doctor_id,

        ];
		return $settings;
	}

    public static function fileInfo($filePath)
    {
        $pathinfo = pathinfo($filePath);
        $file = array();
        $file['name'] = $pathinfo['basename'];
        $file['extension'] = $pathinfo['extension'];
        $file['mimetype'] = mime_content_type ($pathinfo['dirname'] .DIRECTORY_SEPARATOR. $pathinfo['basename']);
        $file['size'] = self::formatSizeUnits(filesize($pathinfo['dirname'] .DIRECTORY_SEPARATOR. $pathinfo['basename']));
        return $file;
    }

    // Snippet from PHP Share: http://www.phpshare.org
    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function calculateAge($dob) {
        $from = new \DateTime($dob);
        $to   = new \DateTime('today');
        if ($from->diff($to)->y == 0) {
            $age = $from->diff($to)->m.' month(s)';
        } else {
            $age = $from->diff($to)->y.' year(s)';
        }
        return  $age;
    }

    public static function getBetweenDates($from, $to,  $format = "Y-m-d") {
        $begin = new \DateTime($from);
        $end = new \DateTime($to);
       
        $daterange = new \DatePeriod($begin, new \DateInterval('P1D'), $end);
        $dateArr = [];
        foreach($daterange as $val){
            $dateArr[] = $val->format($format);
            
        }
        
        $dateArr[] = date($format, strtotime($to));

        return $dateArr;
    }

    public static function getDateForSpecificDayBetweenDates($startDate,$endDate,$day){
        $endDate = strtotime($endDate);
        $date_array = [];
        $days=array('Monday'=>'Monday','Tuesday' => 'Tuesday','Wednesday' => 'Wednesday','Thursday'=>'Thursday','Friday' =>'Friday','Saturday' => 'Saturday','Sunday'=>'Sunday');
        for($i = strtotime($days[$day], strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i))
        $date_array[]=date('Y-m-d',$i);

        return $date_array;
    }

    public static function createTimeSlots($starttime, $endtime, $duration, $format = false) {
        //$starttime = '9:00';  // your start time
        //$endtime = '21:00';  // End time
        //$duration = '30';  // split by 30 mins
        if ($endtime === '12:00 am (midnight next day)') {
            $endtime = '11:59 pm';
        }
        $array_of_time = array ();
        $start_time    = strtotime ($starttime); //change to strtotime
        $end_time      = strtotime ($endtime); //change to strtotime
        $add_mins  = $duration * 60;
        while ($start_time <= $end_time) // loop between time
        {
            if ($format) {
                $array_of_time[date ("H:i:s", $start_time)] = date ("h:i a", $start_time);
            } else {
                $array_of_time[date ("H:i", $start_time)] = date ("h:i", $start_time);
            }
          
           $start_time += $add_mins; // to check endtie=me
        }

        return $array_of_time;
    }

    public static function encryptDecrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'Happy Clinic';
        $secret_iv = 'Happy Clinic';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;      
    }

    public static function timeago($time, $tense='ago') 
    {
        // declaring periods as static function var for future use
        static $periods = array('year', 'month', 'day', 'hr', 'min', 'second');        // checking time format
        if(!(strtotime($time)>0)) {
            return trigger_error("Wrong time format: '$time'", E_USER_ERROR);
        }        // getting diff between now and time
        $now  = new \DateTime('now');
        $time = new \DateTime($time);
        $diff = $now->diff($time)->format('%y %m %d %h %i %s');
        // combining diff with periods
        $diff = explode(' ', $diff);
        $diff = array_combine($periods, $diff);
        // filtering zero periods from diff
        $diff = array_filter($diff);
        // getting first period and value
        $period = key($diff);
        $value  = current($diff);        // if input time was equal now, value will be 0, so checking it
        if(!$value) {
            $period = 'seconds';
            $value  = 0;
        } else {
            // converting days to weeks
            if($period=='day' && $value>=7) {
                $period = 'week';
                $value  = floor($value/7);
            }
            // adding 's' to period for human readability
            if($value>1) {
                $period .= 's';
            }
        }        // returning timeago
        return "$value $period $tense";
    }

    public static function captchaVerification($captchaResponse){
        $secret = env('CAPTCHA_SECRET', '6LdtBHcUAAAAABCvsm6W7dh-cf9LGo004rtXyzTU');
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captchaResponse);
        $responseData = json_decode($verifyResponse);
        if($responseData->success) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_ip_address(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }


    public static function getCountries()
    {
        return array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");       
    }

    public static function generateInvoiceId($prefix, $invoiceId, $leadingZeros = 4) {
        
        $data = $prefix.str_pad($invoiceId, $leadingZeros, "0", STR_PAD_LEFT);
        return $data;
    }

}


?>