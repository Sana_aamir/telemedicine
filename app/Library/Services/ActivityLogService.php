<?php
namespace App\Library\Services;
use App\Library\Contracts\ActivityLogServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Patient;
use App\Models\Doctor;
use App\Models\Appointment;
use App\Models\LabForm;
use App\Models\RepeatBloodTest;
use App\Models\Membership;
use App\Models\Email;
use App\Models\Support;
use App\Models\ActivityLog;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class ActivityLogService implements ActivityLogServiceInterface
{

    public function getAll($request) {

        $input = $request->all();

        $loggedInUserId = Auth::user()->id;
        if (isset($input['start_date']) && $input['start_date'] != '') {
            $start_date = date('Y-m-d', strtotime($input['start_date']));
        } else {
            $start_date = '';
        }

        if (isset($input['end_date']) && $input['end_date'] != '') {
            $end_date = date('Y-m-d', strtotime($input['end_date']));
        } else {
            $end_date = date('Y-m-d');
        }

        $objectIds = ActivityLog::orderBy('created_at', 'DESC');

        if (isset($input['filter_by_module']) && $input['filter_by_module'] != '') {
            $objectIds = $objectIds->where('module','=',$input['filter_by_module']);
        }

        if (isset($input['filter_by_user']) && $input['filter_by_user'] != 0) {
            $objectIds = $objectIds->where('user_id','=',$input['filter_by_user']);
        }
        
        if ($start_date != '') {
            $objectIds = $objectIds->whereDate('created_at', '>=', $start_date)->whereDate('created_at', '<=', $end_date);
        }
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $logs = $objectIds->get();
        }
        else 
        {
            $objectIdsPaginate = $objectIds->paginate($input['limit']);
            $logs = $objectIdsPaginate->items();

        }

        $data = ['data'=>[]];
        $total = count($logs);
        $data['total'] = $total;
        
        if ($total > 0) {
            $i = 0;
            $userService = new UserService;
            foreach ($logs as $log) {
                $userName = '';
                $userRole = '';
                $activity = '';
                /*if ($log->user_id == $loggedInUserId) {
                    $userName = '<strong>You</strong> have ';
                } else {*/
                    $userData =  $userService->get('id', $log->user_id, true);

                
                    if ($userData != NULL) {
                        $userName = '<strong>'.$userData->first_name.' '.$userData->last_name.'</strong> has ';
                        $userRole = $userData->role;
                   }
                /* }*/
                
                if ($log->module == 'roles') {
                    $roleData = $this->role_model->withTrashed()->find($log->log_id);
                    if ($roleData != NULL) {
                        if ($log->log_type == 'updated_status') {
                            $activity = $userName.'<strong>'.$log->text.'</strong> '.$roleData->name.' <strong>role</strong>.';
                        } else {
                            $activity = $userName.'<strong>'.$log->log_type.'</strong> '.$roleData->name.' <strong>role</strong>.';
                        }
                    }
                } else if ($log->module == 'users') {
                    $userData = User::find($log->log_id);
                    if ($userData != NULL) {
                        if ($log->log_type == 'updated_status') {
                            $activity = $userRole.' '.$userName.'<strong>'.$log->text.'</strong> user <strong>'.$userData->first_name.' '.$userData->last_name.' </strong>.';
                        } else if ($log->log_type == 'mobile_number_change') {
                            if ($log->user_type == 'superadmin') {
                                $activity =  $userRole.' '.$userName.'changed user <strong>'.$userData->first_name.' '.$userData->last_name.'</strong> current mobile number <strong>('.$log->text.')</strong> to a new mobile number <strong>('.$userData->mobile_number.')</strong>';
                            } else {
                                $activity =  $userRole.' '.$userName.'changed his/her current mobile number <strong>('.$log->text.')</strong> to a new mobile number <strong>('.$userData->mobile_number.')</strong>';
                            }
                        } else {
                            $activity = $userRole.' '.$userName.'<strong>'.$log->log_type.'</strong> user <strong>'.$userData->first_name.' '.$userData->last_name.' </strong>.';
                        }
                    }
                
                } else if ($log->module == 'patient') {
                    $patientData = Patient::find($log->log_id);

                    if ($patientData != NULL) {
                        if ($log->log_type == 'address_change') {
                            if ($userData->role == 'Superadmin') {
                                $patientUser = User::find($patientData->user_id);
                                $activity =  $userRole.' '.$userName.'have changed patient <strong>'.$patientData->title.'.'.$patientUser->first_name.' '.$patientUser->first_name.'</strong> current home address <strong>('.$log->text.')</strong> to a new address <strong>('.$patientData->address_first_line.', '.$patientData->city.', '.$patientData->country.', '.$patientData->postcode.')</strong>';
                            } else {
                                $activity =  $userRole.' '.$userName.'have changed his/her current home address <strong>('.$log->text.')</strong> to a new address <strong>('.$patientData->address_first_line.', '.$patientData->city.', '.$patientData->country.', '.$patientData->postcode.')</strong>';
                            }
                            
                        } else {
                            $activity = $userRole.' '.$userName.'<strong>'.$log->log_type.'</strong> user <strong>'.$userData->first_name.' '.$userData->last_name.' </strong>.';
                        }
                    }   
                } else if ($log->module == 'doctor') {
                    $docData = Doctor::find($log->log_id);
                    if ($docData != NULL) {
                        
                        $activity = $userRole.' '.$userName.'<strong>'.$log->log_type.'</strong> user <strong>'.$userData->first_name.' '.$userData->last_name.' </strong>.';
                    }   
                } else if ($log->module == 'user_profile') {
                    if ($log->log_type == 'login') {
                       
                        $activity = $userRole.' '.$userName.'<strong>'.$log->log_type.'</strong> to the system. with ip address <strong>'.$log->text.'</strong>';
                    } else if ($log->log_type == 'logout') {
                        $activity = $userRole.' '.$userName.'<strong>'.$log->log_type.'</strong> from the system.';
                    } else if ($log->log_type == 'change_password') {
                        $activity = $userRole.' '.$userName.' updated his account\'s <strong>password</strong>.';
                    } else if ($log->log_type == 'updated') {
                        $activity = $userRole.' '.$userName.' updated his account\'s <strong>personal info</strong>.';
                    }
                } 
                $data['data'][$i]['id'] = $log->id;
                $data['data'][$i]['activity'] = $activity;
                $data['data'][$i]['created_at'] = date('d M, Y h:i a', strtotime($log->created_at));            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }
        return $data;

    }  
}


?>