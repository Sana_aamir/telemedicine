<?php
namespace App\Library\Services;
use App\Library\Contracts\MentalHealthHospitalServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\MentalHealthHospital;
use App\Models\Patient;
use App\Models\Appointment;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class MentalHealthHospitalService implements MentalHealthHospitalServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->name = $request->name;
        $data->address_first_line = $request->address_first_line;
        $data->city = $request->city;
        $data->country = $request->country;
        $data->postcode = $request->postcode;
        $data->telephone_number = $request->telephone_number;
        $data->email = $request->email;
        return $data;
    }

    public function save($request)
    {
        $rec = new MentalHealthHospital();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = MentalHealthHospital::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = MentalHealthHospital::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        return MentalHealthHospital::where($col, $value)->first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new MentalHealthHospital;

        if (isset($input['search_by_name'])) {
            $objects = $objects->where('name','LIKE','%'.$input['search_by_name'].'%');
        }

        if (isset($input['search_by_city_postcode'])) {
            $objects = $objects->where(function($query) use ($input)
                                               {
                                                $query->orwhere('city','LIKE','%'.$input['search_by_city_postcode'].'%')
                                                    ->orwhere('postcode','LIKE','%'.$input['search_by_city_postcode'].'%');
                                               });
        }

        if (isset($input['filter_by_doctor_id'])) {
            $patientIds = Appointment::where('doctor_id', '=', $input['filter_by_doctor_id'])->get(['patient_id']);
            $hospitalIds = Patient::whereIn('id', $patientIds)->get(['mental_hospital_address_id']);
            $objects = $objects->whereIn('id', $hospitalIds);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function importExcel($request)
    {
        if($request->hasFile('import_file')){
            \Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {
                    if (count($row) > 0) {
                        foreach ($row as $k => $v) {
                            $hosiptalExist = MentalHealthHospital::where('email', '=', $v['email'])->first();
                            if (empty($hosiptalExist)) {
                                $data = [];
                                $data['name'] = $v['name'];
                                $data['address_first_line'] = $v['address_first_line'];
                                $data['city'] = $v['city'];
                                $data['country'] = $v['country'];
                                $data['postcode'] = $v['postcode'];
                                $data['telephone_number'] = $v['telephone_number'];
                                $data['email'] = $v['email'];
                                if(!empty($data)) {
                                    MentalHealthHospital::insert($data);
                                }
                            }
                        }
                    } 
                }
            });
        }    

        return true;
    }

}


?>