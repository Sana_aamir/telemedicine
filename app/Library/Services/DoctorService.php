<?php
namespace App\Library\Services;
use App\Library\Contracts\DoctorServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Doctor;
use App\Models\DoctorDocument;
use App\Models\Specialty;
use App\Models\Appointment;
use App\Models\Patient;
use App\Models\DoctorHoliday;
use App\Models\DoctorSchedule;
use App\Models\FlaggedMessage;
use App\Models\AppointmentSession;
use App\Models\Clinical;
use App\Models\ClinicalDoctorAnswer;
use App\Models\LabForm;
use App\Models\LabFormDetail;
use App\Models\Note;
use App\Models\Support;
use App\Models\SupportMessage;
use App\Models\SupportMessageDoc;
use App\Models\ActivityLog;

use App\Mail\SendDocument;
use App\Mail\PasswordRequest;
use App\Library\Helper\ActivityLogManager;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class DoctorService implements DoctorServiceInterface
{
    public $userService;
    public $specaltyService;
    public $signatureImagePath;
    public $doctorDocumentsPath;
    function __construct()
    {
        $this->userService = new UserService;
        $this->specialtyService = new SpecialtyService;
        $this->signatureImagePath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'signature_images'.DIRECTORY_SEPARATOR;
        $this->doctorDocumentsPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'doctor_documents'.DIRECTORY_SEPARATOR;
    }

    public function propertySetter($request, $data)
    {
        $data->user_id = $request->input('user_id');
        $data->specialty_id = $request->input('specialty_id');
        $data->title = $request->input('title');
        $data->telephone_number = $request->input('telephone_number');
        $data->gmc_number = $request->input('gmc_number');
        $data->home_address = $request->input('home_address');
        $data->work_address = $request->input('work_address');
        $data->dob = date('Y-m-d', strtotime($request->input('dob')));
        $data->qualification = $request->input('qualification');
        $data->designation = $request->input('designation');
        $data->description = $request->input('description');
        $data->national_insurance_number = $request->input('national_insurance_number');
        $data->tax_code = $request->input('tax_code');
        $data->bank_name = $request->input('bank_name');
        $data->sort_code = $request->input('sort_code');
        $data->account_number = $request->input('account_number');
        $data->employment_start_date = date('Y-m-d', strtotime($request->input('employment_start_date')));
        $data->employment_end_date = date('Y-m-d', strtotime($request->input('employment_end_date')));
        $data->appraisal_date = date('Y-m-d', strtotime($request->input('appraisal_date')));
        $data->revalidation_date = date('Y-m-d', strtotime($request->input('revalidation_date')));
        if (!empty($request->input('signature_image'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('signature_image'), $this->signatureImagePath.$request->input('signature_image'));
        }
        $data->signature_image = $request->input('signature_image', '');
        return $data;
    }

    public function save($request)
    {
        // create user first
        $userService = new UserService;
        $userData = $request->user;
        $userData = (object) $userData;
        $userData->type = $request->type;
        $user = $userService->save($userData);
        if ($user) {
            $rec = new Doctor();
            $request->merge(['user_id' => $user->id]);
            $rec = $this->propertySetter($request, $rec);
            $rec->save();
            if (isset($request->doctor_documents)) {
                $this->saveDoctorDocuments($request->doctor_documents, $rec->id);
            }
            $resetToken = md5(time());
            $userService->saveResetCode($user, $resetToken);
            // Send email to user
            $emailData = array('name' => $user->first_name.' '.$user->last_name,
                               'action_url' => \URL::to('/request-password/'.$resetToken),
                               'subject' => 'Account Created',
                               'template' => 'register_email'
                                );
            try{
                \Mail::to($user->email)->send(new PasswordRequest($emailData));
            }catch(\Exception $e){

            }
            return $rec->id;
        } else {
            return false;
        }
    }
    
    public function delete($id)
    {
        $rec = Doctor::find($id);
        if(!empty($rec))
        {
            DoctorDocument::where('doctor_id', '=', $rec->id)->delete();
            DoctorHoliday::where('doctor_id', '=', $rec->id)->delete();
            DoctorSchedule::where('doctor_id', '=', $rec->id)->delete();
            FlaggedMessage::where('user_id', '=', $rec->user_id)->delete();

            $appointments = Appointment::where('doctor_id', '=', $rec->id);
            $getAppointments = $appointments->get(['id']);
            AppointmentSession::whereIn('appointment_id', $getAppointments)->delete();

            $clinical = Clinical::whereIn('appointment_id', $getAppointments);
            $getClinical = $clinical->get(['id']);
            ClinicalDoctorAnswer::whereIn('clinical_id', $getClinical)->delete();
            $appointments->delete();
            $clinical->delete();

            $labforms = LabForm::where('doctor_id', '=', $rec->id);
            $getLabForms = $labforms->get(['id']);
            LabFormDetail::whereIn('lab_form_id', $getLabForms)->delete();
            $labforms->delete();

            Note::where('created_by','=' ,$rec->user_id)->orwhere('created_on','=' ,$rec->user_id)->delete();

            $support = Support::where('sender_id','=' ,$rec->user_id)->orwhere('receiver_id','=' ,$rec->user_id);
            $getSupport = $support->get(['id']);
            $supportMessages = SupportMessage::whereIn('support_id', $getSupport);
            $getSupportMessages = $supportMessages->get(['id']);
            SupportMessageDoc::whereIn('support_message_id', $getSupportMessages)->delete();
            $supportMessages->delete();
            $support->delete();

            ActivityLog::where('user_id','=' ,$rec->user_id)->delete();
            User::where('id', '=', $rec->user_id)->delete();
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Doctor::find($id);
        if ($rec) {
            $userData = $request->user;
            $userData['module_id'] = $rec->user_id;
            $userService = new UserService;
            $user = $userService->update((object) $userData);
            $rec = $this->propertySetter($request, $rec);
            $rec->update(); 
            if (isset($request->doctor_documents)) {
                $this->updateDoctorDocuments($request->doctor_documents, $rec->id);
            }      
            return true;
        } else {
            return false;
        } 
    }

    public function get($col, $value, $details = false, $input = [])
    {
        $doctor =  Doctor::where($col, $value)->first();
        if ($details) {
            $doctor->user = $this->userService->get("id", $doctor->user_id, true);
            $doctor->specialty = $this->specialtyService->get("id", $doctor->specialty_id);
            $doctor->doctor_documents = $this->getDoctorDocuments($doctor->id);
            $doctor->doctor_documents_path =  \URL::to('storage/doctor_documents/'.$doctor->id);
            $doctor->age = Utility::calculateAge($doctor->dob);
            if(!empty($doctor->user->profile_pic))
                $doctor->profile_pic_path =  \URL::to('storage/profile_pic/'.$doctor->user->profile_pic);
            else
                $doctor->profile_pic_path =  asset('assets/img/avatar2.png');


            if(!empty($doctor->signature_image))
                $doctor->signature_image_path =  \URL::to('storage/signature_images/'.$doctor->signature_image);
            else
                $doctor->signature_image_path =  '-';

            $doctor->appointments = [];
            $appointments = \DB::table('appointments')
                     ->select('appointment_type_id', \DB::raw('count(*) as total'))
                     ->where('doctor_id', '=', $doctor->id) 
                     ->where('status', '=', 'completed') 
                     ->groupBy('appointment_type_id');
                     
            

            if (!empty($input['filter_by_date_assessments'])) {
                if ($input['filter_by_date_assessments'][0] != 'null') {
                    $startDate = date('Y-m-d', strtotime($input['filter_by_date_assessments'][0]));
                    $endDate = date('Y-m-d', strtotime($input['filter_by_date_assessments'][1]));
                    $appointments = $appointments->where(\DB::raw("date(date_time)"), '>=', $startDate)->where(\DB::raw("date(date_time)"), '<=', $endDate);
                }
                
            }

            $appointments = $appointments->get();
            $appData = array('initial_assessment' => 0, 'followup_type1' => 0,
                                'followup_type2' => 0, 'telephone_advice' => 0);
            if (!empty($appointments)) {
                foreach ($appointments as $key => $value) {
                    if ($value->appointment_type_id == 1) {
                        $appData['initial_assessment'] = $value->total;
                    } else if ($value->appointment_type_id == 2) {
                        $appData['followup_type1'] = $value->total;
                    } else if ($value->appointment_type_id == 3) {
                        $appData['followup_type2'] = $value->total;
                    } else if ($value->appointment_type_id == 4) {
                        $appData['telephone_advice'] = $value->total;
                    }
                }
                
            } 
            $doctor->appointments = $appData;
        }
        return $doctor;
    }

    public function getAll($request)
    {
        $input = $request->all();
        $objects = Doctor::orderBy('id', 'desc');

        if (isset($input['filter_by_specialty'])) {
            $objects = $objects->where('specialty_id', '=', $input['filter_by_specialty']);
        }

        if (isset($input['filter_by_status'])) {
            $users = User::where('is_archived', '=', $input['filter_by_status'])->get(['id']);
            $objects = $objects->whereIn('user_id',$users);
        }

        if (isset($input['filter_by_patient_id'])) {
            $doctors = Appointment::where('patient_id', '=', $input['filter_by_patient_id'])->get(['doctor_id']);
            $objects = $objects->whereIn('id', $doctors);
        }

        if (!empty($input['keyword'])) {
            $objects = $objects->whereIn('user_id',  function($query) use ($input)
                                               {
                                                $query->select('id')
                                                    ->from('users')
                                                    ->orwhere('first_name','LIKE','%'.$input['keyword'].'%')
                                                    ->orwhere('last_name','LIKE','%'.$input['keyword'].'%');
                                               });
        }
        
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }


        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;

            foreach ($objects as $object) {
                $objectData = $this->get('id', $object->id, true, $input);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function saveDoctorDocuments($doctor_documents, $id)
    {
        if (!empty($doctor_documents)) {
            $doctorDocsPath = $this->doctorDocumentsPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($doctorDocsPath)) {
                mkdir($doctorDocsPath, 0777, true);
            }
            
            try{
                $mediaService = new MediaService();
                foreach ($doctor_documents as $key => $doc) {
                    $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->doctorDocumentsPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                   $newDoc = new DoctorDocument;
                   $newDoc->filename = $doc['name'];
                   $newDoc->doctor_id = $id;
                   $newDoc->save();
                }
                
                return true;
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }
        }
    }

    public function updateDoctorDocuments($doctor_documents, $id)
    {

        
        if (!empty($doctor_documents)) {
            $doctorDocsPath = $this->doctorDocumentsPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($doctorDocsPath)) {
                mkdir($doctorDocsPath, 0777, true);
            }
            // get previous docs
            $previousReports = $this->getDoctorDocuments($id);

            // get the file name which are not in new array
            //$recToDelete = array_diff($previousReports, $doctor_documents);
            $recToDelete = array_map('unserialize',
                array_diff(array_map('serialize', $previousReports), array_map('serialize', $doctor_documents))
            );
             DoctorDocument::whereIn('filename', $recToDelete)->delete();
            
            // get new records
            //$recToAdd = array_diff($doctor_documents, $previousReports);
            $recToAdd = array_map('unserialize',
                array_diff(array_map('serialize', $doctor_documents), array_map('serialize', $previousReports))
            );

            if (!empty($recToAdd)) {
                try{
                    $mediaService = new MediaService();
                    foreach ($recToAdd as $key => $doc) {
                        $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->doctorDocumentsPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                       
                       $newDoc = new DoctorDocument;
                       $newDoc->filename = $doc['name'];
                       $newDoc->doctor_id = $id;
                       $newDoc->save();
                    }
                    
                    return true;
                } catch(\Exception $e){
                    //dd($e->getMessage());
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function getDoctorDocuments($doctor_id) {
        $doctor_documents  = DoctorDocument::where("doctor_id", $doctor_id)->get();
        $data = [];
        if (!empty($doctor_documents)) {
            foreach ($doctor_documents as $key => $value) {
                $data[$key]['name'] = $value->filename; 
                $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($value->created_at));
            }
        }
        return $data;
    }

    public function updateBlockLogin($request)
    {
        $rec = Doctor::find($request->id);
        if ($rec) {
            User::where('id', '=', $rec->user_id)->update(['is_blocked_login' => $request->is_blocked_login]);     
            return true;
        } else {
            return false;
        } 
    }

    public function doctorFieldUpdate($column, $value, $id) {
        $doctor = Doctor::find($id);
        if (!empty($doctor)) {
            $doctor->$column = $value;
            $doctor->update();
        } else {
            return false;
        }
    }

    public function getDoctorAssessmentsForMonth($firstDate, $lastDate, $doctor_id) {

        $appData = array('initial_assessment' => ['member' => 0, 'non_member' => 0],      
                        'follow_up_assessment_type_1' => ['member' => 0, 'non_member' => 0], 
                        'follow_up_assessment_type_2' => ['member' => 0, 'non_member' => 0], 
                        'telephone_advice' => ['member' => 0, 'non_member' => 0]
                    );

        $appointments = \DB::table('appointments')
                 ->where('doctor_id', '=', $doctor_id) 
                 ->where('status', '=', 'completed') 
                 ->where(\DB::raw("date(date_time)"), '>=', $firstDate)->where(\DB::raw("date(date_time)"), '<=', $lastDate)
                 ->get();

        if (!empty($appointments)) {
            foreach ($appointments as $key => $value) {
                $patientInfo = Patient::where('id', '=', $value->patient_id)->first();
                if (!empty($patientInfo)) {
                    if ($value->appointment_type_id == 1) {
                        if ($patientInfo->is_member == 0) {
                            $appData['initial_assessment']['non_member'] += 1;  
                        } else if ($patientInfo->is_member == 1) {
                            $appData['initial_assessment']['member'] += 1;
                        }
                    } else if ($value->appointment_type_id == 2) {
                        if ($patientInfo->is_member == 0) {
                            $appData['follow_up_assessment_type_1']['non_member'] += 1;  
                        } else if ($patientInfo->is_member == 1) {
                            $appData['follow_up_assessment_type_1']['member'] += 1;
                        }
                    } else if ($value->appointment_type_id == 3) {
                       if ($patientInfo->is_member == 0) {
                            $appData['follow_up_assessment_type_2']['non_member'] += 1;  
                        } else if ($patientInfo->is_member == 1) {
                            $appData['follow_up_assessment_type_2']['member'] += 1;
                        }
                    } else if ($value->appointment_type_id == 4) {
                        if ($patientInfo->is_member == 0) {
                            $appData['telephone_advice']['non_member'] += 1;  
                        } else if ($patientInfo->is_member == 1) {
                            $appData['telephone_advice']['member'] += 1;
                        }
                    }
                }
            }
        } 

        return $appData;
    }

    public function sendDocuments($request) {
        //p($request->all());
        $doctor = $this->get('id', $request->doctor_id, true);
        $patientService = new PatientService;
        $patient = $patientService->get('id', $request->patient_id,true);

        if (!empty($request->documents)) {
            
            $tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $docs = [];
            foreach ($request->documents as $key => $value) {
                $docs[$key]['file'] = $value;
                $docs[$key]['file_path'] = $tmpPath.$value;
            }

            try {
                $emailData = array ('sender' => $doctor,
                                    'receiver' => $patient,
                                    'docs' => $docs, 
                            );
                \Mail::to($emailData['receiver']->user->email)->send(new SendDocument($emailData));
                //\Mail::to('sana.aamir1909@gmail.com')->send(new SendDocument($emailData));
                
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }

            $docs = [];
            foreach ($request->documents as $key => $value) {
                $docs[$key]['name'] = $value;
                $docs[$key]['created_at'] = date('Y-m-d H:i:s');
            }

            $patientService->saveAttachments($docs, $request->patient_id);
            return true;
        }
    }

    public function getNextDoctor($col, $val) {
        $data = [];
        $data['nextDoctor'] = '';
        $data['previousDoctor'] = '';
        $nextDoctorId = Doctor::where($col, '>', $val)->min('id'); 
        $previousDoctorId =  Doctor::where($col, '<', $val)->max('id');
        if (!empty($nextDoctorId)) {
            $data['nextDoctor'] = $this->get('id', $nextDoctorId, true);
        }

        if (!empty($previousDoctorId)) {
           $data['previousDoctor'] = $this->get('id', $previousDoctorId, true);
        }

        return $data;
    }

}


?>