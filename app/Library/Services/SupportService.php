<?php
namespace App\Library\Services;
use App\Library\Contracts\SupportServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Doctor;
use App\Models\Patient;
use App\Models\Appointment;
use App\Models\Support;
use App\Models\SupportMessage;
use App\Models\SupportMessageDoc;
use App\Models\FlaggedMessage;
use App\Mail\SendSupportRequest;
use App\Helpers\ActivityLogManager;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class SupportService implements SupportServiceInterface
{
    public $supportDocPath;
    public $userService;

    function __construct()
    {
        $this->userService = new UserService;
        $this->supportDocPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'support_docs'.DIRECTORY_SEPARATOR;
    }

    public function propertySetter($request, $data)
    {
        $data->category = $request->category;
        $data->subject = $request->subject;
        $data->sender_id = $request->sender_id;
        $data->receiver_id = $request->user_id;
        if (isset($request->status)) {
            $data->status = $request->status;
        }
        return $data;
    }

    public function save($request)
    {
        if ($request->email_type == 'individual') {
            $this->saveIndividualSupportMessage($request);
        } else if ($request->email_type == 'group') 
            {
            $objects = [];
            if ($request->user_type == 'all_doctors') 
            {
                $objects = Doctor::orderBy('id', 'desc');
                if (!empty($request->filter_by_specialty)) 
                    $objects = $objects->where('specialty_id', '=',$request->filter_by_specialty);

                $objects = $objects->get(['id', 'user_id']);
            } else if ($request->user_type == 'all_patients') 
            {
                $objects = Patient::orderBy('id', 'desc');
                if (!empty($request->filter_by_specialty)) 
                {
                    $patientIds = Appointment::where('specialty_id', '=',$request->filter_by_specialty)->get(['patient_id']);
                    $objects = $objects->whereIn('id', $patientIds);
                }
                $objects = $objects->get(['id', 'user_id']);
            } else if ($request->user_type == 'all_patients_member') 
                {
                $objects = Patient::where('is_member','=', '1')->orderBy('id', 'desc');
                if (!empty($request->filter_by_specialty)) 
                {
                    $patientIds = Appointment::where('specialty_id', '=',$request->filter_by_specialty)->get(['patient_id']);
                    $objects = $objects->whereIn('id', $patientIds);
                }
                $objects = $objects->get(['id', 'user_id']);
            } else if ($request->user_type == 'all_patients_non_member') 
                {
                $objects = Patient::where('is_member','=', '0')->orderBy('id', 'desc');
                if (!empty($request->filter_by_specialty)) 
                {
                    $patientIds = Appointment::where('specialty_id', '=',$request->filter_by_specialty)->get(['patient_id']);
                    $objects = $objects->whereIn('id', $patientIds);
                }
                $objects = $objects->get(['id', 'user_id']);
            }

            if (count($objects) > 0) {
                foreach ($objects as $key => $value) {
                    $request->merge(['user_id' => $value->user_id]);
                    $this->saveIndividualSupportMessage($request);
                }
            }
        }
        return true;
    }

    public function saveIndividualSupportMessage($request) {
        $rec = new Support();
        if ($request->user_type == 'admin') {
            $request->merge(['user_id' =>  User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first()
                        ]);
        }
        //$request->merge(['sender_id' => \Auth::user()->id]);
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        $rec->slug = Utility::encryptDecrypt('encrypt', $rec->id);
        $rec->update();
        // save message
        $request->merge(['support_id' => $rec->id,
                         'receiver_id' => $request->user_id,
                        ]);
        $msg = $this->saveSupportMessage($request);
        return true;
    }
    
    public function delete($id)
    {
        $rec = Support::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Support::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $support = Support::where($col, $value)->first();

        if ($detail) {
            $support->sender = $this->userService->get('id', $support->sender_id, true);
            $support->receiver = $this->userService->get('id', $support->receiver_id, true);
            $support->messages = [];
            $support->timeago = Utility::timeago($support->created_at);
            $support->category = ($support->category == 'changes_messages') ? 'Changes' : ucfirst($support->category);
            //$support->is_updated = $this->supportHasUnreadMsg($support->id);
            $support->is_updated = $support->is_updated;

        }  
        return $support;

    }

    public function getSupportMessages($support_id) 
    {
        $messages = SupportMessage::where('support_id', '=', $support_id)->get();
        if (!empty($messages)) {
            foreach ($messages as $key => $value) {
                $value->docs = [];
                $value->sender = $this->userService->get('id', $value->sender_id, true);
                $value->receiver = $this->userService->get('id', $value->receiver_id, true);
                $value->docs = $this->getMessageDoc($support_id, $value->id);
                $value->timeago = Utility::timeago($value->created_at);
                $admin_id = User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first();
                $value->is_admin_msg = 0;
                if ($value->sender_id == $admin_id) {
                    $value->is_admin_msg = 1;
                }
            }
        }
        return $messages;
    }

    public function getSupportMessage($support_message_id, $print = false) 
    {
        $message = SupportMessage::where('id', '=', $support_message_id)->first();
        if (!empty($message)) {
            $message->docs = [];
            $message->sender = $this->userService->get('id', $message->sender_id, true);
            $message->receiver = $this->userService->get('id', $message->receiver_id, true);
            $message->docs = $this->getMessageDoc($message->support_id, $message->id);
            $message->timeago = Utility::timeago($message->created_at);
            $message->support = $this->get('id', $message->support_id);
            $message->is_flagged = 0;
            if (!$print) { 
                $flagged  = FlaggedMessage::where('support_message_id', '=', $support_message_id)->where('user_id', '=', \Auth::user()->id)->first();
                if (!empty($flagged)) {
                    $message->is_flagged = 1;
                }
            }
            
            if ($message->receiver_id == \Auth::user()->id && $message->is_read == 0) {             
                SupportMessage::where('id', '=', $support_message_id)->update(['is_read' => 1]);
                Support::where('id', '=', $message->support_id)->update(['is_updated' => 0 , 'updated_by' => 0]);
            }
            
            
            $message->encryptedId = Utility::encryptDecrypt('encrypt', $message->id);

        }
        return $message;
    }

    public function getMessageDoc($support_id, $support_message_id) {
        $docData = [];
        $docs = SupportMessageDoc::where('support_message_id', '=', $support_message_id)->get();

        if (!empty($docs)) {
            foreach ($docs as $k => $v) {
                $msgDocs = new \StdClass;
                $msgDocs->file = $v->file;
                $msgDocs->file_path = \URL::to('storage/support_docs/'.$support_id.'/'.$v->file);
                $fileInfo = pathinfo($msgDocs->file_path);
                $msgDocs->extension = $fileInfo['extension'];
                $msgDocs->created_at = date('Y-m-d H:i:s', strtotime($v->created_at));
                $docData[] = $msgDocs; 
            }
        }
        return $docData;
    }

    public function getAllMergedData($request) {
        $data = ['data'=>[]];
        if(isset($request->page) && $request->page == 1)
        {
            $updatedData = $this->getAllUpdated($request);
            $otherData = $this->getAll($request);
            $mergedResult =  array_merge($updatedData['data'],$otherData['data']);
            
            $data['data'] = $mergedResult;
            
        } else {
            $otherData = $this->getAll($request);
            $data['data'] = $otherData['data'];
        }

        $data['pagination'] = $otherData['pagination'];
        
       
        return $data;

    }

    public function getAllUpdated($request) {
        $input = $request->all();

        $objects = new Support;

        $objects = $objects->where('is_updated','=', 1)->where('updated_by','!=', \Auth::user()->id)->where('updated_by', '!=', 0)->orderBy('id', 'desc');

        if (!empty($input['filter_by_category'])) {
            $objects = $objects->where('category','=', $input['filter_by_category']);
        } else {
            $objects = $objects->where('category','!=', 'complaint');
        }

        if (!empty($input['filter_by_user_id'])) {
            $objects = $objects->where(function ($query) use ($input) {
                                $query->orwhere('sender_id','=', $input['filter_by_user_id'])
                                      ->orwhere('receiver_id','=', $input['filter_by_user_id']);
                        });
        }

        if (!empty($input['keyword'])) {
            $users = User::where(function($query) use ($input)
                                           {
                                            $query
                                                ->orwhere('first_name','LIKE','%'.$input['keyword'].'%')
                                                ->orwhere('last_name','LIKE','%'.$input['keyword'].'%');
                                           })->get(['id']);
             $objects = $objects->where(function ($query) use ($users) {
                                $query->orWhereIn('sender_id',$users)
                                      ->orWhereIn('receiver_id',$users);
                        });
        }

        $objects = $objects->get(['id']);

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        return $data; 
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new Support;

        if (!empty($input['filter_by_category'])) {
            $objects = $objects->where('category','=', $input['filter_by_category']);
        } else {
            $objects = $objects->where('category','!=', 'complaint');
        }

        if (!empty($input['filter_by_user_id'])) {
            $objects = $objects->where(function ($query) use ($input) {
                                $query->orwhere('sender_id','=', $input['filter_by_user_id'])
                                      ->orwhere('receiver_id','=', $input['filter_by_user_id']);
                        });
        }

        /*if (!empty($input['filter_by_is_updated'])) {
            $objects = $objects->orderBy('is_updated', 'desc')->orderBy('id', 'desc');
            
        } else {
            $objects = $objects->orderBy('id', 'desc');
        }*/

        if (!empty($input['keyword'])) {
            $users = User::where(function($query) use ($input)
                                           {
                                            $query
                                                ->orwhere('first_name','LIKE','%'.$input['keyword'].'%')
                                                ->orwhere('last_name','LIKE','%'.$input['keyword'].'%');
                                           })->get(['id']);
             $objects = $objects->where(function ($query) use ($users) {
                                $query->orWhereIn('sender_id',$users)
                                      ->orWhereIn('receiver_id',$users);
                        });
        }

        $objects = $objects->where(function ($query) {
                        $query->where('is_updated','=', 0)
                                ->orWhere(function($query2){
                                    $query2->where('is_updated','=', 1)
                                            ->where('updated_by','=', \Auth::user()->id);
                                });   
                    })->orderBy('id', 'desc');
        

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function saveSupportMessage($request) {
        $msg = new SupportMessage;
        $msg->support_id = $request->support_id;
        $msg->sender_id = $request->sender_id;
        $msg->receiver_id = $request->receiver_id;
        $msg->message = $request->message;
        $msg->is_read = 0;
        if ($msg->save()) {
            $admin_id = User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first();
            Support::where('id', '=', $request->support_id)->update(['is_updated' => 1, 'updated_by' => $request->sender_id]);

            
            $request->merge(['message_id' => $msg->id ]);
            $saveDoc = $this->saveSupportDoc($request);
            $supportMessage = $this->getSupportMessage($msg->id, false);

            //$supportData = $this->get('id', $request->support_id, true);
            try {
                $action_url = '';   
                if ($supportMessage->receiver->role == 'Patient') {
                    $action_url = \URL::to('/support/view/'.$supportMessage->support->slug);
                } else if ($supportMessage->receiver->role == 'Doctor') {
                    $action_url = \URL::to('/support-doctor/view/'.$supportMessage->support->slug);
                } else {
                    $action_url = \URL::to('/admin/support/view/'.$supportMessage->support->slug);
                }
                $emailData = array ('sender' => $supportMessage->sender,
                                'receiver' => $supportMessage->receiver,
                                'subject' => $supportMessage->support->subject, 
                                'message' => $request->message,
                                'category' => ucfirst($supportMessage->support->category),
                                'docs' => $this->getMessageDoc($request->support_id, $msg->id), 
                                'action_url' => $action_url,
                            );
                \Mail::to($supportMessage->receiver->email)->send(new SendSupportRequest($emailData));
                //\Mail::to('sana.aamir1909@gmail.com')->send(new SendSupportRequest($emailData));
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }
            return $msg;
        } else {
            return false;
        }
    }

    public function saveSupportDoc($request) {
        if (!empty($request->support_docs)) {
            $supportDocPath = $this->supportDocPath.DIRECTORY_SEPARATOR.$request->support_id.DIRECTORY_SEPARATOR;
            if (!is_dir($supportDocPath)) {
                mkdir($supportDocPath, 0777, true);
            }


            try{
                $mediaService = new MediaService();
                foreach ($request->support_docs as $key => $doc) {
                    if ($request->support_type == 'forward') {
                        $mediaService->moveFile($this->supportDocPath.$request->support_forward_id.DIRECTORY_SEPARATOR.$doc, $this->supportDocPath.$request->support_id.DIRECTORY_SEPARATOR.$doc, false);
                    } else {
                        $mediaService->moveFile($mediaService->tmpPath.$doc, $this->supportDocPath.$request->support_id.DIRECTORY_SEPARATOR.$doc);
                    }

                    $msgDoc = new SupportMessageDoc;
                    $msgDoc->support_message_id = $request->message_id;
                    $msgDoc->file = $doc;
                    $msgDoc->save();
                }
                
                return true;
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }
        }
    }

    public function updateStatus($request, $id)
    {
        $rec = Support::find($id);
        if (!empty($rec)) {
            $rec->status = $request->status;
            $rec->update();        
            return true;
        } else {
            return false;
        }
        
    }

    public function sendReply($request)
    {
        $rec = Support::find($request->support_id);

        if (!empty($rec)) {
            $loggedInUser = \Auth::user();

            if ($loggedInUser->id == $rec->sender_id) {
                $sender_id = $loggedInUser->id;
                $receiver_id = $rec->receiver_id;
            } else if ($loggedInUser->id == $rec->receiver_id) {
                $sender_id = $loggedInUser->id;
                $receiver_id = $rec->sender_id;
            }
            $request->merge(['sender_id' => $sender_id, 'receiver_id' => $receiver_id]);
            $this->saveSupportMessage($request); 
            return true;
        } else {
            return false;
        }
        
    }

    public function getAllMessages($request)
    {
        $input = $request->all();

        $objects = SupportMessage::orderBy('id', 'desc');

        if (!empty($input['filter_by_support_id'])) {
            $objects = $objects->where('support_id', '=', $input['filter_by_support_id']);
        }
        if (!empty($input['filter_by_slug'])) {
            $objects = $objects->whereIn('support_id',  function($query) use ($input)
                                           {
                                            $query->select('id')
                                                ->from('support')
                                                ->where('slug','=', $input['filter_by_slug']);
                                           }); 

            //Support::where('slug', '=', $input['filter_by_slug'])->where('receiver_id', '=', \Auth::user()->id)->update(['is_updated' => 0]);
        }

        if (!empty($input['filter_by_logged_in_user_id'])) {
            $objects = $objects->where(function ($query) use ($input) {
                                $query->orwhere('sender_id','=', \Auth::user()->id)
                                      ->orwhere('receiver_id','=', \Auth::user()->id);
                        });
        } else {
            $objects = $objects->where(function ($query) use ($input) {
                                $query->orwhere('sender_id','=', \Auth::user()->id)
                                      ->orwhere('receiver_id','=',\Auth::user()->id);
                        });
        }

        if (!empty($input['filter_by_other_user_id'])) {
            $objects = $objects->where(function ($query) use ($input) {
                                $query->orwhere('sender_id','=', $input['filter_by_other_user_id'])
                                      ->orwhere('receiver_id','=', $input['filter_by_other_user_id']);
                        });
        }


        if (!empty($input['filter_by_flag'])) {
            $flaggeMsg = FlaggedMessage::where('user_id', $input['filter_by_logged_in_user_id'])->get(['support_message_id']);
            $objects = $objects->whereIn('id', $flaggeMsg);
        }

        if (!empty($input['keyword'])) {
            $objects = $objects->where('message','LIKE','%'.$input['keyword'].'%');
        }

        if (!empty($input['filter_by_not_complaints'])) {
            $objects = $objects->whereIn('support_id',  function($query) use ($input)
           {
            $query->select('id')
                ->from('support')
                ->where('category','!=', 'complaint');
           });
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->getSupportMessage($object->id, false);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function deleteMessage($id)
    {
        $rec = SupportMessage::find($id);
        if(!empty($rec))
        {
            SupportMessageDoc::where('support_message_id', '=', $rec->id)->delete();
            FlaggedMessage::where('support_message_id', '=', $rec->id)->delete();
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function toggleFlag($id)
    {
        $rec = FlaggedMessage::where('support_message_id', '=', $id)->where('user_id', '=', \Auth::user()->id)->first();
        if(!empty($rec))
        {   
            $rec->delete();
        }
        else
        {
            $new = new FlaggedMessage;
            $new->support_message_id = $id;
            $new->user_id = \Auth::user()->id;
            $new->save();
        }

        return true;
    }

    public function supportHasUnreadMsg($support_id) {
        $is_updated =  SupportMessage::where('receiver_id', '=', \Auth::user()->id)->where('support_id', '=', $support_id)->where('is_read', '=', 0)->count();
        if ($is_updated > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /*public function countNewSupportMessage() {
        $supportIDs = Support::where('category', '!=', 'complaint')->get(['id']);
       
        return SupportMessage::where('receiver_id', '=', \Auth::user()->id)->where('is_read', '=', 0)->whereIn('support_id', $supportIDs)->distinct('support_id')->count('support_id');
    }

    public function countNewComplaintMessage() {
        $supportIDs = Support::where('category', '=', 'complaint')->get(['id']);
        return SupportMessage::where('receiver_id', '=', \Auth::user()->id)->where('is_read', '=', 0)->whereIn('support_id', $supportIDs)->distinct('support_id')->count('support_id');

    }*/

    public function countNewSupportMessage() {
        
        return Support::where('category', '!=', 'complaint')->where('is_updated', '=', 1)->where('updated_by', '!=', \Auth::user()->id)->where('updated_by', '!=', 0)->where(function ($query){
                                $query->orwhere('sender_id','=', \Auth::user()->id)
                                      ->orwhere('receiver_id','=', \Auth::user()->id);
                        })->count();

    }

    public function countNewComplaintMessage() {
      return Support::where('category', '=', 'complaint')->where('is_updated', '=', 1)->where('updated_by', '!=', \Auth::user()->id)->where('updated_by', '!=', 0)->where(function ($query) {
                                $query->orwhere('sender_id','=', \Auth::user()->id)
                                      ->orwhere('receiver_id','=', \Auth::user()->id);
                        })->count();

    }


}


?>