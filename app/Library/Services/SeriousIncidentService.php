<?php
namespace App\Library\Services;
use App\Library\Contracts\SeriousIncidentServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\SeriousIncident;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class SeriousIncidentService implements SeriousIncidentServiceInterface
{

    public $docPath;

    function __construct()
    {
        $this->docPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'incidents_docs'.DIRECTORY_SEPARATOR;
    }

    public function propertySetter($request, $data)
    {
        $data->name = $request->input('name');
        $data->date = $request->input('date');
        if (!empty($request->input('document'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('document'), $this->docPath.$request->input('document'));
        }
        $data->document = $request->input('document', '');
        return $data;
    }
    public function save($request)
    {
        $rec = new SeriousIncident();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = SeriousIncident::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = SeriousIncident::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $incident = SeriousIncident::where($col, $value)->first();
        if (!empty($incident)) {
             if(!empty($incident->document))
                $incident->document_path =  \URL::to('storage/incidents_docs/'.$incident->document);
            else
                $incident->document_path =  '-';
        }

        return $incident;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new SeriousIncident;
        if (!empty($input['keyword'])) {
            $objects = $objects->where('name','LIKE','%'.$input['keyword'].'%');
        }

        if (isset($input['filter_by_is_archived'])) {
            $objects = $objects->where('is_archived', '=', $input['filter_by_is_archived']);
        }
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function updateStatus($request)
    {
        $incident = SeriousIncident::find($request->id);
        if (!empty($incident)) {
            $incident->is_archived =  $request->status;
            $incident->update();
            return true;
        } else {
            return false;
        }
    }

}


?>