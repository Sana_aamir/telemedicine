<?php
namespace App\Library\Services;
use App\Library\Contracts\TestimonialServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Testimonial;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class TestimonialService implements TestimonialServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->user_id = $request->input('user_id');
        $data->text = $request->input('text');
        $data->rating = $request->input('rating');
        $data->status = $request->input('status');
        return $data;
    }

    public function save($request)
    {
        $rec = new Testimonial();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Testimonial::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Testimonial::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $testimonial =  Testimonial::where($col, $value)->first();
        if (!empty($testimonial)) {
            $patientService = new PatientService;
            $testimonial->patient = $patientService->get('user_id', $testimonial->user_id, true);
        }
        return $testimonial;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = Testimonial::orderBy('id', 'desc');

        if (isset($input['filter_by_status'])) {
            $objects = $objects->where('status', '=', $input['filter_by_status']);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function updateStatus($request)
    {
        $rec = Testimonial::find($request->id);
        $rec->status = $request->status;
        $rec->update();        
        return true;
    }

}


?>