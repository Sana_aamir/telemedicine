<?php
namespace App\Library\Services;
use App\Library\Contracts\LabFormServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\LabForm;
use App\Models\LabFormDetail;
use App\Models\Laboratory;
use App\Mail\LabFormEmail;
use Carbon\Carbon;
use App\Library\Helper\ActivityLogManager;
use App\Library\Helper\Utility;
use Illuminate\Http\Request;

class LabFormService implements LabFormServiceInterface
{

    public function propertySetter($request, $data)
    {

        $data->doctor_id = $request['doctor_id'];
        $data->patient_id = $request['patient_id'];
        $data->appointment_id = $request['appointment_id'];
        $data->sent_date = $request['sent_date'];
        $data->laboratory_address_id = $request['laboratory_address_id'];
        return $data;
    }

    public function save($request)
    {
        if(!is_array($request)){
            $request = $request->all();
        }

        $rec = new LabForm();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        $form_number = Utility::generateInvoiceId('HC1', $rec->id, 5);
        LabForm::where('id','=', $rec->id)->update(['form_number' => $form_number]);
        $this->saveFormDetails($request, $rec->id);
        $newRequest = new \StdClass;
        $newRequest->id = $rec->id;
        $newRequest->repeat = false;
        $newRequest->single = true;
        $this->sendLabForm($newRequest);
        return $rec->id;
    }

    public function saveFormDetails($request, $labFormId){
        
        $patientForms = LabForm::where('patient_id', '=', $request['patient_id'])->get(['id']);

        /*$previousDetails = LabFormDetail::whereIn('lab_form_id', $patientForms)->get();
        if (!empty($previousDetails)){
            foreach ($previousDetails as $key => $value) {
                if ($value->status == 'single' || $value->status == 'repeat') {
                    $detail = LabFormDetail::find($value->id);
                    $detail->status = 'stopped';
                    $detail->started_at_date = null;
                    $detail->stopped_at_date = date('Y-m-d');
                    $detail->save();
                }
            }
        }*/
        $previousDetails = LabFormDetail::whereIn('lab_form_id', $patientForms)->where(function ($query) {
                                                $query->orwhere('status','=', 'single')
                                                    ->orwhere('status','=', 'repeat');
                                            })->update(['status' => 'stopped', 'stopped_at_date' => date('Y-m-d')]);
       
        if (!empty($request['details'])){
            foreach ($request['details'] as $key => $value) {
                if ($value['status'] != 'stopped') {
                    $rec = new LabFormDetail();
                    $rec->lab_form_id = $labFormId;
                    $rec->test = $value['test'];
                    $rec->sample = $value['sample'];
                    $rec->additional_info = $value['additional_info'];
                    $rec->status = $value['status'];
                    $rec->started_at_date = date('Y-m-d');
                    $rec->save();
                }
                
            }
        }

        return true;
    }

    public function updateFormDetails($request, $labFormDetailId){
        
        $rec = LabFormDetail::find($labFormDetailId);
        if (!empty($rec->details)){
            $rec->lab_form_id = $request->lab_form_id;
            $rec->test = $request->test;
            $rec->sample = $request->sample;
            $rec->additional_info = $request->additional_info;
            $rec->status = $request->status;
            if ($request->status == 'single' || $request->status == 'repeat') {
                $rec->started_at_date = date('Y-m-d');
            } else {
                $rec->stopped_at_date    = date('Y-m-d');
            }
            
            $rec->update();
        }

        return true;
    }
    
    public function delete($id)
    {
        $rec = LabForm::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    /*public function update($request, $id)
    {
        $rec = LabForm::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }*/

    public function get($col, $value,  $repeat = false, $single = false)
    {
        $lab =  LabForm::where($col, $value)->first();
        if (!empty($lab)) {
            $doctorService = new DoctorService;
            $lab->doctor = $doctorService->get('id', $lab->doctor_id, true);
            $patientService = new PatientService;
            $lab->patient = $patientService->get('id', $lab->patient_id, true);
            $lab->laboratory_address = Laboratory::where('id', $lab->laboratory_address_id)->first();
            $lab->intitials = substr($lab->doctor->user->first_name, 0, 1).substr($lab->doctor->user->last_name, 0, 1);
            if ($repeat == 'true') { // repeat blood test form
                $lab->details = $this->getPatientRepeatLabForm($lab->patient_id);
            } else if ($single == 'true') {
                $lab->details = LabFormDetail::where('lab_form_id', '=', $lab->id)->where(function                      ($query) {
                                                $query->orwhere('status','=', 'single')
                                                    ->orwhere('status','=', 'repeat');
                                            })->get();
            } else {
                $lab->details = LabFormDetail::where('lab_form_id', '=', $lab->id)->get();
            }
            $lab->total_items = count($lab->details);
            $lab->encodedId = Utility::encryptDecrypt('encrypt', $lab->id);
        }

        return $lab;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = LabForm::orderBy('id','desc');
        if (isset($input['filter_by_patient_id'])) {
            $objects = $objects->where('patient_id', '=',  $input['filter_by_patient_id']);
        }

        if (isset($input['filter_by_doctor_id'])) {
            $objects = $objects->where('doctor_id', '=',  $input['filter_by_doctor_id']);
        }

        if (!empty($input['filter_by_date_range'])) {
            if ($input['filter_by_date_range'][0] != 'null') {
                $startDate = date('Y-m-d', strtotime($input['filter_by_date_range'][0]));
                $endDate = date('Y-m-d', strtotime($input['filter_by_date_range'][1]));
                $objects = $objects->where(\DB::raw("date(created_at)"), '>=', $startDate)->where(\DB::raw("date(created_at)"), '<=', $endDate)->orderBy('created_at', 'asc');
            } 
        } 
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, false);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function sendLabForm($request) {
        if (isset($request->single)) {
            $lab  = $this->get('id',$request->id, $request->repeat,$request->single );
        } else {
            $lab  = $this->get('id',$request->id, $request->repeat);
        }
        if (!empty($lab)) {
            $tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            if (file_exists($tmpPath.'labform-'.$lab->form_number.'.pdf')) {
                unlink($tmpPath.'labform-'.$lab->form_number.'.pdf');
            }
            $view = \View::make('backend.print.labform', ['data' => $lab]);
            $html = $view->render();
            
            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
                                    'format' => 'A4',
                                    'margin_left' => 0,
                                    'margin_right' => 0,
                                    'margin_top' => 0,
                                    'margin_bottom' => 0,
                                    'margin_header' => 0,
                                    'margin_footer' => 0,
                                    'debug' => true,
                                    'tempDir' => $tmpPath]);

            $mpdf->writeHTML($html);
            $mpdf->output($tmpPath.'labform-'.$lab->form_number.'.pdf', \Mpdf\Output\Destination::FILE);
            try {
                $emailData = array (
                    'subject' => 'Happy Clinic Lab Form',
                    'lab_name' => $lab->patient->laboratory_address->name,
                    'attachement' => $tmpPath.'labform-'.$lab->form_number.'.pdf' 
                );
                //\Mail::to('sana.aamir1909@gmail.com')->send(new LabFormEmail($emailData));
                \Mail::to($lab->patient->laboratory_address->email)->send(new LabFormEmail($emailData));
                LabForm::where('id', '=', $lab->id)->update(['is_emailed' => '1']);
                return true;
            } catch(\Exception $e) {
            }
        }   
        return false;
    }

    public function getPatientRepeatLabForm($patient_id) {
        $labforms = LabForm::where('patient_id', '=', $patient_id)->orderBy('id', 'desc')->get(['id']);
        $repeatlabs = LabFormDetail::whereIn('lab_form_id', $labforms)->where('status', '=', 'repeat')->get();

        return $repeatlabs;
    }

    public function getPatientLatestForm($patient_id) {
        return LabForm::where('patient_id', '=', $patient_id)->orderBy('id', 'desc')->first();
    }

    
}


?>