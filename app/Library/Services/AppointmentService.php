<?php
namespace App\Library\Services;
use App\Library\Contracts\AppointmentServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AppointmentType;
use App\Models\AppointmentFee;
use App\Models\AppointmentReminderSchedule;
use App\Models\Patient;
use App\Models\ReminderEmail;
use App\Models\AppointmentSession;
use App\Models\AppointmentDuration;
use App\Mail\AppointmentScheduleReminder; 
use App\Mail\AppointmentReminder; 
use App\Mail\AppointmentConfirmation; 
use App\Mail\AppointmentUpdate; 
use App\Mail\AppointmentCancelled; 
use App\Mail\InvitationLinkEmail; 
use App\Library\Helper\ActivityLogManager;
use OpenTok\OpenTok;
use OpenTok\Session;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\OutputMode;
use OpenTok\Role;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class AppointmentService implements AppointmentServiceInterface
{

    public $appointment_frequency;
    public $doctorService;
    public $patientService;
    public $opentok;

    function __construct()
    {
        $this->appointment_frequency = '30';
        $this->doctorService = new DoctorService;
        $this->patientService = new PatientService;
        /*if (!empty(env('TOKBOX_API_KEY')) && !empty(env('TOKBOX_SECRET'))) {
            $this->opentok = new OpenTok(env('TOKBOX_API_KEY'), env('TOKBOX_SECRET'));
        } else{
            $this->opentok = NULL;
        }*/
        $this->opentok = new OpenTok('46227052', 'ab8532c3ee9a8ecc48c1c3bbe1bc625a181a1aa5');
    }

    public function propertySetter($request, $data)
    {
        $data->appointment_type_id = $request->appointment_type_id;
        $data->specialty_id = $request->specialty_id;
        $data->doctor_id = $request->doctor_id;
        $data->patient_id = $request->patient_id;
        $data->date_time = date('Y-m-d H:i:s', strtotime($request->date_time));
        if (isset($request->status)) {
            $data->status = $request->status;
        }

        if (isset($request->stripe_charge_id)) {
            $data->stripe_charge_id = $request->stripe_charge_id;
        }

        if (isset($request->fee)) {
            $data->fee = $request->fee;
        }
        
        return $data;
    }

    public function save($request)
    {
        // add apointment details
        $rec = new Appointment();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        $rec->encrypted_id = Utility::encryptDecrypt('encrypt', $rec->id);
        $rec->update();
        if ($request->appointment_type_id == '1') { // inital assessment
            // update patient hospital and lab
            Patient::where('id', '=', $request->patient_id)->update(['mental_hospital_address_id' => $request->mental_hospital_address_id, 'laboratory_address_id' => $request->laboratory_address_id, 'sharing_consent' => $request->sharing_consent]);
            // save Questionaire
            $questionnaireService = new QuestionnaireService;
            $questionnaireService->savePatientQuestionaire($request);
        }

        // Update cooling off period 
        $paymentService = new PaymentService;
        $updatePeriod = $paymentService->updateCoolingOffPeriod($request->patient_id);
        if ($updatePeriod) {
            // email for appointment confirmation
            $appointmentData = $this->get('id', $rec->id, true);
            try {
                // send email to patient
                $emailData = array ('appointment' => $appointmentData,
                    'subject' => 'Appointment Confirmation',
                    'user_type' => 'patient', 
                );
                \Mail::to($appointmentData->patient->user->email)->send(new AppointmentConfirmation($emailData));

                // send appointment confirmation sms to patient
                $smsService = new SMSService;
                $message = $smsService->smsTemplates('appointment_Confirmation_patient');
                $message = str_replace('[DATE]', date('d M, Y', strtotime($appointmentData->date_time)), $message);
                $message = str_replace('[TIME]', date('h:i a', strtotime($appointmentData->date_time)), $message);
                $message = str_replace('[LINK]', 'www.happyclinic.co.uk', $message);
                $sms = $smsService->sendSMS($appointmentData->patient->user->mobile_number, $message);

                // send email to doctor
                $emailData['user_type'] = 'doctor';

                \Mail::to($appointmentData->doctor->user->email)->send(new AppointmentConfirmation($emailData));
                //\Mail::to('sana.aamir1909@gmail.com')->send(new AppointmentConfirmation($emailData));

                // send appointment confirmation sms to doctor
                $message = $smsService->smsTemplates('appointment_Confirmation_doctor');
                $message = str_replace('[PATIENT_INTITIALS]', ucfirst(substr($appointmentData->patient->user->first_name, 0, 1)).ucfirst(substr($appointmentData->patient->user->last_name, 0, 1)), $message);
                $message = str_replace('[DATE]', date('d M, Y', strtotime($appointmentData->date_time)), $message);
                $message = str_replace('[TIME]', date('h:i a', strtotime($appointmentData->date_time)), $message);
                $message = str_replace('[LINK]', 'www.happyclinic.co.uk', $message);
                $sms = $smsService->sendSMS($appointmentData->doctor->user->mobile_number, $message);
            } catch(\Exception $e) {
            }
            return true;
        } else {
            return false;
        }
       
        return true;
    }
    
    public function delete($id)
    {
        $rec = Appointment::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Appointment::find($id);
        $previousRec = $rec;
        $rec = $this->propertySetter($request, $rec);
        if (\Auth::user()->hasRole('patient')) {
            // edit appointment email
            // email for appointment update
            $appointmentData = $this->get('id', $rec->id, true);
            try {
                $doctor_change = 'no';
                if ($previousRec->doctor_id != $rec->doctor_id) {
                    $doctor_change = 'yes';
                }
                // send email to patient
                $emailData = array ('appointment' => $appointmentData,
                    'subject' => 'Appointment Updated',
                    'user_type' => 'patient',
                    'doctor_change' => $doctor_change,
                    'previous_date_time' => $previousRec->date_time, 
                );
                \Mail::to($appointmentData->patient->user->email)->send(new AppointmentUpdate($emailData));

                // send email to doctor
                $emailData['user_type'] = 'doctor';

                \Mail::to($appointmentData->doctor->user->email)->send(new AppointmentUpdate($emailData));

                if ($emailData['doctor_change'] == 'yes') {
                    $appointmentData = $this->get('id', $previousRec->id, true);
                    $emailData = array ('appointment' => $appointmentData,
                                        'subject' => 'Appointment Cancelled',
                                        'user_type' => 'doctor',
                                    );    
                    \Mail::to($appointmentData->doctor->user->email)->send(new AppointmentCancelled($emailData));
                }
            } catch(\Exception $e) {
            }
        } 
        $rec->update();       
        return true;
    }

    public function get($col, $value,  $detail = false, $filters = [])
    {   
        if (isset($filters)) {
            $app = Appointment::where($col, $value)->first();
        } else {
            $filters = $filters->all();
            $app = Appointment::where($col, $value)->where($filters['filter_column'], '=', $filters['filter_value'])->first();
        }
        if (!empty($app)) {
            if ($detail) {
                $app->appointment_type = AppointmentType::where('id', $app->appointment_type_id)->first();
                $app->doctor = $this->doctorService->get('id', $app->doctor_id, true);
                $app->patient = $this->patientService->get('id', $app->patient_id, true);
                $assessmentTime = date('Y-m-d H:i:s', strtotime('-5 minutes', strtotime($app->date_time)));

                if (date('Y-m-d H:i:s') >= $assessmentTime) {
                    $app->isStartTime = true;
                } else {
                    $app->isStartTime = false;
                }
            }
        }

        return $app;
    }

    public function getAll($request)
    {    
        $input = $request->all();

        /*$objects = Appointment::orderBy('id','desc');*/
        $objects = new Appointment;

        if (!empty($input['filter_by_type'])) {
            $objects = $objects->where('appointment_type_id','=',$input['filter_by_type']);
        }

        if (!empty($input['filter_by_doctor_id'])) {
            $objects = $objects->where('doctor_id', $input['filter_by_doctor_id']);
        }

        if (!empty($input['filter_by_specialty_id'])) {
            $objects = $objects->where('specialty_id', $input['filter_by_specialty_id']);
        }

        if (!empty($input['filter_by_patient_id'])) {
            $objects = $objects->where('patient_id', $input['filter_by_patient_id']);
        }

        if (isset($input['filter_by_is_member'])) {
            $patients = Patient::where('is_member', $input['filter_by_is_member'])->get(['id']);
            $objects = $objects->whereIn('patient_id', $patients);
        }

        if (!empty($input['filter_by_status'])) {
            $objects = $objects->where('status', $input['filter_by_status']);
        }
        
        if (!empty($input['filter_by_date_range'])) {
            if ($input['filter_by_date_range'][0] != 'null') {
                $startDate = date('Y-m-d', strtotime($input['filter_by_date_range'][0]));
                $endDate = date('Y-m-d', strtotime($input['filter_by_date_range'][1]));
                $objects = $objects->where(\DB::raw("date(date_time)"), '>=', $startDate)->where(\DB::raw("date(date_time)"), '<=', $endDate);
            } 
        } 

        if (!empty($input['from_date']) && !empty($input['to_date'])) {
            $start = date('Y-m-d', strtotime($input['from_date']));
            $end = date('Y-m-d', strtotime($input['to_date']));
            $objects = $objects->where(\DB::raw("date(date_time)"), '>=', $start)->where(\DB::raw("date(date_time)"), '<=', $end)->orderBy('date_time', 'asc');
        }

        if (!empty($input['filter_by_date'])) {
            $date = date('Y-m-d', strtotime($input['filter_by_date']));
            $objects = $objects->where(\DB::raw("date(date_time)"), $date);
        }


        if (isset($input['filter_by_is_archived'])) {
            $objects = $objects->where('is_archived', '=', $input['filter_by_is_archived']);
        }

        if (!empty($input['today'])) {
            $objects = $objects->where(\DB::raw("date(date_time)"), date('Y-m-d'));
        }

        if (!empty($input['order_by_field'])) {
            $objects = $objects->orderBy($input['order_by_field'], 'desc');
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }
        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {

                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    /*{
        'specialty_id' : 2,
        'doctor_id' : 3,
        'date_time' : '2018-12-21 12:00:00'
    }*/
    /*public function checkDoctorAvailibility($request) {
        $date_time = $request->date_time;

        $scheduleService = new DoctorScheduleService;
        $schedule = $scheduleService->getAvailChart($request, $request->doctor_id);
        

        $request->merge(['curr_month' => [
                            'month' => date('m', strtotime($date_time)),
                            'year' => date('Y', strtotime($date_time)),
                            ],
                        'doctor_schedule' => $schedule,
                         ]);
        $availData = $this->getAppointmentAvailabilityCal($request);
        $date = date('Y-m-d', strtotime($date_time));
        
        $avaiableDate = array_key_exists($date, $availData['avail_dates']);
        if ($avaiableDate) {
            $request->merge(['date_schedule' => $availData['date_schedule'] ]);
            //check time slot
            $timeSlots = $this->getAvailTimeSlots($request, $date);

            $time = date('H:i:s', strtotime($date_time));
            $avaiableTime = array_key_exists($time, $timeSlots['available_slots']);
            if ($avaiableTime) { 
                return 'available';
            } else {
                return 'not_available';
            }
        } else {
            return 'not_available';
        }
    }*/

    public function checkDoctorAvailibility($request) {

        
        $date_time = date('Y-m-d');

        $scheduleService = new DoctorScheduleService;
        $schedule = $scheduleService->getAvailChart($request, $request->doctor_id, $request->appointment_type_id);
        $checkScheduleExist = $scheduleService->checkSchduleSetOfDoctor($schedule);


        if ($checkScheduleExist) {

            $request->merge(['curr_month' => [
                            'month' => date('m', strtotime($date_time)),
                            'year' => date('Y', strtotime($date_time)),
                            ],
                        'doctor_schedule' => $schedule,
                         ]);

            $availData = $this->getAppointmentAvailabilityCal($request);
            reset($availData['avail_dates']);

            $firstAvailDate = key($availData['avail_dates']);
            
            $date = date('Y-m-d', strtotime($firstAvailDate));
            //check time slot
            $request->merge(['date_schedule' => $availData['date_schedule']]);
            $timeSlots = $this->getAvailTimeSlots($request, $date);
            if ($timeSlots) { 
                $earliestTime = date('d F, Y', strtotime($date)).' '.reset($timeSlots['available_slots']);
                return $earliestTime;
            } else {
                return false;
            }
        } else {
            return false;
        } 
    }


    public function getAppointmentAvailabilityCal($request, &$mainData = []) {
        //p($request->curr_month['year']);
        $mainData = [];
        $latestAppontment = $this->getLatestAppointment($request, 1);
        if (!empty($latestAppontment)) {
            $initialAssesmentDate = date('Y-m-d', strtotime($latestAppontment->date_time));
        } else {
            $initialAssesmentDate = '';
        }
        

        if ($request->curr_month['month'].'-'.$request->curr_month['year'] == date('m-Y')) {// if it is current month then date will start from current date
            $first_day_this_month = date('Y-m-d');
                
            if ($initialAssesmentDate >= date('Y-m-d')) {
                $first_day_this_month = date('Y-m-d', strtotime('+1 day', strtotime($initialAssesmentDate)));
            }
        } else if ($request->curr_month['month'].'-'.$request->curr_month['year'] == date('m-Y', strtotime($initialAssesmentDate))) {
            $first_day_this_month = date('Y-m-d', strtotime('+1 day', strtotime($initialAssesmentDate)));
        } else {
            $first_day_this_month = date($request->curr_month['year'].'-'.$request->curr_month['month'].'-01'); // hard-coded '01' for first day
        }

        $last_day_this_month  = date("Y-m-t", strtotime($first_day_this_month));
        $scheduleService = new DoctorScheduleService;
        $schedule = $scheduleService->getAvailChart($request, $request->doctor_id, $request->appointment_type_id);
        $checkScheduleExist = $scheduleService->checkSchduleSetOfDoctor($schedule);
        if ($checkScheduleExist) {
            $holidayService = new DoctorHolidayService;
            $doctor_holidays = $holidayService->getAllDoctorHolidays($first_day_this_month, $last_day_this_month, $request->doctor_id);

            $mainData['holidays'] = array_combine($doctor_holidays, $doctor_holidays);
            // get all appointments of the given month
            $objectIds = Appointment::where('doctor_id', '=', $request->doctor_id)->where('specialty_id', '=', $request->specialty_id)->whereBetween('date_time', array($first_day_this_month, $last_day_this_month))->where('status', '!=', 'cancelled')->get(['id']);

            $data = [];
            $dateSch = [];
            $dateSchFormated = [];
            if (count($objectIds) > 0) {
                foreach ($objectIds as $key => $value) {
                    $appData = $this->get('id', $value->id);
                    $datetime = new \DateTime($appData->date_time);
                    $dateSch[$datetime->format('Y-m-d')][$datetime->format('l')][] = $datetime->format('h:i');
                    $dateSchFormated[$datetime->format('Y-m-d')][$datetime->format('l')][] = $datetime->format('h:i a');
                }
            }

           
            $request->merge([
                        'date_schedule' => $dateSchFormated,
                         ]);


            $mainData['avail_dates'] = $this->getAvailableDate($first_day_this_month, $last_day_this_month, $schedule, $request);

            $mainData['avail_dates'] = array_diff_key($mainData['avail_dates'] , $mainData['holidays'] );

            $mainData['date_schedule'] = $dateSchFormated;
            $mainData['doctor_schedule'] = $schedule;
            
            $mm= date('m', strtotime($first_day_this_month));
            $yy= date('Y', strtotime($first_day_this_month));
            
            $mainData['curr_month']['month'] = $mm;
            $mainData['curr_month']['month_name'] = date('F', strtotime($first_day_this_month));
            $mainData['curr_month']['year'] = $yy;

            $mainData['next_month']['month'] = date( 'm', strtotime( '+1 month', strtotime($first_day_this_month) ) );
            $mainData['next_month']['month_name'] = date('F', strtotime('+1 month',strtotime($first_day_this_month)));
            $mainData['next_month']['year'] = date( 'Y', strtotime( '+1 month', strtotime($first_day_this_month) ) );

            $mainData['prev_month']['month'] = date( 'm', strtotime( '-1 month', strtotime($first_day_this_month) ) );
            $mainData['prev_month']['month_name'] = date('F',strtotime( '-1 month', strtotime($first_day_this_month) ) );
            $mainData['prev_month']['year'] = date( 'Y', strtotime( '-1 month', strtotime($first_day_this_month) ) );

            if (empty($mainData['avail_dates'])) {
                 // if no dates available for this month then get doctor next avaiable date and get data
                $request->merge(
                    array('curr_month' => 
                        array(
                            'month' => $mainData['next_month']['month'], 
                            'year' => $mainData['next_month']['year']
                            )
                        )
                    );
                $this->getAppointmentAvailabilityCal($request, $mainData);

            }
        } else {
            $mainData['avail_dates'] = [];
            $mainData['date_schedule'] = [];
            $mainData['doctor_schedule'] = [];
            $mainData['dateSch'] = [];
            $mm= date('m', strtotime($first_day_this_month));
            $yy= date('Y', strtotime($first_day_this_month));
            
            $mainData['curr_month']['month'] = $mm;
            $mainData['curr_month']['month_name'] = date('F', strtotime($first_day_this_month));
            $mainData['curr_month']['year'] = $yy;

            $mainData['next_month']['month'] = date( 'm', strtotime( '+1 month', strtotime($first_day_this_month) ) );
            $mainData['next_month']['month_name'] = date('F', strtotime('+1 month',strtotime($first_day_this_month)));
            $mainData['next_month']['year'] = date( 'Y', strtotime( '+1 month', strtotime($first_day_this_month) ) );

            $mainData['prev_month']['month'] = date( 'm', strtotime( '-1 month', strtotime($first_day_this_month) ) );
            $mainData['prev_month']['month_name'] = date('F',strtotime( '-1 month', strtotime($first_day_this_month) ) );
            $mainData['prev_month']['year'] = date( 'Y', strtotime( '-1 month', strtotime($first_day_this_month) ) );
        }

        

        
        

       return $mainData;
    }



    public function getAvailableDate($first_day_this_month, $last_day_this_month, $schedule, $request) {

        $data = [];
        $dates = Utility::getBetweenDates($first_day_this_month, $last_day_this_month, "Y-m-d");
        $result= $dates;

        $unavaiableScheduleDates = $this->getUnavailableDatesFromSchedule($first_day_this_month, $last_day_this_month, $schedule);
       
        $result1 = array_diff($result,$unavaiableScheduleDates); // remove schdule unavailable dates
        
        if (count($result1) > 0) {
            foreach ($result1 as $key => $value) {
                if ($value == date('Y-m-d')) {
                    $request->merge([
                        'doctor_schedule' => $schedule,
                         ]);
                    $available_slots = $this->getAvailTimeSlots($request, $value);
                    //p($available_slots);
                    if ($available_slots['available_slots']) {
                        $data[$value] = date('l d', strtotime($value));
                    }
                } else {
                    $data[$value] = date('l d', strtotime($value));
                }
                

            }
        }
        return $data;
    }

    public function getUnavailableDatesFromSchedule($start, $end, $schedule) {
        $unavaiableDates =  [];
        if (!empty($schedule)) {
            foreach ($schedule as $day => $value) {
                if (empty($value)) {
                    $unavaiableDates =  array_merge($unavaiableDates, Utility::getDateForSpecificDayBetweenDates($start,$end,$day)); 
                }
            }
        }
        return $unavaiableDates;
    }

    public function getAvailTimeSlots($request, $date) {
        $day = date('l', strtotime($date));
        $available_slots = [];
        //$unavailable_slots = [];
        $slots = [];
        //$allSlots = [];
        //p($request->doctor_schedule);
        if (!empty($request->doctor_schedule)) {
            if (isset($request->doctor_schedule[$day])) {

                $slots[$day] = [];
                $appointment_frequency = $this->getDuration($request->specialty_id, $request->appointment_type_id);
                foreach ($request->doctor_schedule[$day] as $k => $val) {

                    if ($date == date('Y-m-d')) {
                        /*$nextTwoHours = date('h:i a',strtotime('+2 hours', strtotime(date("h:00 a"))));

                        if ($nextTwoHoursTime < strtotime('11:59 pm')) {
                            $openingTime = strtotime($val['opening_time_name']);
                            if ($openingTime <= $nextTwoHoursTime) {
                                $val['opening_time_name'] = $nextTwoHours;
                            }

                            $slots[$day] =  array_merge($slots[$day], Utility::createTimeSlots($val['opening_time_name'], $val['closing_time_name'], $appointment_frequency, true));
                        }*/
                    } else {
                        $slots[$day] =  array_merge($slots[$day], Utility::createTimeSlots($val['opening_time_name'], $val['closing_time_name'], $appointment_frequency, true));
                    }
                }
            }

            //p($request->date_schedule);
            if (isset($request->date_schedule)) {
                if (isset($request->date_schedule[$date][$day])) {

                    //$unavailable_slots[$date] = array_intersect($slots[$day], $request->date_schedule[$date][$day]);
                    $available_slots = array_diff($slots[$day], $request->date_schedule[$date][$day]);
                } else {
                    $available_slots = $slots[$day];
                }
            } else {
                $available_slots = $slots[$day];
            } 
        }

        // for all slots of given date
        /*$curr_day  = date('l', strtotime($date));
        if (isset($allSlots[$date][$curr_day])) {
            $allSlots[$date] = $allSlots[$date][$curr_day];
        } else {
            $allSlots[$date] = [];
        }*/
        
        $data = [];
        //$data['unavailable_slots'] =  $unavailable_slots;
       
        array_pop($available_slots);
        $data['available_slots'] =  $available_slots;
        //$data['all_slots'] =  $allSlots;
        return $data;
    }

    public function updateStatus($request)
    {
        $appointment = Appointment::find($request->id);
        if (!empty($appointment)) {
            $appointment->is_archived =  $request->status;
            $appointment->update();
            return true;
        } else {
            return false;
        }
    }

    public function updateAppointmentStatus($request)
    {
        $appointment = Appointment::find($request->id);
        if (!empty($appointment)) {
            $appointment->status =  $request->status;
            
            if ($appointment->status == 'cancelled' && \Auth::user()->hasRole('patient')) {
                if (!$request->panelty && $appointment->stripe_charge_id != null) {
                    // deduct 20% and then refund
                    /*$deductCost = (20*$request->fee)/100;
                    $refundAmount = ($request->fee - $deductCost)*100;
                    $patient = Patient::find($appointment->patient_id);
                    $patient->refund($appointment->stripe_charge_id, array('amount' => $refundAmount));*/

                    // deduct admin fee and then refund
                    $busniessSettingService = new BusinessSettingService;
                    $setting = $busniessSettingService->get();
                    $amount = preg_replace('/\D/', '', $setting->admin_fee_percentage);
                    $deductAmount = (($amount/100)*($request->fee));
                    $refundAmount = ($request->fee - $deductAmount)*100;
                    $patient = Patient::find($appointment->patient_id);
                    $patient->refund($appointment->stripe_charge_id, array('amount' => $refundAmount));
                }

                try {
                    $appointmentData = $this->get('id', $appointment->id, true);
                    $emailData = array ('appointment' => $appointmentData,
                                        'subject' => 'Appointment Cancelled',
                                        'user_type' => 'doctor',
                                    );    
                    \Mail::to($appointmentData->doctor->user->email)->send(new AppointmentCancelled($emailData));
                    
                    $emailData['user_type'] = 'patient'; 
                    \Mail::to($appointmentData->patient->user->email)->send(new AppointmentCancelled($emailData));
                } catch(\Exception $e) {
                }
            }
            $appointment->update();
            if ($appointment->appointment_type_id == 1) { // initial assessment
                $exists = $this->checkFollowupExist($appointment);
            }

            return true;
        } else {
            return false;
        }
    }

    public function checkFollowupExist($appointment) {
        $appointments = Appointment::where('patient_id', '=', $appointment->patient_id)->where('specialty_id', '=', $appointment->specialty_id)->where('appointment_type_id','>',1)->where('status', '=', 'booked')->where('date_time', '>', $appointment->date_time)->get();

        $curr_date = date('Y-m-d H:i:s');
        if (!empty($appointments)) {
            foreach ($appointments as $key => $appointment) {
                
                $appointment_type = '0';
                if ($appointment->appointment_type_id == 1) {
                    $appointment_type = 'initial_assessment';
                } else if ($appointment->appointment_type_id == 2) {
                    $appointment_type = 'follow_up_assessment_type_1';
                } else if ($appointment->appointment_type_id == 3) {
                    $appointment_type = 'follow_up_assessment_type_2';
                } else if ($appointment->appointment_type_id == 4) {
                    $appointment_type = 'telephone_advice';
                }

                $appointmentData = $this->get('id', $appointment->id, true);
                $patient_type = 'non_member';
                if ($appointmentData->patient->is_member == 0) {
                    $patient_type = 'non_member';
                } else {
                    $patient_type = 'member';
                }
                $bookingRequest = new \Illuminate\Http\Request();
                $bookingRequest->replace(['appointment_type' => $appointment_type,
                                    'specialty_id' => $appointment->specialty_id,
                                    'patient_type' => $patient_type,
                                    ]);

                $fee = $this->getAppointmentFeeForBooking($bookingRequest);
                $before_seven_days = date('Y-m-d H:i:s', strtotime('-7 days', strtotime($appointment
                    ->date_time)));
                $panelty = false; 
                if ($curr_date > $before_seven_days) {
                    $panelty = true; //100% deduction
                } else {
                    $panelty = false; //admin fee
                }
                
                $request = new \Illuminate\Http\Request();

                $request->replace(['id' => $appointment->id,
                                    'panelty' => $panelty,
                                    'status' => "cancelled",
                                    'fee' => $fee->fee,  
                                    ]);

                $this->updateAppointmentStatus($request);
            }
        }

        return true;
    }

    public function appointmentFee($request)
    {
        $input = $request->all();
        $appointmentFee = AppointmentFee::where('specialty_id', '=', $input['specialty_id'])->orderBy('id','asc')->first();
        if (!empty($appointmentFee)) { // fee exists of that specialty
            foreach ($input['fee'] as $key => $value) {
                if (isset($value['member'])) {
                    AppointmentFee::where('specialty_id', '=', $input['specialty_id'])->where('appointment_type', '=', $key)->where('patient_type', '=', 'member')->update(['fee' => $value['member']]);
                }
                if (isset($value['non_member'])) {
                    AppointmentFee::where('specialty_id', '=', $input['specialty_id'])->where('appointment_type', '=', $key)->where('patient_type', '=', 'non_member')->update(['fee' => $value['non_member']]);
                }
            }
        } else { // new specialty
            foreach ($input['fee'] as $key => $value) {
                foreach ($value as $k => $v) {
                    $fee = new AppointmentFee;
                    $fee->specialty_id = $input['specialty_id'];
                    $fee->appointment_type = $key;
                    $fee->patient_type = $k; 
                    $fee->fee = $v; 
                    $fee->save(); 
                }
            }
        }
        
        return true;
    }

    public function getAppointmentFee($request)
    {
        $appointmentFee = AppointmentFee::where('specialty_id', '=', $request->specialty_id)->orderBy('id','asc')->get();
        $data['data'] = [];
        if (!empty($appointmentFee)) {
            foreach ($appointmentFee as $key => $value) {
               $data['data'][$value->appointment_type][$value->patient_type] = $value->fee; 
            }
        }
        return $data;
    }

    public function getAppointmentFeeForBooking($request)
    {
        return AppointmentFee::where('specialty_id', '=', $request->specialty_id)->where('appointment_type', '=', $request->appointment_type)->where('patient_type', '=', $request->patient_type)->first();
    }

    public function getAppointmentReminders($request)
    {
        $appointmentReminders = AppointmentReminderSchedule::orderBy('id','asc')->get();
        $data = [];
        if (!empty($appointmentReminders)) {
            foreach ($appointmentReminders as $key => $value) {
               $data[] = $value->period; 
            }
        }
        return $data;
    }

    public function appointmentReminders($request)
    {
        $input = $request->all();
        if (!empty($input)) {
            AppointmentReminderSchedule::where('id', '!=', '0')->delete();
            foreach ($input as $key => $value) {
                $appointmentReminders = new AppointmentReminderSchedule;
                $appointmentReminders->period = $value;
                $appointmentReminders->save();
            }
        }
        
        return true;
    }

    public static function appointmentScheduleReminder() 
    {
        $curr_date = date('Y-m-d');
        $reminderPeriods = AppointmentReminderSchedule::orderBy('period','asc')->get();
        if (!empty($reminderPeriods)) {
            foreach ($reminderPeriods as $key => $value) {
                $appointments = \DB::select('SELECT t1.*
                                    FROM appointments AS t1
                                    LEFT OUTER JOIN appointments AS t2
                                      ON t1.patient_id = t2.patient_id 
                                            AND (t1.date_time < t2.date_time 
                                             OR (t1.date_time = t2.date_time AND t1.id < t2.id))
                                    WHERE t2.patient_id IS NULL');
                if (!empty($appointments)) {
                    foreach ($appointments as $k => $v) {
                        $reminderDate = date('Y-m-d', strtotime($v->date_time .'+'.$value->period));
                        if ($reminderDate == $curr_date) {
                            // send reminder
                            $patientService = new PatientService;
                            $patientData = $patientService->get('id', $v->patient_id, true);
                            try {
                                $emailData = array ('patient' => $patientData,
                                    'subject' => 'Appointment Schedule Reminder', 
                                );
                                \Mail::to($patientData->user->email)->send(new AppointmentScheduleReminder($emailData));
                            } catch(\Exception $e) {
                            }
                            $appointmentService = new AppointmentService;
                            $appointmentService->saveReminderEmail('schedule_reminder', $patientData->user_id);
                            // send reminder sms to patient
                            $smsService = new SMSService;
                            $message = $smsService->smsTemplates('appointment_schedule_reminder');
                            $message = str_replace('[PATIENT_NAME]', $patientData->user->first_name , $message);
                            $message = str_replace('[LINK]', 'www.happyclinic.co.uk', $message);
                            $sms = $smsService->sendSMS($patientData->user->mobile_number, $message);
                        }
                    }
                }
            }
        } 
        return true;
    }

    public static function appointmentReminder() {
        $date = date('Y-m-d H:i:s');
        $appointments = Appointment::select(\DB::raw('*, DATE_SUB(date_time, INTERVAL 24 HOUR) as appointmentReminder'))
                        ->having('appointmentReminder', '<', $date)
                        ->where('email_reminder', '=', 'pending')
                        ->where('status', '!=', 'cancelled')
                        ->orderBy('id','desc')->get(['id', 'patient_id']);                                     
        if (!empty($appointments)) {
            foreach ($appointments as $key => $value) {
                $appointmentService = new AppointmentService;
                $appointmentData = $appointmentService->get('id', $value->id, true);
                if (!empty($appointmentData->patient->user->email)) {
                    try {
                       $emailData = array ('patient' => $appointmentData->patient,
                                            'doctor' => $appointmentData->doctor,
                                            'date_time' => $appointmentData->date_time,
                                            'subject' => 'Appointment Reminder', 
                        );
                        \Mail::to($appointmentData->patient->user->email)->send(new AppointmentReminder($emailData));
                    } catch(\Exception $e) {
                    }

                    Appointment::where('id', '=', $value->id)->update(array('email_reminder'=> 'sent'));
                    $appointmentService->saveReminderEmail('appointment_reminder', $appointmentData->patient->user_id);

                    // send reminder sms to patient
                    $smsService = new SMSService;
                    $message = $smsService->smsTemplates('appointment_reminder_patient');
                    $message = str_replace('[PATIENT_NAME]', $appointmentData->patient->user->first_name , $message);
                    $message = str_replace('[DATE]', date('d M, Y', strtotime($appointmentData->date_time)), $message);
                    $message = str_replace('[TIME]', date('h:i a', strtotime($appointmentData->date_time)), $message);
                    $message = str_replace('[LINK]', 'www.happyclinic.co.uk', $message);
                    $sms = $smsService->sendSMS($appointmentData->patient->user->mobile_number, $message);

                     Appointment::where('id', '=', $value->id)->update(array('sms_reminder'=> 'sent'));

                     // send reminder sms to doctor
                    $message = $smsService->smsTemplates('appointment_reminder_doctor');
                    $message = str_replace('[PATIENT_INTITIALS]', ucfirst(substr($appointmentData->patient->user->first_name, 0, 1)).ucfirst(substr($appointmentData->patient->user->last_name, 0, 1)) , $message);
                    $message = str_replace('[DATE]', date('d M, Y', strtotime($appointmentData->date_time)), $message);
                    $message = str_replace('[TIME]', date('h:i a', strtotime($appointmentData->date_time)), $message);
                    $message = str_replace('[LINK]', 'www.happyclinic.co.uk', $message);
                    $sms = $smsService->sendSMS($appointmentData->doctor->user->mobile_number, $message);
                }
            }    
        }
        return true;
    } 

    public function saveReminderEmail($type, $user_id) {
        $reminder = new ReminderEmail;
        $reminder->type = $type;
        $reminder->user_id = $user_id;
        $reminder->save();
        return true;
    }

    public function getLatestAppointment($request, $appointment_type_id = false) {
         $object = Appointment::where('patient_id', '=', $request->patient_id)
                            ->where(function($q)
                               {
                                $q->orwhere('status','=','completed')
                                    ->orwhere('status','=','booked');
                                })->orderBy('id', '=', 'desc');
        if ($appointment_type_id) {
            $object = $object->where('appointment_type_id', '=', $appointment_type_id);
        }                    
        if (isset($request->specialty_id)) {
            $object = $object->where('specialty_id', '=', $request->specialty_id);
        }
        if (isset($request->doctor_id) ) {
            $object = $object->where('doctor_id', '=', $request->doctor_id);
        }
        return $object->first();
    }

    public function assessmentCompleted($request) {
        if (isset($request->clinical)) {
            $questionnaireService = new QuestionnaireService;
            $saveClinicalRecId =  $questionnaireService->saveClinicalRecord($request->clinical);
            if ($saveClinicalRecId) {
                if ($request->consent_Sharing == 1) { // send record to hospital
                    $questionnaireService->sendHostpitalRecord($saveClinicalRecId);
                } 
            } 
        }

        if (isset($request->labforms)) {
            $labFormService = new LabFormService;
            $labFormService->save($request->labforms);
        }

        Appointment::where('id', '=', $request->clinical['appointment_id'])->update(['status' => 'completed']);

        return true;
    }

    public function createOpenTalkSession($appointmentData) {
        // Create a session that attempts to use peer-to-peer streaming:
        //$session = $this->opentok->createSession();

        // An automatically archived session:
        $sessionOptions = array(
            //'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED,
        );  
        // A session that uses the OpenTok Media Router, which is required for archiving:
        $session = $this->opentok->createSession($sessionOptions);

        $sessionId = $session->getSessionId();
        $token = $session->generateToken();
        $pub_token = $session->generateToken(array(
                    'role' => Role::MODERATOR,
                ));
        $mod_token = $session->generateToken(array(
                    'role' => Role::PUBLISHER,
                ));
        $saveSession = $this->saveOpenTalkSession($sessionId, $appointmentData, $token, $mod_token, $pub_token );
        if ($saveSession) {
            return $saveSession;
        } else {
            return false;
        }
    }

        
    public function saveOpenTalkSession($sessionId, $appointmentData, $token, $mod_token, $pub_token) {

        $session = new AppointmentSession;
        $session->session_id = $sessionId;
        $session->appointment_id = $appointmentData->id;
        $session->token = $token;
        $session->mod_token = $mod_token;
        $session->pub_token = $pub_token;
        $session->status = 'in_process';
        if ($session->save()) {
            return $session;
        } else {
            return false;
        }
    }

    public function startArchiveSession($request) {
        $session = AppointmentSession::find($request->input('session_id'));

        if (!empty($session)) {
            if ($session->archive_id == NULL) {
                $archiveOptions = array(
                    'name' => 'Session',     // default: null
                    'hasAudio' => true,                     // default: true
                    'hasVideo' => true,                     // default: true
                    'outputMode' => OutputMode::COMPOSED  // default: OutputMode::COMPOSED
                );
                
                $archive = $this->opentok->startArchive($session->session_id, $archiveOptions);
                $archiveId =  $archive->id;
                $session->archive_id = $archiveId;
                if ($session->save()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                //$this->opentok->stopArchive($session->archive_id);
            }
        }
    }

    public function updateSessionStatus($request){

        $session = AppointmentSession::find($request->input('id'));
        if (!empty($session)) {
            if ($session->status != 'completed') {
                $session->status = 'completed';
                $session->session_closed_time = date('Y-m-d H:i:s');
                if ($session->save()) {
                    if (!empty($session->archive_id)) {
                        //get opentok archive for this session 
                        $archive = $this->opentok->getArchive($session->archive_id);
                        if (!empty($archive)) {
                            if ($archive->status == 'started' || $archive->status == 'paused') {
                                $this->opentok->stopArchive($session->archive_id);
                            }
                        }
                        $appointment = Appointment::find($session->appointment_id);
                        $appointment->status = 'completed';
                        $appointment->save();
                    }    
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function checkIfSessionExist($appointment_id) {
        $response = '';
        $session = AppointmentSession::where('appointment_id', '=', $appointment_id)->first();
        if (!empty($session)) {
            if ($session->status == 'completed') {
                $response = 'completed';
            } else if ($session->status == 'in_process') {
                $response = $session;
            }
        } else {
            $response = 'not_exist';
        }

        return $response;
    }

    public function calculateDuration($to_time, $from_time) {
        $to_time = new \DateTime($to_time);
        $from_time = new \DateTime($from_time);
        $interval = $to_time->diff($from_time);

        $hours_in_minutes = $interval->h * 60;
        $seconds_in_minutes = $interval->s / 60;
        $total_minutes = $hours_in_minutes + $seconds_in_minutes + $interval->i;
        return $total_minutes;
    }

    public function sendInvitationLink($id) {
        $appointment = $this->get('id',$id, true); 
        if (!empty($appointment)) {
            try {
               $emailData = array ('patient' => $appointment->patient,
                                    'doctor' => $appointment->doctor,
                                    'appointment' => $appointment,
                                    'subject' => 'Assessment Invitation Link', 
                                    'action_url' => \URL::to('/assessment/'.$appointment->encrypted_id),
                );
                \Mail::to($appointment->patient->user->email)->send(new InvitationLinkEmail($emailData));
               //\Mail::to('sana.aamir1909@gmail.com')->send(new InvitationLinkEmail($emailData));
               
            } catch(\Exception $e) {
            }

            // send inivation sms to patient
            $smsService = new SMSService;
            $message = $smsService->smsTemplates('assessment_invitation');
            $message = str_replace('[LINK]', 'www.happyclinic.co.uk/assessment/'.$appointment->encrypted_id, $message);
            $sms = $smsService->sendSMS($appointment->patient->user->mobile_number, $message);
            return true;
        } else {
            return false;
        }
    }

    public static function completeSession() {
        $date = date('Y-m-d H:i:s');
        $session = AppointmentSession::select(\DB::raw('id,appointment_id, DATE_ADD(created_at, INTERVAL 5 HOUR) as sessionStartTime'))
                ->having('sessionStartTime', '<=', $date)
                ->where('status', '=', 'in_process')
                ->orderBy('id', 'desc')
                ->get();
        if (!empty($session)) {
            foreach ($session as $key => $value) {
                $sessionData = AppointmentSession::find($value->id);
                $sessionData->status = 'completed';
                $sessionData->save();
                $appointment = Appointment::find($value->appointment_id);
                $appointment->status = 'completed';
                $appointment->save();
            }
        }
        return true;        
    } 

    public function appointmentDurations($request) {
        $input = $request->all();
        //p($input);
        $appointmentDuration = AppointmentDuration::where('specialty_id', '=', $input['specialty_id'])->orderBy('id','asc')->first();
        if (!empty($appointmentDuration)) { // fee exists of that specialty
            foreach ($input['duration'] as $key => $value) {
                
                    AppointmentDuration::where('specialty_id', '=', $input['specialty_id'])->where('appointment_type', '=', $key)->update(['duration' => $value]);
                
            }
        } else { // new specialty
            foreach ($input['duration'] as $key => $value) {
                $duration = new AppointmentDuration;
                $duration->specialty_id = $input['specialty_id'];
                $duration->appointment_type = $key;
                $duration->duration = $value; 
                $duration->save(); 
            }
        }
        
        return true;
    }

     public function getAppointmentDurations($request)
    {
        $appointmentDuration = AppointmentDuration::where('specialty_id', '=', $request->specialty_id)->orderBy('id','asc')->get();
        $data['data'] = [];
        if (!empty($appointmentDuration)) {
            foreach ($appointmentDuration as $key => $value) {
               $data['data'][$value->appointment_type] = $value->duration; 
            }
        }
        return $data;
    }

    public function getDuration($specialty_id, $appointment_type_id){
        $appointment_type = '0';
        if ($appointment_type_id == 1) {
            $appointment_type = 'initial_assessment';
        } else if ($appointment_type_id == 2) {
            $appointment_type = 'follow_up_assessment_type_1';
        } else if ($appointment_type_id == 3) {
            $appointment_type = 'follow_up_assessment_type_2';
        } else if ($appointment_type_id == 4) {
            $appointment_type = 'telephone_advice';
        }
        $appointmentDuration = AppointmentDuration::where('specialty_id', '=', $specialty_id)->where('appointment_type', '=', $appointment_type)->first();
        if (!empty($appointmentDuration)) {
            return $appointmentDuration->duration;
        } else {
            return 30; // default time duration
        }
    }

    public function checkAppointmentExist($request) {

        $input = $request->all();
        $objects = Appointment::where('status','=', 'booked')->where('doctor_id', '=', $input['doctor_id'])->where(\DB::raw("date(date_time)"), '>=', date('Y-m-d'));

        if (!empty($input['filter_by_date_range'])) {
            if ($input['filter_by_date_range'][0] != 'null') {
                $startDate = date('Y-m-d', strtotime($input['filter_by_date_range'][0]));

                $endDate = date('Y-m-d', strtotime($input['filter_by_date_range'][1]));
                $objects = $objects->where(\DB::raw("date(date_time)"), '>=', $startDate)->where(\DB::raw("date(date_time)"), '<=', $endDate)->orderBy('date_time', 'asc');
            } 
        }

        if (!empty($input['filter_by_day'])) {
            $objects = $objects->where(\DB::raw("DAYNAME(date_time)"), '=', $input['filter_by_day'])->orderBy('date_time', 'asc');
        }

        $data = $objects->first();
        if (!empty($data)) {
            return true;
        } else {
            return false;
        }
    }
}


?>