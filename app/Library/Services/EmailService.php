<?php
namespace App\Library\Services;
use App\Library\Contracts\EmailServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Email;
use App\Models\MentalHealthHospital;
use App\Models\Laboratory;
use Carbon\Carbon;
use App\Mail\SendEmail;
use App\Library\Helper\ActivityLogManager;
use App\Library\Helper\Utility;
  
class EmailService implements EmailServiceInterface
{
    public $emailFilePath;

    function __construct()
    {
        $this->emailFilePath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'email_attachments'.DIRECTORY_SEPARATOR;
    }
    public function send($request)
    {
        $attachments = [];
        if (!empty($request->attachments)) {
            $mediaService = new MediaService();
            foreach ($request->attachments as $key => $value) {
                try {
                    $mediaService->moveFile($mediaService->tmpPath.$value, $this->emailFilePath.DIRECTORY_SEPARATOR.$value);
                } catch(\Exception $e){
                    //dd($e->getMessage());
                }
                $newFile = new \StdClass;
                $newFile->file = $value;
                $newFile->file_path =  \URL::to('storage/email_attachments/'.$value);
                $attachments[] = $newFile;
            }
        }

        $receiverArr = [];
        if ($request->email_type == 'individual') {
            $receiverArr = $request->receiver_arr;
        } else if ($request->email_type == 'group') {
            if ($request->receiver_type == 'all_hospitals') {
                $receiverArr = MentalHealthHospital::orderBy('id','desc')->pluck('email')->toArray();
            } else if ($request->receiver_type == 'all_laboratories') {
                $receiverArr = Laboratory::orderBy('id','desc')->pluck('email')->toArray();
            }
        }
        if (!empty($receiverArr)) {

            foreach ($receiverArr as $key => $email) {
                try {
                    $emailData = array ('content' => $request->content,
                                    'subject' => $request->subject, 
                                    'attachments' => $attachments, 
                                );
                    \Mail::to($email)->send(new SendEmail($emailData));
                    //\Mail::to('sana.aamir1909@gmail.com')->send(new SendEmail($emailData));
                } catch(\Exception $e){
                    //dd($e->getMessage());
                }    
            }
        }
        return true;
    }
}


?>