<?php
namespace App\Library\Services;
use App\Library\Contracts\SpecialtyServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Specialty;
use App\Models\AppointmentFee;
use App\Models\AppointmentDuration;
use App\Models\Questionnaire;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class SpecialtyService implements SpecialtyServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->name = $request->input('name');
        return $data;
    }

    public function save($request)
    {
        $rec = new Specialty();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Specialty::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Specialty::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false, $onlyComplete = false)
    {
        $specialty =  Specialty::where($col, $value)->first();

        if ($onlyComplete) {
            $completeSpecialty = $this->checkForCompleteSpecialty($specialty->id);
            if ($completeSpecialty) {
                return $specialty;
            } else {
                return false;
            }
            
        }

        return $specialty;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = Specialty::orderBy('id', 'desc');
        
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }
        $completeCheck = false;
        if (!empty($input['filter_by_complete_specialty_only'])) {
            $completeCheck = true;
        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, false, $completeCheck);
                if ($objectData) {
                    $data['data'][$i] = $objectData;            
                    $i++;
                }
                
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function checkForCompleteSpecialty($specialty_id) {

        $error = [];
        $appontmentDurationExist = AppointmentDuration::where('specialty_id', '=', $specialty_id)->first();
        if (empty($appontmentDurationExist)) {
            array_push($error, 1);
        }

        $appointmentFeeExist = AppointmentFee::where('specialty_id', '=', $specialty_id)->first();
        if (empty($appointmentFeeExist)) {
            array_push($error, 1);
        }

        $questionnaireExist = Questionnaire::where('specialty_id', '=', $specialty_id)->first();

        if (empty($questionnaireExist)) {
            array_push($error, 1);
        }

        if (!empty($error)) {
            return false;
        } else {
            return true;
        }

    }

}


?>