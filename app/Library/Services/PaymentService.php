<?php
namespace App\Library\Services;
use App\Library\Contracts\PaymentServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Patient;
use App\Models\Subscription;
use App\Models\RepeatBloodTest;
use App\Models\MembershipSubscription;
use App\Models\MembershipCancelRequest;
use App\Models\AppointmentType;
use App\Library\Helper\ActivityLogManager;
use Carbon\Carbon;
use Laravel\Cashier\Cashier;
use App\Library\Helper\Utility;
  
class PaymentService implements PaymentServiceInterface
{
    function __construct()
    {
        Cashier::useCurrency('GBP', '£');
    }

    public function stripePayment($request)
    {
        try {
            $patient = Patient::find($request->patient_id);
            
            if ($patient->stripe_id == null) {
                $patient->createAsStripeCustomer($request->token['id'], [
                    'email' => $request->token['email'],
                ]);
            }
            

            $appointment_type_name = 'Initial assessment';
            if (isset($request->appointment_type_id)) {
                $appointment_type = AppointmentType::find($request->appointment_type_id);
                $appointment_type_name = $appointment_type->name;
            }
            /*$patient->charge($request->amount * 100, [
                'description' => 'Appointment booking for initial assessment',
                'receipt_email' => $request->token['email']
            ]);*/
            $invoice = $patient->invoiceFor('Appointment Booking', $request->amount, [
                                    'description' => 'Appointment booking for '.$appointment_type_name.' ('.$request->date_time.')'
                                ]); // create charge and also create invoice
            return $invoice;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function saveMembershipSubscription($request, $subs) {
        $coolingOffPeriod = env('COOLING_OFF_PERIOD_DAYS');
        $subscription = new MembershipSubscription;
        $subscription->patient_id = $request->patient_id;
        $subscription->stripe_sub_id = $subs->stripe_id;
        $subscription->subscription_date = date('Y-m-d');
        $subscription->cooling_off_period_end = date('Y-m-d H:i:s', strtotime('+'.$coolingOffPeriod.' days', strtotime($subscription->subscription_date.' 23:59:00')));
        $subscription->status = 'activated';
        $subscription->save();
        return true;
    }

    public function membershipSubscription($request) {
        $patient = Patient::find($request->patient_id);
        if ($patient->stripe_id == null) {
            $patient->createAsStripeCustomer($request->token['id'], [
                'email' => $request->token['email'],
            ]);
        }
        $plan_id = env('MEMBERSHIP_PLAN_ID');
        try {
            $subs = $patient->newSubscription('main', $plan_id)->create($request->token['id']);
            $this->saveMembershipSubscription($request, $subs);
            $patient->is_member = 1;
            $patient->update();
            return true;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function invoices($request)
    {
        $patient = Patient::find($request->patient_id);
        $data = ['data'=>[]];
        if ($patient->stripe_id != null) {
            if (isset($request->limit)) {
                $limit = $request->limit;
            } else {
                $limit = 5;
            }
            
            $invoice_id = '';
            if (isset($request->limit)) {
                $limit = $request->limit;
            }
            $settings = array('limit' => $limit); 
            if ($request->invoice_id) {
                $settings['starting_after'] = $request->invoice_id;
            }
            
            $invoices = $patient->invoicesIncludingPending($settings);
            if (!empty($invoices)) {
                
                 $i = 0;
                foreach ($invoices as $key => $invoice) {
                    $objData = $this->getInvoice($invoice, $request->patient_id);
                    $data['data'][$i] = $objData;            
                    $i++;
                }

                $arrayEndedAt =  end($data['data']);
                $data['last_id'] = $arrayEndedAt['id'];
            }
        }

        return $data;  
    }

    public function getStripeInvoice($id, $patient_id) {
       $patient = Patient::find($patient_id);
       if ($patient->stripe_id != null) {
            $invoice = $patient->findInvoice($id);
            if (!empty($invoice)) {
                return $this->getInvoice($invoice, $patient_id);
            }
       }
       return false;
    }

    public function getInvoice($invoice, $patient_id) {
        $obj = [];
        $obj['id'] = $invoice->id;
        $obj['total'] = $invoice->total();
        $obj['status'] = $invoice->status;
        $obj['number'] = $invoice->number;
        $obj['date'] = $invoice->date;
        $obj['finalized_at'] = $invoice->finalized_at;
        $obj['amount_due'] = $invoice->amount_due/100;
        $obj['amount_paid'] = $invoice->amount_paid/100;
        $obj['amount_remaining'] = $invoice->amount_remaining/100;

        $obj['type'] = 'manual';
        $obj['subscriptions'] = [];
        $obj['items'] = [];
        $patientService = new PatientService;
        $obj['patient'] = $patientService->get('id', $patient_id, true);
        $items = $invoice->invoiceItems();
        if (!empty($items)) {
            foreach ($items as $key => $item) {
                $obj['items']['description'] = $item->description; 
                $obj['items']['total']= $item->total(); 
            }
        }
        $subscriptions = $invoice->subscriptions();
        if (!empty($subscriptions)) {
            foreach ($subscriptions as $key => $subscription) {
                $obj['type'] = 'subscription';
                $obj['subscriptions']['description'] = $subscription->description; 
                $obj['subscriptions']['quantity'] = $subscription->quantity; 
                $obj['subscriptions']['start_date'] = $subscription->startDateAsCarbon()->formatLocalized('%B %e, %Y'); 
                $obj['subscriptions']['end_date'] = $subscription->endDateAsCarbon()->formatLocalized('%B %e, %Y'); 
                $obj['subscriptions']['total'] = $subscription->total(); 
            }
        }
        return $obj;
    }

    /*public function invoiceDelete($id) {
        
    }*/

    public function getRBT($col, $value) {
        $rbt = RepeatBloodTest::where($col, '=',$value)->first();
        if (!empty($rbt)) {
            $patientService = new PatientService;
            $rbt->patient = $patientService->get('id', $rbt->patient_id, true);
            $doctorService = new DoctorService;
            $rbt->doctor = $doctorService->get('id', $rbt->doctor_id, true);
            $labformService = new LabFormService;
            $latestForm = $labformService->getPatientLatestForm($rbt->patient_id);
            if (!empty($latestForm)) {
                $rbt->lab_form_id = $latestForm->id;
            } else {
                $rbt->lab_form_id = 0;
            }
            //$rbt->lab_form_id = $latestForm->id;
            
        }

        return $rbt;
    }

    public function allRBT($request) {
        $input = $request->all();

        $objects = RepeatBloodTest::where('status', '=', 'activated')->orderBy('id', 'desc');

        if (!empty($input['filter_by_patient_id'])) {
            $objects = $objects->where('patient_id', $input['filter_by_patient_id']);
        }

        if (!empty($input['filter_by_upcoming_send_date'])) {
            $objects = $objects->whereDate('upcoming_form_send_date','=',  $input['filter_by_upcoming_send_date']);
        }
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->getRBT('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;
    }

    public function updateRBT($request) {
        $rbt = RepeatBloodTest::where('patient_id', '=', $request->patient_id)->first();
        if (!empty($rbt)) {
            $rbt->upcoming_test_date = $request->date;
            $rbt->upcoming_form_send_date = date('Y-m-d', strtotime('-7 days', strtotime($request->date)));
            $rbt->status = $request->status;
            $rbt->update();
            return true;
        } else {
            $rbt = new RepeatBloodTest;
            $rbt->patient_id = $request->patient_id;
            $rbt->doctor_id = $request->doctor_id;
            $rbt->upcoming_test_date = $request->date;
            $rbt->upcoming_form_send_date = date('Y-m-d', strtotime('-7 days', strtotime($request->date)));
            $rbt->status = $request->status;
            $rbt->save();
            return true;
        }
        return false;
    }   

    public function getMembershipSubscriptions($request) {
        $input = $request->all();

        //$objects = MembershipSubscription::where('status', '=', 'activated')->orderBy('id', 'desc');
        $objects = MembershipSubscription::orderBy('id', 'desc');
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->getMembershipSubscription('id',$object->id, false);
                if ($objectData != null) {
                    $data['data'][$i] = $objectData;            
                    $i++;
                }
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;
    }

    public function getMembershipSubscription($col, $value, $subObj = false) {

        $subscription = MembershipSubscription::where($col, '=',$value)->where('status', '=', 'activated')->first();

        if (!empty($subscription)) {
            $userService = new UserService;
            
            $subscription->subscription = Subscription::where('stripe_id', '=', $subscription->stripe_sub_id)->first();

            
            $patient = Patient::find($subscription->patient_id);
            $subscription->has_membership = $patient->subscribed('main');
            $subscription->patient = $patient;
            $subscription->user = $userService->get('id', $patient->user_id, true);
            if ($subObj) {
                $subscription->subData = $this->getSubscriptionObject($subscription->patient_id, $subscription->stripe_sub_id);
                $subscription->hasCancelRequest = $this->checkSubscriptionCancelRequest($subscription->id);
                $subscription->coolingOffPeriod = $subscription->cooling_off_period_end;
            }
        }
        return $subscription;
    }

    public function checkSubscriptionCancelRequest($subscription_id) {
        return MembershipCancelRequest::where('membership_subscription_id', '=', $subscription_id)->where('status', '=', 'due')->first();
    }

    public function getSubscriptionObject($patient_id, $subscription_id) {
        $subData = [];
        $patient = Patient::find($patient_id);
        // getStripeKey is a static method
        \Stripe\Stripe::setApiKey( $patient::getStripeKey() );

        if ($patient->hasStripeId()) {
            // Get stripe id for customer using public method
            $cu = \Stripe\Customer::retrieve( $patient->stripe_id );

            // Get current stripe subscription id using public method
            $subscription = $cu->subscriptions->retrieve(  $subscription_id );
            if (!empty($subscription)) {
                $subData['current_period_start'] = date('d F, Y', $subscription->current_period_start);
                $subData['current_period_end'] = date('d F, Y', $subscription->current_period_end);
                $subData['status'] = $subscription->status;
             }
        }

        return $subData;
        
    }

    public function deleteSubscription($id) {
        $subscription = MembershipSubscription::find($id);
        if (!empty($subscription)) {
            $patient = Patient::find($subscription->patient_id);
            if ($patient->subscribed('main')) {
                $patient->subscription('main')->cancelNow();
                $subscription->status = 'deactivated';
                $subscription->save();  
                MembershipCancelRequest::where('membership_subscription_id', '=', $subscription->id)->update(['status' => 'cancelled']);
                $patient->is_member = 0;
                $patient->update();
            }
            return true;
        } else {
            return false;
        }
    }

    public function deleteSubscriptionCancelRequest($id) {
        $cancelRequest = MembershipCancelRequest::find($id);
        if (!empty($cancelRequest)) {
            $cancelRequest->delete();
            return true;
        } else {
            return false;
        }
    }

    public function membershipCancel($request) {
        $subscription = MembershipSubscription::where('stripe_sub_id', '=', $request->stripe_sub_id)->first();
        if (!empty($subscription)) {
            if ($subscription->cooling_off_period_end != null && $subscription->stripe_charge_id != null) {
                $busniessSettingService = new BusinessSettingService;
                $setting = $busniessSettingService->get();
                $membership_fee = env('MEMBERSHIP_FEE');
                
                $amount = preg_replace('/\D/', '', $setting->admin_fee_percentage);
                $deductAmount = (($amount/100)*($membership_fee));
                
                $refundedAmount = ($membership_fee - $deductAmount)*100;
                $refund = $this->refundPayment($subscription->patient_id, $subscription->stripe_charge_id, $refundedAmount);
                if ($refund) {
                    $cancelSub = $this->deleteSubscription($subscription->id);
                    if ($cancelSub) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*public function getCoolingOffPeriod($stripe_sub_id) {
        $subscription = MembershipSubscription::where('stripe_sub_id', '=', $stripe_sub_id)->first();
        if (!empty($subscription)) {
            if ($subscription->cooling_off_period_end != null) {
                return $subscription->cooling_off_period_end;
            }
        }    
        return false;
    }*/

    public function getChargeObject($patient_id, $charge_id) {
        $patient = Patient::find($patient_id);
        // getStripeKey is a static method
        \Stripe\Stripe::setApiKey( $patient::getStripeKey() );

        if ($patient->hasStripeId()) {
            // Get stripe id for customer using public method
            $charge = \Stripe\Charge::retrieve($charge_id);

            if (!empty($charge)) {
                return $charge;
            } else {
                return false;
            }
         }
         return false;
    }


    public function refundPayment($patient_id, $charge_id, $amount = null) {
        $patient = Patient::find($patient_id);
        if ($amount != null) {
            $refund = $patient->refund($charge_id, array('amount' => $amount));
        } else {
            $refund = $patient->refund($charge_id);
        }
        
        if ($refund) {
            return true;
        } else {
            return $refund;
        }
    }

    public function membershipCancelRequest($request) {
        $patient = Patient::find($request->patient_id);
        if (!empty($patient)) {
            if ($patient->subscribed('main')) {
                $subscription = MembershipSubscription::where('patient_id','=', $request->patient_id)->where('status','=', 'activated')->first();
               
                //$membershipOneYear = date('Y-m-d', strtotime('+1 year', strtotime($subscription->subscription_date)));

                // check if it under one year
                /*$currDate = date('Y-m-d');
                if ($currDate <= $membershipOneYear) { // membership under one year
                    $cancelDate = $membershipOneYear;
                } else { // membership after one year 
                    $cancelDate = date('Y-m-d', strtotime('+1 month', strtotime($request->requested_date)));
                }*/
                $cancelDate = date('Y-m-d', strtotime($request->requested_date));
                $obj = new MembershipCancelRequest;
                $obj->membership_subscription_id = $subscription->id;
                $obj->requested_date = $request->requested_date;
                $obj->cancel_at = $cancelDate;
                $obj->status = 'due';
                $obj->save();
                return true;
            } 
        } 
        return false;
    }

    public function getMembershipCancelRequests($request) {
        $input = $request->all();
        $objects = MembershipCancelRequest::orderBy('id', 'desc');
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->getMembershipCancelRequest('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;
    }

    public function getMembershipCancelRequest($col, $value) {
        $subscription = MembershipCancelRequest::where($col, '=',$value)->first();
        if (!empty($subscription)) {
            $subscription->subscription = MembershipSubscription::where('id', '=', $subscription->membership_subscription_id)->first();
            $patientService = new PatientService;
            $subscription->patient = $patientService->get('id',$subscription->subscription->patient_id, true);
            if ($subscription->subscription->stripe_charge_id != null) {
                $subscription->charge = $this->getChargeObject($subscription->subscription->patient_id, $subscription->subscription->stripe_charge_id);
            } else {
                $subscription->charge = null;
            }
        }
        return $subscription;
    }

    public static function autoCancelMembershipSubsription() {
        $currDate = date('Y-m-d');
        $requests = MembershipCancelRequest::orderBy('id', 'desc')->where('cancel_at', '=',$currDate)->get();
        if (!empty($requests)) {
            $paymentService = new PaymentService;
            foreach ($requests as $key => $value) {
                $subscription = MembershipSubscription::where('id', '=', $value->membership_subscription_id)->first();
                if (!empty($subscription)) {
                    $paymentService->cancelAllAppointmentAfter($subscription->patient_id, $currDate);
                }
                $paymentService->deleteSubscription($value->membership_subscription_id);
                MembershipCancelRequest::where('id', '=', $value->id)->update(['status' => 'cancelled']);
                
            }
        }
        return true;
    }

    public function cancelAllAppointmentAfter($patient_id, $after_date) {
        $appointments = Appointment::where('patient_id', '=', $patient_id)->whereDate('date_time', '>=', $after_date)->get();
        $busniessSettingService = new BusinessSettingService;
        $setting = $busniessSettingService->get();
        $appointmentService = new AppointmentService;
        if (!empty($appointments)) {
            foreach ($appointments as $key => $appointment) {
                $appointment = Appointment::find($appointment->id);
                $amount = preg_replace('/\D/', '', $setting->admin_fee_percentage);
                $deductAmount = (($amount/100)*($appointment->fee));
                
                $refundedAmount = ($appointment->fee - $deductAmount)*100;
                $this->refundPayment($appointment->patient_id, $appointment->charge_id, $redundedAmount);
                $appointment->status = 'cancelled';
                $appointment->save();
            }
        }
        return true;
    }

    public static function autoSendRepeatBloodTestForm() {
        $currDate = date('Y-m-d');
        $rbtForms = RepeatBloodTest::where('upcoming_form_send_date', '=', $currDate)->where('status', '=', 'activated')->get();
        if (!empty($rbtForms)) {
            $labformService = new LabFormService;
            foreach ($rbtForms as $key => $value) {
                $labForm = $labformService->getPatientLatestForm($value->patient_id);
                if (!empty($labForm)) {
                    $obj = new \StdClass;
                    $obj->id = $labForm->id;
                    $obj->repeat = true;
                    $labformService->sendLabForm($obj);
                }
            }
        }
        return true;
    }

    public function updateCoolingOffPeriod($patient_id) {
        $subscription = MembershipSubscription::where('patient_id', '=', $patient_id)->where('status', '=', 'activated')->first();
        if (!empty($subscription)) {
            MembershipSubscription::where('id','=', $subscription->id)->update(['cooling_off_period_end' => null ]); 
        } 
        return true;
    }

    public static function autoEndCoolingOffPeriod() {
        $curr_date = date('Y-m-d H:i:s');
        $subscriptions = MembershipSubscription::where('cooling_off_period_end', '>=', $curr_date)->get();
        if (!empty($subscriptions)) {
            foreach ($subscriptions as $key => $value) {
                MembershipSubscription::where('id','=', $value->id)->update(['cooling_off_period_end' => null ]);
            }
        }
    }
}


?>