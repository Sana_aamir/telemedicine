<?php
namespace App\Library\Services;
use App\Library\Contracts\StaffServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Staff;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class StaffService implements StaffServiceInterface
{
    public $docPath;

    function __construct()
    {
        $this->docPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'staff_docs'.DIRECTORY_SEPARATOR;
    }

    public function propertySetter($request, $data)
    {
        $data->name = $request->input('name');
        $data->date = $request->input('date');
        if (!empty($request->input('document'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('document'), $this->docPath.$request->input('document'));
        }
        $data->document = $request->input('document', '');
        return $data;
    }

    public function save($request)
    {
        $rec = new Staff();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Staff::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Staff::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $staff = Staff::where($col, $value)->first();
        if (!empty($staff)) {
             if(!empty($staff->document))
                $staff->document_path =  \URL::to('storage/staff_docs/'.$staff->document);
            else
                $staff->document_path =  '-';
        }

        return $staff;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new Staff;
        if (!empty($input['keyword'])) {
            $objects = $objects->where('name','LIKE','%'.$input['keyword'].'%');
        }

        if (isset($input['filter_by_is_archived'])) {
            $objects = $objects->where('is_archived', '=', $input['filter_by_is_archived']);
        }
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function updateStatus($request)
    {
        $staff = Staff::find($request->id);
        if (!empty($staff)) {
            $staff->is_archived =  $request->status;
            $staff->update();
            return true;
        } else {
            return false;
        }
    }

}


?>