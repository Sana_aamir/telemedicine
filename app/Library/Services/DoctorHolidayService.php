<?php
namespace App\Library\Services;
use App\Library\Contracts\DoctorHolidayServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\DoctorHoliday;
use App\Library\Helper\ActivityLogManager;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class DoctorHolidayService implements DoctorHolidayServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->doctor_id = $request->input('doctor_id');
        $data->name = $request->input('name');
        $data->from_date = date('Y-m-d', strtotime($request->input('from_date')));
        $data->to_date = date('Y-m-d', strtotime($request->input('to_date')));
        return $data;
    }

    public function save($request)
    {
        $request->merge(['filter_by_date_range' => [$request->from_date, $request->to_date] ]);
        $appointmentService = new AppointmentService;
        $checkAppointment = $appointmentService->checkAppointmentExist($request);
        if (!$checkAppointment) {
            $rec = new DoctorHoliday();
            $rec = $this->propertySetter($request, $rec);
            $rec->save();
            return 'success';
        } else {
            return 'appoitment_exist';
        }
        
    }
    
    public function delete($id)
    {
        $rec = DoctorHoliday::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {   
        $request->merge(['filter_by_date_range' => [$request->from_date, $request->to_date] ]);
        $appointmentService = new AppointmentService;
        $checkAppointment = $appointmentService->checkAppointmentExist($request);
        if (!$checkAppointment) {
            $rec = DoctorHoliday::find($id);
            $rec = $this->propertySetter($request, $rec);
            $rec->update();        
            return 'success';
        } else {
            return 'appoitment_exist';
        }
    }

    public function get($col, $value,  $detail = false)
    {
        $holiday =  DoctorHoliday::where($col, $value)->first();
        if (!empty($holiday)) {
            $days = Utility::getBetweenDates($holiday->from_date, $holiday->to_date);
            $holiday->number_of_days = count($days);
            $holiday->date_range = [date('Y-m-d', strtotime($holiday->from_date)), date('Y-m-d', strtotime($holiday->to_date))];
        }

        return $holiday;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = DoctorHoliday::orderBy('id', 'desc');

        if (isset($input['filter_by_doctor_id'])) {
            $objects = $objects->where('doctor_id', '=', $input['filter_by_doctor_id']);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function getAllDoctorHolidays($from, $to, $doctor_id) {
        $holidays = DoctorHoliday::where('doctor_id', '=', $doctor_id)->where('from_date', '>=', $from)->where('to_date', '<=', $to)->get();
        $holidaysArr = [];
        if (count($holidays) > 0) {
            foreach ($holidays as $key => $value) {
               $dates = Utility::getBetweenDates($value->from_date, $value->to_date);
               $holidaysArr = array_merge($holidaysArr, $dates);
            }
        }
        
        $holidaysArr = array_unique($holidaysArr);
        return $holidaysArr;
    } 

}


?>