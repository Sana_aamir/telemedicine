<?php
namespace App\Library\Services;
use App\Library\Contracts\PatientServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Patient;
use App\Models\PsychiatricReport;
use App\Models\Laboratory;
use App\Models\MentalHealthHospital;
use App\Models\Appointment;
use App\Models\PatientAttachment;
use App\Models\PatientIdentityDoc;
use App\Models\RepeatBloodTest;
use App\Models\PatientTextField;
use App\Models\PatientAnswer;
use App\Models\FlaggedMessage;
use App\Models\AppointmentSession;
use App\Models\Clinical;
use App\Models\ClinicalDoctorAnswer;
use App\Models\LabForm;
use App\Models\LabFormDetail;
use App\Models\MembershipSubscription;
use App\Models\MembershipCancelRequest;
use App\Models\Subscription;
use App\Models\Note;
use App\Models\Support;
use App\Models\SupportMessage;
use App\Models\SupportMessageDoc;
use App\Models\Testimonial;
use App\Models\ActivityLog;

use App\Mail\Registration;
use App\Mail\SendDocument;
use App\Mail\ApprovalEmail;
use App\Library\Helper\ActivityLogManager;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class PatientService implements PatientServiceInterface
{
    public $photoIdPath;
    public $utilityBillPath;
    public $psychiatricReportPath;
    public $attachmentsPath;
    public $userService;

    function __construct()
    {
        $this->userService = new UserService;
        $this->photoIdPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'photo_id'.DIRECTORY_SEPARATOR;
        $this->utilityBillPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'utility_bills'.DIRECTORY_SEPARATOR;
        $this->psychiatricReportPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'psychiatric_reports'.DIRECTORY_SEPARATOR;
        $this->attachmentsPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'patient_attachments'.DIRECTORY_SEPARATOR;
    }

    public function propertySetter($request, $data)
    {
        $data->title = $request->input('title');
        $data->user_id = $request->input('user_id');
        $data->dob = date('Y-m-d', strtotime($request->input('dob')));
        /*if (!empty($request->input('photo_id'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('photo_id'), $this->photoIdPath.$request->input('photo_id'));
        }
        $data->photo_id = $request->input('photo_id', '');

        if (!empty($request->input('utility_bill'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('utility_bill'), $this->utilityBillPath.$request->input('utility_bill'));
        }
        $data->utility_bill = $request->input('utility_bill', '');*/
        if (isset($data->id)) {
            if ($data->address_first_line != $request->input('address_first_line') || $data->city != $request->input('city') || $data->country != $request->input('country') || $data->postcode != $request->input('postcode')) { // address change
                try {
                    $newParams = array(
                                'user_id'   => Auth::user()->id,
                                'module'    => 'patient',
                                'user_type' => ucfirst(Auth::user()->roles->first()->name),
                                'log_id'    => $data->id,
                                'text'    => $data->address_first_line.', '.$data->city.', '.$data->country.', '.$data->postcode,
                                'log_type'  => 'address_change'
                            );
                    ActivityLogManager::create($newParams);
                } catch(Exception $e){
                }

                if(\Auth::user()->hasRole('patient')) {
                    // generate ticket
                    $msgRequest = new \Illuminate\Http\Request();
                    $msgRequest->replace(
                            [
                                'email_type' => 'individual',
                                'user_type' => 'patient',
                                'user_id' => $data->user_id,
                                'sender_id' => User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first(),
                                'category' => 'changes_messages',
                                'subject' => 'Address Changed',
                                'message' => 'You have changed your current home address <strong>('.$data->address_first_line.', '.$data->city.', '.$data->country.', '.$data->postcode.')</strong> to a new address <strong>('.$request->input('address_first_line').', '.$request->input('city').', '.$request->input('country').', '.$request->input('postcode').')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'</strong>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.',
                            ]
                        );
                    $supportService = new SupportService;
                    $supportService->save($msgRequest);
                }
            } 
        }
        $data->address_first_line = $request->input('address_first_line');
        $data->city = $request->input('city');
        $data->country = $request->input('country');
        $data->postcode = $request->input('postcode');

        return $data;
    }

    public function save($request)
    {
        
        // create user first
        $userService = new UserService;
        $userData = $request->user;
        $userData = (object) $userData;
        $userData->type = $request->type;
        $user = $userService->save($userData);
        if ($user) {
            $rec = new Patient();
            $request->merge(['user_id' => $user->id]);
            $rec = $this->propertySetter($request, $rec);
            $rec->save();
            if (isset($request->identity_docs)) {
                $this->saveIdentityDoc($request->identity_docs, $rec->id);
            }

            if (isset($request->psychiatric_reports)) {
                $this->savePsychiatricReports($request->psychiatric_reports, $rec->id);
            }
            if (isset($request->attachments)) {
                $this->saveAttachments($request->attachments, $rec->id);
            }
            if ($request->type == 'register') {
                try {
                    // send register email to patient
                    $emailData = array (
                        'user_name' => $rec->title.' '.$user->first_name.' '.$user->last_name,
                        'first_name' => $user->first_name,
                        'action_url' => \URL::to('/api/verify/'.$user->code),
                    );
                    //\Mail::to('sana.aamir1909@gmail.com')->send(new Registration($emailData));
                    \Mail::to($user->email)->send(new Registration($emailData));

                    
                } catch(\Exception $e) {
                }
            }
            return $rec->id;
        } else {
            return false;
        }
    }
    
    public function delete($id)
    {
        $rec = Patient::find($id);
        if(!empty($rec))
        {
            PatientAttachment::where('patient_id', '=', $rec->id)->delete();
            PatientIdentityDoc::where('patient_id', '=', $rec->id)->delete();
            PsychiatricReport::where('patient_id', '=', $rec->id)->delete();
            PatientAnswer::where('patient_id', '=', $rec->id)->delete();
            PatientTextField::where('patient_id', '=', $rec->id)->delete();
            FlaggedMessage::where('user_id', '=', $rec->user_id)->delete();

            $appointments = Appointment::where('patient_id', '=', $rec->id);
            $getAppointments = $appointments->get(['id']);
            AppointmentSession::whereIn('appointment_id', $getAppointments)->delete();

            $clinical = Clinical::whereIn('appointment_id', $getAppointments);
            $getClinical = $clinical->get(['id']);
            ClinicalDoctorAnswer::whereIn('clinical_id', $getClinical)->delete();
            $appointments->delete();
            $clinical->delete();

            $labforms = LabForm::where('patient_id', '=', $rec->id);
            $getLabForms = $labforms->get(['id']);
            LabFormDetail::whereIn('lab_form_id', $getLabForms)->delete();
            $labforms->delete();

            $membership = MembershipSubscription::where('patient_id', '=', $rec->id);
            $getMemberships = $membership->get(['id']);
            MembershipCancelRequest::whereIn('membership_subscription_id', $getMemberships)->delete();
            $membership->delete();
            Subscription::where('patient_id', '=', $rec->id)->delete();

            Note::where('created_by','=' ,$rec->user_id)->orwhere('created_on','=' ,$rec->user_id)->delete();

            RepeatBloodTest::where('patient_id', '=', $rec->id)->delete();
            

            $support = Support::where('sender_id','=' ,$rec->user_id)->orwhere('receiver_id','=' ,$rec->user_id);
            $getSupport = $support->get(['id']);
            $supportMessages = SupportMessage::whereIn('support_id', $getSupport);
            $getSupportMessages = $supportMessages->get(['id']);
            SupportMessageDoc::whereIn('support_message_id', $getSupportMessages)->delete();
            $supportMessages->delete();
            $support->delete();

            Testimonial::where('user_id','=' ,$rec->user_id)->delete();
            ActivityLog::where('user_id','=' ,$rec->user_id)->delete();
            User::where('id', '=', $rec->user_id)->delete();
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Patient::find($id);
        if ($rec) {
            // update user first
            
            $userService = new UserService;
            $userData = $request->user;
            $userData['module_id'] = $rec->user_id;
            $user = $userService->update((object) $userData);
            $rec = $this->propertySetter($request, $rec);
            $rec->update();
            if (isset($request->identity_docs)) {
                $this->updateIdentityDoc($request->identity_docs, $rec->id);
            }  

            if (isset($request->psychiatric_reports)) {
                $this->updatePsychiatricReports($request->psychiatric_reports, $rec->id);
            }
            if (isset($request->attachments)) {
                $this->updateAttachments($request->attachments, $rec->id);
            }        
            return true;
        } else {
            return false;
        } 
    }

    public function get($col, $value, $details = false)
    {
        $patient =  Patient::where($col, $value)->first();
       
        if ($details) {
            $patient->user = $this->userService->get("id", $patient->user_id, true);
            $patient->psychiatric_reports = $this->getPsychiatricReports($patient->id);
            $patient->attachments = $this->getAttachments($patient->id);
            $patient->identity_docs = $this->getIdentityDocs($patient->id);
            $patient->age = Utility::calculateAge($patient->dob);
            if(!empty($patient->user->profile_pic))
                $patient->profile_pic_path =  \URL::to('storage/profile_pic/'.$patient->user->profile_pic);
            else
                $patient->profile_pic_path =  asset('assets/img/avatar2.png');

            $appointment = Appointment::where('patient_id', '=', $patient->id)->where('appointment_type_id','=', '1')->orderBy('id', 'asc')->first();
            if (!empty($appointment)) {
                $patient->initial_app_specialty = $appointment->specialty_id;
            } else {
                $patient->initial_app_specialty = '';
            }

            $patient->mental_hospital_address = MentalHealthHospital::where('id', $patient->mental_hospital_address_id)->first();
           
            $patient->laboratory_address = Laboratory::where('id', $patient->laboratory_address_id)->first();


            if (!empty($patient->address_first_line)) {
                $patient->full_address = $patient->address_first_line.', '.$patient->city.', '.$patient->country.', '.$patient->postcode;
            } else {
                $patient->full_address = '';
            }
            $patient->subscription = null;
            if ($patient->is_member == 1)  {
                $paymentService = new PaymentService;
                $patient->subscription = $paymentService->getMembershipSubscription('patient_id', $patient->id);
            }

            $rbt = RepeatBloodTest::where('patient_id', '=', $patient->id)->first();
            if (!empty($rbt)) {
                $patient->rbt = $rbt;
            } else {
                $patient->rbt = null;
            }
            
        }
        return $patient;
    }

    public function getAll($request)
    {
        $input = $request->all();
        $objects = Patient::orderBy('id', 'desc');

        if (isset($input['filter_by_status'])) {
            $users = User::where('is_archived', '=', $input['filter_by_status'])->get(['id']);
            $objects = $objects->whereIn('user_id',$users);
        }

        if (isset($input['filter_by_is_approved'])) {
            $objects = $objects->where('is_approved', '=', $input['filter_by_is_approved']);
        }

        if (isset($input['filter_by_is_member'])) {
            $objects = $objects->where('is_member', '=', $input['filter_by_is_member']);
        }

        if (isset($input['filter_by_doctor_id'])) {
            $patients = Appointment::where('doctor_id', '=', $input['filter_by_doctor_id'])->get(['patient_id']);
            $objects = $objects->whereIn('id', $patients);
        }

        if (!empty($input['keyword'])) {
           
            $objects = $objects->where(function ($query) use ($input) {
                            $query->whereIn('user_id',function($q) use ($input)
                               {
                                $q->select('id')
                                    ->from('users')
                                    ->orwhere('first_name','LIKE','%'.$input['keyword'].'%')
                                    ->orwhere('last_name','LIKE','%'.$input['keyword'].'%');
                                })->orWhere(function($query) use ($input){
                                    $query->where('dob','LIKE','%'.$input['keyword'].'%');
                                })->orWhere(function($query) use ($input){
                                    $query->where('postcode','LIKE','%'.$input['keyword'].'%');
                                });
            });
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function savePsychiatricReports($psychiatric_reports, $id)
    {
        if (!empty($psychiatric_reports)) {
            $patientReportsPath = $this->psychiatricReportPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($patientReportsPath)) {
                mkdir($patientReportsPath, 0777, true);
            }
            try{
                $mediaService = new MediaService();
                foreach ($psychiatric_reports as $key => $doc) {
                    $fileInfo = Utility::fileInfo($mediaService->tmpPath.$doc['name']);
                    $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->psychiatricReportPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                    
                   $doc = new PsychiatricReport;
                   $doc->filename = $fileInfo['name'];
                   $doc->filesize = $fileInfo['size'];
                   $doc->filetype = $fileInfo['extension'];
                   $doc->patient_id = $id;
                   $doc->save();
                }
                
                return true;
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }
        }
    }

    public function updatePsychiatricReports($psychiatric_reports, $id)
    {

        
        if (!empty($psychiatric_reports)) {
            $patientReportsPath = $this->psychiatricReportPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($patientReportsPath)) {
                mkdir($patientReportsPath, 0777, true);
            }
            // get previous reports
            $previousReports = $this->getPsychiatricReports($id);

            // get the file name which are not in new array
            //$recToDelete = array_diff($previousReports, $psychiatric_reports);
            $recToDelete = array_map('unserialize',
                array_diff(array_map('serialize', $previousReports), array_map('serialize', $psychiatric_reports))
            );
            PsychiatricReport::whereIn('filename', $recToDelete)->delete();
            
            // get new records
            //$recToAdd = array_diff($psychiatric_reports, $previousReports);
            $recToAdd = array_map('unserialize',
                array_diff(array_map('serialize', $psychiatric_reports), array_map('serialize', $previousReports))
            );

            if (!empty($recToAdd)) {
                try{
                    $mediaService = new MediaService();
                    foreach ($recToAdd as $key => $doc) {
                        $fileInfo = Utility::fileInfo($mediaService->tmpPath.$doc['name']);
                        $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->psychiatricReportPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                       
                       $doc = new PsychiatricReport;
                       $doc->filename = $fileInfo['name'];
                       $doc->filesize = $fileInfo['size'];
                       $doc->filetype = $fileInfo['extension'];
                       $doc->patient_id = $id;
                       $doc->save();
                    }
                    
                    return true;
                } catch(\Exception $e){
                    //dd($e->getMessage());
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function getPsychiatricReports($patient_id) {
        $psychiatric_reports  = PsychiatricReport::where("patient_id", $patient_id)->orderBy('id','desc')->get();
        $data = [];
        if (!empty($psychiatric_reports)) {
            foreach ($psychiatric_reports as $key => $value) {
                
                $data[$key]['name'] = $value->filename; 
                $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($value->created_at));

            }
        }
        return $data;
    }

    public function saveAttachments($attachments, $id)
    {
        if (!empty($attachments)) {
            $patientAttachmentPath = $this->attachmentsPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($patientAttachmentPath)) {
                mkdir($patientAttachmentPath, 0777, true);
            }
            //try{
                $mediaService = new MediaService();
                foreach ($attachments as $key => $doc) {
                    $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->attachmentsPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                    
                   $newDoc = new PatientAttachment;
                   $newDoc->filename = $doc['name'];
                   $newDoc->patient_id = $id;
                   $newDoc->save();
                }
                
                return true;
            /*} catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }*/
        }
    }

    public function updateAttachments($attachments, $id)
    {

        
        if (!empty($attachments)) {
            $patientAttachmentPath = $this->attachmentsPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($patientAttachmentPath)) {
                mkdir($patientAttachmentPath, 0777, true);
            }
            // get previous reports
            $previousAttachments = $this->getAttachments($id);

            // get the file name which are not in new array
            //$recToDelete = array_diff($previousAttachments, $attachments);
            $recToDelete = array_map('unserialize',
                array_diff(array_map('serialize', $previousAttachments), array_map('serialize', $attachments))
            );
            PatientAttachment::whereIn('filename', $recToDelete)->delete();
            
            // get new records
            //$recToAdd = array_diff($attachments, $previousAttachments);
            $recToAdd = array_map('unserialize',
                array_diff(array_map('serialize', $attachments), array_map('serialize', $previousAttachments))
            );

            if (!empty($recToAdd)) {
                try{
                    $mediaService = new MediaService();
                    foreach ($recToAdd as $key => $doc) {
                        $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->attachmentsPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                       
                       $newDoc = new PatientAttachment;
                       $newDoc->filename = $doc['name'];
                       $newDoc->patient_id = $id;
                       $newDoc->save();
                    }
                    
                    return true;
                } catch(\Exception $e){
                    //dd($e->getMessage());
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function getAttachments($patient_id) {
        $attachments  = PatientAttachment::where("patient_id", $patient_id)->orderBy('id','desc')->get();
        $data = [];
        if (!empty($attachments)) {
            foreach ($attachments as $key => $value) {
                $data[$key]['name'] = $value->filename; 
                $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($value->created_at));
            }
        }
        return $data;
    }


    public function saveIdentityDoc($identity_docs, $id)
    {
        if (!empty($identity_docs)) {
            $patientIdentityPath = $this->photoIdPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
           
            if (!is_dir($patientIdentityPath)) {
                mkdir($patientIdentityPath, 0777, true);
            }
            try{
                $mediaService = new MediaService();
                foreach ($identity_docs as $key => $doc) {
                    $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->photoIdPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                    
                   $newDoc = new PatientIdentityDoc;
                   $newDoc->filename = $doc['name'];
                   $newDoc->patient_id = $id;
                   $newDoc->save();
                }
                
                return true;
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }
        }
    }

    public function updateIdentityDoc($identity_docs, $id)
    {

        
        if (!empty($identity_docs)) {
            $patientIdentityPath = $this->photoIdPath.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
            if (!is_dir($patientIdentityPath)) {
                mkdir($patientIdentityPath, 0777, true);
            }
            // get previous reports
            $previouDocs = $this->getIdentityDocs($id);
           
            // get the file name which are not in new array
            //$recToDelete = array_diff($previouDocs, $identity_docs);
            $recToDelete = array_map('unserialize',
                array_diff(array_map('serialize', $previouDocs), array_map('serialize', $identity_docs))
            );
            PatientIdentityDoc::whereIn('filename', $recToDelete)->delete();
            
            // get new records
            //$recToAdd = array_diff($identity_docs, $previouDocs);
            $recToAdd = array_map('unserialize',
                array_diff(array_map('serialize', $identity_docs), array_map('serialize', $previouDocs))
            );

            if (!empty($recToAdd)) {
                try{
                    $mediaService = new MediaService();
                    foreach ($recToAdd as $key => $doc) {
                        $mediaService->moveFile($mediaService->tmpPath.$doc['name'], $this->photoIdPath.$id.DIRECTORY_SEPARATOR.$doc['name']);
                       
                       $newDoc = new PatientIdentityDoc;
                       $newDoc->filename = $doc['name'];
                       $newDoc->patient_id = $id;
                       $newDoc->save();
                    }
                    
                    return true;
                } catch(\Exception $e){
                    //dd($e->getMessage());
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function getIdentityDocs($patient_id) {
        $docs  = PatientIdentityDoc::where("patient_id", $patient_id)->get();
        $data = [];
        if (!empty($docs)) {
            foreach ($docs as $key => $value) {
                $data[$key]['name'] = $value->filename; 
                $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($value->created_at));
            }
        }
        return $data;
    }

    public function updateApproval($request)
    {
        $rec = Patient::find($request->id);
        if ($rec) {

            $rec->is_approved = $request->is_approved;
            $rec->update();
            if ($rec->is_approved == '1') { // pnly approved emails for now
                $patient = $this->get('id', $rec->id, true);
                try {

                    //$subject = ($rec->is_approved == '1') ? 'Account Approved' : 'Account Disapproved'; 
                    $subject = 'Account Approved';
                    $emailData = array ('patient' => $patient,
                                        //'is_approved' => $rec->is_approved,
                                        'subject' => $subject,
                                        'action_url' => \URL::to('/login')
                                );
                    \Mail::to($emailData['patient']->user->email)->send(new ApprovalEmail($emailData));
                    //\Mail::to('sana.aamir1909@gmail.com')->send(new ApprovalEmail($emailData));
                    return true;
                } catch(\Exception $e){
                    //dd($e->getMessage());
                    return false;
                }
            }
            
            return true;
        } else {
            return false;
        } 
    }

    public function updateBlockLogin($request)
    {
        $rec = Patient::find($request->id);
        if ($rec) {
            User::where('id', '=', $rec->user_id)->update(['is_blocked_login' => $request->is_blocked_login]);     
            return true;
        } else {
            return false;
        } 
    }

    public function updateCommon($request)
    {
        $rec = Patient::find($request->id);
        if ($rec) {
           
            if(\Auth::user()->hasRole('patient')) {
                $subject = '';
                $message = '';
                if ($request->column == 'laboratory_address_id') {
                    $laboratoryService = new LaboratoryService;
                    $laboratoryOld = $laboratoryService->get('id', $rec->laboratory_address_id);
                    $laboratoryNew = $laboratoryService->get('id', $request->value);
                    $subject = 'Laboratory Address Changed';
                    $message = 'You have changed your current laboratory address <strong>('.$laboratoryOld->name.', '.$laboratoryOld->address_first_line.', '.$laboratoryOld->city.', '.$laboratoryOld->country.', '.$laboratoryOld->postcode.')</strong> to a new laboratory address <strong>('.$laboratoryNew->name.', '.$laboratoryNew->address_first_line.', '.$laboratoryNew->city.', '.$laboratoryNew->country.', '.$laboratoryNew->postcode.')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'<strong/>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.';
                } else if ($request->column == 'mental_hospital_address_id') {
                    $hospitalService = new MentalHealthHospitalService;
                    $hospitalOld = $hospitalService->get('id', $rec->mental_hospital_address_id);
                    $hospitalNew = $hospitalService->get('id', $request->value);
                    $subject = 'Hospital Address Changed';
                    $message = 'You have changed your current hospital address <strong>('.$hospitalOld->name.', '.$hospitalOld->address_first_line.', '.$hospitalOld->city.', '.$hospitalOld->country.', '.$hospitalOld->postcode.')</strong> to a new hospital address <strong>('.$hospitalNew->name.', '.$hospitalNew->address_first_line.', '.$hospitalNew->city.', '.$hospitalNew->country.', '.$hospitalNew->postcode.')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'</strong>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.';
                } else if ($request->column == 'sharing_consent') {
                    $sharing_consent_old = ($rec->value == 0) ? 'No' : 'Yes';

                    $sharing_consent_new = ($request->value == 0) ? 'No' : 'Yes';
                    $subject = 'Sharing Information Consent Changed';
                    $message = 'You have changed your sharing information consent from <strong>('. $sharing_consent_old.')</strong> to <strong>('. $sharing_consent_new. ')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'</strong>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.';       
                } else if ($request->column == 'is_member') {
                    $subject = 'Membership Subscription Changed';
                    $message = 'You have changed your membership subscription from <strong>('. ($rec->is_member == 0) ? 'No' : 'Yes'.')</strong> to <strong>('. ($request->is_member == 0) ? 'No' : 'Yes'.')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'</strong>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.';
                }


                if ($subject != '' && $message != '') {
                    // generate ticket
                    $msgRequest = new \Illuminate\Http\Request();
                    $msgRequest->replace(
                            [
                                'email_type' => 'individual',
                                'user_type' => 'patient',
                                'user_id' => $rec->user_id,
                                'sender_id' => User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first(),
                                'category' => 'changes_messages',
                                'subject' => $subject,
                                'message' => $message,
                            ]
                        );
                    $supportService = new SupportService;
                    $supportService->save($msgRequest);
                }

                try {
                    $newParams = array(
                                'user_id'   => Auth::user()->id,
                                'module'    => 'user_profile',
                                'user_type' => ucfirst(Auth::user()->roles->first()->name),
                                'log_id'    => $rec->id,
                                'log_type'  => 'updated'
                            );
                    ActivityLogManager::create($newParams);
                } catch(Exception $e){
                }
               
            }

            $column = $request->column;
            $rec->$column = $request->value;
            $rec->update();   
            return $this->get('id', $rec->id, true);
        } else {
            return false;
        } 
    }

    public function sendDocuments($request) {
        //p($request->all());
        $patient = $this->get('id', $request->patient_id, true);
        $doctorService = new DoctorService;
        $doctor = $doctorService->get('id', $request->doctor_id,true);
        if (!empty($request->documents)) {
            

            $tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            $docs = [];
            foreach ($request->documents as $key => $value) {
                $docs[$key]['file'] = $value;
                $docs[$key]['file_path'] = $tmpPath.$value;
            }

            try {
                $emailData = array ('sender' => $patient,
                                    'receiver' => $doctor,
                                    'docs' => $docs, 
                            );
                \Mail::to($emailData['receiver']->user->email)->send(new SendDocument($emailData));
                //\Mail::to('sana.aamir1909@gmail.com')->send(new SendDocument($emailData));
               
            } catch(\Exception $e){
                //dd($e->getMessage());
                return false;
            }

            $docs = [];
            foreach ($request->documents as $key => $value) {
                $docs[$key]['name'] = $value;
                $docs[$key]['created_at'] = date('Y-m-d H:i:s');
            }
            $this->saveAttachments($docs, $request->patient_id);

             return true;
        }
    }
}


?>