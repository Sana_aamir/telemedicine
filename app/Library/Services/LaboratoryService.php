<?php
namespace App\Library\Services;
use App\Library\Contracts\LaboratoryServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Laboratory;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class LaboratoryService implements LaboratoryServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->name = $request->name;
        $data->address_first_line = $request->address_first_line;
        $data->city = $request->city;
        $data->country = $request->country;
        $data->postcode = $request->postcode;
        $data->telephone_number = $request->telephone_number;
        $data->email = $request->email;
        $data->fax_number = $request->fax_number;
        return $data;
    }

    public function save($request)
    {
        $rec = new Laboratory();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Laboratory::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Laboratory::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        return Laboratory::where($col, $value)->first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new Laboratory;
        if (isset($input['search_by_postcode'])) {
            $objects = $objects->where('postcode','LIKE','%'.$input['search_by_postcode'].'%');
        }

        if (isset($input['filter_by_id'])) {
            $objects = $objects->where('id','=',$input['filter_by_id']);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function importExcel($request)
    {
        if($request->hasFile('import_file')){
            \Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                foreach ($reader->toArray() as $key => $row) {
                    if (count($row) > 0) {
                        //foreach ($row as $k => $v) {
                            $labExist = Laboratory::where('email', '=', $row['telephone_number'])->first();
                            if (empty($labExist)) {
                                $data = [];
                                $data['name'] = $row['name'];
                                if (isset($row['address_first_line'])) {
                                    $data['address_first_line'] = $row['address_first_line'];
                                }

                                if (isset($row['city'])) {
                                    $data['city'] = $row['city'];
                                }

                                if (isset($row['country'])) {
                                    $data['country'] = $row['country'];
                                }

                                if (isset($row['postcode'])) {
                                    $data['postcode'] = $row['postcode'];
                                }
                                if (isset($row['telephone_number'])) {
                                    $data['telephone_number'] = $row['telephone_number'];
                                }
                                if (isset($row['email'])) {
                                    $data['email'] = $row['email'];
                                }
                                
                                if (isset($row['fax_number'])) {
                                    $data['fax_number'] = $row['fax_number'];
                                } else {
                                    $data['fax_number'] = $row['telephone_number'];
                                }
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $data['updated_at'] = date('Y-m-d H:i:s');
                                if(!empty($data)) {
                                    Laboratory::insert($data);
                                }
                            }
                        //}
                    } 
                }
            });
        }    

        return true;
    }

    public function getDistance() {
        $postcode1='W1J0DB';
        $postcode2='W23UW';
        $result = array();

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$postcode1&destinations=$postcode2&mode=driving&language=en-EN&sensor=false&key=AIzaSyD5TDCFhzWjRRjeSaj4SDyp2saRM2J7HJM";

        $data = @file_get_contents($url);

        $result = json_decode($data, true);
        if ($result['status'] == 'OK') {
            if (!empty($result['rows'])) {
                $distance = $result['rows'][0]['elements'][0]['distance']['text'];
                
            }
        }        
    }

    public function getLatLong($code){

        $query = 'http://api.postcodes.io/postcodes?q=' . urlencode($code) . '';
        $data = file_get_contents($query);

        if($data){
            // convert into readable format
            $data = json_decode($data);
            if (!empty($data->result)) {
                return array('longitude'=>$data->result[0]->longitude,'latitude'=>$data->result[0]->latitude);
            } else {
                return false;
            }
            
        } else {
            return false;
        }

        
    }

    public function laboratoriesDistance($request) {
        $results = [];
        $postcode = $this->getLatLong($request->search_by_postcode);
        if (!empty($postcode)) {
            $laboratories = Laboratory::all();
            
            if (!empty($laboratories)) {
                foreach ($laboratories as $key => $value) {
                    
                    $results[$value->postcode] = $this->distance($postcode['latitude'], $postcode['longitude'], $value->lat, $value->lng, 'k');
                    
                }
                $lowerLimit = 0;
                $upperLimit = 8;
                if (isset($request->page)) {
                    $lowerLimit = $request->page+8;
                    $upperLimit = $lower+8;
                }
                arsort($results);
                $reversedArr = array_reverse($results);
                $closet = array_slice($reversedArr, $lowerLimit, $upperLimit);

                $objects = [];
                if (!empty($closet)) {
                    $i = 0;
                    foreach ($closet as $key => $value) {
                        $objects[$i] = Laboratory::where('postcode', '=', $key)->first()->toArray();
                        $i++;
                    }
                }
                
                /*$objects = Laboratory::whereIn('postcode', array_keys($closet))->groupBy('postcode')->get(['id','name','postcode'])->toArray();
                */
                if (!empty($objects)) {
                    foreach ($objects as $key => &$value) {
                        $value['distance'] = $closet[$value['postcode']];
                    }
                }

                usort($objects, function($a, $b) {
                    return $a['distance'] <=> $b['distance'];
                });

                $final['data'] = $objects;
                return $final;
            }
        }
        $final['data'] = '';
        return $final;
        
    }


    public function distance($lat1, $lon1, $lat2, $lon2, $u='1') {
        $u=strtolower($u);
        if ($u == 'k') { $u=1.609344; } // kilometers
        elseif ($u == 'n') { $u=0.8684; } // nautical miles
        elseif ($u == 'm') { $u=1; } // statute miles (default)
        $d=sin(deg2rad($lat1))*sin(deg2rad($lat2))+cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lon1-$lon2));
        $d=rad2deg(acos($d));
        $d=$d*60*1.1515;
        $d=($d*$u); // apply unit
        $d=round($d); // optional
        return $d;
    }

}


?>