<?php
namespace App\Library\Services;
use App\Library\Contracts\PageServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Page;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class PageService implements PageServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->title = $request->input('title');
        $data->slug = $request->input('slug');
        if ($request->input('type') == 'save') {
            $data->draft_content = $request->input('content');
        } else if ($request->input('type') == 'publish') {
            $data->draft_content = $request->input('content');
            $data->content = $request->input('content');
        }

        if ($request->input('status') !== null) {
            $data->status = $request->input('status');
        }
        return $data;
    }

    public function save($request)
    {
        $rec = new Page();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Page::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Page::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        return Page::where($col, $value)->first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new Page;
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

}


?>