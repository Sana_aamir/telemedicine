<?php
namespace App\Library\Services;
use App\Library\Contracts\QuestionnaireServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Questionnaire;
use App\Models\PatientTextField;
use App\Models\PatientAnswer;
use App\Models\Clinical;
use App\Models\ClinicalDoctorAnswer;
use App\Models\MentalHealthHospital;
use App\Models\Specialty;
use App\Models\Appointment;
use App\Models\Patient;
use App\Mail\HospitalRecordEmail;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class QuestionnaireService implements QuestionnaireServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->specialty_id = $request->input('specialty_id');
        $data->question = $request->input('question');
        return $data;
    }

    public function save($request)
    {
        $rec = new Questionnaire();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Questionnaire::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Questionnaire::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false, $input = [])
    {
        $question = Questionnaire::where($col, $value)->first();
        if ($detail) {
            $question->specialty = Specialty::where('id', $question->specialty_id )->first();
            if (!empty($input['get_patient_answer'])) { // get patient answer
                $answer = PatientAnswer::where('question_id', $question->id)->where('patient_id', $input['filter_by_patient_id'])->first();
                if (!empty($answer)) {
                    $question->answer = $answer->answer;
                } else {
                    $question->answer = '-';
                }
            }

        }
        return $question;
    }

    public function getAll($request)
    {
        $input = $request->all();
        $objects = new Questionnaire;

        if (isset($input['filter_by_specialty'])) {
            $objects = $objects->where('specialty_id', '=', $input['filter_by_specialty']);
        }
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true, $input);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        if (!empty($input['filter_by_patient_id']) && !empty($input['filter_by_specialty'])) {

            $data['patient_text'] = PatientTextField::where('patient_id', '=', $input['filter_by_patient_id'])->where('specialty_id', '=', $input['filter_by_specialty'])->first();

        } else {
            $data['patient_text'] = '';
        }
        
 
        return $data;        
    }

    
   /* questionnaire : [
        {
            question_id : 1,
            answer : 'yes'
        }
    ]*/
    public function savePatientQuestionaire($request) {
        if (!empty($request->questionnaire)) {
            foreach ($request->questionnaire as $key => $value) {
                if (!empty($value['question_id'])) {
                    $patientAns = new PatientAnswer;
                    $patientAns->patient_id = $request->patient_id;
                    $patientAns->question_id = $value['question_id'];
                    $patientAns->answer = $value['answer'];
                    $patientAns->save();
                }
            }
        }
        if (isset($request->questionnaire_text)) {
            if (!empty($request->questionnaire_text)) {
                $patientText = new PatientTextField;
                $patientText->patient_id = $request->patient_id;
                $patientText->specialty_id = $request->specialty_id;
                $patientText->text = $request->questionnaire_text;
                $patientText->save();
            }
        }
        
        return true;
    }

    public function saveClinicalRecord ($request) {
        $clinical  = new Clinical;
        $clinical->appointment_id = $request['appointment_id'];
        $clinical->hospital_id = $request['hospital_id'];
        $clinical->doctor_final_comment = $request['doctor_final_comment'];
        if ($clinical->save()) {
            if (isset($request['questionnaire'])) {
                foreach ($request['questionnaire'] as $key => $value) {
                    if (!empty($value['answer'])) {
                        $doctorAns = new ClinicalDoctorAnswer;
                        $doctorAns->clinical_id = $clinical->id;
                        $doctorAns->doctor_id = $request['doctor_id'];
                        $doctorAns->question_id = $value['question_id'];
                        $doctorAns->answer = $value['answer'];
                        if (!empty($value['comment'])) {
                            $doctorAns->comment = $value['comment'];
                        } else {
                            $doctorAns->comment = '';
                        }
                        
                        $doctorAns->save();
                    }
                }
            }
        }
        return  $clinical->id;
    }

    public function getClinicalRecord($col, $val) 
    {
        $clinical = Clinical::where($col, '=', $val)->first();
        if (!empty($clinical)) {
            $doctor_ans = ClinicalDoctorAnswer::where('clinical_id', '=', $clinical->id)->get();
            if (!empty($doctor_ans)) {
                foreach ($doctor_ans as $key => &$value) {
                    $question = Questionnaire::where('id', '=', $value->question_id)->first();
                    $value->question = $question->question;
                }
            }
            $clinical->doctor_ans = $doctor_ans;
            $clinical->mental_hospital_address = MentalHealthHospital::where('id', $clinical->hospital_id)->first();
            $appointmentService = new AppointmentService;
            $clinical->appointment = $appointmentService->get('id', $clinical->appointment_id, true);
            if (!empty($clinical->appointment)) {
                $clinical->patient_text = PatientTextField::where('patient_id', '=', $clinical->appointment->patient_id)->where('specialty_id', '=', $clinical->appointment->specialty_id)->orderBy('id', 'desc')->first();
            }

            $clinical->encodedId = Utility::encryptDecrypt('encrypt', $clinical->id);
           
        }
        return $clinical;
    }

    public function getClinicalRecords($request)
    {
        $input = $request->all();
        $objects = Clinical::orderBy('id','desc');

        if (isset($input['filter_by_appointment_id'])) {
            $objects = $objects->where('appointment_id', '=', $input['filter_by_appointment_id']);
        }
        if (isset($input['filter_by_patient_id'])) {
            $appointments = Appointment::where('patient_id', '=', $input['filter_by_patient_id'])->where('status', '=', 'completed')->get(['id']);
            $objects = $objects->whereIn('appointment_id', $appointments);
        }
        if (isset($input['filter_by_doctor_id'])) {
            $appointments = Appointment::where('doctor_id', '=', $input['filter_by_doctor_id'])->where('status', '=', 'completed')->get(['id']);
            $objects = $objects->whereIn('appointment_id', $appointments);
        }

        if (isset($input['filter_by_consent_sharing'])) {
            $patients = Patient::where('sharing_consent', '=', $input['filter_by_consent_sharing'])->get(['id']);

            $appointments = Appointment::whereIn('patient_id', $patients)->where('status', '=', 'completed')->get(['id']);
            //p($appointments);
            $objects = $objects->whereIn('appointment_id', $appointments);
        }
        if (!empty($input['filter_by_date_range'])) {
            if ($input['filter_by_date_range'][0] != 'null') {
                $startDate = date('Y-m-d', strtotime($input['filter_by_date_range'][0]));
                $endDate = date('Y-m-d', strtotime($input['filter_by_date_range'][1]));
                $objects = $objects->where(\DB::raw("date(created_at)"), '>=', $startDate)->where(\DB::raw("date(created_at)"), '<=', $endDate)->orderBy('created_at', 'asc');
            } 
        } 

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->getClinicalRecord('id',$object->id);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }
        
 
        return $data;        
    }

    public function sendHostpitalRecord($id) {
        $clinical  =  $this->getClinicalRecord('id', $id);
        if (!empty($clinical)) {
            $tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
            if (file_exists($tmpPath.'hospital-record-'.$clinical->id.'.pdf')) {
                chmod($tmpPath.'hospital-record-'.$clinical->id.'.pdf',0775);
                unlink($tmpPath.'hospital-record-'.$clinical->id.'.pdf');
            }
            $view = \View::make('backend.print.hospital_record', ['data' => $clinical]);
            $html = $view->render();
            
            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8',
                                    'format' => 'A4',
                                    'margin_left' => 0,
                                    'margin_right' => 0,
                                    'margin_top' => 0,
                                    'margin_bottom' => 0,
                                    'margin_header' => 0,
                                    'margin_footer' => 0,
                                    'debug' => true,
                                    'tempDir' => $tmpPath]);

            $mpdf->writeHTML($html);
            $mpdf->output($tmpPath.'hospital-record-'.$clinical->id.'.pdf', \Mpdf\Output\Destination::FILE);
            try {
                $emailData = array (
                    'subject' => 'Patient Review',
                    'hospital_name' => $clinical->mental_hospital_address->name,
                    'attachement' => $tmpPath.'hospital-record-'.$clinical->id.'.pdf',
                    'patient' =>  $clinical->appointment->patient,
                );
                //\Mail::to('sana.aamir1909@gmail.com')->send(new HospitalRecordEmail($emailData));
                \Mail::to($clinical->mental_hospital_address->email)->send(new HospitalRecordEmail($emailData));
                Clinical::where('id', '=', $clinical->id)->update(['is_emailed' => '1']);
                return true;
            } catch(\Exception $e) {
            }
        }   
        return false;
    }

    public function deleteClinicalRecord($id)
    {
        $rec = Clinical::find($id);
        if(!empty($rec))
        {
            ClinicalDoctorAnswer::where('clinical_id', '=', $rec->id)->delete();
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }
}


?>