<?php
namespace App\Library\Services;
use App\Library\Contracts\DoctorScheduleServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\DoctorSchedule;
use App\Library\Helper\ActivityLogManager;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class DoctorScheduleService implements DoctorScheduleServiceInterface
{

    public function propertySetter($rec, $data)
    {
        $data->doctor_id = $rec['doctor_id'];
        $data->day = $rec['dayName'];
        $data->appointment_type_id = $rec['appointmentTypeId'];
        $data->opening_time = $rec['openingTime'];
        $data->opening_time_name = $rec['openingTimeName'];
        $data->closing_time = $rec['closingTime'];
        $data->closing_time_name = $rec['closingTimeName'];
        return $data;
    }


    public function update($request, $id)
    {
        // add schdeule
        DoctorSchedule::where('doctor_id', '=', $id)->delete();
        if (isset($request->schedule_days)) {
            foreach ($request->schedule_days as $day => $value) {
                $value['doctor_id'] = $id;
                $rec = new DoctorSchedule;
                $rec = $this->propertySetter($value, $rec);
                $rec->save();
            }
        }      
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $doctorSchedule = DoctorSchedule::where($col, $value)->first();
        $data = new \StdClass;
        $data->doctor_id = $doctorSchedule->doctor_id;
        $data->dayName = $doctorSchedule->day;
        $data->openingTime = $doctorSchedule->opening_time;
        $data->openingTimeName = $doctorSchedule->opening_time_name;
        $data->closingTime = $doctorSchedule->closing_time;
        $data->closingTimeName = $doctorSchedule->closing_time_name;
        $data->appointmentTypeId = $doctorSchedule->appointment_type_id;
        if ($data->appointmentTypeId == 1) {
            $data->appointmentTypeName = 'Initial assessment';
        } else if ($data->appointmentTypeId == 2) {
            $data->appointmentTypeName = 'Follow up assessment - type 1';
        } else if ($data->appointmentTypeId == 3) {
            $data->appointmentTypeName = 'Follow up assessment - type 2';
        } else if ($data->appointmentTypeId == 4) {
            $data->appointmentTypeName = 'Telephone advice';
        }
        return $data;
    }

    public function getAll($request, $id)
    {
        $input = $request->all();

        $objects = DoctorSchedule::where('doctor_id', $id);
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public function getAvailChart ($request, $id, $appointment_type_id = 0) {
        $days = ["Monday", "Tuesday", "Wednesday", "Thursday", 'Friday', 'Saturday', "Sunday"];
        $data = [];
        foreach ($days as $key => $value) {
            if ($appointment_type_id != 0) {
                $schdeule = DoctorSchedule::where('doctor_id', '=', $id)->where('appointment_type_id', '=', $appointment_type_id)->where('day','=', $value)->get()->toArray();
            } else {
                $schdeule = DoctorSchedule::where('doctor_id', '=', $id)->where('day','=', $value)->get()->toArray();
            }
            
            if (!empty($schdeule)) {
                foreach ($schdeule as $k => &$v) {
                    if ($v['appointment_type_id'] == 1) {
                        $v['appointment_name'] = 'Initial assessment';
                    } else if ($v['appointment_type_id'] == 2) {
                        $v['appointment_name'] = 'Follow up assessment - type 1';
                    } else if ($v['appointment_type_id'] == 3) {
                        $v['appointment_name'] = 'Follow up assessment - type 2';
                    } else if ($v['appointment_type_id'] == 4) {
                        $v['appointment_name'] = 'Telephone advice';
                    } else {
                        $v['appointment_name'] = '';
                    }
                   $data[$value][] = $v;

                }
                 
            } else {
                $data[$value] = [];
            }
        }
        return $data;
    }

    public function checkSchduleSetOfDoctor($schedule) {
        $data = [];
        foreach ($schedule as $key => $value) {
           if (!empty($value)) {
                $data[$key] = $value;
           }
        }
        if (count($data) > 0)  {
            return true;
        } else {
            return false;
        }
    }
}


?>