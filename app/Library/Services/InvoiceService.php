<?php
namespace App\Library\Services;
use App\Library\Contracts\InvoiceServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Invoice;
use App\Models\Doctor;
use Carbon\Carbon;
use App\Library\Helper\ActivityLogManager;
use App\Library\Helper\Utility;
  
class InvoiceService implements InvoiceServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->user_type = $request->user_type;
        $data->user_id = $request->user_id;
        $data->description = $request->description;
        $data->amount = $request->amount;
        $data->issue_date = $request->issue_date;
        $data->pay_period_start = $request->pay_period_start;
        $data->pay_period_end = $request->pay_period_end;
        return $data;
    }

    public function save($request)
    {
        $rec = new Invoice();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        $invoice_number = Utility::generateInvoiceId('AC230', $rec->id);
        Invoice::where('id','=', $rec->id)->update(['invoice_number' => $invoice_number]);
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Invoice::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Invoice::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        $invoice =  Invoice::where($col, $value)->first();
        if (!empty($invoice)) {
            $invoice->description = unserialize($invoice->description);
            $invoice->percentage_to_doctor = env('DOCTOR_PAYMENT_PERCENTAGE');
            if ($invoice->user_type == 'doctor') {
                $doctorService = new DoctorService;
                $invoice->doctor = $doctorService->get('user_id', $invoice->user_id, true);
            }
            $invoice->encodedId = Utility::encryptDecrypt('encrypt', $invoice->id);
        }

        return $invoice;
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = Invoice::orderBy('id', 'desc');

        if (isset($input['filter_by_user_type'])) {
            $objects = $objects->where('user_type', '=', $input['filter_by_user_type']);
        }

        if (isset($input['filter_by_user_id'])) {
            $objects = $objects->where('user_id', '=', $input['filter_by_user_id']);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

    public static function generateInvoice() {
        // get all doctors
        $month_ini = new \DateTime("first day of last month");
        $month_end = new \DateTime("last day of last month");

        $firstDate = $month_ini->format('Y-m-d'); // 2012-02-01
        $lastDate = $month_end->format('Y-m-d'); // 2012-02-29
        
        $doctors = Doctor::whereIn('user_id',  function($query)
                                   {
                                    $query->select('id')
                                        ->from('users')
                                        ->where('is_archived','=', 0);
                                   })->get();
        if (!empty($doctors)) {
            $appService = new AppointmentService;
            $doctorService = new DoctorService;
            $percentage = env('DOCTOR_PAYMENT_PERCENTAGE');
            foreach ($doctors as $key => $doctor) {
                $request = new \StdClass;
                $request->specialty_id = $doctor->specialty_id;
                $fees = $appService->getAppointmentFee($request);
                $doctorAssessments = $doctorService->getDoctorAssessmentsForMonth($firstDate, $lastDate, $doctor->id);
                $appData = array('initial_assessment' => 
                                [
                                    'member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                    'non_member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                ],      
                                'follow_up_assessment_type_1' => 
                                [
                                    'member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                    'non_member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                ], 
                                'follow_up_assessment_type_2' => 
                                [
                                    'member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                    'non_member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                ],
                                'telephone_advice' =>
                                [
                                    'member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                    'non_member' => [
                                        'patients' => 0,
                                        'fee' => 0,
                                        'total' => 0
                                    ], 
                                ], 
                    );
                $total = 0;
                foreach ($appData as $type => $membership) {
                    foreach ($membership as $k => $v) {
                        $appData[$type][$k]['patients'] = $doctorAssessments[$type][$k];
                        $appData[$type][$k]['fee'] = $fees['data'][$type][$k];
                        $appData[$type][$k]['total'] = $fees['data'][$type][$k]*$doctorAssessments[$type][$k];
                        $appData[$type][$k]['percentage'] = ($appData[$type][$k]['total']*$percentage)/100;
                       $total += $fees['data'][$type][$k]*$doctorAssessments[$type][$k];
                    }
                }
                $cal_per = ($total*$percentage)/100;

                $invoice = new \StdClass;
                $invoice->user_type = 'doctor';
                $invoice->user_id = $doctor->user_id;
                $invoice->amount = $cal_per;
                $invoice->description = serialize($appData);
                $invoice->issue_date = date('Y-m-d');
                $invoice->pay_period_start = $firstDate;
                $invoice->pay_period_end = $lastDate;
                $invoiceService = new InvoiceService;
                $invoiceService->save($invoice);
            }

            return true;
        }
    }

}


?>