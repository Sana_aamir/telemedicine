<?php
namespace App\Library\Services;
use App\Library\Contracts\BusinessSettingServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\BusinessSetting;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class BusinessSettingService implements BusinessSettingServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->name = $request->input('name');
        $data->address_first_line  = $request->input('address_first_line');
        $data->city = $request->input('city');
        $data->country = $request->input('country');
        $data->postcode = $request->input('postcode');
        $data->admin_fee_percentage = $request->input('admin_fee_percentage');

        return $data;
    }

    public function save($request)
    {
        $rec = new BusinessSetting();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = BusinessSetting::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = BusinessSetting::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get()
    {
        return BusinessSetting::first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new BusinessSetting;
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

}


?>