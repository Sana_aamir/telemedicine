<?php
namespace App\Library\Services;
use App\Library\Contracts\NoteServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Note;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class NoteService implements NoteServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->created_by = $request->input('created_by');
        $data->created_on = $request->input('created_on');
        $data->module = $request->input('module');
        $data->subject = $request->input('subject');
        $data->note = $request->input('note');
        return $data;
    }

    public function save($request)
    {
        $request->merge(['created_by' => \Auth::user()->id]);
        $rec = new Note();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = Note::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = Note::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        return Note::where($col, $value)->first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = Note::orderBy('id', 'desc');
        
        if (isset($input['filter_by_created_on'])) {
            $objects = $objects->where('created_on', '=', $input['filter_by_created_on']);
        }

        if (isset($input['filter_by_created_by'])) {
            $objects = $objects->where('created_by', '=', $input['filter_by_created_by']);
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

}


?>