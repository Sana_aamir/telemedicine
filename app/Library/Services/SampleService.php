<?php
namespace App\Library\Services;
use App\Library\Contracts\SampleServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\ModelName;
use Carbon\Carbon;
use App\Library\Helper\Utility;
  
class SampleService implements SampleServiceInterface
{

    public function propertySetter($request, $data)
    {
        $data->title = $request->input('title');
        return $data;
    }

    public function save($request)
    {
        $rec = new ModelName();
        $rec = $this->propertySetter($request, $rec);
        $rec->save();
        return $rec->id;
    }
    
    public function delete($id)
    {
        $rec = ModelName::find($id);
        if(!empty($rec))
        {
            $rec->delete();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function update($request, $id)
    {
        $rec = ModelName::find($id);
        $rec = $this->propertySetter($request, $rec);
        $rec->update();        
        return true;
    }

    public function get($col, $value,  $detail = false)
    {
        return ModelName::where($col, $value)->first();
    }

    public function getAll($request)
    {
        $input = $request->all();

        $objects = new ModelName;
        if(isset($input['limit']) && $input['limit'] == 0)
        {
            $objects = $objects->get(['id']);
        }
        else 
        {
            $objectIdsPaginate = $objects->paginate($input['limit'], ['id']);
            $objects = $objectIdsPaginate->items(['id']);

        }

        $data = ['data'=>[]];
        if (count($objects) > 0) {
            $i = 0;
            foreach ($objects as $object) {
                $objectData = $this->get('id',$object->id, true);
                $data['data'][$i] = $objectData;            
                $i++;
            }
        }

        if(isset($input['limit']) && $input['limit'] == 0)
        {
            // do nothing
        } else {
            $data = Utility::paginator($data, $objectIdsPaginate, $input['limit']);
        }

        return $data;        
    }

}


?>