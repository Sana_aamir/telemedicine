<?php
namespace App\Library\Services;
use App\Library\Contracts\MediaServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Media;
use Carbon\Carbon;
use App\Library\Helper\UploadHandler;
use Illuminate\Support\Facades\File;
use Image;

  
class MediaService implements MediaServiceInterface
{
    public $tmpPath;

    function __construct()
    {
        $this->tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
    }

    public function upload($request)
    {
        $file = $request->file;

        
        $fileName = time().'-'.strtolower($file->getClientOriginalName());
        $data  = [];
        $data['status'] = '';
        $data['name'] = '';
        $data['messages'] = [];
        // create image object
        $image = \Image::make($file);
        if (isset($request->isTake)) {
            $image = \Image::make($file)->orientate();
        } else {
            if (isset($request->type)) {
                if ($request->type == 'profile_pic') {
                    $fileName = time().'-profile.jpg';
                    $image = (string) Image::make($file)->encode('jpg', 75);
                    file_put_contents($this->tmpPath.$fileName, $image);
                    $data['name'] = $fileName;
                    $data['status'] = 'success';
                }  else {
                    $filePath = $this->tmpPath.$fileName;
                    // resize and save large logo
                    $image->save($filePath);
                    $data['name'] = $fileName;
                    $data['status'] = 'success';
                } 
            } else {
                $filePath = $this->tmpPath.$fileName;
                // resize and save large logo
                $image->save($filePath);
                $data['name'] = $fileName;
                $data['status'] = 'success';
            }
        }

        // if (isset($request->type)) {
        //     if ($request->type == 'profile_pic') {
        //         // image width height must be greater then 512
        //         /*if (($image->height() != $image->width()) || ($image->height() < 200 || $image->width() < 200)) {
        //             $data['messages'] = 'Width and Height of the image must be equal and greater then 200px';
        //             $data['status'] = 'error';
        //         } else {*/

        //             $filePath = $this->tmpPath.$fileName;
        //             $image->resize(200,200)->save($filePath);
        //             $data['name'] = $fileName;
        //             $data['status'] = 'success';
        //         //}
        //     } else {
        //         $filePath = $this->tmpPath.$fileName;
        //         $image->save($filePath);
        //         $data['name'] = $fileName;
        //         $data['status'] = 'success';
        //     }
        // } else {
        //     $filePath = $this->tmpPath.$fileName;
        //     // resize and save large logo
        //     $image->save($filePath);
        //     $data['name'] = $fileName;
        //     $data['status'] = 'success';
        // }

        

        return $data;
     }

     public function deleteFile()
     {

     }

     public function renameFile($file)
     {
        return str_replace(' ', '-', $file);
     }

     public function moveFile($source, $destination, $replace = true)
     {
        if(file_exists($source))
        {
            if(!$replace && file_exists($destination))
            {
                return false;
            }
            else
            {
                File::move($source, $destination);
            }
        }
        else
        {
            return false;
        }
     }

}


?>