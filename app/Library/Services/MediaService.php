<?php
namespace App\Library\Services;
use App\Library\Contracts\MediaServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Media;
use Carbon\Carbon;
use App\Library\Helper\UploadHandler;
use Illuminate\Support\Facades\File;
use Image;

  
class MediaService implements MediaServiceInterface
{
    public $tmpPath;

    function __construct()
    {
        $this->tmpPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR;
    }

    public function upload($request)
    {
        error_reporting(E_ALL | E_STRICT);
        $file = $request->file;

        $fileName = time().'-'.strtolower($file->getClientOriginalName());
        $data  = [];
        $data['status'] = '';
        $data['name'] = '';
        $data['messages'] = [];
        
        if (isset($request->isTake)) {
            // create image object
            $image = \Image::make($file)->orientate();
            $filePath = $this->tmpPath.$fileName;
            $image->save($filePath);
            $data['name'] = $fileName;
            $data['status'] = 'success';
        } else { // isUpload

            if (isset($request->type)) {
                if ($request->type == 'profile_pic') {
                    $fileName = time().'-profile.jpg';
                    $image = (string) Image::make($file)->encode('jpg', 75);
                    file_put_contents($this->tmpPath.$fileName, $image);
                    $data['name'] = $fileName;
                    $data['status'] = 'success';
                }  else {

                    $options = array('upload_dir' => $this->tmpPath,
                                     'param_name' => 'file',
                                     'print_response' => false,
                                     'max_file_size' => 10000000 //10MB
                                    );

                    $upload_handler = new UploadHandler($options);
                    if(!isset($upload_handler->response['file'][0]->error))
                    {
                        // file uploaded
                        $data['name'] = $upload_handler->response['file'][0]->name;
                        $data['status'] = 'success';
                    }
                    else
                    {
                        $data['status'] = 'error';
                    }
                } 
            } else {
                $options = array('upload_dir' => $this->tmpPath,
                                 'param_name' => 'file',
                                 'print_response' => false,
                                 'max_file_size' => 10000000 //10MB
                                );

                $upload_handler = new UploadHandler($options);
                if(!isset($upload_handler->response['file'][0]->error))
                {
                    // file uploaded
                    $data['name'] = $upload_handler->response['file'][0]->name;
                    $data['status'] = 'success';
                }
                else
                {
                    $data['status'] = 'error';
                }
            }
        }
        return $data;
     }

     public function deleteFile()
     {

     }

     public function renameFile($file)
     {
        return str_replace(' ', '-', $file);
     }

     public function moveFile($source, $destination, $replace = true)
     {
        if(file_exists($source))
        {
            if(!$replace && file_exists($destination))
            {
                return false;
            }
            else
            {
                File::move($source, $destination);
            }
        }
        else
        {
            return false;
        }
     }

}


?>