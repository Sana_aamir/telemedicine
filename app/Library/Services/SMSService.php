<?php
namespace App\Library\Services;
use App\Library\Contracts\SMSServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Twilio\Rest\Client;
use App\Library\Helper\Utility;
  
class SMSService implements SMSServiceInterface
{

    public $account_sid;
    public $auth_token;
    public $twilio_phone_number;

    function __construct()
    {
        $this->account_sid = env('SMS_ACCOUNT_ID');
        $this->auth_token = env('SMS_AUTH_TOKEN');
        $this->twilio_phone_number = env('TWILIO_PHONE_NUMBER');
    }

    public function sendSMS($phone_number, $message) {
        $formated_phone_number = $this->formatPhoneNumber($phone_number);
        try {
            $client = new Client($this->account_sid, $this->auth_token);
            // $message = $client->messages->create(
            //   $formated_phone_number, // Text this number
            //   array(
            //     'from' => $this->twilio_phone_number, // From a valid Twilio number
            //     'body' => $message
            //   )
            // );
            return true;
        } catch(\Exception $e){
            //dd($e->getMessage());
            return false;
        }   
    }

    public function formatPhoneNumber($original) {
        // Strip out all characters apart from numbers
        $phone = preg_replace('/[^0-9]+/', '', $original);
        // Remove the 2 digit international code (+44)
        if (substr($phone, 0, 2) == '44') {
            $phone = substr($phone, 2);
        }
        // Remove the 4 digit international code (0044)
        if (substr($phone, 0, 4) == '0044') {
            $phone = substr($phone, 4);
        }
        // Remove the initial Zero from the number
        // Some people write it in international numbers like this: +44 (0)1234 567 890
        // But it shouldn't be entered when dialling
        if (substr($phone, 0, 1) == '0') {
            $phone = substr($phone, 1);
        }
        // Add the international prefix
        $phone = '+44' . $phone;
        return $phone;
    }

    public function smsTemplates($type) {
        $template = '';
        if ($type == 'appointment_reminder_patient') 
        {
            $template = "[PATIENT_NAME] Don't forget your appointment on [DATE] at [TIME], at Happy Clinic. Please login to [LINK]";
        } 
        else if ($type == 'appointment_reminder_doctor') 
        {
            $template = "There is a patient([PATIENT_INTITIALS]) booked in to see you at Happy Clinic on [DATE] at [TIME]. [LINK]";
        } 
        else if ($type == 'appointment_schedule_reminder') 
        {
            $template = "[PATIENT_NAME] your routine appointment at Happy Clinic is overdue. Please login to [LINK] to book your appointment online";
        }
        else if ($type == 'appointment_Confirmation_patient') 
        {
            $template = "Your Happy Clinic appointment on [DATE] at [TIME] is confirmed. Details can be found at [LINK]";
        }
        else if ($type == 'appointment_Confirmation_doctor') 
        {
            $template = "There is a patient([PATIENT_INTITIALS]) booked in to see you at Happy Clinic on [DATE] at [TIME]. [LINK]";
        }
        else if ($type == 'assessment_invitation') 
        {
            $template = "Please click the link to start the video consultation: [LINK]";
        } 
        return $template;
    } 

}


?>