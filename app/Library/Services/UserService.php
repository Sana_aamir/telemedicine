<?php
namespace App\Library\Services;
use App\Library\Contracts\UserServiceInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\Role;
use App\Models\PasswordReset;
use App\Mail\PasswordRequest;
use App\Mail\ContactEmail;
use App\Library\Helper\ActivityLogManager;
use App\Library\Helper\Utility;
use Carbon\Carbon;

  
class UserService implements UserServiceInterface
{
    public $profilePath;

    function __construct()
    {
        $this->profilePath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'profile_pic'.DIRECTORY_SEPARATOR;
    }

    /*public function login($request)
    {
        $credentials = request(['email', 'password']);
        
		if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) 
		{

            //$user = User::where('email', '=', $request->input('email'))->update(['token' => bcrypt(date('Y-m-d H:i:s'))]);
            $user = $this->get('email',  $request->input('email'), true);
            if ($user->role == 'Superadmin' && $request->input('from') == 'site') {
                return false;
            } else if (($user->role == 'Patient' ||  $user->role == 'Doctor') && $request->input('from') == 'admin') {
                return false;
            } else if ($user->email_verified == 0) {
                return 'not_verified_email';
            } else if ($user->is_archived == 1) {
                return 'archived_account';
            } else if ($user->is_blocked_login == 1) {
                return 'blocked_login';
            } else {
                // to maintain log
                try {
                    $newParams = array(
                                'user_id'   => Auth::user()->id,
                                'user_type'   => ucfirst(Auth::user()->roles->first()->name),
                                'module'    => 'user_profile',
                                'log_id'    => Auth::user()->id,
                                'text'    => Utility::get_ip_address(),
                                'log_type'  => 'login'
                            );
                    
                    ActivityLogManager::create($newParams);
                } catch(\Exception $e){
                }
                return 'success';
            }
        }
        else
        {
        	return false;
        }
    }*/


    public function getRoles($request)
    {
        return Role::get();
    }

    public function updateProfile($request)
    {
        if (!empty($request->input('profile_pic'))) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->input('profile_pic'), $this->profilePath.$request->input('profile_pic'));
        }

        $user = Auth::user();
        $user->first_name = $request->input('first_name', '');
        $user->last_name = $request->input('last_name', '');
        $user->profile_pic = $request->input('profile_pic', '');
        $user->email = $request->input('email', '');
        $user->update();

        return true;
    }



    public function resetPasswordByToken($request)
    {
        $resetData = PasswordReset::where('token', $request->input('token'))->first();
        if(!empty($resetData))
        {
            $this->resetPassword($resetData->email, $request->input('password'));
            return true;
        }
        else
        {
            return false;
        }
    }

    public function delete($id)
    {
        $data = User::find($id);

        if(!empty($data))
        {
            $data->delete();
            // to maintain log
            try {
                $newParams = array(
                            'user_id'   => Auth::user()->id,
                            'user_type' => ucfirst(Auth::user()->roles->first()->name),
                            'module'    => 'users',
                            'log_id'    => $id,
                            'log_type'  => 'deleted'
                        );
                ActivityLogManager::create($newParams);
            } catch(Exception $e){

            }
            return true;
        }
        else
        {
            return false;
        }
    }


    public function propertySetter($request, $data)
    {
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        if(!empty($request->password)) {
            $data->password = bcrypt($request->password);
        }

        if (!empty($request->profile_pic)) {
            $mediaService = new MediaService();
            $mediaService->moveFile($mediaService->tmpPath.$request->profile_pic, $this->profilePath.$request->profile_pic);
        }
        
        $data->profile_pic = $request->profile_pic;
        if (isset($data->id)) {
            if ($data->mobile_number != $request->mobile_number) {
                try {
                    $newParams = array(
                                'user_id'   => Auth::user()->id,
                                'module'    => 'users',
                                'user_type' => ucfirst(Auth::user()->roles->first()->name),
                                'log_id'    => $data->id,
                                'text'    => $data->mobile_number,
                                'log_type'  => 'mobile_number_change'
                            );
                    ActivityLogManager::create($newParams);
                } catch(Exception $e){

                }
                if(\Auth::user()->hasRole('patient') || \Auth::user()->hasRole('doctor')) {
                    // generate ticket
                    $msgRequest = new \Illuminate\Http\Request();
                    $msgRequest->replace(
                            [
                                'email_type' => 'individual',
                                'user_type' => $data->roles->first()->name,
                                'user_id' => $data->id,
                                'sender_id' => User::with(['roles' => function($q){
                                                    $q->where('name', 'superadmin');
                                                }])->pluck('id')->first(),
                                'category' => 'changes_messages',
                                'subject' => 'Mobile Number Changed',
                                'message' => 'You have changed your mobile number <strong>('.$data->mobile_number.')</strong> to a new mobile number <strong>('.$request->mobile_number.')</strong> on <strong>'.date('dS F Y').' at '.date('h:i a').'</strong>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                    <br/><br/>
                                    If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.',
                            ]
                        );
                    $supportService = new SupportService;
                    $supportService->save($msgRequest);
                }
            }

        }
        $data->mobile_number = $request->mobile_number;
        $data->gender = $request->gender;
        return $data;
    }

    public function update($request)
    {
        $user = User::find($request->module_id);
        $user = $this->propertySetter($request, $user);
         // to maintain log
        try {
            $newParams = array(
                        'user_id'   => Auth::user()->id,
                        'module'    => 'users',
                        'user_type' => ucfirst($user->roles->first()->name),
                        'log_id'    => $user->id,
                        'log_type'  => 'updated'
                    );
            ActivityLogManager::create($newParams);
        } catch(Exception $e){
        }
        
        return $user->update();
    }

    public function save($request)
    {
        $user = new User();
        $user = $this->propertySetter($request, $user);
        $user->code = md5(time());
        $user->save();

        // Send reset email
        //$request->merge([ 'forgot_email' => $user->email, 'template' => 'register_email',  ]);
        //$this->requestPassword($request);

        if ($request->role == 'doctor') {
            // attach doctor role
            $user->attachRole(2);
        } else if ($request->role == 'patient') {
            // attach patient role
            $user->attachRole(3);
        }
        if ($request->type == 'register') {
            // to maintain log
            try {
                $newParams = array(
                            'user_id'   => $user->id,
                            'module'    => 'users',
                            'user_type' => 'patient',
                            'log_id'    => $user->id,
                            'log_type'  => 'registered'
                        );
                ActivityLogManager::create($newParams);
            } catch(Exception $e){

            }
        } else {
            // to maintain log
            try {
                $newParams = array(
                            'user_id'   => Auth::user()->id,
                            'module'    => 'users',
                            'user_type' => ucfirst($user->roles->first()->name),
                            'log_id'    => $user->id,
                            'log_type'  => 'created'
                        );
                ActivityLogManager::create($newParams);
            } catch(Exception $e){

            }
        }
        
        
        return $user;

    }

    public function resetPassword($email, $password)
    {
        $userRec = $this->get('email', $email);
        $userRec->password = \Hash::make($password);
        $userRec->email_verified = 1;
        $userRec->code = NULL;
        $userRec->update();

        if (\Auth::user()) {
            if(\Auth::user()->hasRole('patient') || \Auth::user()->hasRole('doctor')) {
                $role = '';
                if (\Auth::user()->hasRole('patient')) {
                    $role = 'patient';    
                } else {
                    $role = 'doctor';  
                }
                // reset password by user
                $subject = 'Password Changed';
                $message = 'You have changed your password on <strong>'.date('dS F Y').' at '.date('h:i a').'<strong/>. You have also agreed to our terms and conditions and privacy policy at the same time.
                                <br/><br/>
                                If you did make this change, please disregard this message. However, if you did not make this change please contact us immediately.';

                // generate ticket
                $msgRequest = new \Illuminate\Http\Request();
                $msgRequest->replace(
                        [
                            'email_type' => 'individual',
                            'user_type' => $role,
                            'user_id' => $userRec->id,
                            'sender_id' => User::with(['roles' => function($q){
                                                $q->where('name', 'superadmin');
                                            }])->pluck('id')->first(),
                            'category' => 'changes_messages',
                            'subject' => $subject,
                            'message' => $message,
                        ]
                    );
                $supportService = new SupportService;
                $supportService->save($msgRequest);  
            }
             
                        
            // to maintain log
            try {
                $newParams = array(
                            'user_id'   => Auth::user()->id,
                            'module'    => 'user_profile',
                            'user_type' => ucfirst(Auth::user()->roles->first()->name),
                            'log_id'    => Auth::user()->id,
                            'log_type'  => 'change_password'
                        );
                ActivityLogManager::create($newParams);
            } catch(Exception $e){

            }
        }
        
        return true;
    }

    public function checkOldPassword($email, $old_password) {
        $userRec = $this->get('email', $email);

        if (\Hash::check($old_password, $userRec->password))
        {
            return true;
        } else {
            return false;
        }

    }

    public function canResetPass($token)
    {
        $resetData = PasswordReset::where('token', $token)->where('created_at','>',Carbon::now()->subHours(24))->first();
        return $resetData;
    }

    public function getAll($request)
    {
        if($request->input('limit') === 0)
        {
            return $users = User::get();
        }
        else 
        {
            return $users = User::paginate(10);
        }
    }

    public function saveResetCode($user, $token)
    {
        $reset = new PasswordReset();
        $reset->email = $user->email;
        $reset->token = $token;
        $reset->created_at = date('Y-m-d H:i:s');
        $reset->save();
    }

    public function requestPassword($request)
    {
        $userData = $this->get('email', $request->input('forgot_email'));

        if (!empty($userData)) 
        {
            $forgotToken = md5(time());
            
            $this->saveResetCode($userData, $forgotToken);

            // Send email to user
            $emailData = array('name' => $userData->full_name,
                               'action_url' => \URL::to('request-password/'.$forgotToken),
                               'subject' => 'Password Requested',
                               'template' => $request->input('template')
                                );

            try{
                \Mail::to($request->input('forgot_email'))->send(new PasswordRequest($emailData));
            }catch(\Exception $e){

            }

            return true;
        }
        else
        {
            return false;
        }
    }

    /*public function updateUserDetail($entity, $request)
    {
        if(empty($rec))
        {
            $rec= new UserDetail();
        }

        $rec->entity_id = $entity['entity_id'];
        $rec->entity_type = $entity['entity_type'];        
        $rec->address1 = $request->input('address1', '');
        $rec->address2 = $request->input('address2', '');
        $rec->city = $request->input('city', '');
        $rec->county = $request->input('county', '');
        $rec->postcode = $request->input('postcode', '');
        $rec->country = 'United Kingdom';
        $rec->phone = $request->input('phone', '');
        $rec->mobile = $request->input('mobile', '');
        $rec->preferred_method = $request->input('preferred_method', 'Mobile');




        if(!isset($rec->id))
            $rec->save();
        else
            $rec->update();            
    }
*/
    public function get($col, $value, $full_name = false)
    {
        $user =  User::where($col, $value)->first();
        if (!empty($full_name)) {
            $user->full_name = $user->first_name.' '.$user->last_name;
            $user->role = $user->roles ? ucfirst($user->roles->first()->name) : 'No role';
            if(!empty($user->profile_pic))
                $user->profile_pic_path =  \URL::to('storage/profile_pic/'.$user->profile_pic);
            else
                $user->profile_pic_path =  asset('assets/img/avatar2.png');

            $user->intitials = $this->getIntitials($user->first_name, $user->last_name);
        }

        
        return $user;
    }

    /*public function logout() {
        //User::where('id', '=', Auth::user()->id)->update(['token' => '']);
        
        // to maintain log
        try {
            $newParams = array(
                        'user_id'   => Auth::user()->id,
                        'user_type'   => ucfirst(Auth::user()->roles->first()->name),
                        'module'    => 'user_profile',
                        'log_id'    => Auth::user()->id,
                        'log_type'  => 'logout'
                    );
            
            ActivityLogManager::create($newParams);
        } catch(\Exception $e){
        }
        Auth::logout();
        return true;
    }*/

    public function resetPasswordByAdmin($request)
    {
        $userData = $this->get('id', $request->id, true);
        if(!empty($userData))
        {
            $resetToken = md5(time());
            $this->saveResetCode($userData, $resetToken);

            // Send email to user
            $emailData = array('name' => $userData->full_name,
                               'action_url' => \URL::to('/request-password/'.$resetToken),
                               'subject' => 'Set Password Request',
                               'template' => 'register_email'
                                );
            try{
                \Mail::to($userData->email)->send(new PasswordRequest($emailData));
            }catch(\Exception $e){

            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function updateStatus($request)
    {
        $user = User::find($request->id);
        if (!empty($user)) {
            $user->is_archived =  $request->status;
            $user->update();
            // to maintain log
            try {
                $newParams = array(
                            'user_id'   => Auth::user()->id,
                            'module'    => 'users',
                            'user_type' => ucfirst(Auth::user()->roles->first()->name),
                            'log_id'    => $user->id,
                            'log_type'  => 'updated_status',
                            'text'      => ($user->is_archived == 1)?'archived':'Unarchived'
                        );
                ActivityLogManager::create($newParams);
            } catch(Exception $e){

            }
            return true;
        } else {
            return false;
        }
    }

    public function verifyEmail($code)
    {

        $user = $this->get('code', $code);
        if(!empty($user))
        {
           
            //unset($user->is_approved);
            $user->email_verified = 1;
            //$user->code = '';
            $user->update();
            return $user;
        }
        else

        {
            return false;
        }
    }

    public function contactEmail($request) {
        $adminEmail = env('ADMIN_EMAIL');
        // Send email to user
        $emailData = array('data' => $request,
                           'subject' => 'Happy Clinic Contact Email',
                            );
        try{
            \Mail::to($adminEmail)->send(new ContactEmail($emailData));
        }catch(\Exception $e){

        }
        return true;
    }

    public function getIntitials($first_name, $last_name) {
        return ucfirst(substr($first_name, 0, 1)).ucfirst(substr($last_name, 0, 1));
    }

    public function getIsApproved($user_id) {
        $is_approved = 0;
        $patientService = new PatientService;
        $patient =  $patientService->get('user_id', $user_id);
        if (!empty($patient)) {
            $is_approved = $patient->is_approved;
        } 

        return $is_approved;
    }

}


?>