<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('backend.emails.email-from-admin')
                        ->from('admin@happyclinic.co.uk', 'Happy Clinic')
                        ->subject($this->data['subject']);
        // $attachments is an array with file paths of attachments
        foreach($this->data['attachments'] as $doc){
            $email->attach($doc->file_path);
        }
        return $email;
    }
}
