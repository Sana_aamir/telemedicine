<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSupportRequest extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('backend.emails.support')->subject("Support Request - ".$this->data['subject']);
        try {
            // $attachments is an array with file paths of attachments
            foreach($this->data['docs'] as $doc){
                $email->attach($doc->file_path);
            }
        } catch(\Exception $e){
            //dd($e->getMessage());
            return false;
        }    
        return $email;
    }
}
