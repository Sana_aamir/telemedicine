<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipCancelRequest  extends Model
{
   protected $table = 'membership_cancel_requests';    
}