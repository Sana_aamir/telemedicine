<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PsychiatricReport extends Model
{
   protected $table = 'psychiatric_reports';    
}