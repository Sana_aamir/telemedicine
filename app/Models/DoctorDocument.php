<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorDocument extends Model
{
   protected $table = 'doctor_documents';    
}