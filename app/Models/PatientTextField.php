<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientTextField extends Model
{
   protected $table = 'patient_text_fields';    
}