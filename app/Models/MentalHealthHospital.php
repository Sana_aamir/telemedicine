<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MentalHealthHospital extends Model
{
   protected $table = 'mental_health_hospitals';    
}