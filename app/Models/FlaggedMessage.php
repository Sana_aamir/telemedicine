<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlaggedMessage extends Model
{
   protected $table = 'flagged_messages';    
}