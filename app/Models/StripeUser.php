<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class StripeUser extends Model
{
	use Billable;
	protected $table = 'stripe_user';    
}