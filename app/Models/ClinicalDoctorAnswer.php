<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClinicalDoctorAnswer extends Model
{
   protected $table = 'clinical_doctor_answers';    
}