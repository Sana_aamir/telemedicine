<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientAttachment extends Model
{
   protected $table = 'patient_attachments';    
}