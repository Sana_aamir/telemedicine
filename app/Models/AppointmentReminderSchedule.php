<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentReminderSchedule extends Model
{
   protected $table = 'appointment_reminder_schedule';    
}