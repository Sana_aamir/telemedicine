<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Patient extends Model
{
	use Billable;
    protected $table = 'patients';    
}