<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeriousIncident extends Model
{
   protected $table = 'serious_incidents';    
}