<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipSubscription  extends Model
{
   protected $table = 'membership_subscriptions';    
}