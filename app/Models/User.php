<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait; 
    //use EntrustUserTrait { restore as private restoreA; }
    //use SoftDeletes { restore as private restoreB; }

    /*public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }*/
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getProfilePicture()
    {
        if(!empty($this->profile_pic))
            $pic =  \URL::to('storage/profile_pic/'.$this->profile_pic);
        else
            $pic =  asset('assets/img/avatar2.png');
        return $pic;
    }   


    public function isManager()
    {
        if($this->hasRole('manager'))
            return true;
        else
            return false;
    }

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
   

}
