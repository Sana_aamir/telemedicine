<?php namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const ADMIN = 1;
    const DOCTOR = 2;
    const PATIENT = 3;

    const ROLENAMES = ['admin'=>1, 'doctor'=>2, 'patient'=>3];
}