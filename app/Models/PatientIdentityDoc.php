<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientIdentityDoc extends Model
{
   protected $table = 'patient_identity_docs';    
}