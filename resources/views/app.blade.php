<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{ env('CLINIC_NAME') }}</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="{{asset('htmlv1/images/favicon/64x64-2.png')}}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- Styles -->
    <link href="{{asset('assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/css/amaran.min.css')}}" rel="stylesheet" type="text/css" media="screen">
     <link href="{{asset('assets/css/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{asset('assets/pages/css/themes/corporate.css?ver=')}}{{time()}}" rel="stylesheet" type="text/css" />

    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <!-- <link href="{{ asset('htmlv1/stylesheets/static-page.css') }}" rel="stylesheet"> -->
     <style type="text/css">
        body {
            font-family: "Segoe UI", Arial, sans-serif !important;
        }

         .btn-applyplus,
        .btn-applyplus:focus,
        .btn-applyplus:hover {
          color: #fff;
          background-color: #f58513;
          border-color: #f58513;
        }
        .btn-complete,
        .btn-complete:focus,
        .btn-complete:hover {
          color: #fff;
          background-color: #37b0e9;
          border-color: #37b0e9;
        }
     </style>
    <script type="text/javascript">
    <?php 

    $appSettings = App\Library\Helper\Utility::appSettings();
    ?>
    var configConstants = <?php echo json_encode($appSettings);?>;
    </script>
    
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer>
</script> 
</head>
<body class="fixed-header menu-pin menu-behind">
    <span id="app">
        <router-view></router-view>  
    </span>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
