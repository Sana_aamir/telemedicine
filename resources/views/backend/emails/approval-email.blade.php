<!DOCTYPE html>
<html>
<head>

</head>
<body>
Dear {{ $data['patient']->user->first_name }},
<br/>
<br/>
Your account has now been approved by Happy Clinic. <u>You can now start booking appointments.</u> Please consider subscribing to our <u>membership</u> to enjoy all the benefits and save 100’s of £££<br>
<br/>
Please use the link below for login.
<br> 
<br> 
<a href="{{$data['action_url']}}">Click here to login</a>
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>