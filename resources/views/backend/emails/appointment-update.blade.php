<!DOCTYPE html>
<html>
<head>

</head>
<body>
@if ( $data['user_type'] == 'patient') 
	Dear {{ $data['appointment']['patient']['user']['first_name'] }},
	<br/>
	<br/>
	Your appointment has been rescheduled successfully. <br>
	<br>

 	Your new appointment with doctor  {{ $data['appointment']['doctor']['title'] }} {{ $data['appointment']['doctor']['user']['full_name'] }} on <u>{{ date('d F,Y \a\\t h:i a', strtotime($data['appointment']['date_time'])) }}</u> at Happy Clinic is confirmed.
 
@elseif ($data['user_type'] == 'doctor')
	@if ($data['doctor_change'] == 'yes') 
		Dear {{ $data['appointment']['doctor']['title']}} {{ $data['appointment']['doctor']['user']['full_name'] }},
		<br/>
		<br/>
		Patient, {{ $data['appointment']['patient']['title'] }} {{ $data['appointment']['patient']['user']['full_name'] }} has booked an appointment with you on {{ date('d F,Y \a\\t h:i a', strtotime($data['appointment']['date_time'])) }} at Happy Clinic. 
		<br>
		<br/>
		Please login to your Happy Clinic account for more details
	@elseif ($data['doctor_change'] == 'no')
		Dear {{ $data['appointment']['doctor']['title']}} {{ $data['appointment']['doctor']['user']['full_name'] }},
		<br/>
		<br/>
		Patient, {{ $data['appointment']['patient']['title'] }} {{ $data['appointment']['patient']['user']['full_name'] }} has cancelled the appointment with you on <u>{{ date('d F,Y \a\\t h:i a', strtotime($data['previous_date_time'])) }}</u> and rescheduled it to {{ date('d F,Y \a\\t h:i a', strtotime($data['appointment']['date_time'])) }} at Happy Clinic.
		<br>
		<br/>
		Please login to your Happy Clinic account for more details 

	@endif 	
@endif 	
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>