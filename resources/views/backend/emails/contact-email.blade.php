<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear Admin,
<br/>
<br/>
{{ $data['data']->fullname }} has sent  Happy Clinic admin an email using the Contact Us form.
<br/>
<br/>
Email: {{ $data['data']->email }}
<br/>
Mobile Number: {{ $data['data']->mobile_number }}
<br/>
<br/>
Message : {{ $data['data']->message }}
<br>
<br/>
Please login to your Happy Clinic account for more details
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>