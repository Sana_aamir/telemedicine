<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['first_name'] }},
<br/>
<br/>
Welcome to Happy Clinic. 
<br/>
<br/>
Please click below to validate your email.
<br/>
<br/>
<a href="{{$data['action_url']}}">Validate Email</a>
<br/>
<br/>
Once your email has been validated, Happy Clinic will approve your registration dependent on the information provided. Please allow us up to 24 hours to approve your registration
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>