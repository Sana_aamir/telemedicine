<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['patient']['user']['first_name']}},
<br/>
<br/>
Please don’t forget your appointment with doctor, {{ $data['doctor']['title'] }} {{ $data['doctor']['user']['full_name'] }} on <u>{{ date('d F,Y \a\\t h:i a', strtotime($data['date_time'])) }}</u> at Happy Clinic.
<br>
<br/>
Please login to your Happy Clinic account for more details
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>