@extends('backend.layouts.email')

@section('title', 'Set up a account for config("app.name")')

@section('content')

    <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
      <!-- Body content -->
      <tr>
        <td class="content-cell">
          <h1>Hi {{$data['name']}},</h1>
          <p>We have created account on {{config("app.name")}} account. Use the button below to set a password. </p>
          <!-- Action -->
          <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td align="center">
                <!-- Border based button
           https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                            <a href="{{$data['action_url']}}" class="button button--green" target="_blank">Set your password</a>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <p>Thanks,
            <br>The {{config("app.name")}} Team</p>
          <!-- Sub copy -->
          <table class="body-sub">
            <tr>
              <td>
                <p class="sub">If you’re having trouble with the button above, copy and paste the URL below into your web browser.</p>
                <p class="sub">{{$data['action_url']}}</p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
@endsection