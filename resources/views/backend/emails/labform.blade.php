<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
 body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  background-color: #666;
  font-family: Arial, Helvetica, sans-serif;
  line-height: 16px;
  font-size: 11.5px;
}
.wrap {
 width: 800px;
  padding: 40px;
  padding-bottom: 40px;
  height: 13.86px;
  background-color: #FFF;
  border-bottom-width: 2px;
  border-bottom-style: dashed;
  border-bottom-color: #666;
  display: table;
  position: relative;
  background-image: url( "{{ $message->embed("assets/img/watermark.gif") }}" );
  background-position: center bottom;
  background-repeat: no-repeat;
}
.mainwrap {
  width: 800px;
  margin-right: auto;
  margin-left: auto;
  padding: 20px;
}
.scale {
  float: left;
  width: 5cm;
  background-color: #F00;
  height: 14.85cm;
}
.wrap2 {
  width: 800px;
  height: auto;
  background-color: #DAF5C0;
  display: table;
  padding-top: 9px;
  padding-right: 1cm;
  padding-bottom: 1cm;
  padding-left: 1cm;
  /*background-image: url({{ URL::asset('assets/img/2.png') }});*/
  background-image: url(" {{ $message->embed('assets/img/2.png') }}");
  background-repeat: no-repeat;
  background-position: center top;
  font-size: 11px;
  line-height: 15px;
}
.date {
  float: left;
  width: 100%;
  padding-top: 11px;
  text-align: right;
}
.matter {
  width: 100%;
  margin-top: 21px;
  float: left;
}
.from2 {
  float: left;
  width: 222px;
  margin-top: 20px;
}
.address-box {
  width: 222px;
  float: left;
  position: absolute;
  top: 5cm;
  left: 2.5cm;
}
.from {
  float: left;
  width: 222px;
  margin-top: 21px;
}
.add2 {
  float: right;
  width: 222px;
  padding-top: 11px;
  padding-right: 11px;
  padding-bottom: 11px;
  padding-left: 11px;
  border: 1px solid #666;
}
.patient_details {
  float: left;
  width: 250px;
}
.medication {
  float: right;
  width: 465px;
}
.bordrbox {
  border-top-width: 1px;
  border-right-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-top-style: none;
  border-right-style: none;
  border-bottom-style: solid;
  border-left-style: solid;
  border-top-color: #333;
  border-right-color: #333;
  border-bottom-color: #333;
  border-left-color: #333;
  background-color: #FFF;
}
.bordrbox tr td {
  border-top-width: 1px;
  border-top-style: solid;
  border-top-color: #666;
  border-right-width: 1px;
  border-right-style: solid;
  border-right-color: #666;
}
.div100 {
  float: left;
  width: 100%;
  margin-top: 11px;
}
.div110 {
  float: left;
  width: 100%;
  margin-top: 2px;
}
strong {
  display: block;
  margin-top: 7px;
  margin-bottom: 7px;
}
  </style>
</head>
<body>
 <!--  <div style="width:1080px;">
    <div style="width:100%;"> -->
        <div class="mainwrap">
          <div class="wrap">
            <div class="address-box">
              @php 
                $data = $data['data']; 
               @endphp
              <br><br><br><br><br><br>
              {{ $data->patient->laboratory_address->name }}<br />
              {{ $data->patient->laboratory_address->address_first_line }}<br />
              {{ $data->patient->laboratory_address->city }}<br />
              {{ $data->patient->laboratory_address->country }}<br />
              {{ $data->patient->laboratory_address->postcode }}<br />
            </div>
            <div class="add2">
              {!! env('CLINIC_ADDRESS') !!}
              Tel: {{ env('CLINIC_PHONE') }}<br><br>
              Trading Name: {{ env('CLINIC_NAME') }}<br>
              URL: {{ env('CLINIC_URL') }}
            </div>  
            <div class="date">Date : {{ date('d-m-Y', strtotime($data->sent_date)) }}</div>
            
          <div class="from2">Dear {{ $data->patient->laboratory_address->name }}</div>
            <div style="float:left;    width: 100%;">
                  <br/>
            Patient Details: <br/>
            Name: {{ $data->patient->title}} {{$data->patient->user->full_name}} <br/>
            DOB: {{ $data->patient->dob}}<br/>
            Mobile Number: {{ $data->patient->user->mobile_number}} </div>
            <div class="matter">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  book. dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </div>
            <div class="from">Yours Faithfully<br />
              Happy Clinic Customer Team
            </div>
          </div>
          <div class="wrap2" style="">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
            </table><div align="right"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 20px;"><tr>
                <td width="580" align="center"><h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LAB FORM</h2></td>
                <td width="138" align="right">{{ $data->form_number }}</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 20px;">
              <tr>
                <td width="39%" align="center"><strong>Patient Details</strong></td>
                <td width="3%">&nbsp;</td>
                <td align="center"><strong>Test</strong></td>
              </tr>
            </table>
            <div class="patient_details">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <!-- <tr>
                  <td colspan="2">Title, First Name, Last Name and Address</td>
                </tr> -->
                <tr>
                  <td colspan="2">{{ $data->patient->title}} {{ $data->patient->user->full_name}}<br />
                   {{ $data->patient->address_first_line}}<br />
                        {{ $data->patient->city}}<br />
                        {{ $data->patient->country}}<br />
                      {{ $data->patient->postcode}} </td>
                </tr>
                <tr>
                  <td>Age : {{ $data->patient->age}}</td>
                  <td>DOB: {{ date('d-m-Y',strtotime($data->patient->dob)) }} </td>
                </tr>
              </table>
            </div>
            <div class="medication">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <tr>
                  <td width="35%">Number of items : {{ $data->total_items }}</td>
                  <td>Type of Samples : in brackets</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">
                  @if (!empty($data->details))
                    @foreach($data->details as $key => $value)
                      {{ $value->test }} ({{ $value->sample }}): {{ $value->additional_info }}<br />
                    @endforeach
                  @endif
                  </td>
                </tr>
              </table>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 40px; ">
              <tr>
                <td align="center"><strong>Prescriber Details</strong></td>
              </tr>
            </table>
            <div class="div110">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                    <tr>
                      <td width="17%">Name of Doctor</td>
                      <td width="39%">{{ $data->doctor->title}} {{ $data->doctor->user->full_name}}</td>
                      <td width="16%"> Intitials : {{ $data->intitials}}</td>
                      <td width="28%">Signature of the Doctor<br /></td>
                    </tr>
                    <tr>
                      <td width="17%">Qualifications</td>
                      <td colspan="2">{{ $data->doctor->qualification}}</td>
                      <td rowspan="5" align="center">
                        @if ($data->doctor->signature_image != '')
                        <!-- <img alt="Signature Image" height="100" data-src-retina="{{ $data->doctor->signature_image_path }}" class="signature_image inline" data-src="{{ $data->doctor->signature_image_path }}"  src="{{ $data->doctor->signature_image_path }}"> -->
                        <img alt="Signature Image" width="100" height="100" src="{{ $message->embed($data->doctor->signature_image_path) }}">
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td width="17%">Designation</td>
                      <td colspan="2">{{ $data->doctor->designation}}</td>
                    </tr>
                    <tr>
                      <td width="17%">GMC Number</td>
                      <td colspan="2">{{ $data->doctor->gmc_number}}</td>
                    </tr>
                    <tr>
                      <td width="17%">Address</td>
                      <td colspan="2">{{ strip_tags(env('CLINIC_ADDRESS')) }}</td>
                    </tr>
                    <tr>
                      <td width="17%">Mobile Number</td>
                      <td colspan="2">{{ env('CLINIC_PHONE') }}</td>
                    </tr>
                    <tr>
                      <td width="17%">Website</td>
                      <td colspan="2">{{ env('CLINIC_URL') }}</td>
                      <td>Date : {{ date('d-m-Y', strtotime($data->sent_date)) }}</td>
                    </tr>
                  </table>
            </div>
            <div class="div100">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <tr>
                  <td>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged-> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
   <!--  </div>
  </div> -->
   
</body>
</html>
