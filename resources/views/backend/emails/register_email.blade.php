<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['name'] }},
<br/>
<br/>
Welcome to Happy Clinic. 
<br/>
<br/>
We have created an account for you on Happy Clinic. Use the button below to set a password.
<br/>
<br/>
<a href="{{$data['action_url']}}"> Set your password</a>
<br/>
<br/>
<p class="sub">If you’re having trouble with the button above, copy and paste the URL below into your web browser.</p>
<p class="sub">{{$data['action_url']}}</p>
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>