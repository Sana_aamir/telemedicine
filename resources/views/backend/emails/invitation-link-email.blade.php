<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['patient']['user']['first_name'] }},
<br/>
<br/>
Appointment Date: {{ date('d F,Y', strtotime($data['appointment']['date_time'])) }}<br/>
Appointment time: {{ date('h:i a', strtotime($data['appointment']['date_time'])) }}<br/>
<br/>
<br/>
Please click on the link below now to start the Video call with doctor {{ $data['doctor']['title']}} {{ $data['doctor']['user']['full_name'] }} at Happy Clinic. When you click the link below we will also assume that we have your full consent for the assessment to take place. We have also sent an SMS message to your mobile number with the link to start the video call.
<br/>
<br/>
<a href="{{$data['action_url']}}">Click here to start video consultation</a>
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>

</body>
</html>