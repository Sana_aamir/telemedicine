<!DOCTYPE html>
<html>
<head>

</head>
<body>
@if ( $data['user_type'] == 'patient') 
	Dear $data['appointment']['patient']['user']['first_name'] }},
	<br/>
	<br/>
	Your appointment with doctor, {{ $data['appointment']['doctor']['title'] }} {{ $data['appointment']['doctor']['user']['full_name'] }} on <u>{{ date('d F,Y \a\\t h:i a', strtotime($data['appointment']['date_time'])) }}</u> at Happy Clinic has been cancelled. 
	<br>
	<br/>
	Please login to your Happy Clinic account for more details

@elseif ($data['user_type'] == 'doctor')
	Dear {{ $data['appointment']['doctor']['title']}} {{ $data['appointment']['doctor']['user']['full_name'] }},
	<br/>
	<br/>
	Your appointment with patient, {{ $data['appointment']['patient']['title'] }} {{ $data['appointment']['patient']['user']['full_name'] }} on <u>{{ date('d F,Y \a\\t h:i a', strtotime($data['appointment']['date_time'])) }}</u> at Happy Clinic has been cancelled. 
	<br>
	<br/>
	Please login to your Happy Clinic account for more details

@endif 	
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>