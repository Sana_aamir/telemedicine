<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{$data['receiver']->user->first_name}},	
<br/>
<br/>
Please find attached the document(s) from {{ lcfirst($data['sender']->user->role) }}, {{ $data['sender']->title }}.{{$data['sender']->user->full_name}}.
<br>
<br/>
Please login to your Happy Clinic account for more details
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>