<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear doctor,	
<br/>
<br/>
<u>Re: {{ $data['patient']->title }} {{ $data['patient']->user->full_name }} ; DOB: {{ date('d-m-Y', strtotime($data['patient']->dob)) }}.</u>
<br/>
<br/>
This patient was recently reviewed in Happy Clinic. Happy Clinic is an online medical service for UK patients. Visit our website <a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a> for more details.
<br/>
<br/>
<u>Please find attached the assessment record for this patient.</u> Please read it carefully especially the Care Plan and update your records accordingly.
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>