<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['patient']['user']['first_name'] }},
<br/>
<br/>
Our records indicate that you are due for an appointment with a doctor at Happy Clinic.

<br/>
<br/>
Regular reviews by the doctor are necessary to ensure your well-being. Please login to your Happy Clinic account to schedule an appointment.
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>
