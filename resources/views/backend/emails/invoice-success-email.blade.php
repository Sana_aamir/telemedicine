<!DOCTYPE html>
<html>
<head>

</head>
<body>

@if ($data['user_type'] == 'patient')
	Dear {{ $data['patient']->user->full_name }},	
	<br/>
	<br/>
	This is an automatic email from {{env('CLINIC_NAME')}} confirming that your payment has been successfully processed.
	<br/>
	<br/>
	<p>	<strong>Payment Details:</strong></p>
	<p>
		Amount: <b>{{$data['amount'] }}</b> <br /> 
		Credit Card: <b>{{ $data['card']->brand }}</b> <br />
		Last 4 of CC: <b>{{ $data['card']->last4 }}</b><br />
		CC Exp. Date:  <b> {{ $data['card']->exp_month }} / {{ $data['card']->exp_year }}</b> </p> 
@else 
	Dear Admin,	
	<br/>
	<br/>
	This is an automatic email from {{env('CLINIC_NAME')}} confirming that patient <strong>{{ $data['patient']->user->full_name }}'s</strong> payment has been successfully processed.
	<br/>
	<br/>
	<p>	<strong>Payment Details:</strong></p>
	<p>
		Amount: <b>{{$data['amount'] }}</b> <br /> 
		Credit Card: <b>{{ $data['card']->brand }}</b> <br />
		Last 4 of CC: <b>{{ $data['card']->last4 }}</b><br />
		CC Exp. Date:  <b> {{ $data['card']->exp_month }} / {{ $data['card']->exp_year }}</b> </p> 
@endif		
<br/>
<br/>
Yours Faithfully
<br/>
<br/>
{{env('CLINIC_NAME')}} Customer Team
</body>
</html>