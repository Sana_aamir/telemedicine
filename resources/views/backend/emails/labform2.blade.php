<!DOCTYPE html>
<html>
<head>

</head>
<body>

Dear {{ $data['lab_name'] }},	
<br/>
<br/>
Please find attached the Lab form for a patient, whose details are on the attached form. The patient is aware that the lab form will be sent to your lab. Please would you notify the patient that you have received the lab form from Happy Clinic.
<br/>
<br/>
Happy Clinic is an online medical service for UK patients. Visit our website <a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a> for more details.
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>
</body>
</html>