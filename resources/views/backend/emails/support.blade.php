<!DOCTYPE html>
<html>
<head>

</head>
<body>
@if ($data['receiver']->role == 'Patient') 
	Dear {{$data['receiver']->first_name}},<br/>
@else 
	Dear {{$data['receiver']->full_name}},<br/>
@endif	
<br/>
{{$data['sender']->full_name}} has sent you a message.<br/>
<br/>
Category: {{ $data['category'] }},<br/>
<br/>
Subject: {{$data['subject']}},<br/>
<br/>	
Message:{!! $data['message'] !!}<br/>
<br/>
<br/>
<a href="{{$data['action_url']}}">Click here to reply</a>
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong>

</body>
</html>