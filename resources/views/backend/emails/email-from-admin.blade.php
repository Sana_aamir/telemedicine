<!DOCTYPE html>
<html>
<head>

</head>
<body>

{!! $data['content'] !!}
<br/>
<br/>
Kind regards
<br/>
<br/>
Customer Service Team at Happy Clinic<br>
<a href="www.happyclinic.co.uk">www.happyclinic.co.uk</a>
<br>
<br>
<!-- <strong>***This is an automated email. Please do not reply as this email address is not monitored. ***</strong> -->
</body>
</html>
