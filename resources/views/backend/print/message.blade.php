<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
  td{
  padding:10px;
  border-bottom:1px solid gray;
  }
  p{
  font-family:Verdana;
  }
  table{
  font-family:Verdana;
  }s
  </style>
</head>
<body>
  <div style="width:1080px; margin-top: 20px; padding:40px; float: left;">
    <div style="width:100%;">
      <p style="font-size: 25px;text-align: left; margin: 0px;">Message From {{ $data['sender']->first_name }} {{ $data['sender']->last_name }} to {{ $data['receiver']->first_name }} {{ $data['receiver']->last_name }}</p>
      <p>Date Time: {{ date('d-m-Y h:i a',strtotime($data->created_at)) }}</p>
    </div>
    <div><p style="font-size: 16px;text-align: left; margin-top:20px;">{!! $data['message'] !!}</p></div>
  </div>

</body>
</html>