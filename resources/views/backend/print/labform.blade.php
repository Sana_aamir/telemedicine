<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
 body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  background-color: #666;
  font-family: Arial, Helvetica, sans-serif;
  line-height: 16px;
  font-size: 11.5px;
}
.wrap {
  width: 17cm;
  height: 13.86px;
  background-color: #FFF;
  padding-top: 1cm;
  padding-left: 2.5cm;
  border-bottom-width: 2px;
  border-bottom-style: dashed;
  border-bottom-color: #666;
  padding-right: 2.5cm;
  display: table;
  padding-bottom: 2cm;
  position: relative;
  background-image: url("{{ URL::asset('assets/img/watermark.gif') }}");
  background-position: center bottom;
  background-repeat: no-repeat;
  background-size: 52% auto;
}
.mainwrap {
  height: 29.7cm;
  width: 22cm;
  background-color: #DAF5C0;
  margin-right: auto;
  margin-left: auto;
}
.scale {
  float: left;
  width: 5cm;
  background-color: #F00;
  height: 14.85cm;
}
.wrap2 {
  width: 20cm;
  height: 13.86px;
  background-color: #DAF5C0;
  display: table;
  padding-top: 9px;
  padding-right: 1cm;
  padding-bottom: 1cm;
  padding-left: 1cm;
  background-image: url({{ URL::asset('assets/img/2.png') }});
  background-repeat: no-repeat;
  background-position: center top;
  font-size: 11px;
  line-height: 15px;
}
.date {
  float: left;
  width: 100%;
  padding-top: 11px;
  text-align: right;
}
.matter {
  width: 100%;
  margin-top: 21px;
  float: left;
}
.from2 {
  float: left;
  width: 222px;
  margin-top: 20px;
}
.address-box {
  width: 222px;
  float: left;
  position: absolute;
  top: 5cm;
  left: 2.5cm;
}
.from {
  float: left;
  width: 222px;
  margin-top: 21px;
}
.add2 {
  float: right;
  width: 222px;
  padding-top: 11px;
  padding-right: 11px;
  padding-bottom: 11px;
  padding-left: 11px;
  color: #1a1a1a;

      background: #f1f1f1;
    border: none;
    border-radius: 14px;
    margin-top: -53px;
}
.patient_details {
  float: left;
  width: 250px;
}
.medication {
  float: right;
  width: 465px;
}
.bordrbox {
  border-top-width: 1px;
  border-right-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-top-style: none;
  border-right-style: none;
  border-bottom-style: solid;
  border-left-style: solid;
  border-top-color: #333;
  border-right-color: #333;
  border-bottom-color: #333;
  border-left-color: #333;
  background-color: #FFF;
}
.bordrbox tr td {
  border-top-width: 1px;
  border-top-style: solid;
  border-top-color: #666;
  border-right-width: 1px;
  border-right-style: solid;
  border-right-color: #666;
}
.div100 {
  float: left;
  width: 100%;
  margin-top: 11px;
}
.div110 {
  float: left;
  width: 100%;
  margin-top: 2px;
}
strong {
  display: block;
  margin-top: 7px;
  margin-bottom: 7px;
}

.form_logo{
  max-width: 250px;
    height: 70px;
    margin-top: 20px;
}

.signature_image {
  max-width: 150px;
  max-height: 150px;
}
  </style>
</head>
<body>
 <!--  <div style="width:1080px;">
    <div style="width:100%;"> -->
        <div class="mainwrap">
          <div class="wrap">
            <div class="address-box">
              <div>
                <img alt="Logo" height="100" data-src-retina="{{ asset('htmlv1/images/logo-wide.png') }}" class="form_logo inline" data-src="{{ asset('htmlv1/images/logo-wide.png') }}"  src="{{ asset('htmlv1/images/logo-wide.png') }}">
              </div>
              <br><br><br><br>
              {{ $data->laboratory_address->name }}<br />
              {{ $data->laboratory_address->address_first_line }}<br />
              {{ $data->laboratory_address->city }}<br />
              {{ $data->laboratory_address->country }}<br />
              {{ $data->laboratory_address->postcode }}<br />
            </div>
            <div class="add2" style="margin-top: 40px;">
              <strong>{{ env('CLINIC_NAME') }}</strong><br>
              {!! env('CLINIC_ADDRESS') !!}
              Tel: {{ env('CLINIC_PHONE') }}<br><br>
              Trading Name: {{ env('CLINIC_NAME') }}<br>
              URL: {{ env('CLINIC_URL') }}
            </div>  
            <div class="date">Date : {{ date('d-m-Y', strtotime($data->sent_date)) }}</div>
            
          <div class="from2">Dear {{ $data->laboratory_address->name }},</div>
            <div style="float:left;    width: 100%;">
                  <br/>
            <strong><u>Patient Details:</u>
            {{ $data->patient->title}} {{$data->patient->user->full_name}}, 
            (DOB: {{ date('d-m-Y', strtotime($data->patient->dob)) }}), 
            Mobile Number: {{ $data->patient->user->mobile_number}}</strong></div>
            <div class="matter">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  book. dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </div>
            <div class="from">Yours Faithfully<br />
              Happy Clinic Customer Team
            </div>
          </div>
          <div class="wrap2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
            </table><div align="right"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 20px;"><tr>
                <td width="580" align="center"><h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LAB FORM</h2></td>
                <td width="138" align="right">{{ $data->form_number }}</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 20px;">
              <tr>
                <td width="39%" align="center"><strong>Patient Details</strong></td>
                <td width="3%">&nbsp;</td>
                <td align="center"><strong>Test</strong></td>
              </tr>
            </table>
            <div class="patient_details">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <!-- <tr>
                  <td colspan="2">Title, First Name, Last Name and Address</td>
                </tr> -->
                <tr>
                  <td colspan="2">{{ $data->patient->title}} {{ $data->patient->user->full_name}}<br />
                   {{ $data->patient->address_first_line}}<br />
                        {{ $data->patient->city}}<br />
                        {{ $data->patient->country}}<br />
                      {{ $data->patient->postcode}} </td>
                </tr>
                <tr>
                  <td>Age : {{ $data->patient->age}}</td>
                  <td>DOB: {{ date('d-m-Y',strtotime($data->patient->dob)) }} </td>
                </tr>
              </table>
            </div>
            <div class="medication">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <tr>
                  <td width="35%">Number of items : {{ $data->total_items }}</td>
                  <td>Type of Samples : in brackets</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">
                  @if (!empty($data->details))
                    @foreach($data->details as $key => $value)
                      {{ $value->test }} ({{ $value->sample }}): {{ $value->additional_info }}<br />
                    @endforeach
                  @endif
                  </td>
                </tr>
              </table>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 40px; ">
              <tr>
                <td align="center"><strong>Prescriber Details</strong></td>
              </tr>
            </table>
            <div class="div110">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                    <tr>
                      <td width="17%">Name of Doctor</td>
                      <td width="39%">{{ $data->doctor->title}} {{ $data->doctor->user->full_name}}</td>
                      <td width="16%"> Intitials : {{ $data->intitials}}</td>
                      <td width="28%">Signature of the Doctor<br /></td>
                    </tr>
                    <tr>
                      <td width="17%">Qualifications</td>
                      <td colspan="2">{{ $data->doctor->qualification}}</td>
                      <td rowspan="5" align="center">
                        @if ($data->doctor->signature_image != '')
                        <img alt="Signature Image" height="100" data-src-retina="{{ $data->doctor->signature_image_path }}" class="signature_image inline" data-src="{{ $data->doctor->signature_image_path }}"  src="{{ $data->doctor->signature_image_path }}">
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td width="17%">Designation</td>
                      <td colspan="2">{{ $data->doctor->designation}}</td>
                    </tr>
                    <tr>
                      <td width="17%">GMC Number</td>
                      <td colspan="2">{{ $data->doctor->gmc_number}}</td>
                    </tr>
                    <tr>
                      <td width="17%">Address</td>
                      <td colspan="2">{{ env('CLINIC_ADDRESS_WITHOUT_BR') }}</td>
                    </tr>
                    <tr>
                      <td width="17%">Mobile Number</td>
                      <td colspan="2">{{ $data->doctor->user->mobile_number}}</td>
                    </tr>
                    <tr>
                      <td width="17%">Website</td>
                      <td colspan="2">{{ env('CLINIC_URL') }}</td>
                      <td>Date : {{ date('d-m-Y', strtotime($data->sent_date)) }}</td>
                    </tr>
                  </table>
            </div>
            <div class="div100">
              <table width="100%" cellpadding="5" cellspacing="0" class="bordrbox">
                <tr>
                  <td>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged-> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
   <!--  </div>
  </div> -->

</body>
</html>