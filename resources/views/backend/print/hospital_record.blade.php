<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
 body {
  margin-left: 0px;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  font-family: Arial, Helvetica, sans-serif;
  line-height: 16px;
  font-size: 11.5px;
}
.wrap {
  width: 17cm;
  height: 13.86px;
  background-color: #FFF;
  padding-top: 1cm;
  padding-left: 2.5cm;
  padding-right: 2.5cm;
  display: table;
  padding-bottom: 2cm;
  background-repeat: no-repeat;
}
.mainwrap {
  height: 29.7cm;
  width: 22cm;
  margin-right: auto;
  margin-left: auto;
}

.cqc-logo {
    display: block;
    width: 103px;
    margin: 0 auto 0px;
    position: absolute;
    margin-top: 5px;
    right: 0;
}    

.date {
  float: left;
  width: 100%;
  padding-top: 11px;
  text-align: right;
}
.matter {
  width: 100%;
  margin-top: 21px;
  float: left;
}
.from2 {
  float: left;
  width: 222px;
  margin-top: 10px;
}
.address-box {
  width: 222px;
  float: left;
  position: absolute;
  top: 5cm;
  left: 2.5cm;
}

.add2 {
  float: right;
  width: 222px;
  padding-top: 11px;
  padding-right: 11px;
  padding-bottom: 11px;
  padding-left: 11px;
  color: #1a1a1a;

      background: #f1f1f1;
    border: none;
    border-radius: 14px;
    margin-top: -53px;
}

strong {
  display: block;
  margin-top: 7px;
  margin-bottom: 7px;
}

table {
  border-collapse: collapse;
}
td, th {
  border: 1px solid #1a1a1a;
}

.form_logo{
  max-width: 255px;
    height: auto;
    margin-top: 20px;
}
  </style>
</head>
<body>
 <!--  <div style="width:1080px;">
    <div style="width:100%;"> -->
        <div class="mainwrap">
          <div class="wrap">
            <div class="address-box">
              <div>
                <img alt="Logo" data-src-retina="{{ asset('htmlv1/images/logo-wide.png') }}" class="form_logo inline" data-src="{{ asset('htmlv1/images/logo-wide.png') }}"  src="{{ asset('htmlv1/images/logo-wide.png') }}">
              </div>
              <br><br>
              To,<br>
              {{ $data->appointment->patient->mental_hospital_address->name }}<br />
              {{ $data->appointment->patient->mental_hospital_address->address_first_line }}<br />
              {{ $data->appointment->patient->mental_hospital_address->city }}<br />
              {{ $data->appointment->patient->mental_hospital_address->country }}<br />
              {{ $data->appointment->patient->mental_hospital_address->postcode }}<br />
            </div>
            <div class="add2" style="margin-top:40px;">
              <strong>{{ env('CLINIC_NAME') }}</strong><br>
              {!! env('CLINIC_ADDRESS') !!}
              Tel: {{ env('CLINIC_PHONE') }}<br><br>
              Trading Name: {{ env('CLINIC_NAME') }}<br>
              URL: {{ env('CLINIC_URL') }}
            </div>  
            <div class="date">Date : {{ date('d-m-Y h:i a', strtotime($data->created_at)) }}</div>
            
          <div class="from2">Dear Doctor,</div>
            <div style="float:left;    width: 100%;">
                  <br/>
            <strong>Re:
            {{ $data->appointment->patient->title}} {{$data->appointment->patient->user->full_name}}, 
            (DOB: {{ date('d-m-Y', strtotime($data->appointment->patient->dob)) }}), <br>
            {{ $data->appointment->patient->address_first_line}} {{ $data->appointment->patient->city}} {{ $data->appointment->patient->country}} {{ $data->appointment->patient->postcode}}
            </strong></div>
            <br>
            <div class="matter">
              <strong>Date of Assessment:</strong> {{ date('d-m-Y h:i a', strtotime($data->appointment->date_time )) }}<br>
              <strong>Type of Assessment:</strong> {{ $data->appointment->appointment_type->name }}
            </div>
            <br/>
            <br/>
            <strong>Problem Reported By Patient:</strong> <br/>
            @if (!empty($data->patient_text)) 
              <div>{{ $data->patient_text->text }} </div>
            @endif  
            <br/>
            <br/>
            @if (!empty($data->doctor_ans))
            <strong>Questionnaire:</strong><br/><br/>
            <div style="width:100%;" >
              <table width="100%">
                <tr>
                  <td width="5%">Num</td>
                  <td width="60%">Question</td>
                  <td width="10%">Answer</td>
                  <td width="30%">Doctor Comment</td>
                </tr>
                @foreach($data->doctor_ans as $key => $value)
                <tr>
                  <td width="5%">{{ $key+1 }}</td>
                  <td width="60%">{{ $value->question }}</td>
                  <td width="10%">{{ $value->answer }}</td>
                  <td width="30%">{{ $value->comment }}</td>
                </tr>
                <br>
                @endforeach
              </table>
            </div> 
            @endif 
            <br>
            <strong>Care Plan:</strong>
            <div>
              Consulting Doctor: {{ $data->appointment->doctor->title}} {{ $data->appointment->doctor->user->full_name }}<br>
              Designation: {{ $data->appointment->doctor->designation }}
            </div>
            
            <div class="matter">{{  $data->doctor_final_comment }}</div><br>
            <strong>Consent for sharing information with hospital:</strong> {{ ($data->appointment->patient->sharing_consent == '1') ? 'Yes' : 'No' }}
            <hr>
            <div style="width: 100%;margin: 0 auto;">©2018 Happy Clinic | All Rights Reserved, Registered by CQC <span class="cqc-logo"><a href="javascirpt:;"><img width="100" height="40" src="{{ asset('htmlv1/images/images.png') }}"></a></span></div>
          </div>
        </div>
   <!--  </div>
  </div> -->

</body>
</html>