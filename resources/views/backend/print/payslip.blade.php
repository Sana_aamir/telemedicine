<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
  td{
  padding:10px;
  border-bottom:1px solid gray;
  }
  p{
  font-family:Verdana;
  }
  table{
  font-family:Verdana;
  }
  .form_logo{
      max-width: 255px;
      height: 70px;
  }
  </style>
</head>
<body>
  
<div style="width:1080px; margin-top: 20px; padding:40px; float: left;">
<div style="width:49%;  float: left;"><!-- <p style="font-size: 30px;text-align: left; margin: 0px;">{{ env('CLINIC_NAME') }}</p> -->
    <img alt="Logo" height="100" data-src-retina="{{ asset('htmlv1/images/logo-wide.png') }}" class="form_logo inline" data-src="{{ asset('htmlv1/images/logo-wide.png') }}"  src="{{ asset('htmlv1/images/logo-wide.png') }}">
</div>
<div style="width:49%;  float: left;"><p style="font-size:16px;margin-top: 55px;margin: 0px;text-align: right;padding-right: 30px; color:#808080;">Number#: {{ $data['invoice_number'] }}</p></div>



<div style="width:49%;  float: left; margin-top:20px;"><p style="font-size: 16px;text-align: left; margin:0px; color:#808080;">To<br>
  {{ $data['doctor']['user']['full_name'] }} <br>
  Work Address: {{ $data['doctor']['work_address'] }}<br>
  Home Address: {{ $data['doctor']['home_address'] }}<br>
  Mobile: {{ $data['doctor']['user']['mobile_number'] }}<br>
  Email: {{$data['doctor']['user']['email'] }}<br></p></div>
<div style="width:49%;  float: left;"><!-- <p style="font-size:20px;margin-top: 20px;margin: 0px;text-align: right;padding-right:30px;">{{ env('CLINIC_NAME') }}</p> -->
<p style="font-size:16px;text-align: right;padding-right: 30px; color:#808080;">
  {!! env('CLINIC_ADDRESS') !!}
  Tel: {{ env('CLINIC_PHONE') }}<br>
  Trading Name: {{ env('CLINIC_NAME') }}<br>
  URL: {{ env('CLINIC_URL') }}
</div>  
<br>  
<div style="width:100%; border:1px solid gray; background: #D3D3D3;"><p align="center">PAYSLIP</p></div>              
<div style="width:100%; float: left;">
<p style="font-size:16px;margin-top:10px;text-align:left;padding-right: 30px; color:#808080;">
 Name of employee : {{ $data['doctor']['user']['full_name'] }}<br>
 Designation: {{ $data['doctor']['designation'] }}<br>
 Bank Name : {{ $data['doctor']['bank_name'] }}<br>
 Account Number : {{ $data['doctor']['account_number'] }}<br>
 Sort Code : {{ $data['doctor']['sort_code'] }}<br>
 National Insurance Number : {{ $data['doctor']['national_insurance_number'] }}<br>
 Tax Code : {{ $data['doctor']['tax_code'] }}<br>
 Pay Period: {{ date('d-m-Y', strtotime($data['pay_period_start'])) }} - {{ date('d-m-Y', strtotime($data['pay_period_end'])) }}<br>
 Date of issue : {{ date('d-m-Y', strtotime($data['issue_date']))  }}
</p>  
</div>

<div style="width:100%; border:1px solid gray; background: #D3D3D3; padding:20px;"><div style="width: 50%; float: left;"><p align="left" style="margin:0px;">Service</p></div>
<div style="width: 50%; float: right;"><p align="right" style="margin:0px;">No: Fees Amount %to Doctor Doctor's Fee</p></div></div>

<div style="width:100%; padding:0px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="40%" rowspan="2">Initial Assessment</td>
      <td width="20%">Member </td>
      <td width="10%" align="right">{{ $data['description']['initial_assessment']['member']['patients']}}</td>
      <td width="10%">£{{ $data['description']['initial_assessment']['member']['fee']}}</td>
      <td width="10%">£{{ $data['description']['initial_assessment']['member']['total']}}
      </td>
       <td width="10%">
        {{ $data['percentage_to_doctor'] }}%
      </td>
      <td width="18%" align="right">
        £{{ $data['description']['initial_assessment']['member']['percentage']}}
      </td>
    </tr>
    <tr>
      <td>
           Non Member 
        </td>
        <td width="4%" align="right">
            {{ $data['description']['initial_assessment']['non_member']['patients']}}
        </td>
        <td width="7%">
          £{{ $data['description']['initial_assessment']['non_member']['fee']}}
        </td>
         <td>
          £{{ $data['description']['initial_assessment']['non_member']['total']}}
        </td>
         <td>
          {{ $data['percentage_to_doctor'] }}%
        </td>
         <td width="18%" align="right">
          £{{ $data['description']['initial_assessment']['non_member']['percentage']}}
        </td>
    </tr>  
    <tr>
      <td rowspan="2">Follow up Assessment Type 1</td>
      <td >
         Member 
      </td>
      <td align="right">{{ $data['description']['follow_up_assessment_type_1']['member']['patients']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_1']['member']['fee']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_1']['member']['total']}}</td>
      <td>{{ $data['percentage_to_doctor'] }}%</td>
      <td align="right">£{{ $data['description']['follow_up_assessment_type_1']['member']['percentage']}}</td>
    </tr>
    <tr>
      <td >
        Non Member 
      </td>
      <td align="right">{{ $data['description']['follow_up_assessment_type_1']['non_member']['patients']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_1']['non_member']['fee']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_1']['non_member']['total']}}</td>
      <td>{{ $data['percentage_to_doctor'] }}%</td>
      <td align="right">£{{ $data['description']['follow_up_assessment_type_1']['non_member']['percentage']}}</td>
    </tr>
    <tr>
      <td rowspan="2">Follow up Assessment Type 2</td>
      <td >
         Member 
      </td>
      <td align="right">{{ $data['description']['follow_up_assessment_type_2']['member']['patients']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_2']['member']['fee']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_2']['member']['total']}}</td>
      <td>{{ $data['percentage_to_doctor'] }}%</td>
      <td align="right">£{{ $data['description']['follow_up_assessment_type_2']['member']['percentage']}}</td>
    </tr>
    <tr>
      <td >
        Non Member 
      </td>
      <td align="right">{{ $data['description']['follow_up_assessment_type_2']['non_member']['patients']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_2']['non_member']['fee']}}</td>
      <td>£{{ $data['description']['follow_up_assessment_type_2']['non_member']['total']}}</td>
      <td>{{ $data['percentage_to_doctor'] }}%</td>
      <td align="right">£{{ $data['description']['follow_up_assessment_type_2']['non_member']['percentage']}}</td>
    </tr>
    <tr>
      <td rowspan="2">Telephone Advice</td>
       <td >
           Member 
        </td>
        <td align="right">{{ $data['description']['telephone_advice']['member']['patients']}}</td>
        <td>£{{ $data['description']['telephone_advice']['member']['fee']}}</td>
        <td>£{{ $data['description']['telephone_advice']['member']['total']}}</td>
        <td>{{ $data['percentage_to_doctor'] }}%</td>
        <td align="right">£{{ $data['description']['telephone_advice']['member']['percentage']}}</td>
    </tr>
    <tr>
      <td >
        Non Member 
      </td>
      <td align="right">{{ $data['description']['telephone_advice']['non_member']['patients']}}</td>
      <td>£{{ $data['description']['telephone_advice']['non_member']['fee']}}</td>
      <td>£{{ $data['description']['telephone_advice']['non_member']['total']}}</td>
      <td>{{ $data['percentage_to_doctor'] }}%</td>
      <td align="right">£{{ $data['description']['telephone_advice']['non_member']['percentage']}}</td>
    </tr>
    <tr>
      <td style="border-bottom:0px;"><strong>Total Payment to doctor (Before NI and tax deductions)</strong></td>
      <td style="border-bottom:0px;">&nbsp;</td>
      <td style="border-bottom:0px;">&nbsp;</td>
      <td style="border-bottom:0px;">&nbsp;</td>
      <td style="border-bottom:0px;">&nbsp;</td>
      <td style="border-bottom:0px;">&nbsp;</td>
      <td align="right" style="border-bottom:0px;"><strong> £{{ $data['amount'] }}</strong></td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom:0px;">&nbsp;</td>
      </tr>
    <tr>
      <td colspan="6" style="border-bottom:0px;"><p>Copyright © 2018 - {{ env('CLINIC_NAME') }}. Registered in UK</p></td>
    </tr>
  </table>
</div>

</div>

</body>
</html>