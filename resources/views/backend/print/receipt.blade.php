<!DOCTYPE html>
<html>
<head>
  <title>{{ env('CLINIC_NAME') }}</title>
  <style>
  td{
  padding:10px;
  border-bottom:1px solid gray;
  }
  p{
  font-family:Verdana;
  }
  table{
  font-family:Verdana;
  }
  .form_logo{
    max-width: 255px;
      height: 70px;
      margin-top: 20px;
  }
  </style>
</head>
<body>
  
<div style="width:1080px; margin-top: 20px; padding:40px; float: left;">
<div style="width:49%;  float: left;"> 
  <img alt="Logo" height="100" data-src-retina="{{ asset('htmlv1/images/logo-wide.png') }}" class="form_logo inline" data-src="{{ asset('htmlv1/images/logo-wide.png') }}"  src="{{ asset('htmlv1/images/logo-wide.png') }}"></div>
<div style="width:49%;  float: left;"><p style="font-size:16px;margin-top: 55px;margin: 0px;text-align: right;padding-right: 30px; color:#808080;">Number#: {{ $data['number'] }}</p></div>



<div style="width:49%;  float: left; margin-top:20px;"><p style="font-size: 16px;text-align: left; margin:0px; color:#808080;">To<br>
  {{ $data['patient']['user']['full_name'] }} <br>
  {{ $data['patient']['address_first_line'] }} <br>
  {{ $data['patient']['city'] }} <br>
  {{ $data['patient']['country'] }} <br>
  {{ $data['patient']['postcode'] }} <br></p></div>
<div style="width:49%;  float: left;"><!-- <p style="font-size:20px;margin-top: 20px;margin: 0px;text-align: right;padding-right:30px;">Telemedicine</p> -->
<p style="font-size:16px;margin-top:10px;text-align: right;padding-right: 30px; color:#808080;">
  {!! env('CLINIC_ADDRESS') !!}
  Tel: {{ env('CLINIC_PHONE') }}<br>
  Trading Name: {{ env('CLINIC_NAME') }}<br>
  URL: {{ env('CLINIC_URL') }}
</div>  
<br>  
<div style="width:100%; border:1px solid gray; background: #D3D3D3;"><p align="center">RECEIPT</p></div>              
<div style="width:100%; float: left;">
<p style="font-size:16px;margin-top:10px;text-align:left;padding-right: 30px; color:#808080;">
Name of patient : {{ $data['patient']['user']['full_name'] }}<br>
Date of Birth: {{ date('d-m-Y', strtotime($data['patient']['dob'])) }} 
</p>  
</div>

<div style="width:100%; border:1px solid gray; background: #D3D3D3; padding:20px;">
    <div style="width: 40%; float: left;">
      <p align="left" style="margin:0px;">Amount Paid</p>
    </div>

    <div style="width: 20%; float: left;">
      <p align="left" style="margin:0px;"> Date of Payment
      </p>
    </div>
    <div style="width: 30%; float: right;">
      <p align="right" style="margin:0px;">Payment Status
      </p>
    </div>
    
  </div>

<div style="width:100%; padding:0px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="40%">£{{ $data['amount_paid'] }}</td>
      <td width="20%">{{ date('d-m-Y', $data['date']) }} </td>
     
       <td width="30%" align="right">
        {{ $data['status']}}
      </td>
      
    </tr>
    </table>
</div>
<div style="width:100%; border:1px solid gray; background: #D3D3D3; padding:20px;">
    <div>
      <p align="center" style="margin:0px;">SUMMARY</p>
    </div>
  </div>
<div style="width:100%; padding:0px;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!-- <th colspan="3">SUMMARY</th> -->
    @if ($data['type'] == 'subscription')
    <tr>
      <td width="30%" colspan="3">Type of Service : {{$data['subscriptions']['description'] }}</td>
    </tr>
    <tr>  
      <td width="30%" colspan="3">Fees : £{{ $data['amount_paid'] }} </td>
    </tr>
    <tr width="30%">
      <td colspan="3">Pay Period: {{ date('d-m-Y', strtotime($data['subscriptions']['start_date'])) }} - {{ date('d-m-Y', strtotime($data['subscriptions']['end_date'])) }}</td>
    </tr>
    @else
      <tr>
        <td width="30%" colspan="3">Type of Service : {{$data['items']['description'] }}</td>
      </tr>
      <tr>  
        <td width="30%" colspan="3">Fees : £{{ $data['amount_paid'] }} </td>
      </tr>
    @endif
    <tr>  
      <td width="30%" colspan="3">Date of issue : {{ date('d-m-Y', $data['date'])  }} </td>
    </tr>
    </table>
</div>
<p>Copyright © 2018 - {{ env('CLINIC_NAME') }}. Registered in UK</p>

</div>

</body>
</html>