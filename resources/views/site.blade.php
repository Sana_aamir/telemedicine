<!DOCTYPE html>
<html>
 <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('CLINIC_NAME') }}</title>
    <meta name="viewport" content="width=device-width,height=device-height,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <link rel="apple-touch-icon" href="touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('htmlv1/images/favicon-new/144x144.jpg') }}">
    <link rel="apple-touch-icon" sizes="192x192" href="{{ asset('htmlv1/images/favicon-new/192x192.jpg') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('htmlv1/images/favicon-new/152x152.jpg') }}">
    <link rel="icon" type="image/x-icon" href="{{asset('htmlv1/images/favicon/64x64-2.png')}}" />
    <link href="{{ asset('front/stylesheets/plain-all.css') }}" rel="stylesheet">
    <link rel="manifest" href="{{ asset('manifest.json')}}">
    
    <script type="text/javascript">
    <?php 

    $appSettings = App\Library\Helper\Utility::appSettings();
    ?>
    var configConstants = <?php echo json_encode($appSettings);?>;
    </script>
    
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer>
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111273313-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-111273313-4');
    </script>
</head>
  <body>
    @php $flashMessage = NULL; @endphp
     @if (session()->get('flash') != NULL)
      @php  $flashMessage = session()->get('flash'); @endphp
    @endif
    <span id="app">
        <router-view :flashmessage="'{{ $flashMessage }}'" :key="$route.fullPath"></router-view>  
 
        <cookie-component></cookie-component>
    </span>
    <script type="text/javascript" src="{{asset('js/frontend/all.js')}}"></script>
    <input type="hidden" id="service_worker_path" value="{{asset('service-worker.js')}}">

    
  </body>
  <script>
      $( document ).ready(function() {
          if ('serviceWorker' in navigator) {
            console.log("Will the service worker register?");
            navigator.serviceWorker.register($('#service_worker_path').val())
              .then(function(reg){
                console.log("Yes, it did.");
              }).catch(function(err) {
                console.log("No it didn't. This happened: ", err)
              });
        }
        

      // Detects if device is on iOS 
      const isIos = () => {
        const userAgent = window.navigator.userAgent.toLowerCase();
        console.log(userAgent);
        return /iphone|ipad|ipod/.test( userAgent );
      }
      // Detects if device is in standalone mode
      const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);
        console.log('standalone ' + isInStandaloneMode);
      // Checks if should display install popup notification:
      if (isIos() && !isInStandaloneMode()) {
        console.log('inside ios check');
        this.setState({ showInstallMessage: true });
      }

    });
        
    </script>
</html>