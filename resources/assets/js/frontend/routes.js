import Vue from 'vue';
import VueRouter from 'vue-router';
import VueRouterMiddleware, { middleware } from 'vue-router-middleware';

// frontend Components
import Index from './../components/frontend/index.vue';
import Login from './../components/frontend/user/login.vue';
import Logout from './../components/frontend/user/logout.vue';
import Register from './../components/frontend/user/register.vue';
import NewPassword from './../components/frontend/user/NewPassword.vue';
import ErrorComponent from './../components/error/error404.vue';
import ErrorComponent404 from './../components/error/error404.vue';
import UploadAvatar from './../components/frontend/uploadAvatar.vue';
import Dashboard from './../components/frontend/doctor/Dashboard.vue';
import AccountIndex from './../components/frontend/account/AccountIndex.vue';
import AccountMessageView from './../components/frontend/account/AccountIndex.vue';
import BookAppointment from './../components/frontend/appointment/BookAppointment.vue';
import ContactUs from './../components/frontend/user/ContactUs.vue';
import ForDoctors from './../components/frontend/doctor/ForDoctors.vue';

import GetMembership from './../components/frontend/membership/GetMembership.vue';
import Membership from './../components/frontend/membership/Membership.vue';
import AboutUs from './../components/frontend/static/AboutUs.vue';
import CQC from './../components/frontend/static/CQC.vue';
import FAQ from './../components/frontend/static/FAQ.vue';
import HowItWorks from './../components/frontend/static/HowItWorks.vue';
import Pricing from './../components/frontend/static/Pricing.vue';
import Privacy from './../components/frontend/static/Privacy.vue';
import CookiePolicy from './../components/frontend/static/CookiePolicy.vue';
import Terms from './../components/frontend/static/Terms.vue';
import Testimonial from './../components/frontend/static/Testimonial.vue';
import MainAssessment from './../components/frontend/patient-assessement/MainAssessment.vue';
import DownloadPage from './../components/frontend/static/DownloadPage.vue';

//for doctor panel
import DoctorPanel from './../components/frontend/doctor-panel/Main.vue';
import DoctorMessageView from './../components/frontend/doctor-panel/Main.vue';

//patient profile
/*import PatientProfile from './../components/frontend/patient/Main.vue';*/
import PatientProfileDetail from './../components/frontend/patient/ProfileDetail.vue';
import DoctorProfileDetail from './../components/frontend/doctor/DoctorProfile.vue';

//patient video view
import PatientAssessementVideoView from './../components/frontend/patient-assessement/PatientAssessmentView.vue';

//what we treat
import WhatWeTreat from './../components/frontend/static/whatwetreat.vue';
import OurDoctors from './../components/frontend/doctor/OurDoctors.vue';
/*import Pages from './../components/frontend/static/Page.vue';*/

import AssessmentThankYou from './../components/frontend/patient-assessement/AssessmentThankYou.vue';


const routes = [

	// frontend routes
    {   path: '/',
        component:Index,
        name: 'Index'
    },
    {   path: '/login',
        component:Login,
        name: 'Login'
    },
    {   path: '/logout',
        component:Logout,
        name: 'Logout'
    },
    {   path: '/register',
        component:Register,
        name: 'Register'
    },
    {   path: '/patient-video',
        component:PatientAssessementVideoView,
        name: 'patient-assessement-view-view'
    },
    /*{   path: '/static/:slug',
        component:Pages,
        name: 'Pages'
    },  */

    {   path: '/assessment/thank-you/:id',
        component:AssessmentThankYou,
        name: 'AssessmentThankYou'
    },
    ...middleware('require-auth', [
        {   path: '/dashboard',
            component:Dashboard,
            name: 'Dashboard'
        },
        {   path: '/uploadAvatar',
            component:UploadAvatar,
            name: 'UploadAvatar'
        },


        ...middleware({ 'check-permission': ['patient'] }, [ // can have multiple permission like ['superadmin', 'doctor']
            // Active middlewares in sequence ['require-auth', 'check-permission']
            {   path: '/book-appointment',
                component: BookAppointment,
                name: 'BookAppointment'
            },
            {   path: '/my-account',
                component:AccountIndex,
                name: 'AccountIndex',
            },
            {   path: '/support/view/:slug',
                component:AccountMessageView,
                name: 'AccountMessageView'
            },
        ]),
        ...middleware({ 'check-permission': ['doctor'] }, [ // can have multiple permission like ['superadmin', 'doctor']
            // Active middlewares in sequence ['require-auth', 'check-permission']


            {   path: '/doctor-panel',
                component:DoctorPanel,
                name: 'DoctorPanel',
            },
            {   path: '/support-doctor/view/:slug',
                component:DoctorMessageView,
                name: 'DoctorMessageView'
            },
            {   path: '/patient-profile-detail/:id',
                component:PatientProfileDetail,
                name: 'PatientProfileDetail'
            },

        ]),
    ]),

    {   path: '/assessment/:id',
        component:MainAssessment,
        name: 'MainAssessment'
    },

    {   path: '/request-password/:token',
        component:NewPassword,
        name: 'NewPassword'
    },
    {   path: '/get-membership',
        component:GetMembership,
        name: 'GetMembership'
    },
    {   path: '/membership',
        component:Membership,
        name: 'Membership'
    },
    {   path: '/about-us',
        component:AboutUs,
        name: 'AboutUs'
    },
    {   path: '/download-mobile-app',
        component:DownloadPage,
        name: 'DownloadPage'
    },
    {   path: '/cqc',
        component:CQC,
        name: 'CQC'
    },
    {   path: '/faq',
        component:FAQ,
        name: 'FAQ'
    },
    {   path: '/privacy',
        component:Privacy,
        name: 'Privacy'
    },
    {   path: '/cookie-policy',
        component:CookiePolicy,
        name: 'CookiePolicy'
    },
    {   path: '/how-it-works',
        component:HowItWorks,
        name: 'HowItWorks'
    },
    {   path: '/pricing',
        component:Pricing,
        name: 'Pricing'
    },
    {   path: '/terms-conditions',
        component:Terms,
        name: 'Terms'
    },

    {   path: '/testimonials',
        component:Testimonial,
        name: 'Testimonial'
    },
    {   path: '/contact-us',
        component:ContactUs,
        name: 'ContactUs'
    },
    {   path: '/doctors',
        component:ForDoctors,
        name: 'ForDoctors'
    },
    {   path: '/what-we-treat',
        component:WhatWeTreat,
        name: 'what-we-treat'
    },
    {   path: '/doctor-profile/:id',
        component:DoctorProfileDetail,
        name: 'DoctorProfileDetail'
    },

    {   path: '/our-doctors',
        component:OurDoctors,
        name: 'OurDoctors'
    },
    {   path: '*',
        component: ErrorComponent ,
        name: 'ErrorComponent'
    },
    {   path: '/404',
        component: ErrorComponent404,
        name: 'ErrorComponent404'
    },
];

const router = new VueRouter({
    base: '/telemedicine/public/',
    mode : 'history',
    routes,
    scrollBehavior: (to) => {
        if (to.hash) {
            return { selector: to.hash }
        } else {
            return { x: 0, y: 0 }
        }
    },
})

/*Vue.use(VueRouter);*/
Vue.use(VueRouterMiddleware, {
    router,
    middlewares: {

        // Convert to camelcase to dash string ex. requireAuth saves require-auth
        requireAuth(params, to, from, next) {
            if (to.name == 'MessageView' && from.name == null) {
                var support_view = to.params.slug;
                window.localStorage.setItem('support_view', JSON.stringify(support_view));
            }

            if (to.name == 'AccountIndex' && to.hash != '') {
                window.localStorage.setItem('accountHash', JSON.stringify(to.hash));
            }

            let user;
            if (router.app.$store.getters.getAuthUser != 'undefined') {
                user = JSON.parse(router.app.$store.getters.getAuthUser);
            }

            if (!user) {
                return router.push({ name: 'Login' });
            } else {
                return next();
            }

        },
        checkPermission(params, to, from, next) {

            let user = JSON.parse(router.app.$store.getters.getAuthUser);

            if (jQuery.inArray(user.role, params) !== -1) {
                if (to.name == 'BookAppointment' && user.is_member == 0 && from.name != 'Membership') {
                    return router.push({ name: 'Membership' });
                }
                return next();
            } else  {
                return router.push({ name: 'ErrorComponent' });
            }
        },
    }
})

export default router;
