
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./../admin/bootstrap');
require('./../../../../public/htmlv1/js/bootstrap-datepicker.js');
require('./../../../../public/htmlv1/js/summernote.min.js');
require('./../../../../public/htmlv1/js/jquery.bootstrap.wizard.min.js');
require('./../../../../public/htmlv1/js/prettify.js');
require('./../../../../public/htmlv1/js/opentok.min.js');
require('./../../../../public/htmlv1/js/summernote.min.js');

import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store.js';
import Router from './routes.js';
import Paginate from 'vuejs-paginate';
import MaskedInput from 'vue-masked-input';
import VeeValidate from 'vee-validate';
import VueSelect2 from 'vue-select';
import VueMoment from 'vue-moment';
import VueUploadComponent from 'vue-upload-component';
import 'cropperjs/dist/cropper.min.css';
import axios from 'axios';
import Vuex from 'vuex';
import VueCookies from 'vue-cookies'


Vue.use(VueRouter);
Vue.use(Router);
Vue.use(VeeValidate);
Vue.use(VueMoment);
Vue.use(VueCookies);

Vue.use(Vuex);

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value;
  var text = div.textContent || div.innerText || ", ";
  return text;
});

Vue.filter('removeUnderscore', function (str) {
  return str.replace(/_/g, ' ');
})

VeeValidate.Validator.extend('verify_password', {
  // Custom validation message
  getMessage: (field) => `Your password must be more than 8 characters long, should contain <br>at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.`,
  // Custom validation rule
  validate: function (value) {
    return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(value)
  },
});



// global constants
Vue.prototype.$baseUrl = configConstants.baseUrl;
Vue.prototype.$apiUrl = configConstants.apiUrl;
Vue.prototype.$imageUrl = configConstants.baseUrl+'/htmlv1/images/';
Vue.prototype.$clinicAddress = 'Foress Media Ltd<br />\
    Bessemer Drive,<br />\
    Stevenage<br />\
    Herts<br />\
    SG1 2DX<br />';
Vue.prototype.$clinicPhone = '07801477895';
Vue.prototype.$clinicWebsite = 'www.happyclinic.co.uk';
Vue.prototype.$clinicName = 'Happy Clinic';
Vue.prototype.$membershipFee = 12;
Vue.prototype.$stripeKey = 'pk_test_GEv9RXF3IKoqqnEJCVcEEBli';
Vue.prototype.$openTokKey = '46227052';

Vue.prototype.$accept = 'image/png,image/jpeg,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document';
Vue.prototype.$extensions = 'jpg,png,pdf,doc,docx';

Vue.prototype.$isMobile = isMobile();

function isMobile() {
  var check = false;
  (function(a){
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) 
    check = true;
    })(navigator.userAgent||navigator.vendor||window.opera);
  return check;
    
}

// Defining Components
import HeaderComponent  from './../components/frontend/common/header.vue';
import FooterComponent from './../components/frontend/common/footer.vue';
import BannerComponent from './../components/frontend/common/banner.vue';
import CookieComponent from './../components/frontend/common/cookie.vue';
import AuthHeaderComponent from './../components/frontend/common/auth-header.vue';
import FlashMessageComponent from './../components/frontend/common/flash-message.vue';
import DeleteRecModal from './../components/frontend/common/deleteRecModal.vue';
import AttachmentViewModal from './../components/frontend/common/attachmentViewModal.vue';
import GlobalSpinner from './../components/frontend/common/GlobalSpinner.vue';
import ImageUploadComponent from './../components/frontend/common/ImageUploadComponent.vue';

//popups
import SendMessage from './../components/frontend/patient-assessement/SendMessage.vue';
import AddNotes from './../components/frontend/patient-assessement/AddNotes.vue';
import SendEmail from './../components/frontend/patient-assessement/SendEmail.vue';
import RepeatDate from './../components/frontend/patient-assessement/RepeatDate.vue';
import ViewNotePopup from './../components/frontend/patient-assessement/ViewNote.vue';
/*import MessagesView from './../components/admin/common/MessagesView.vue';*/


Vue.component('header-component', HeaderComponent)
Vue.component('footer-component', FooterComponent)
Vue.component('banner-component', BannerComponent)
Vue.component('cookie-component', CookieComponent)
Vue.component('auth-header', AuthHeaderComponent)
Vue.component('flash-message', FlashMessageComponent)
Vue.component('delete-record-modal', DeleteRecModal)
Vue.component('attachment-view-modal', AttachmentViewModal)
Vue.component('c-spinner', GlobalSpinner)
Vue.component('image-upload-component', ImageUploadComponent)
/*Vue.component('messages-view', MessagesView)*/

//popups
Vue.component('send-message-popup', SendMessage)
Vue.component('add-notes-popup', AddNotes)
Vue.component('send-email-popup', SendEmail)
Vue.component('repeat-date-popup', RepeatDate)
Vue.component('view-notes-popup', ViewNotePopup)


// Registering Components
Vue.component('masked-input', MaskedInput);
Vue.component('paginate', Paginate);
Vue.component('file-upload', VueUploadComponent);
Vue.component('vue-select', VueSelect2);



window.EventBus = new Vue;
var access_token = JSON.parse(store.getters.getAuthToken);
if (access_token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
}

setTimeout(function() {
   if (store.getters.getAuthenticated == 1) {
    store.commit('setAuthenticated', 0);
  }
}, 600000);//10 minutes



axios.interceptors.response.use((response) => {
    return response;
}, function (error) {
    let originalRequest = error.config
    // Do something with response error
    if (error.response.status === 401 && !originalRequest._retry) { // if the error is 401 and hasent already been retried
      Router.replace('/logout');
    }
    if(error.response.status === 406 || error.response.status === 422 || error.response.status === 400){  
     return Promise.reject(error);
    }
});


// refresh token code
/*axios.interceptors.response.use((response) => {
    return response;
}, function (error) {
    let originalRequest = error.config
    // Do something with response error
    if (error.response.status != 401) {
      return Promise.reject(error);
     
    }
    axios.get(configConstants.apiUrl+'refresh')
    .then(response => {
        store.commit('setAuthToken', response.data.access_token);
        error.response.config.headers['Authorization'] = 'Bearer ' + response.data.access_token;
        return axios(error.response.config);
    }).catch(error => {
        store.dispatch('clearAuthToken');
        localStorage.removeItem('authToken')
        Router.push('/login');
        return Promise.reject(error);
    })
    //if(error.response.status === 406 || error.response.status === 422){  
     
    //}
});*/
/*Vue.use(axios);*/
const app = new Vue({ 
  el : '#app',
  store,
  router : Router,
    components: {
    },
    methods:{
    },
    created() {
      this.$store.watch(
        (state, getters) => getters.getAuthToken,
        (newValue, oldValue) => {
          // set access token header on every request
          if (newValue) {
            var access_token = JSON.parse(newValue);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
            this.complex = {
              deep: 'some deep object',
            };
          }
        },
      );
    } 
});


