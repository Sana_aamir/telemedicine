
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueRouterMiddleware, { middleware } from 'vue-router-middleware';

import LoginIndex from './../components/admin/user/LoginIndex.vue';
import Logout from './../components/admin/user/Logout.vue';
import ForgotPasswordIndex from './../components/admin/user/ForgotPasswordIndex.vue';
import RegisterIndex from './../components/admin/user/RegisterIndex.vue';
import ResetPassword from './../components/admin/user/ResetPassword.vue';
import DashboardIndex from './../components/admin/user/DashboardComponent.vue';
import AccountComponent from './../components/admin/user/AccountComponent.vue';
import ProfileComponent from './../components/admin/user/ProfileComponent.vue';
import ErrorComponent from './../components/error/error404.vue';

import SpecialtyIndex from './../components/admin/specialty/SpecialtyIndex.vue';

import QuestionnaireIndex from './../components/admin/questionnaire/QuestionnaireIndex.vue';

import DoctorIndex from './../components/admin/doctor/DoctorIndex.vue';
import DoctorForm from './../components/admin/doctor/DoctorForm.vue';
import DoctorView from './../components/admin/doctor/DoctorView.vue';
import DoctorSchedule from './../components/admin/doctor/DoctorSchedule.vue';
import DoctorScheduleEdit from './../components/admin/doctor/DoctorScheduleEdit.vue';

import PatientIndex from './../components/admin/patient/PatientIndex.vue';
import PatientForm from './../components/admin/patient/PatientForm.vue';
import PatientView from './../components/admin/patient/PatientView.vue';
import PatientAssessment from './../components/admin/patient/PatientAssessment.vue';

import AppointmentIndex from './../components/admin/appointment/AppointmentIndex.vue';
import AppointmentEdit from './../components/admin/appointment/AppointmentEdit.vue';
import AppointmentCreate from './../components/admin/appointment/AppointmentCreate.vue';
import AppointmentCalendar from './../components/admin/appointment/AppointmentCalendar.vue';
import AppointmentFeeForm from './../components/admin/appointment/AppointmentFeeForm.vue';
import AppointmentReminderSchedule from './../components/admin/appointment/AppointmentReminderSchedule.vue';
import AppointmentDuration from './../components/admin/appointment/AppointmentDuration.vue';

import MentalHealthHospitalIndex from './../components/admin/mental_health_hospital/MentalHealthHospitalIndex.vue';
import MentalHealthHospitalForm from './../components/admin/mental_health_hospital/MentalHealthHospitalForm.vue';

import LaboratoryIndex from './../components/admin/laboratory/LaboratoryIndex.vue';
import LaboratoryForm from './../components/admin/laboratory/LaboratoryForm.vue';

import SupportIndex from './../components/admin/support/SupportIndex.vue';
import SupportForm from './../components/admin/support/SupportForm.vue';
import SupportView from './../components/admin/support/SupportView.vue';
import SupportForward from './../components/admin/support/SupportForward.vue';

import AuditsIndex from './../components/admin/audit/AuditsIndex.vue';
import AuditsForm from './../components/admin/audit/AuditsForm.vue';

import PolicyIndex from './../components/admin/policy/PolicyIndex.vue';
import PolicyForm from './../components/admin/policy/PolicyForm.vue';

import StaffIndex from './../components/admin/staff/StaffIndex.vue';
import StaffForm from './../components/admin/staff/StaffForm.vue';

import SeriousIncidentIndex from './../components/admin/serious_incident/SeriousIncidentIndex.vue';
import SeriousIncidentForm from './../components/admin/serious_incident/SeriousIncidentForm.vue';

import ComplaintIndex from './../components/admin/complaint/ComplaintIndex.vue';
import EmailIndex from './../components/admin/email/EmailIndex.vue';

import PageIndex from './../components/admin/page/PageIndex.vue';
import PageForm from './../components/admin/page/PageForm.vue';

import LabFormIndex from './../components/admin/lab_form/LabFormIndex.vue';
import HospitalRecordIndex from './../components/admin/hospital_record/HospitalRecordIndex.vue';
import BloodTestIndex from './../components/admin/repeat-blood-test/BloodTestIndex.vue';

import MembershipIndex from './../components/admin/membership/MembershipIndex.vue';
import MembershipCancelRequests from './../components/admin/membership/MembershipCancelRequests.vue';

import LogIndex from './../components/admin/log/LogIndex.vue';
import VideoCall from './../components/admin/common/VideoCall.vue';

import BusinessSettings from './../components/admin/business-setting/BusinessSetting.vue';
import TestimonialIndex from './../components/admin/testimonial/TestimonialIndex.vue';



const routes = [
 
    ...middleware('require-auth', [
        // Active middleware ['require-auth']
        {   path: '/dashboard', 
            component: DashboardIndex, 
            name: 'DashboardIndex',
        },
        {   path: '/account', 
            component: AccountComponent, 
            name: 'AccountComponent', 
        },
        {   path: '/profile', 
            component: ProfileComponent, 
            name: 'ProfileComponent',
        },
     
        // Pass parameters to middleware
        ...middleware({ 'check-permission': ['superadmin'] }, [ // can have multiple permission like ['superadmin', 'doctor']
            // Active middlewares in sequence ['require-auth', 'check-permission']
            {   path: '/specialties', 
                component: SpecialtyIndex, 
                name: 'SpecialtyIndex',
            },
            {   path: '/questionnaire/:id', 
                component: QuestionnaireIndex, 
                name: 'QuestionnaireIndex',
            },
            {   path: '/appointment-durations/:id', 
                component: AppointmentDuration, 
                name: 'AppointmentDuration',
            },
            {   path: '/doctors', 
                component: DoctorIndex, 
                name: 'DoctorIndex', 
            },
            {   path: '/doctors/create', 
                component: DoctorForm, 
                name: 'DoctorCreate', 
            },
            {   path: '/doctors/edit/:id', 
                component: DoctorForm, 
                name: 'DoctorEdit', 
            },
            {   path: '/doctors/view/:id', 
                component: DoctorView, 
                name: 'DoctorView', 
            },
            {   path: '/doctors/schedule/:id', 
                component: DoctorSchedule, 
                name: 'DoctorSchedule', 
            },
            {   path: '/doctors/schedule/edit/:id', 
                component: DoctorScheduleEdit, 
                name: 'DoctorScheduleEdit', 
            },
            {   path: '/patients', 
                component: PatientIndex, 
                name: 'PatientIndex', 
            },
            {   path: '/patients/create', 
                component: PatientForm, 
                name: 'PatientCreate', 
            },
            {   path: '/patients/edit/:id', 
                component: PatientForm, 
                name: 'PatientEdit', 
            },
            {   path: '/patients/view/:id', 
                component: PatientView, 
                name: 'PatientView', 
            },
            {   path: '/patients/assessment/:id', 
                component: PatientAssessment, 
                name: 'PatientAssessment', 
            },
            {   path: '/appointments', 
                component: AppointmentIndex, 
                name: 'AppointmentIndex',
            },
            {   path: '/appointments/create', 
                component: AppointmentCreate, 
                name: 'AppointmentCreate', 
            },
            {   path: '/appointments/edit/:id/:specialty_id', 
                component: AppointmentEdit, 
                name: 'AppointmentEdit',
            },
            {   path: '/mental_health_hospitals', 
                component: MentalHealthHospitalIndex, 
                name: 'MentalHealthHospitalIndex',
            },
            {   path: '/mental_health_hospitals/create', 
                component: MentalHealthHospitalForm, 
                name: 'MentalHealthHospitalCreate', 
            },
            {   path: '/mental_health_hospitals/edit/:id', 
                component: MentalHealthHospitalForm, 
                name: 'MentalHealthHospitalEdit', 
            },
            {   path: '/laboratories', 
                component: LaboratoryIndex, 
                name: 'LaboratoryIndex', 
            },
            {   path: '/laboratories/create', 
                component: LaboratoryForm, 
                name: 'LaboratoryCreate',
            },
            {   path: '/laboratories/edit/:id', 
                component: LaboratoryForm, 
                name: 'LaboratoryEdit', 
            },
            {   path: '/audits', 
                component: AuditsIndex, 
                name: 'AuditsIndex', 
            },
            {   path: '/audits/create', 
                component: AuditsForm, 
                name: 'AuditsCreate',
            },
            {   path: '/audits/edit/:id', 
                component: AuditsForm, 
                name: 'AuditsEdit', 
            },
            {   path: '/policies', 
                component: PolicyIndex, 
                name: 'PolicyIndex', 
            },
            {   path: '/policies/create', 
                component: PolicyForm, 
                name: 'PolicyCreate',
            },
            {   path: '/policies/edit/:id', 
                component: PolicyForm, 
                name: 'PolicyEdit', 
            },
            {   path: '/staff', 
                component: StaffIndex, 
                name: 'StaffIndex', 
            },
            {   path: '/staff/create', 
                component: StaffForm, 
                name: 'StaffCreate',
            },
            {   path: '/staff/edit/:id', 
                component: StaffForm, 
                name: 'StaffEdit', 
            },
            {   path: '/serious_incidents', 
                component: SeriousIncidentIndex, 
                name: 'SeriousIncidentIndex', 
            },
            {   path: '/serious_incidents/create', 
                component: SeriousIncidentForm, 
                name: 'SeriousIncidentCreate',
            },
            {   path: '/serious_incidents/edit/:id', 
                component: SeriousIncidentForm, 
                name: 'SeriousIncidentEdit', 
            },
            {   path: '/support-requests', 
                component: SupportIndex, 
                name: 'SupportIndex', 
            },
            {   path: '/support/create/:type/:id', 
                component: SupportForm, 
                name: 'SupportForm', 
            },
            {   path: '/support/forward/:id', 
                component: SupportForward, 
                name: 'SupportForward', 
            },
            {   path: '/support/view/:slug', 
                component: SupportView, 
                name: 'SupportView', 
            },
            {   path: '/complaints', 
                component: ComplaintIndex, 
                name: 'ComplaintIndex', 
            },
            {   path: '/emails', 
                component: EmailIndex, 
                name: 'EmailIndex', 
            },
            {   path: '/pages', 
                component: PageIndex, 
                name: 'PageIndex', 
            },
            {   path: '/pages/create', 
                component: PageForm, 
                name: 'PageCreate',
            },
            {   path: '/pages/edit/:id', 
                component: PageForm, 
                name: 'PageEdit', 
            },
            {   path: '/settings/appointment-fee', 
                component: AppointmentFeeForm, 
                name: 'AppointmentFeeForm', 
            },
            {   path: '/settings/appointment-reminder-scheduler', 
                component: AppointmentReminderSchedule, 
                name: 'AppointmentReminderSchedule', 
            },
            {   path: '/lab-forms', 
                component: LabFormIndex, 
                name: 'LabFormIndex', 
            },
            {   path: '/hospital-records', 
                component: HospitalRecordIndex, 
                name: 'HospitalRecordIndex', 
            },
            {   path: '/repeat-blood-test', 
                component: BloodTestIndex, 
                name: 'BloodTestIndex', 
            },
            {   path: '/memberships', 
                component: MembershipIndex, 
                name: 'MembershipIndex', 
            },
            {   path: '/membership-cancel-requests', 
                component: MembershipCancelRequests, 
                name: 'MembershipCancelRequests', 
            },
            {   path: '/activity-log', 
                component: LogIndex, 
                name: 'LogIndex', 
            },
            {   path: '/video-call', 
                component: VideoCall, 
                name: 'VideoCall', 
            },
            {   path: '/business-settings', 
                component: BusinessSettings, 
                name: 'BusinessSettings', 
            },
            {   path: '/testimonials', 
                component: TestimonialIndex, 
                name: 'TestimonialIndex', 
            },
        ])
     
    ]),
    // Route without middlewares
    {   path: '/login', 
        component:LoginIndex , 
        name: 'LoginIndex'
    }, 
    {   path: '/logout', 
        component:Logout, 
        name: 'Logout'
    },
    {   path: '/forgot-password', 
        component:ForgotPasswordIndex, 
        name: 'ForgotPasswordIndex'
    },
    {   path: '/register', 
        component:RegisterIndex, 
        name: 'RegisterIndex'
    },

    {   path: '/request-password/:token', 
        component:ResetPassword, 
        name: 'ResetPassword'
    },
    {   path: '/404', 
        component: ErrorComponent ,
        name: 'ErrorComponent'
    },
]

const router = new VueRouter({ 
    base: '/telemedicine/public/admin/',
    //base: '/admin/',
    mode : 'history',
    routes 
})
 
Vue.use(VueRouter);
Vue.use(VueRouterMiddleware, {
    router,
    middlewares: {
        // Convert to camelcase to dash string ex. requireAuth saves require-auth
        requireAuth(params, to, from, next) {
            if (to.name == 'SupportView' && from.name == null) {
                var support_view = to.params.slug;
                window.localStorage.setItem('support_view', JSON.stringify(support_view));
            }
            const authUser = JSON.parse(window.localStorage.getItem('lbUser'));
            if (!authUser ) {
                return router.push({ name: 'LoginIndex' });
            }
            return next();
        },
        checkPermission(params, to, from, next) {
            const authUser = JSON.parse(window.localStorage.getItem('lbUser'));
            if (jQuery.inArray(authUser.role, params) !== -1) {
                return next();
            } else {
                return router.push({ name: 'ErrorComponent' });
            }
        },
    }
})



export default router;