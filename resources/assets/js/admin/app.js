
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./summernote.min.js');

import Vue from 'vue';
import Router from './routes.js';
import store from './store.js';
import Paginate from 'vuejs-paginate';
import VueSelect2 from 'vue-select';
import MaskedInput from 'vue-masked-input';
import VeeValidate from 'vee-validate';
import VueTabs from 'vue-nav-tabs/dist/vue-tabs.js';
import VueMoment from 'vue-moment';
import 'vue-nav-tabs/themes/vue-tabs.css';
import VueUploadComponent from 'vue-upload-component';
import VueScheduler from 'v-calendar-scheduler';
import 'v-calendar-scheduler/lib/main.css';
import axios from 'axios';
import Vuex from 'vuex';

Vue.use(VeeValidate);
Vue.use(VueTabs);
Vue.use(VueMoment);
Vue.use(Vuex);
Vue.use(VueScheduler, {
  locale: 'en',
  minDate: null,
  maxDate: null,
  timeRange: [0, 23],
  availableViews: ['month', 'week', 'day'],
  initialDate: new Date(),
  initialView: 'month',
  use12: true,
  showTimeMarker: true,
  showTodayButton: true,
  eventDisplay: null
});

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value;
  var text = div.textContent || div.innerText || ", ";
  return text;
});

Vue.filter('removeUnderscore', function (str) {
  return str.replace(/_/g, ' ');
})


VeeValidate.Validator.extend('verify_password', {
  // Custom validation message
  getMessage: (field) => `Your password must be more than 8 characters long, should contain <br>at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.`,
  // Custom validation rule
  validate: function (value) {
    return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(value)
  },
});

// global constants
Vue.prototype.$baseUrl = configConstants.baseUrl;
Vue.prototype.$apiUrl = configConstants.apiUrl;
//Vue.prototype.$loggedInUser = JSON.parse(store.getters.getAuthUser);
//Vue.prototype.$loggedInUserPic = configConstants.user_picture;
// Vue.prototype.$loggedInUserAddress = 'Foress Media Ltd<br />\
//     Bessemer Drive,<br />\
//     Stevenage<br />\
//     Herts<br />\
//     SG1 2DX<br />';
Vue.prototype.$clinicAddress = 'Foress Media Ltd<br />\
    Bessemer Drive,<br />\
    Stevenage<br />\
    Herts<br />\
    SG1 2DX<br />';
Vue.prototype.$clinicPhone = '07801477895';
Vue.prototype.$clinicWebsite = 'www.happyclinic.co.uk';
Vue.prototype.$clinicName = 'Happy Clinic';


Vue.prototype.$accept = 'image/png,image/jpeg,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document';
Vue.prototype.$extensions = 'jpg,png,jpeg,pdf,doc,docx';
Vue.prototype.$membershipFee = 20;
//Vue.prototype.$openTokKey = '46227052';
Vue.prototype.$imageUrl = configConstants.baseUrl+'/htmlv1/images/';


// Defining Components
import LogoComponent from './../components/admin/common/LogoComponent.vue';
import NoAuthLeftComponent from './../components/admin/common/NoAuthLeftBanner.vue';
import NavsComponent from './../components/admin/common/NavsComponent.vue';
import HeaderComponent  from './../components/admin/common/HeaderComponent.vue';
import FooterComponent from './../components/admin/common/FooterComponent.vue';
import PopupComponent from './../components/admin/common/PopupComponent.vue';
import DeleteRecModal from './../components/admin/common/DeleteRecModal.vue';
import ResetPasswordModal from './../components/admin/common/ResetPasswordModal.vue';
import ArchiveRestoreModal from './../components/admin/common/ArchiveRestoreModal.vue';
import AddNoteModal from './../components/admin/common/AddNoteModal.vue';
import MessagesView from './../components/admin/common/MessagesView.vue';
import AttachmentViewModal from './../components/admin/common/AttachmentViewModal.vue';
import ViewNoteModal from './../components/admin/common/ViewNoteModal.vue';
import ImageUploadComponent from './../components/admin/common/ImageUploadComponent.vue';

// Registering Components
Vue.component('masked-input', MaskedInput)
Vue.component('vue-select', VueSelect2)
Vue.component('paginate', Paginate)
Vue.component('noauth-left-component', NoAuthLeftComponent)
Vue.component('nav-component', NavsComponent)
Vue.component('logo-component', LogoComponent)
Vue.component('header-component', HeaderComponent)
Vue.component('footer-component', FooterComponent)
Vue.component('popup-component', PopupComponent)
Vue.component('delete-record-modal', DeleteRecModal)
Vue.component('reset-password-modal', ResetPasswordModal)
Vue.component('archive-restore-modal', ArchiveRestoreModal)
Vue.component('add-note-modal', AddNoteModal)
Vue.component('messages-view', MessagesView)
Vue.component('attachment-view-modal', AttachmentViewModal)
Vue.component('note-view-modal', ViewNoteModal)
Vue.component('file-upload', VueUploadComponent)
Vue.component('image-upload-component', ImageUploadComponent)

window.EventBus = new Vue;
var access_token = JSON.parse(store.getters.getAuthToken);
if (access_token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
}

const app = new Vue({ 
  el : '#app',
  store,
  router : Router,
  components: {
    },
    methods:{
    },
  created() {
      this.$store.watch(
        (state, getters) => getters.getAuthToken,
        (newValue, oldValue) => {
          // set access token header on every request
          if (newValue) {
            var access_token = JSON.parse(newValue);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
            this.complex = {
              deep: 'some deep object',
            };
          }
        },
      );
    }   
});

axios.interceptors.response.use((response) => {
    return response;
}, function (error) {
    let originalRequest = error.config
    // Do something with response error
    if (error.response.status === 401  && !originalRequest._retry) {
          app.$router.push({ name: 'Logout' })
    }
    if(error.response.status === 406 || error.response.status === 422 || error.response.status === 400){  
     return Promise.reject(error);
    }
});
