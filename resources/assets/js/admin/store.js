import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);


export default new Vuex.Store({


    // You can use it as state property
    state: {
      authUser : localStorage.getItem('lbUser')?localStorage.getItem('lbUser'):false,
      authToken : localStorage.getItem('authToken')?localStorage.getItem('authToken'):false,
    },

    // You can use it as a state getter function (probably the best solution)
    getters: {
      getAuthUser(state){
        return     state.authUser;
      },
      getAuthToken(state){
        return     state.authToken;
      }
   },

    // Mutation for when you use it as state property
    mutations: {
      setAuthUser(state, data){
       localStorage.setItem('lbUser', JSON.stringify(data));
       state.authUser = localStorage.getItem('lbUser');
     },
     setAuthToken(state, data){
       localStorage.setItem('authToken', JSON.stringify(data));
       state.authToken = localStorage.getItem('authToken');
     },
     clearAuthUser(state) {
        state.authUser = false
      },
      clearAuthToken(state) {
        state.authToken = false
      }

    },

    actions : {
      clearAuthUser: ({commit}) => {
        commit('clearAuthUser')
      },
      clearAuthToken: ({commit}) => {
        commit('clearAuthToken')
      }   
    },
});